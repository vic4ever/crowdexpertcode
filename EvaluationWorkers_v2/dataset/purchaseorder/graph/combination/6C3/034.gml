graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 3
    label ""
  ]
  node [
    id 4
    label ""
  ]
  edge [
    source 3
    target 0
    label "3-TO-0"
  ]
  edge [
    source 4
    target 3
    label "4-TO-3"
  ]
  edge [
    source 3
    target 4
    label "3-TO-4"
  ]
  edge [
    source 0
    target 3
    label "0-TO-3"
  ]
  edge [
    source 0
    target 4
    label "0-TO-4"
  ]
  edge [
    source 4
    target 0
    label "4-TO-0"
  ]
]
