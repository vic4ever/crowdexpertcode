graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 4
    label ""
  ]
  node [
    id 5
    label ""
  ]
  edge [
    source 4
    target 5
    label "4-TO-5"
  ]
  edge [
    source 5
    target 0
    label "5-TO-0"
  ]
  edge [
    source 0
    target 4
    label "0-TO-4"
  ]
  edge [
    source 5
    target 4
    label "5-TO-4"
  ]
  edge [
    source 4
    target 0
    label "4-TO-0"
  ]
  edge [
    source 0
    target 5
    label "0-TO-5"
  ]
]
