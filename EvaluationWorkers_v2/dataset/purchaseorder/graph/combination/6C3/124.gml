graph [
  directed 1
  id 0
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 4
    label ""
  ]
  edge [
    source 2
    target 4
    label "2-TO-4"
  ]
  edge [
    source 2
    target 1
    label "2-TO-1"
  ]
  edge [
    source 4
    target 2
    label "4-TO-2"
  ]
  edge [
    source 4
    target 1
    label "4-TO-1"
  ]
  edge [
    source 1
    target 2
    label "1-TO-2"
  ]
  edge [
    source 1
    target 4
    label "1-TO-4"
  ]
]
