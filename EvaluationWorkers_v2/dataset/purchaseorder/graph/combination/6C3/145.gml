graph [
  directed 1
  id 0
  node [
    id 1
    label ""
  ]
  node [
    id 4
    label ""
  ]
  node [
    id 5
    label ""
  ]
  edge [
    source 4
    target 5
    label "4-TO-5"
  ]
  edge [
    source 5
    target 1
    label "5-TO-1"
  ]
  edge [
    source 4
    target 1
    label "4-TO-1"
  ]
  edge [
    source 5
    target 4
    label "5-TO-4"
  ]
  edge [
    source 1
    target 4
    label "1-TO-4"
  ]
  edge [
    source 1
    target 5
    label "1-TO-5"
  ]
]
