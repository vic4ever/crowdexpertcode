graph [
  directed 1
  id 0
  node [
    id 2
    label ""
  ]
  node [
    id 4
    label ""
  ]
  node [
    id 5
    label ""
  ]
  edge [
    source 2
    target 5
    label "2-TO-5"
  ]
  edge [
    source 4
    target 5
    label "4-TO-5"
  ]
  edge [
    source 2
    target 4
    label "2-TO-4"
  ]
  edge [
    source 4
    target 2
    label "4-TO-2"
  ]
  edge [
    source 5
    target 4
    label "5-TO-4"
  ]
  edge [
    source 5
    target 2
    label "5-TO-2"
  ]
]
