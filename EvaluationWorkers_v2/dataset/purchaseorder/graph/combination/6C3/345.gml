graph [
  directed 1
  id 0
  node [
    id 3
    label ""
  ]
  node [
    id 4
    label ""
  ]
  node [
    id 5
    label ""
  ]
  edge [
    source 4
    target 5
    label "4-TO-5"
  ]
  edge [
    source 3
    target 5
    label "3-TO-5"
  ]
  edge [
    source 4
    target 3
    label "4-TO-3"
  ]
  edge [
    source 3
    target 4
    label "3-TO-4"
  ]
  edge [
    source 5
    target 4
    label "5-TO-4"
  ]
  edge [
    source 5
    target 3
    label "5-TO-3"
  ]
]
