graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 4
    label ""
  ]
  edge [
    source 2
    target 4
    label "2-TO-4"
  ]
  edge [
    source 2
    target 1
    label "2-TO-1"
  ]
  edge [
    source 0
    target 1
    label "0-TO-1"
  ]
  edge [
    source 0
    target 2
    label "0-TO-2"
  ]
  edge [
    source 4
    target 2
    label "4-TO-2"
  ]
  edge [
    source 4
    target 1
    label "4-TO-1"
  ]
  edge [
    source 2
    target 0
    label "2-TO-0"
  ]
  edge [
    source 0
    target 4
    label "0-TO-4"
  ]
  edge [
    source 4
    target 0
    label "4-TO-0"
  ]
  edge [
    source 1
    target 2
    label "1-TO-2"
  ]
  edge [
    source 1
    target 4
    label "1-TO-4"
  ]
  edge [
    source 1
    target 0
    label "1-TO-0"
  ]
]
