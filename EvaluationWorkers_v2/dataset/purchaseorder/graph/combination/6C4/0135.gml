graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 3
    label ""
  ]
  node [
    id 5
    label ""
  ]
  edge [
    source 3
    target 1
    label "3-TO-1"
  ]
  edge [
    source 3
    target 0
    label "3-TO-0"
  ]
  edge [
    source 5
    target 0
    label "5-TO-0"
  ]
  edge [
    source 3
    target 5
    label "3-TO-5"
  ]
  edge [
    source 0
    target 1
    label "0-TO-1"
  ]
  edge [
    source 5
    target 1
    label "5-TO-1"
  ]
  edge [
    source 0
    target 3
    label "0-TO-3"
  ]
  edge [
    source 1
    target 3
    label "1-TO-3"
  ]
  edge [
    source 5
    target 3
    label "5-TO-3"
  ]
  edge [
    source 1
    target 5
    label "1-TO-5"
  ]
  edge [
    source 0
    target 5
    label "0-TO-5"
  ]
  edge [
    source 1
    target 0
    label "1-TO-0"
  ]
]
