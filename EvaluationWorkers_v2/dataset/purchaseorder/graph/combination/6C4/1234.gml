graph [
  directed 1
  id 0
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 3
    label ""
  ]
  node [
    id 4
    label ""
  ]
  edge [
    source 3
    target 1
    label "3-TO-1"
  ]
  edge [
    source 2
    target 3
    label "2-TO-3"
  ]
  edge [
    source 3
    target 2
    label "3-TO-2"
  ]
  edge [
    source 2
    target 4
    label "2-TO-4"
  ]
  edge [
    source 2
    target 1
    label "2-TO-1"
  ]
  edge [
    source 4
    target 3
    label "4-TO-3"
  ]
  edge [
    source 3
    target 4
    label "3-TO-4"
  ]
  edge [
    source 4
    target 2
    label "4-TO-2"
  ]
  edge [
    source 4
    target 1
    label "4-TO-1"
  ]
  edge [
    source 1
    target 2
    label "1-TO-2"
  ]
  edge [
    source 1
    target 3
    label "1-TO-3"
  ]
  edge [
    source 1
    target 4
    label "1-TO-4"
  ]
]
