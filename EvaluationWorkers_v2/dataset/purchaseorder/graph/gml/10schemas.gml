graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 3
    label ""
  ]
  node [
    id 4
    label ""
  ]
  node [
    id 5
    label ""
  ]
  node [
    id 6
    label ""
  ]
  node [
    id 7
    label ""
  ]
  node [
    id 8
    label ""
  ]
  node [
    id 9
    label ""
  ]
  edge [
    source 0
    target 1
    label "0-TO-1"
  ]
  edge [
    source 0
    target 2
    label "0-TO-2"
  ]
  edge [
    source 0
    target 3
    label "0-TO-3"
  ]
  edge [
    source 0
    target 4
    label "0-TO-4"
  ]
  edge [
    source 1
    target 8
    label "1-TO-8"
  ]
  edge [
    source 1
    target 9
    label "1-TO-9"
  ]
  edge [
    source 1
    target 6
    label "1-TO-6"
  ]
  edge [
    source 1
    target 7
    label "1-TO-7"
  ]
  edge [
    source 6
    target 8
    label "6-TO-8"
  ]
  edge [
    source 6
    target 7
    label "6-TO-7"
  ]
  edge [
    source 6
    target 9
    label "6-TO-9"
  ]
  edge [
    source 6
    target 4
    label "6-TO-4"
  ]
  edge [
    source 6
    target 3
    label "6-TO-3"
  ]
  edge [
    source 6
    target 5
    label "6-TO-5"
  ]
  edge [
    source 6
    target 0
    label "6-TO-0"
  ]
  edge [
    source 6
    target 2
    label "6-TO-2"
  ]
  edge [
    source 6
    target 1
    label "6-TO-1"
  ]
  edge [
    source 8
    target 7
    label "8-TO-7"
  ]
  edge [
    source 9
    target 0
    label "9-TO-0"
  ]
  edge [
    source 8
    target 6
    label "8-TO-6"
  ]
  edge [
    source 9
    target 1
    label "9-TO-1"
  ]
  edge [
    source 8
    target 5
    label "8-TO-5"
  ]
  edge [
    source 9
    target 2
    label "9-TO-2"
  ]
  edge [
    source 5
    target 0
    label "5-TO-0"
  ]
  edge [
    source 9
    target 3
    label "9-TO-3"
  ]
  edge [
    source 5
    target 1
    label "5-TO-1"
  ]
  edge [
    source 9
    target 4
    label "9-TO-4"
  ]
  edge [
    source 9
    target 5
    label "9-TO-5"
  ]
  edge [
    source 8
    target 9
    label "8-TO-9"
  ]
  edge [
    source 9
    target 6
    label "9-TO-6"
  ]
  edge [
    source 8
    target 0
    label "8-TO-0"
  ]
  edge [
    source 5
    target 4
    label "5-TO-4"
  ]
  edge [
    source 0
    target 8
    label "0-TO-8"
  ]
  edge [
    source 9
    target 7
    label "9-TO-7"
  ]
  edge [
    source 0
    target 7
    label "0-TO-7"
  ]
  edge [
    source 9
    target 8
    label "9-TO-8"
  ]
  edge [
    source 0
    target 6
    label "0-TO-6"
  ]
  edge [
    source 5
    target 2
    label "5-TO-2"
  ]
  edge [
    source 0
    target 5
    label "0-TO-5"
  ]
  edge [
    source 5
    target 3
    label "5-TO-3"
  ]
  edge [
    source 8
    target 4
    label "8-TO-4"
  ]
  edge [
    source 5
    target 8
    label "5-TO-8"
  ]
  edge [
    source 8
    target 3
    label "8-TO-3"
  ]
  edge [
    source 5
    target 9
    label "5-TO-9"
  ]
  edge [
    source 8
    target 2
    label "8-TO-2"
  ]
  edge [
    source 5
    target 6
    label "5-TO-6"
  ]
  edge [
    source 3
    target 8
    label "3-TO-8"
  ]
  edge [
    source 8
    target 1
    label "8-TO-1"
  ]
  edge [
    source 5
    target 7
    label "5-TO-7"
  ]
  edge [
    source 0
    target 9
    label "0-TO-9"
  ]
  edge [
    source 3
    target 9
    label "3-TO-9"
  ]
  edge [
    source 3
    target 1
    label "3-TO-1"
  ]
  edge [
    source 3
    target 0
    label "3-TO-0"
  ]
  edge [
    source 3
    target 2
    label "3-TO-2"
  ]
  edge [
    source 3
    target 5
    label "3-TO-5"
  ]
  edge [
    source 3
    target 4
    label "3-TO-4"
  ]
  edge [
    source 3
    target 7
    label "3-TO-7"
  ]
  edge [
    source 3
    target 6
    label "3-TO-6"
  ]
  edge [
    source 2
    target 8
    label "2-TO-8"
  ]
  edge [
    source 2
    target 7
    label "2-TO-7"
  ]
  edge [
    source 2
    target 9
    label "2-TO-9"
  ]
  edge [
    source 4
    target 8
    label "4-TO-8"
  ]
  edge [
    source 4
    target 7
    label "4-TO-7"
  ]
  edge [
    source 4
    target 6
    label "4-TO-6"
  ]
  edge [
    source 4
    target 5
    label "4-TO-5"
  ]
  edge [
    source 4
    target 3
    label "4-TO-3"
  ]
  edge [
    source 4
    target 2
    label "4-TO-2"
  ]
  edge [
    source 4
    target 1
    label "4-TO-1"
  ]
  edge [
    source 4
    target 0
    label "4-TO-0"
  ]
  edge [
    source 7
    target 5
    label "7-TO-5"
  ]
  edge [
    source 7
    target 4
    label "7-TO-4"
  ]
  edge [
    source 7
    target 6
    label "7-TO-6"
  ]
  edge [
    source 7
    target 9
    label "7-TO-9"
  ]
  edge [
    source 7
    target 8
    label "7-TO-8"
  ]
  edge [
    source 1
    target 2
    label "1-TO-2"
  ]
  edge [
    source 1
    target 3
    label "1-TO-3"
  ]
  edge [
    source 1
    target 4
    label "1-TO-4"
  ]
  edge [
    source 1
    target 5
    label "1-TO-5"
  ]
  edge [
    source 7
    target 1
    label "7-TO-1"
  ]
  edge [
    source 4
    target 9
    label "4-TO-9"
  ]
  edge [
    source 7
    target 0
    label "7-TO-0"
  ]
  edge [
    source 7
    target 3
    label "7-TO-3"
  ]
  edge [
    source 1
    target 0
    label "1-TO-0"
  ]
  edge [
    source 7
    target 2
    label "7-TO-2"
  ]
  edge [
    source 2
    target 5
    label "2-TO-5"
  ]
  edge [
    source 2
    target 6
    label "2-TO-6"
  ]
  edge [
    source 2
    target 3
    label "2-TO-3"
  ]
  edge [
    source 2
    target 4
    label "2-TO-4"
  ]
  edge [
    source 2
    target 1
    label "2-TO-1"
  ]
  edge [
    source 2
    target 0
    label "2-TO-0"
  ]
]
