graph [
  directed 1
  id 0
  node [
    id 0
    label "Apertum"
  ]
  node [
    id 1
    label "CIDX"
  ]
  node [
    id 2
    label "Excel"
  ]
  node [
    id 3
    label "Noris"
  ]
  node [
    id 4
    label "Paragon"
  ]
  node [
    id 5
    label "OpenTrans"
  ]
  node [
    id 6
    label "xCBL"
  ]
  node [
    id 7
    label "CRM"
  ]
  node [
    id 8
    label "RosettaNet"
  ]
  node [
    id 9
    label "SAP"
  ]
  edge [
    source 0
    target 1
    label "0-TO-1"
  ]
  edge [
    source 0
    target 2
    label "0-TO-2"
  ]
  edge [
    source 0
    target 3
    label "0-TO-3"
  ]
  edge [
    source 0
    target 4
    label "0-TO-4"
  ]
  edge [
    source 6
    target 8
    label "6-TO-8"
  ]
  edge [
    source 6
    target 7
    label "6-TO-7"
  ]
  edge [
    source 6
    target 5
    label "6-TO-5"
  ]
  edge [
    source 8
    target 7
    label "8-TO-7"
  ]
  edge [
    source 8
    target 6
    label "8-TO-6"
  ]
  edge [
    source 5
    target 0
    label "5-TO-0"
  ]
  edge [
    source 5
    target 1
    label "5-TO-1"
  ]
  edge [
    source 8
    target 9
    label "8-TO-9"
  ]
  edge [
    source 9
    target 6
    label "9-TO-6"
  ]
  edge [
    source 5
    target 4
    label "5-TO-4"
  ]
  edge [
    source 9
    target 7
    label "9-TO-7"
  ]
  edge [
    source 9
    target 8
    label "9-TO-8"
  ]
  edge [
    source 5
    target 2
    label "5-TO-2"
  ]
  edge [
    source 0
    target 5
    label "0-TO-5"
  ]
  edge [
    source 5
    target 3
    label "5-TO-3"
  ]
  edge [
    source 5
    target 6
    label "5-TO-6"
  ]
  edge [
    source 3
    target 1
    label "3-TO-1"
  ]
  edge [
    source 3
    target 0
    label "3-TO-0"
  ]
  edge [
    source 3
    target 2
    label "3-TO-2"
  ]
  edge [
    source 3
    target 5
    label "3-TO-5"
  ]
  edge [
    source 3
    target 4
    label "3-TO-4"
  ]
  edge [
    source 4
    target 5
    label "4-TO-5"
  ]
  edge [
    source 4
    target 3
    label "4-TO-3"
  ]
  edge [
    source 4
    target 2
    label "4-TO-2"
  ]
  edge [
    source 4
    target 1
    label "4-TO-1"
  ]
  edge [
    source 4
    target 0
    label "4-TO-0"
  ]
  edge [
    source 7
    target 6
    label "7-TO-6"
  ]
  edge [
    source 7
    target 9
    label "7-TO-9"
  ]
  edge [
    source 7
    target 8
    label "7-TO-8"
  ]
  edge [
    source 1
    target 2
    label "1-TO-2"
  ]
  edge [
    source 1
    target 3
    label "1-TO-3"
  ]
  edge [
    source 1
    target 4
    label "1-TO-4"
  ]
  edge [
    source 1
    target 5
    label "1-TO-5"
  ]
  edge [
    source 1
    target 0
    label "1-TO-0"
  ]
  edge [
    source 2
    target 5
    label "2-TO-5"
  ]
  edge [
    source 2
    target 3
    label "2-TO-3"
  ]
  edge [
    source 2
    target 4
    label "2-TO-4"
  ]
  edge [
    source 2
    target 1
    label "2-TO-1"
  ]
  edge [
    source 2
    target 0
    label "2-TO-0"
  ]
]
