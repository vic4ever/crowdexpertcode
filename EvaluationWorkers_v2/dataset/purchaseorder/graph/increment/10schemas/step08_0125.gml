graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 5
    label ""
  ]
  edge [
    source 2
    target 5
    label "2-TO-5"
  ]
  edge [
    source 2
    target 1
    label "2-TO-1"
  ]
  edge [
    source 5
    target 0
    label "5-TO-0"
  ]
  edge [
    source 5
    target 1
    label "5-TO-1"
  ]
  edge [
    source 0
    target 2
    label "0-TO-2"
  ]
  edge [
    source 2
    target 0
    label "2-TO-0"
  ]
  edge [
    source 1
    target 2
    label "1-TO-2"
  ]
  edge [
    source 5
    target 2
    label "5-TO-2"
  ]
  edge [
    source 1
    target 5
    label "1-TO-5"
  ]
  edge [
    source 0
    target 5
    label "0-TO-5"
  ]
]
