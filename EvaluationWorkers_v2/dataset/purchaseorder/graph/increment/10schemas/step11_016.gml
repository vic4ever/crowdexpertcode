graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 6
    label ""
  ]
  edge [
    source 0
    target 1
    label "0-TO-1"
  ]
  edge [
    source 6
    target 0
    label "6-TO-0"
  ]
  edge [
    source 0
    target 6
    label "0-TO-6"
  ]
  edge [
    source 1
    target 6
    label "1-TO-6"
  ]
  edge [
    source 6
    target 1
    label "6-TO-1"
  ]
  edge [
    source 1
    target 0
    label "1-TO-0"
  ]
]
