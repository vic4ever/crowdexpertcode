graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 3
    label ""
  ]
  node [
    id 4
    label ""
  ]
  node [
    id 7
    label ""
  ]
  edge [
    source 7
    target 4
    label "7-TO-4"
  ]
  edge [
    source 3
    target 4
    label "3-TO-4"
  ]
  edge [
    source 3
    target 7
    label "3-TO-7"
  ]
  edge [
    source 0
    target 4
    label "0-TO-4"
  ]
  edge [
    source 1
    target 4
    label "1-TO-4"
  ]
  edge [
    source 7
    target 1
    label "7-TO-1"
  ]
  edge [
    source 7
    target 0
    label "7-TO-0"
  ]
  edge [
    source 2
    target 7
    label "2-TO-7"
  ]
  edge [
    source 7
    target 3
    label "7-TO-3"
  ]
  edge [
    source 7
    target 2
    label "7-TO-2"
  ]
  edge [
    source 4
    target 7
    label "4-TO-7"
  ]
  edge [
    source 2
    target 4
    label "2-TO-4"
  ]
  edge [
    source 4
    target 3
    label "4-TO-3"
  ]
  edge [
    source 4
    target 2
    label "4-TO-2"
  ]
  edge [
    source 4
    target 1
    label "4-TO-1"
  ]
  edge [
    source 4
    target 0
    label "4-TO-0"
  ]
  edge [
    source 0
    target 7
    label "0-TO-7"
  ]
  edge [
    source 1
    target 7
    label "1-TO-7"
  ]
]
