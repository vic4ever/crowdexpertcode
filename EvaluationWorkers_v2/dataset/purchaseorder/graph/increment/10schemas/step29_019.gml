graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 9
    label ""
  ]
  edge [
    source 9
    target 0
    label "9-TO-0"
  ]
  edge [
    source 9
    target 1
    label "9-TO-1"
  ]
  edge [
    source 0
    target 1
    label "0-TO-1"
  ]
  edge [
    source 1
    target 9
    label "1-TO-9"
  ]
  edge [
    source 1
    target 0
    label "1-TO-0"
  ]
  edge [
    source 0
    target 9
    label "0-TO-9"
  ]
]
