graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 3
    label ""
  ]
  node [
    id 4
    label ""
  ]
  node [
    id 9
    label ""
  ]
  edge [
    source 3
    target 4
    label "3-TO-4"
  ]
  edge [
    source 0
    target 4
    label "0-TO-4"
  ]
  edge [
    source 1
    target 4
    label "1-TO-4"
  ]
  edge [
    source 4
    target 9
    label "4-TO-9"
  ]
  edge [
    source 2
    target 9
    label "2-TO-9"
  ]
  edge [
    source 9
    target 0
    label "9-TO-0"
  ]
  edge [
    source 9
    target 1
    label "9-TO-1"
  ]
  edge [
    source 2
    target 4
    label "2-TO-4"
  ]
  edge [
    source 9
    target 2
    label "9-TO-2"
  ]
  edge [
    source 4
    target 3
    label "4-TO-3"
  ]
  edge [
    source 9
    target 3
    label "9-TO-3"
  ]
  edge [
    source 9
    target 4
    label "9-TO-4"
  ]
  edge [
    source 4
    target 2
    label "4-TO-2"
  ]
  edge [
    source 4
    target 1
    label "4-TO-1"
  ]
  edge [
    source 4
    target 0
    label "4-TO-0"
  ]
  edge [
    source 1
    target 9
    label "1-TO-9"
  ]
  edge [
    source 3
    target 9
    label "3-TO-9"
  ]
  edge [
    source 0
    target 9
    label "0-TO-9"
  ]
]
