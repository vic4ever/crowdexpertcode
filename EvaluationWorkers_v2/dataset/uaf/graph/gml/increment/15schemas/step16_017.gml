graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 7
    label ""
  ]
  edge [
    source 0
    target 1
    label "0-TO-1"
  ]
  edge [
    source 0
    target 7
    label "0-TO-7"
  ]
  edge [
    source 1
    target 7
    label "1-TO-7"
  ]
  edge [
    source 7
    target 1
    label "7-TO-1"
  ]
  edge [
    source 7
    target 0
    label "7-TO-0"
  ]
  edge [
    source 1
    target 0
    label "1-TO-0"
  ]
]
