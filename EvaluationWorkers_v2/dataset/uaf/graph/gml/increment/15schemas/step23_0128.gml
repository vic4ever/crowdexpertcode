graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 8
    label ""
  ]
  edge [
    source 2
    target 1
    label "2-TO-1"
  ]
  edge [
    source 0
    target 2
    label "0-TO-2"
  ]
  edge [
    source 2
    target 0
    label "2-TO-0"
  ]
  edge [
    source 1
    target 2
    label "1-TO-2"
  ]
  edge [
    source 1
    target 8
    label "1-TO-8"
  ]
  edge [
    source 0
    target 8
    label "0-TO-8"
  ]
  edge [
    source 8
    target 0
    label "8-TO-0"
  ]
  edge [
    source 2
    target 8
    label "2-TO-8"
  ]
  edge [
    source 8
    target 2
    label "8-TO-2"
  ]
  edge [
    source 8
    target 1
    label "8-TO-1"
  ]
]
