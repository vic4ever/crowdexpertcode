graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 10
    label ""
  ]
  edge [
    source 1
    target 10
    label "1-TO-10"
  ]
  edge [
    source 2
    target 1
    label "2-TO-1"
  ]
  edge [
    source 0
    target 2
    label "0-TO-2"
  ]
  edge [
    source 2
    target 0
    label "2-TO-0"
  ]
  edge [
    source 1
    target 2
    label "1-TO-2"
  ]
  edge [
    source 2
    target 10
    label "2-TO-10"
  ]
  edge [
    source 10
    target 0
    label "10-TO-0"
  ]
  edge [
    source 10
    target 1
    label "10-TO-1"
  ]
  edge [
    source 0
    target 10
    label "0-TO-10"
  ]
  edge [
    source 10
    target 2
    label "10-TO-2"
  ]
]
