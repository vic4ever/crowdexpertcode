graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 11
    label ""
  ]
  edge [
    source 1
    target 11
    label "1-TO-11"
  ]
  edge [
    source 0
    target 1
    label "0-TO-1"
  ]
  edge [
    source 11
    target 1
    label "11-TO-1"
  ]
  edge [
    source 0
    target 11
    label "0-TO-11"
  ]
  edge [
    source 1
    target 0
    label "1-TO-0"
  ]
  edge [
    source 11
    target 0
    label "11-TO-0"
  ]
]
