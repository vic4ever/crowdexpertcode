graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 12
    label ""
  ]
  edge [
    source 1
    target 12
    label "1-TO-12"
  ]
  edge [
    source 0
    target 1
    label "0-TO-1"
  ]
  edge [
    source 12
    target 1
    label "12-TO-1"
  ]
  edge [
    source 12
    target 0
    label "12-TO-0"
  ]
  edge [
    source 1
    target 0
    label "1-TO-0"
  ]
  edge [
    source 0
    target 12
    label "0-TO-12"
  ]
]
