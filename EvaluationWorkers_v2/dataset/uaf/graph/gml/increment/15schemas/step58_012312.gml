graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 3
    label ""
  ]
  node [
    id 12
    label ""
  ]
  edge [
    source 3
    target 1
    label "3-TO-1"
  ]
  edge [
    source 3
    target 0
    label "3-TO-0"
  ]
  edge [
    source 3
    target 2
    label "3-TO-2"
  ]
  edge [
    source 3
    target 12
    label "3-TO-12"
  ]
  edge [
    source 0
    target 3
    label "0-TO-3"
  ]
  edge [
    source 1
    target 3
    label "1-TO-3"
  ]
  edge [
    source 0
    target 12
    label "0-TO-12"
  ]
  edge [
    source 2
    target 3
    label "2-TO-3"
  ]
  edge [
    source 1
    target 12
    label "1-TO-12"
  ]
  edge [
    source 2
    target 12
    label "2-TO-12"
  ]
  edge [
    source 12
    target 1
    label "12-TO-1"
  ]
  edge [
    source 12
    target 0
    label "12-TO-0"
  ]
  edge [
    source 12
    target 3
    label "12-TO-3"
  ]
  edge [
    source 12
    target 2
    label "12-TO-2"
  ]
]
