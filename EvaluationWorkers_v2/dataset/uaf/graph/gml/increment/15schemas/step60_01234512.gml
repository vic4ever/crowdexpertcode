graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 3
    label ""
  ]
  node [
    id 4
    label ""
  ]
  node [
    id 5
    label ""
  ]
  node [
    id 12
    label ""
  ]
  edge [
    source 3
    target 5
    label "3-TO-5"
  ]
  edge [
    source 3
    target 12
    label "3-TO-12"
  ]
  edge [
    source 1
    target 5
    label "1-TO-5"
  ]
  edge [
    source 5
    target 12
    label "5-TO-12"
  ]
  edge [
    source 0
    target 12
    label "0-TO-12"
  ]
  edge [
    source 2
    target 5
    label "2-TO-5"
  ]
  edge [
    source 4
    target 5
    label "4-TO-5"
  ]
  edge [
    source 1
    target 12
    label "1-TO-12"
  ]
  edge [
    source 5
    target 0
    label "5-TO-0"
  ]
  edge [
    source 5
    target 1
    label "5-TO-1"
  ]
  edge [
    source 2
    target 12
    label "2-TO-12"
  ]
  edge [
    source 5
    target 4
    label "5-TO-4"
  ]
  edge [
    source 12
    target 1
    label "12-TO-1"
  ]
  edge [
    source 4
    target 12
    label "4-TO-12"
  ]
  edge [
    source 12
    target 0
    label "12-TO-0"
  ]
  edge [
    source 5
    target 2
    label "5-TO-2"
  ]
  edge [
    source 5
    target 3
    label "5-TO-3"
  ]
  edge [
    source 0
    target 5
    label "0-TO-5"
  ]
  edge [
    source 12
    target 5
    label "12-TO-5"
  ]
  edge [
    source 12
    target 4
    label "12-TO-4"
  ]
  edge [
    source 12
    target 3
    label "12-TO-3"
  ]
  edge [
    source 12
    target 2
    label "12-TO-2"
  ]
]
