graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 3
    label ""
  ]
  node [
    id 4
    label ""
  ]
  node [
    id 5
    label ""
  ]
  node [
    id 6
    label ""
  ]
  node [
    id 7
    label ""
  ]
  node [
    id 8
    label ""
  ]
  node [
    id 9
    label ""
  ]
  node [
    id 10
    label ""
  ]
  node [
    id 11
    label ""
  ]
  node [
    id 12
    label ""
  ]
  node [
    id 13
    label ""
  ]
  edge [
    source 8
    target 12
    label "8-TO-12"
  ]
  edge [
    source 7
    target 12
    label "7-TO-12"
  ]
  edge [
    source 8
    target 13
    label "8-TO-13"
  ]
  edge [
    source 7
    target 13
    label "7-TO-13"
  ]
  edge [
    source 0
    target 13
    label "0-TO-13"
  ]
  edge [
    source 9
    target 13
    label "9-TO-13"
  ]
  edge [
    source 0
    target 12
    label "0-TO-12"
  ]
  edge [
    source 12
    target 8
    label "12-TO-8"
  ]
  edge [
    source 12
    target 9
    label "12-TO-9"
  ]
  edge [
    source 12
    target 6
    label "12-TO-6"
  ]
  edge [
    source 12
    target 7
    label "12-TO-7"
  ]
  edge [
    source 13
    target 11
    label "13-TO-11"
  ]
  edge [
    source 13
    target 10
    label "13-TO-10"
  ]
  edge [
    source 5
    target 13
    label "5-TO-13"
  ]
  edge [
    source 13
    target 12
    label "13-TO-12"
  ]
  edge [
    source 5
    target 12
    label "5-TO-12"
  ]
  edge [
    source 9
    target 12
    label "9-TO-12"
  ]
  edge [
    source 12
    target 13
    label "12-TO-13"
  ]
  edge [
    source 2
    target 13
    label "2-TO-13"
  ]
  edge [
    source 2
    target 12
    label "2-TO-12"
  ]
  edge [
    source 12
    target 1
    label "12-TO-1"
  ]
  edge [
    source 12
    target 0
    label "12-TO-0"
  ]
  edge [
    source 12
    target 10
    label "12-TO-10"
  ]
  edge [
    source 12
    target 11
    label "12-TO-11"
  ]
  edge [
    source 12
    target 5
    label "12-TO-5"
  ]
  edge [
    source 12
    target 4
    label "12-TO-4"
  ]
  edge [
    source 10
    target 13
    label "10-TO-13"
  ]
  edge [
    source 12
    target 3
    label "12-TO-3"
  ]
  edge [
    source 10
    target 12
    label "10-TO-12"
  ]
  edge [
    source 12
    target 2
    label "12-TO-2"
  ]
  edge [
    source 3
    target 12
    label "3-TO-12"
  ]
  edge [
    source 3
    target 13
    label "3-TO-13"
  ]
  edge [
    source 13
    target 1
    label "13-TO-1"
  ]
  edge [
    source 13
    target 2
    label "13-TO-2"
  ]
  edge [
    source 13
    target 0
    label "13-TO-0"
  ]
  edge [
    source 13
    target 5
    label "13-TO-5"
  ]
  edge [
    source 13
    target 6
    label "13-TO-6"
  ]
  edge [
    source 13
    target 3
    label "13-TO-3"
  ]
  edge [
    source 13
    target 4
    label "13-TO-4"
  ]
  edge [
    source 13
    target 9
    label "13-TO-9"
  ]
  edge [
    source 13
    target 8
    label "13-TO-8"
  ]
  edge [
    source 11
    target 12
    label "11-TO-12"
  ]
  edge [
    source 13
    target 7
    label "13-TO-7"
  ]
  edge [
    source 11
    target 13
    label "11-TO-13"
  ]
  edge [
    source 4
    target 13
    label "4-TO-13"
  ]
  edge [
    source 4
    target 12
    label "4-TO-12"
  ]
  edge [
    source 6
    target 12
    label "6-TO-12"
  ]
  edge [
    source 6
    target 13
    label "6-TO-13"
  ]
  edge [
    source 1
    target 12
    label "1-TO-12"
  ]
  edge [
    source 1
    target 13
    label "1-TO-13"
  ]
]
