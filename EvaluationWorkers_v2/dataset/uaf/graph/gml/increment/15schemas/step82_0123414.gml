graph [
  directed 1
  id 0
  node [
    id 0
    label ""
  ]
  node [
    id 1
    label ""
  ]
  node [
    id 2
    label ""
  ]
  node [
    id 3
    label ""
  ]
  node [
    id 4
    label ""
  ]
  node [
    id 14
    label ""
  ]
  edge [
    source 14
    target 4
    label "14-TO-4"
  ]
  edge [
    source 0
    target 14
    label "0-TO-14"
  ]
  edge [
    source 14
    target 1
    label "14-TO-1"
  ]
  edge [
    source 3
    target 4
    label "3-TO-4"
  ]
  edge [
    source 14
    target 0
    label "14-TO-0"
  ]
  edge [
    source 14
    target 3
    label "14-TO-3"
  ]
  edge [
    source 3
    target 14
    label "3-TO-14"
  ]
  edge [
    source 14
    target 2
    label "14-TO-2"
  ]
  edge [
    source 0
    target 4
    label "0-TO-4"
  ]
  edge [
    source 1
    target 4
    label "1-TO-4"
  ]
  edge [
    source 2
    target 4
    label "2-TO-4"
  ]
  edge [
    source 4
    target 3
    label "4-TO-3"
  ]
  edge [
    source 2
    target 14
    label "2-TO-14"
  ]
  edge [
    source 4
    target 2
    label "4-TO-2"
  ]
  edge [
    source 4
    target 1
    label "4-TO-1"
  ]
  edge [
    source 4
    target 0
    label "4-TO-0"
  ]
  edge [
    source 1
    target 14
    label "1-TO-14"
  ]
  edge [
    source 4
    target 14
    label "4-TO-14"
  ]
]
