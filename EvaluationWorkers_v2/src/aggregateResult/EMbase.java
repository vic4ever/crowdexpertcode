package aggregateResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import question.BinaryQuestion;
import question.MultiChoiceQuestion;
import question.Question;
import question.PossibleAnswer;
import tools.Utils;
import tools.io.TxtReader;
import user.Characteristic;
import user.TruthFulWorker;
import user.Worker;

import config.Mainconfig;
import convertLabelProject.AssignedLabel;
import convertLabelProject.Category;
import convertLabelProject.CorrectLabel;
import convertLabelProject.DawidSkene;
import convertLabelProject.MisclassificationCost;
import data.PossibleCategories;
import feedback.FeedBack;

/**
 * @author Lamtran3101 <h4>Description</h4> This class convert data into GETAL
 *         project's input.
 */
public class EMbase extends AggregateResult {
	private DawidSkene ds;
	private Set<Category> categories;
	private Set<AssignedLabel> labels;
	private Set<CorrectLabel> correct;
	private List<FeedBack> feedbacks;
	private List<user.Worker> workers;
	private List<Question> questions;
	private Map<Integer, user.Worker> setWorkers;
	private Map<String, Question> setQuestions;

	protected double competiontime;

	public EMbase(List<FeedBack> feedbacks, List<user.Worker> workers,
			List<Question> questions) {
		this.feedbacks = feedbacks;
		this.workers = workers;
		this.questions = questions;
		initializedSetWorkers();
		initializedSetQuestions();
	}

	public void execute() {
		initialized();

		Mainconfig.getInstance().getTimer().start();
		ds.evaluateWorkers();
		ds.estimate(1);
		print2File();

		Mainconfig.getInstance().getTimer().stop();
		this.setCompetiontime(Mainconfig.getInstance().getTimer()
				.getElapsedTime());

		updateData();
		updateWorkersQuality();
	}

	private void updateData() {
		// TODO Auto-generated method stub
		
	}

	public void execute(int times) {
		initialized();

		Mainconfig.getInstance().getTimer().start();
		ds.evaluateWorkers();
		for (int i = 0; i < times; i++) {
			ds.estimate(1);
		}
		Mainconfig.getInstance().getTimer().stop();
		this.setCompetiontime(Mainconfig.getInstance().getTimer()
				.getElapsedTime());

		print2File();

		updateData();

		updateWorkersQuality();

	}

	private void initializedSetWorkers() {
		setWorkers = new HashMap<Integer, user.Worker>();
		for (user.Worker worker : workers) {
			int ID = worker.getWID();
			setWorkers.put(ID, worker);
		}
	}

	private void initializedSetQuestions() {
		setQuestions = new HashMap<String, Question>();
		for (Question question : questions) {
			String value = question.getCorrespondence().getValue();
			setQuestions.put(value, question);
		}
	}

	private void print2File() {
		String summary_report = ds.printAllWorkerScores(false);
		String detailed_report = ds.printAllWorkerScores(true);
		Utils.writeFile(summary_report, "results/worker-statistics-summary.txt");
		Utils.writeFile(detailed_report,
				"results/worker-statistics-detailed.txt");

		String objectProbs = ds.printObjectClassProbabilities();
		Utils.writeFile(objectProbs, "results/object-probabilities.txt");
	}

//	private void updateData() {
//		TxtReader reader = new TxtReader();
//		reader.readfile("object-probabilities");
//		String[] content = reader.getContent().split("\n");
//		int size = content.length;
//		int start = 1;
//		int numLabels = Integer.parseInt(Mainconfig.getInstance()
//				.getListConfig().get("InputNumberLabels"));
//		String inputType = Mainconfig.getInstance().getListConfig()
//				.get("InputDataType");
//
//		while (start < size) {
//			String[] data = content[start].split("\t");
//			String category = data[3];
//			if (!inputType.equals("Binary")) {
//				category = data[numLabels + 1];
//			}
//			String value = data[0];
//			Question question = setQuestions.get(value);
//			/* if(question != null){ */
//			question.getCorrespondence().setEstResult(
//					PossibleCategories.valueOf(category));
//
//			start++;
//		}
//	}

	private void updateWorkersQuality() {
		/*
		 * TxtReader reader = new TxtReader();
		 * reader.readfile("worker-statistics-detailed"); String[] content =
		 * reader.getContent().split("\n"); int size = content.length; int start
		 * = 0; while (start < size) { int ID =
		 * Integer.parseInt(content[start].split(" ")[1]); String errorRate =
		 * content[start + 1].split(" ")[3].split("%")[0]; double r = 1 -
		 * Double.parseDouble(errorRate) / 100; start += 16; user.Worker worker
		 * = setWorkers.get(ID); Characteristic user = new TruthFulWorker(); if
		 * (r < Mainconfig.getInstance().getReliabilityThreshold()) { user = new
		 * Spammer(); }
		 * 
		 * user.setReliableDegree(r); worker.setEstChar(user);
		 * 
		 * }
		 */
		for (Worker worker : workers) {
			double numCorrectAnswers = 0;
			double totalAnswers = worker.getListAnswers().size();
			for (String value : worker.getListAnswers().keySet()) {
				FeedBack feedback = worker.getListAnswers().get(value);
				PossibleAnswer assignedLabel = feedback.getAnswer();
				PossibleAnswer correctLabeli = feedback.getQuestion()
						.getCorrespondence().getEstResult().getAnswer();
				if (assignedLabel == correctLabeli) {
					numCorrectAnswers++;
				}
			}
			Characteristic c = new TruthFulWorker();
			c.setReliableDegree(numCorrectAnswers / totalAnswers);
			worker.setEstChar(c);
		}
	}

	private void initialized() {

		//File
		//Question,Worker,Answer
		//1,1,catA
		initializedCategories();
		initializedWorker();
		for (MisclassificationCost mcc : getCosts()) {
			ds.addMisclassificationCost(mcc);
			//Gia tri khi danh label sai: cost danh sai khi label=0 ma truth = 1
		}
		initializedInput();
		for (AssignedLabel l : labels) {
			// System.out.println(l.getCategoryName());
			ds.addAssignedLabel(l);
		}
		initializedCorrectlabel(); // Ko can khoit ao
		for (CorrectLabel l : correct) {
			ds.addCorrectLabel(l);
		}
		initializedEvaluationFile();//Gan ground truth dung cho question
	}

	/**
	 * Construct list of categories, need to modify later for automatic
	 * construction Right now, we will build them statically.
	 */
	private void initializedCategories() {

		String inputType = Mainconfig.getInstance().getListConfig()
				.get("InputDataType");
		if (inputType.equals("Binary")) { //Khoi tao category
			String[] cats = new String[2];
			cats[0] = "True";
			cats[1] = "False";
			categories = getCategories(cats);
		} else {
			int numLabels = Integer.parseInt(Mainconfig.getInstance()
					.getListConfig().get("InputNumberLabels"));
			PossibleCategories[] fullList = { PossibleCategories.A,
					PossibleCategories.B, PossibleCategories.C,
					PossibleCategories.D, PossibleCategories.E,
					PossibleCategories.F, PossibleCategories.G,
					PossibleCategories.H };
			int start = 0;
			String[] cats = new String[numLabels];
			while (start < numLabels) {
				cats[start] = fullList[start].toString();
				start++;
			}
			categories = getCategories(cats);
		}
	}

	/**
	 * From array String of lines, we construct a list of categories. Each line
	 * equals to one category, which has a pattern: category_name
	 * prior<optional>
	 * 
	 * @param lines
	 * @return Set<Category>
	 */
	private Set<Category> getCategories(String[] lines) {
		Set<Category> categories = new HashSet<Category>();
		for (String line : lines) {
			String[] l = line.split("\t");
			if (l.length == 1) {
				Category c = new Category(line);
				categories.add(c);
			} else if (l.length == 2) {
				String name = l[0];
				Double prior = new Double(l[1]);
				Category c = new Category(name);
				c.setPrior(prior);
				categories.add(c);
			}
		}
		return categories;
	}

	private void initializedWorker() {
		ds = new DawidSkene(categories);
	}

	private Set<MisclassificationCost> getCosts() {
		String inputType = Mainconfig.getInstance().getListConfig()
				.get("InputDataType");
		String[] lines_cost = null;
		if (inputType.equals("Binary")) {
			lines_cost = new String[4];
			lines_cost[0] = "True\tTrue\t0";
			lines_cost[1] = "True\tFalse\t1";
			lines_cost[2] = "False\tTrue\t1";
			lines_cost[3] = "False\tFalse\t0";
		} else {
			int numLabels = Integer.parseInt(Mainconfig.getInstance()
					.getListConfig().get("InputNumberLabels"));
			PossibleCategories[] fullList = { PossibleCategories.A,
					PossibleCategories.B, PossibleCategories.C,
					PossibleCategories.D, PossibleCategories.E,
					PossibleCategories.F, PossibleCategories.G,
					PossibleCategories.H };
			List<PossibleCategories> answers = new ArrayList<>();
			int start = 0;
			String[] cats = new String[numLabels];
			while (start < numLabels) {
				cats[start] = fullList[start].toString();
				start++;
			}
			int numlines = numLabels * numLabels;
			lines_cost = new String[numlines];
			for (int i = 0; i < cats.length; i++) {
				for (int j = 0; j < cats.length; j++) {
					int index = cats.length * i + j;
					int cost = 1;
					if (i == j) {
						cost = 0;
					}
					lines_cost[index] = cats[i] + "\t" + cats[j] + "\t" + cost;
				}
			}
		}
		Set<MisclassificationCost> costs = getClassificationCost(lines_cost);
		return costs;
	}

	private Set<MisclassificationCost> getClassificationCost(String[] lines) {
		Set<MisclassificationCost> labels = new HashSet<MisclassificationCost>();
		int cnt = 1;
		for (String line : lines) {
			String[] entries = line.split("\t");
			if (entries.length != 3) {
				throw new IllegalArgumentException(
						"Error while loading from assigned labels file (line "
								+ cnt + "):" + line);
			}
			cnt++;
			String from = entries[0];
			String to = entries[1];
			Double cost = Double.parseDouble(entries[2]);

			MisclassificationCost mcc = new MisclassificationCost(from, to,
					cost);
			labels.add(mcc);
		}
		return labels;
	}

	private void initializedInput() {
		this.labels = getAssignedLabels(feedbacks);
		//Label: 1 triple q w a
	}

	private Set<AssignedLabel> getAssignedLabels(List<FeedBack> listFeedBacsk) {

		Set<AssignedLabel> labels = new HashSet<AssignedLabel>();
		for (FeedBack feedback : listFeedBacsk) {
			String workername = feedback.getWorker().getWID() + "";
			String objectname = feedback.getQuestion().getCorrespondence()
					.getValue();
			String categoryname = convertAnswer2Categories(feedback.getAnswer());
			AssignedLabel al = new AssignedLabel(workername, objectname,
					categoryname);
			labels.add(al);
		}
		return labels;
	}

	private String convertAnswer2Categories(PossibleAnswer answer) {
		switch (answer) {
		case Yes:
			return PossibleCategories.True.toString();
		case No:
			return PossibleCategories.False.toString();
		case A:
			return PossibleCategories.A.toString();
		case B:
			return PossibleCategories.B.toString();
		case C:
			return PossibleCategories.C.toString();
		case D:
			return PossibleCategories.D.toString();
		case E:
			return PossibleCategories.E.toString();
		case F:
			return PossibleCategories.F.toString();
		case G:
			return PossibleCategories.G.toString();
		case H:
			return PossibleCategories.H.toString();
		default:
			return PossibleCategories.Unknown.toString();
		}

	}

	private void initializedCorrectlabel() {
		correct = new HashSet<CorrectLabel>();
	}

	private void initializedEvaluationFile() {
		getEvaluationLabels(questions);
	}

	private Set<CorrectLabel> getEvaluationLabels(List<Question> listQuestion) {

		Set<CorrectLabel> labels = new HashSet<CorrectLabel>();
		for (Question question : listQuestion) {

			String objectname = question.getCorrespondence().getValue();
			String categoryname = question.getCorrespondence().getGroundTruth()
					.name();

			CorrectLabel cl = new CorrectLabel(objectname, categoryname);
			labels.add(cl);
		}
		return labels;
	}

	public List<user.Worker> getWorkers() {
		return workers;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public double getCompetiontime() {
		return competiontime;
	}

	public void setCompetiontime(double competiontime) {
		this.competiontime = competiontime;
	}
}
