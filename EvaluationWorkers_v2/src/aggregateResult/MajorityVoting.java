package aggregateResult;

import java.util.List;


import question.PossibleAnswer;
import question.Question;
import user.Characteristic;
import user.TruthFulWorker;
import user.Worker;
import config.Mainconfig;
import data.PossibleCategories;
import feedback.FeedBack;

/**
 * @author Lamtran3101
 * 
 */
public class MajorityVoting extends AggregateResult {
	private List<FeedBack> feedbacks;
	private List<user.Worker> workers;
	private List<Question> questions;
	private double time;

	public MajorityVoting(List<FeedBack> feedbacks, List<user.Worker> workers,
			List<Question> questions) {
		this.feedbacks = feedbacks;
		this.workers = workers;
		this.questions = questions;
	}

	public void execute() {
		Mainconfig.getInstance().getTimer().start();
		estimateLabels();

		Mainconfig.getInstance().getTimer().stop();
		setTime(Mainconfig.getInstance().getTimer().getElapsedTime());

		estimateWorkers();
	}

	private void estimateLabels() {
		String inputType = Mainconfig.getInstance().getListConfig()
				.get("InputDataType");
		if (inputType.equals("Binary")) {
			estimateLabelsB();
		} else {
			estimateLabelsM();
		}
	}

	private void estimateLabelsM() {
		PossibleCategories[] fullList = { PossibleCategories.A,
				PossibleCategories.B, PossibleCategories.C,
				PossibleCategories.D, PossibleCategories.E,
				PossibleCategories.F, PossibleCategories.G,
				PossibleCategories.H };
		for (Question question : questions) {
			int[] total = new int[8];
			for (int i = 0; i < total.length; i++) {
				total[i] = 0;
			}
			for (Integer key : question.getListFeedbacks().keySet()) {
				FeedBack feedback = question.getListFeedbacks().get(key);
				if (checkexist(key)) {
					PossibleAnswer answer = feedback.getAnswer();
					switch (answer) {
					case A:
						total[0]++;
						break;
					case B:
						total[1]++;
						break;
					case C:
						total[2]++;
						break;
					case D:
						total[3]++;
						break;
					case E:
						total[4]++;
						break;
					case F:
						total[5]++;
						break;
					case G:
						total[6]++;
						break;
					case H:
						total[7]++;
						break;
					default:
						break;
					}
				}

			}
			int index = 0;
			int max = total[0];
			for (int j = 0; j < total.length; j++) {
				if (max < total[j]) {
					index = j;
					max = total[j];
				}
			}

			question.getCorrespondence().setEstResult(fullList[index]);
			System.out.println("size: " + workers.size());
		}
	}

	private boolean checkexist(int key) {
		for (Worker worker : workers) {
			if (worker.getWID() == key)
				return true;
		}
		return false;
	}

	private void estimateLabelsB() {
		for (Question question : questions) {
			double sum = 0;
			double total = 0;
			for (Integer key : question.getListFeedbacks().keySet()) {
				FeedBack feedback = question.getListFeedbacks().get(key);
				Worker worker = feedback.getWorker();
				if (workers.contains(worker)) {
					total++;
					if (feedback.getAnswer() == PossibleAnswer.Yes)
						sum++;
				}
			}
			question.getCorrespondence().setEstResult(
					PossibleCategories.Unknown);
			// System.out.println("size: "+ workers.size() +",total: " + total);
			if (total != 0) {
				question.getCorrespondence().setEstResult(
						PossibleCategories.False);
				if ((sum / total) >= 0.5)
					question.getCorrespondence().setEstResult(
							PossibleCategories.True);
			}

		}
	}

	private void estimateWorkers() {
		for (Worker worker : workers) {
			double numCorrectAnswers = 0;
			double totalAnswers = worker.getListAnswers().size();
			for (String value : worker.getListAnswers().keySet()) {
				FeedBack feedback = worker.getListAnswers().get(value);
				PossibleAnswer assignedLabel = feedback.getAnswer();
				PossibleAnswer correctLabeli = feedback.getQuestion()
						.getCorrespondence().getEstResult().getAnswer();
				if (assignedLabel == correctLabeli) {
					numCorrectAnswers++;
				}
			}
			Characteristic c = new TruthFulWorker();
			c.setReliableDegree(numCorrectAnswers / totalAnswers);
			worker.setEstChar(c);
		}
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}
}
