package answer;

import java.util.List;

import question.PossibleAnswer;

public abstract class AnswerType {
	public abstract List<PossibleAnswer> getPossibleAnswers();
}
