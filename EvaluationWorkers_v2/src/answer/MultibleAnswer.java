package answer;

import java.util.ArrayList;
import java.util.List;

import question.PossibleAnswer;

public class MultibleAnswer extends AnswerType{

	@Override
	public List<PossibleAnswer> getPossibleAnswers() {
		List<PossibleAnswer> list = new ArrayList<>();
		list.add(PossibleAnswer.Yes);
		list.add(PossibleAnswer.No);
		list.add(PossibleAnswer.Unknown);
		return list;
	}

}
