package config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;


import org.perf4j.StopWatch;

import question.HoneyPotDistributor;
import question.QuestionDistributor;
import tools.io.ConfigReader;
import user.FixedDistributor;
import user.MixedDistributor;
import user.NormalDistributor;
import user.UniformDistributor;
import user.WorkerDistributor;
import ch.epfl.lsir.nisb.dataAccess.dataset.DatasetProvider.DatasetCollection;
import feedback.FeedBacksConstraintDistributor;
import feedback.FeedBacksDistributor;
import feedback.FeedBacksPerQuestionDistributor;
import feedback.FeedBacksPerWorkerDistributor;

/**
 * @author lamtran3101 
 * <h4>Description</h4>
 * This file read the init.int for initializing all
 *         parameters.
 */
public class Mainconfig {

	private static class StaticHolder {
		public static Mainconfig singleton = new Mainconfig();
	}

	private ConfigReader reader = new ConfigReader();
	private Map<String, String> listConfig;

	private WorkerDistributor worker_distributor;
	private double reliabilityThreshold;

	private DatasetCollection dataset;

	private QuestionDistributor question_distributor;
	private FeedBacksDistributor feedback_distributor;
	private Random rand;
	private String[] algorithms;

	public enum ListDatasets {
		BUSINESS_PARTNER, PURCHASE_ORDER, BUSINESS_PARTNER_OLD, PURCHASE_ORDER_OLD, UNIVERSITY_APPLICATION_FORM, CIRCLE_TEST
	}

	private List<ListDatasets> Datasets = new ArrayList<>();

	private StopWatch timer = new StopWatch();

	public void setData(ListDatasets data) {
		dataset = DatasetCollection.valueOf(data.name());
	}

	private Mainconfig() {
		reader.readfile("config.ini");
		listConfig = reader.getConfig();
	}

	public static Mainconfig getInstance() {
		return StaticHolder.singleton;
	}

	/**
	 * This function initializes starting parameters
	 */
	public void initialized() {
		initializedRandom();
		initializedWorkers();
		initializedQuestions();
		initializedFeedBacks();
		initializedAlgorithms();
		initializedDatasets();
	}

	/**
	 * This function gets seed from init.ini file and generate a global variable rand.
	 */
	private void initializedRandom() {
		rand = new Random(Long.parseLong(listConfig.get("seed")));
	}

	/**
	 * Create set of algorithms.
	 */
	private void initializedAlgorithms() {
		algorithms = listConfig.get("algorithms").split(",");
	}

	/**
	 * Create set of data.
	 */
	private void initializedDatasets() {
		String[] values = listConfig.get("datasets").split(",");
		Datasets.clear();
		for (String value : values) {
			Datasets.add(ListDatasets.valueOf(value.trim()));
		}
	}

	/**
	 * This function does 2 tasks. <br/>
	 * <ul>
	 * <li>1st task: set the reliability threshold. </li>
	 * <li>2nd task: Type of worker distributor. There are 4 types in total: 
	 * 		<ul>
	 * 		<li>{@link user.FixedDistributor fixed distributor} </li>
	 * 		<li> {@link user.UniformDistributor uniform distributor} </li>
	 * 		 <li> {@link user.NormalDistributor normal distributor} </li>
	 * 		<li> {@link user.MixedDistributor mixed distributor} </li>
	 * 		</ul></li>
	 * </ul>
	 */
	private void initializedWorkers() {
		reliabilityThreshold = Double.parseDouble(listConfig
				.get("reliabilityThreshold"));
		switch (listConfig.get("workerDistributor")) {
		case ("FixedDistributor"):
			worker_distributor = new FixedDistributor();
			break;
		case ("UniformDistributor"):
			worker_distributor = new UniformDistributor();
			break;
		case ("MixedDistributor"):
			worker_distributor = new MixedDistributor();
			break;
		default:
			worker_distributor = new NormalDistributor();
		}

	}

	/**
	 * This function set type of question distributor. Currently, there's only one type : 
	 * {@link  question.HoneyPotDistributor distributor} 
	 */
	private void initializedQuestions() {
		switch (listConfig.get("QuestionDistributor")) {
		case (""):
			break;
		default:
			question_distributor = new HoneyPotDistributor();
		}
	}

	/** 
	 *  This function set types of constrains for generating feedbacks. <br/>
	 *  <ul>
	 *  <li>{@link feedback.FeedBacksPerQuestionDistributor Feedbacks per question constrains} : minimum feedback for one question.</li>
	 *  <li>{@link feedback.FeedBacksPerWorkerDistributor Feedbacks per worker constrains} : minimum answers one worker gives.</li>
	 *  <li>{@link feedback.FeedBacksConstraintDistributor Feedbacks constrains distributor} : constrains both feedback per question and feedback per worker.</li>
	 *  </ul>
	 */ 
	private void initializedFeedBacks() {
		switch (listConfig.get("feedbacksDistributor")) {
		case ("FeedBacksPerQuestionDistributor"):
			feedback_distributor = new FeedBacksPerQuestionDistributor();
			break;
		case ("FeedBacksPerWorkerDistributor"):
			feedback_distributor = new FeedBacksPerWorkerDistributor();
			break;
		default:
			feedback_distributor = new FeedBacksConstraintDistributor();
			break;
		}
	}

	public Map<String, String> getListConfig() {
		return listConfig;
	}

	public StopWatch getTimer() {
		return timer;
	}

	public FeedBacksDistributor getFeedBackDistributor() {
		return feedback_distributor;
	}

	public List<ListDatasets> getDatasets() {
		return Datasets;
	}

	public DatasetCollection getDataset() {
		return this.dataset;
	}

	public QuestionDistributor getQuestionDistributor() {
		return this.question_distributor;
	}

	public double getTotalWorkers() {
		return Double.parseDouble(listConfig.get("total"));
	}

	public WorkerDistributor getWorkerDsitributor() {
		return worker_distributor;
	}

	public double getReliabilityThreshold() {
		return reliabilityThreshold;
	}

	public String[] getAlgorithms() {
		return algorithms;
	}

	public Random getRand() {
		return rand;
	}
}
