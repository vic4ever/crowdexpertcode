package convertLabelProject;
import java.util.HashMap;

public class Category {

	private String name;

	/**
	 * The prior probability for this category
	 */
	private Double prior;

	/**
	 * The misclassification cost when we classify an object of this category
	 * into some other category. The HashMap key is the other category, and the Double
	 * is the cost.
	 */
	private HashMap<String, Double>	misclassification_cost;

	public Category(String name) {

		this.name = name;
		this.prior = -1.0;
		this.misclassification_cost = new HashMap<String, Double>();
	}

	public void setCost(String to, Double cost) {

		misclassification_cost.put(to, cost);
	}

	public Double getCost(String to) {

		return misclassification_cost.get(to);
	}

	public Double getPrior() {

		return prior;
	}

	public void setPrior(Double prior) {

		assert (prior >= 0.0 && prior <= 1.0);
		this.prior = prior;
	}

	public boolean hasPrior() {
		if (this.prior != -1)
			return true;
		else
			return false;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Category))
			return false;
		Category other = (Category) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

}
