package data;

/**
 * @author Lamtran3101 <h4>Description</h4> Each correspondence has 4
 *         parameters.
 *         <ul>
 *         <li>estResult : estimate category which is assigned by an algorithm</li>
 *         <li>groundtruth: true category this correspondence belongs. If
 *         there's no ground-truth, this will be set to unknown</li>
 *         <li>mode : indicate type of one specific question. 0 for normal; 1
 *         for trapped question.</li>
 *         <li>value : FullmappingID</li>
 *         </ul>
 * 
 */
public class Correspondence {
	private PossibleCategories estResult;
	private PossibleCategories groundtruth;
	private int mode = 0;
	private String value = null;

	/*
	 * public List<PossibleAnswers> getPossibleAnswers(){ List<PossibleAnswers>
	 * res = new ArrayList<>(); res.add(PossibleAnswers.True);
	 * res.add(PossibleAnswers.False); res.add(PossibleAnswers.Unknown); return
	 * res; }
	 */

	public Correspondence() {
		estResult = PossibleCategories.Unknown;
		groundtruth = PossibleCategories.Unknown;
	}

	public void refreshData() {
		estResult = PossibleCategories.Unknown;
	}

	public void setMode(int n) {
		mode = n;
	}

	public int getMode() {
		return mode;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public void setGroundTruth(PossibleCategories value) {
		groundtruth = value;
	}

	public PossibleCategories getGroundTruth() {
		return this.groundtruth;
	}

	public void setEstResult(PossibleCategories value) {
		estResult = value;
	}

	public PossibleCategories getEstResult() {
		return this.estResult;
	}

	@Override
	public Correspondence clone() throws CloneNotSupportedException {
		Correspondence clone = new Correspondence();
		clone.estResult = this.estResult;
		clone.groundtruth = this.estResult;
		clone.mode = this.mode;
		clone.value = this.value;
		return clone;
	}

}
