package data;

import question.PossibleAnswer;

/**
 * @author Lamtran3101 <h4>Description</h4> This class contains all categories
 *         that one object may belong. Currently, there are three categories.
 *         <ul>
 *         <li>True : when user say Yes</li>
 *         <li>False : when user say No</li>
 *         <li>Unknown : for initialization.
 *         </ul>
 */
public enum PossibleCategories {
	True(PossibleAnswer.Yes), False(PossibleAnswer.No), Unknown(
			PossibleAnswer.Unknown), A(PossibleAnswer.A), B(PossibleAnswer.B), C(
			PossibleAnswer.C), D(PossibleAnswer.D), E(PossibleAnswer.E), F(
			PossibleAnswer.F), G(PossibleAnswer.G), H(PossibleAnswer.H);

	private PossibleAnswer answer;

	PossibleCategories(PossibleAnswer Answer) {
		answer = Answer;
	}

	public PossibleAnswer getAnswer() {
		return answer;
	}

};
