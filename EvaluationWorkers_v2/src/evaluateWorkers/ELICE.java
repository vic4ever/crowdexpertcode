package evaluateWorkers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import question.PossibleAnswer;
import question.Question;
import user.Characteristic;
import user.TruthFulWorker;
import user.Worker;
import config.Mainconfig;
import data.PossibleCategories;
import feedback.FeedBack;
import feedback.FeedBackModel;

/**
 * @author lamtran3101 
 * <h4>Description</h4>
 * This algorithm based on paper "Quality control of Crowd Labeling through Expert Evaluation" <br/>
 * We need to transform workers' quality from (-1,1) -> (0,1) by using this equation f(x) = (x + 1)/2. <br/>
 *    Furthermore, in the reference paper, labels are
 *         divided into positive label (+1) or negative label (-1). In our case,
 *         True (1) or false (0).
 */
public class ELICE extends EvaluateWorker {

	private double subsetSize = 20;
	private Map<String, Question> subQuestions;

	public ELICE(FeedBackModel model) {
		super(model);
		initialized();
	}

	@Override
	public void execute() {
		Mainconfig.getInstance().getTimer().start();
		for (Worker worker : model.getListWorkers()) {
			System.out.println("ID:" + worker.getWID() + ", "
					+ worker.getReliability() + ", "
					+ worker.getEstChar().getReliableDegree());
		}

		subQuestions = getSubsetsQuestions();
		System.out.println("subQuestions: " + subQuestions.size());
		if (subQuestions.size() != 0) {
			updateWorkersQuality();
			setDifficultDegree();
			updateInferredLabel();
		}
		Mainconfig.getInstance().getTimer().stop();
		this.setCompetiontime(Mainconfig.getInstance().getTimer()
				.getElapsedTime());

		estimateWorkers();
	}

	/**
	 * In this function, we will initialized size of the subset n
	 */
	private void initialized() {
		/*
		 * subsetSize = (int) (Double.parseDouble(Mainconfig.getInstance()
		 * .getListConfig().get("ratioSubset")) * model.getListQuestions()
		 * .size());
		 */
		this.subsetSize = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("ratioSubset"));
	}

	private void estimateWorkers() {
		for (Worker worker : model.getListWorkers()) {
			double numCorrectAnswers = 0;
			double totalAnswers = worker.getListAnswers().size();
			for (String value : worker.getListAnswers().keySet()) {
				FeedBack feedback = worker.getListAnswers().get(value);
				PossibleAnswer assignedLabel = feedback.getAnswer();
				PossibleAnswer correctLabeli = feedback.getQuestion()
						.getCorrespondence().getEstResult().getAnswer();
				if (assignedLabel == correctLabeli) {
					numCorrectAnswers++;
				}
			}
			Characteristic c = new TruthFulWorker();
			c.setReliableDegree(numCorrectAnswers / totalAnswers);
			worker.setEstChar(c);
		}
	}

	private void setDifficultDegree() {
		for (Question question : model.getListQuestions()) {

			Question subquestion = subQuestions.get(question
					.getCorrespondence().getValue());
			if (subquestion != null) {
				double sum = 0;
				for (Integer value : subquestion.getListFeedbacks().keySet()) {
					FeedBack feedback = subquestion.getListFeedbacks().get(
							value);
					boolean check = checkCorrectAnswer(feedback);
					if (check) {
						sum++;
					}

				}
				// if (subquestion.getCorrectAnswer() == PossibleAnswer.No) i
				// ++;
				subquestion.setDifficultDegree(sum
						/ subquestion.getListFeedbacks().size());
			} else {
				double probibility = 0;
				PossibleAnswer estLabel = PossibleAnswer.Yes;
				for (Integer value : question.getListFeedbacks().keySet()) {
					FeedBack feedback = question.getListFeedbacks().get(value);
					double elice_quality = revertQuality(feedback.getWorker()
							.getEstChar().getReliableDegree());
					probibility += (elice_quality * convertAnswer(feedback
							.getAnswer()));
				}
				// System.out.println("probibility: " + probibility);
				probibility = probibility
						/ Mainconfig.getInstance().getTotalWorkers();
				if (probibility < 0) {
					estLabel = PossibleAnswer.No;
				}
				// System.out.println("estLabel: " + estLabel);
				double sum2 = 0;
				for (Integer value : question.getListFeedbacks().keySet()) {
					FeedBack feedback = question.getListFeedbacks().get(value);
					String answer = feedback.getAnswer().name();
					if (answer.equals(estLabel.name())) {
						sum2++;
					}

				}
				question.setDifficultDegree(sum2
						/ question.getListFeedbacks().size());
				// System.out.println("difficulty: " + sum2
				// / question.getListFeedbacks().size());
				// System.out.println("-----------------------");
			}

		}
	}

	private void updateInferredLabel() {
		for (Question question : model.getListQuestions()) {
			double nominator = 0;
			double sum = 0;
			for (Integer value : question.getListFeedbacks().keySet()) {
				FeedBack feedback = question.getListFeedbacks().get(value);
				double elice_quality = revertQuality(feedback.getWorker()
						.getEstChar().getReliableDegree());

				nominator = 1.0 + Math.exp((-1.0) * elice_quality
						* question.getDifficultDegree());

				sum += (convertAnswer(feedback.getAnswer()) / nominator);

			}
			System.out.println("result: " + sum);
			PossibleCategories inferredLabel = PossibleCategories.True;
			if (sum / Mainconfig.getInstance().getTotalWorkers() < 0) {
				inferredLabel = PossibleCategories.False;
			}
			question.getCorrespondence().setEstResult(inferredLabel);
		}
	}

	private void updateWorkersQuality() {
		for (Worker worker : model.getListWorkers()) {
			double correctAnswers = 0;
			Map<String, FeedBack> listAnswers = worker.getListAnswers();
			for (String value : subQuestions.keySet()) {
				for (String content : listAnswers.keySet()) {
					if (value.equals(content)) {
						boolean checkAnswer = checkCorrectAnswer(listAnswers
								.get(content));
						if (checkAnswer)
							correctAnswers++;
						else
							correctAnswers--;
					}
				}
			}
			Characteristic c = new TruthFulWorker();
			double size = subQuestions.size();
			double elice_quality = correctAnswers / size;
			// System.out.println("convertQuality: " +
			// convertQuality(elice_quality) + ", " + worker.getReliability() +
			// ", " + worker.getWID());
			c.setReliableDegree(convertQuality(elice_quality));
			// System.out.println(worker.getWID() + " " +
			// convertQuality(elice_quality) + " " + elice_quality);
			worker.setEstChar(c);
		}
	}

	/**
	 * Check if the answer is correct or not. <br/> If users didn't answer this
	 * question, feedback will be null
	 * 
	 */
	private boolean checkCorrectAnswer(FeedBack feedback) {
		if (feedback == null)
			return false;
		String correctAnswer = feedback.getQuestion().getCorrectAnswer().name();
		String assignedLabel = feedback.getAnswer().name();
		// System.out.println(correctAnswer + " " + assignedLabel);
		if (correctAnswer.equals(assignedLabel)) {
			return true;
		}
		return false;
	}

	/**
	 * Get a subset of questions that all workers answered every question in this set.
	 */
	private Map<String, Question> getSubsetsQuestions() {
		Map<String, Question> setQuestions = new HashMap<>();
		for (Question question : model.getListQuestions()) {
			if (checkAllWorkers(question) == true) {
				setQuestions.put(question.getCorrespondence().getValue(),
						question);
			}
		}

		return setQuestions;
	}

	/*
	 * private PossibleCategories convertWorkerAnswer(PossibleAnswer answer) {
	 * switch (answer) { case Yes: return PossibleCategories.True; case No:
	 * return PossibleCategories.False; default: return
	 * PossibleCategories.Unknown; } }
	 */

	/**
	 * Check if all workers answer this question or not. Assume that every worker
	 * answered a question only one time.
	 */
	private boolean checkAllWorkers(Question question) {
		int totalAnswers = question.getListFeedbacks().size();
		if (totalAnswers == model.getListWorkers().size()) {
			return true;
		}
		return false;
	}

	/**
	 * Convert (-1,1) -> (0,1)
	 */
	private double convertQuality(double n) {
		return (n + 1) / 2;
	}

	/**
	 * Convert (0,1) -> (-1,1)
	 */
	private double revertQuality(double n) {
		return (2 * n) - 1;
	}

	/**
	 * Assume that there'are only two possible answer: Yes or No
	 */
	private double convertAnswer(PossibleAnswer answer) {
		switch (answer) {
		case Yes:
			return 1;
		default:
			return -1;
		}
	}

	@Override
	public List<Worker> getWorkersResult() {
		return model.getListWorkers();
	}

	@Override
	public List<Question> getQuestionsResult() {
		return model.getListQuestions();
	}

}
