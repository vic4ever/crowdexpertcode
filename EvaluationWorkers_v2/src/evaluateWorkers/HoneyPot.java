package evaluateWorkers;

import java.util.ArrayList;
import java.util.List;


import question.PossibleAnswer;
import question.Question;
import user.Characteristic;
import user.TruthFulWorker;
import user.Worker;
import aggregateResult.MajorityVoting;
import config.Mainconfig;
import data.PossibleCategories;
import feedback.FeedBack;
import feedback.FeedBackModel;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * This algorithm based on paper "Matching schemas in online communities: A web 2.0 approach" 
 */
public class HoneyPot extends EvaluateWorker {
	private double evaluatedMinimum;
	private double evaluatedThreshold;
	private double evaluatedNumberOfQuestions;
	private List<Worker> truthfulWorkers;
	private List<FeedBack> realiableFeedBacks;
	/*private List<Question> remainingQuestions;*/
	private List<FeedBack> listFeedBacks;

	public HoneyPot(FeedBackModel model) {
		super(model);
		evaluatedNumberOfQuestions = getNumberofEvaluatedQuestions(this.model
				.getListQuestions());

		/*
		 * evaluatedMinimum = (evaluatedNumberOfQuestions *
		 * Double.parseDouble(Mainconfig
		 * .getInstance().getListConfig().get("evaluatedMinimum")));
		 */
		evaluatedMinimum = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("evaluatedMinimum"));
		/*
		 * evaluatedThreshold = (evaluatedMinimum *
		 * Double.parseDouble(Mainconfig
		 * .getInstance().getListConfig().get("evaluatedThreshold")));
		 */
		evaluatedThreshold = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("evaluatedThreshold"));

		truthfulWorkers = new ArrayList<>();
		realiableFeedBacks = new ArrayList<>();
		listFeedBacks = model.getListFeedBacks();
		
	}

	@Override
	public void execute() {
		/*
		 * System.out.println(evaluatedMinimum);
		 * System.out.println(evaluatedThreshold);
		 * System.out.println(evaluatedNumberOfQuestionss);
		 * System.out.println(model.getListQuestions().size());
		 */
		truthfulWorkers = getListOfTruthFulWorker();
		//System.out.println("truthfulWorkers: " + truthfulWorkers.size() );
		realiableFeedBacks = getListOfRealiableFeedBacks();
		/*remainingQuestions = getListOfRemainingQuestions();*/
		
		/*
		 * System.out.println("Numbers of workers: " +
		 * model.getListWorkers().size());
		 * System.out.println("Numbers of truthful workers: " +
		 * truthfulWorkers.size()); System.out.println("Numbers of feedbacks: "
		 * + model.getListFeedBacks().size());
		 * System.out.println("Numbers of true feedbacks: " +
		 * realiableFeedBacks.size());
		 */

		if (truthfulWorkers.size() != 0) {
			MajorityVoting result = new MajorityVoting(realiableFeedBacks,
					truthfulWorkers, model.getListQuestions());
			result.execute();
			ReEstimateWorkerExpertise(truthfulWorkers);
			this.setCompetiontime(result.getTime());		
		}

		/*
		 * System.out.println(model.getListWorkers().size());
		 * System.out.println(res ult.getWorkers().size());
		 */

	}
	
	private void ReEstimateWorkerExpertise(List<Worker> workers){
		for (Worker worker : workers) {
			double numCorrectAnswers = 0;
			double totalAnswers = 0;
			for (String value : worker.getListAnswers().keySet()) {
				FeedBack feedback = worker.getListAnswers().get(value);
				
				if(feedback.getQuestion().getCorrespondence().getMode() != 1){
					PossibleAnswer assignedLabel = feedback.getAnswer();
					PossibleAnswer correctLabeli = feedback.getQuestion()
							.getCorrespondence().getEstResult().getAnswer();
					if (assignedLabel == correctLabeli) {
						numCorrectAnswers++;
					}
					totalAnswers++;
				}
				
				
			}			
			Characteristic c = new TruthFulWorker();
			c.setReliableDegree(numCorrectAnswers / totalAnswers);
			worker.setEstChar(c);
		}
	}

	private int getNumberofEvaluatedQuestions(List<Question> list) {
		int total = 0;
		for (Question question : list) {
			if (question.getCorrespondence().getMode() == 1)
				total++;
		}
		return total;
	}

	private ArrayList<FeedBack> getListOfRealiableFeedBacks() {
		ArrayList<FeedBack> feedbacks = new ArrayList<>();

		for (FeedBack feedback : model.getListFeedBacks()) {
			for (Worker worker : truthfulWorkers) {
				if (worker.getWID() == feedback.getWorker().getWID()) {
					feedbacks.add(feedback);
				}
			}
		}
		
		double total = 0;
		for(Worker worker: model.getListWorkers()){
			total+= worker.getListAnswers().size();
		}
		System.out.println("origin: " + model.getListFeedBacks().size());
		System.out.println("after: " + feedbacks.size());
		System.out.println("totalWF: " + total);
		return feedbacks;
	}
	
/*	private ArrayList<Question> getListOfRemainingQuestions(){
		ArrayList<Question> listQuestions = new ArrayList<>();
		for(Question question : model.getListQuestions()){
			Question new_question = new Question();
		}
		
		
		return listQuestions;
	}*/

	private ArrayList<Worker> getListOfTruthFulWorker() {
		ArrayList<Worker> listWorkers = new ArrayList<>();

		for (Worker w : model.getListWorkers()) {
			boolean valid = computeMinimumQuestions(w);
			if (valid) {
				w.setEstChar(new TruthFulWorker());
				listWorkers.add(w);
			} else {
				w.setEstChar(new TruthFulWorker());
				w.getEstChar().setReliableDegree(0.5);
			}
		}
		return listWorkers;
	}

	private boolean computeMinimumQuestions(Worker w) {
		int id = w.getWID();
		double minimum = 0;
		double truthful = 0;
		/*for(FeedBack feedback : listFeedBacks){
			System.out.println("answer: " + feedback.getAnswer());
		}*/
		for (FeedBack feedback : listFeedBacks) {
			if (feedback.getWorker().getWID() == id) {
				/*
				 * PossibleAnswers answer =
				 * convertWorkerAnswer(feedback.getAnswer()); if
				 * (answer.equals(feedback
				 * .getQuestion().getCorrespondence().getGroundTruth())){
				 * minimum++; }
				 */
				if (feedback.getQuestion().getCorrespondence().getMode() == 1) {
					minimum++;
					PossibleAnswer answer = feedback.getAnswer();
					PossibleAnswer correct = feedback.getQuestion()
							.getCorrectAnswer();
				//	System.out.println("answer: " + answer + ", correct: " + correct);
					if (answer == correct)
						truthful++;

				}
			}
		}
		//System.out.println("evaluatedThreshold: " + evaluatedThreshold + ", truthful: " + truthful);
		if (minimum >= evaluatedMinimum && truthful >= evaluatedThreshold) {
			return true;
		}
		/*
		 * System.out.println("evaluatedMinimum: " + evaluatedMinimum +
		 * ", evaluatedThreshold: " + evaluatedThreshold);
		 * System.out.println("ID: " + id + ", minimum: " + minimum +
		 * ", truthful: " + truthful);
		 */
		return false;
	}

	/*
	 * private PossibleCategories convertWorkerAnswer(PossibleAnswer answer) {
	 * switch (answer) { case Yes: return PossibleCategories.True; case No:
	 * return PossibleCategories.False; default: return
	 * PossibleCategories.Unknown; } }
	 */

	@Override
	public List<Worker> getWorkersResult() {
		return model.getListWorkers();
	}

	@Override
	public List<Question> getQuestionsResult() {
		return model.getListQuestions();
	}

}
