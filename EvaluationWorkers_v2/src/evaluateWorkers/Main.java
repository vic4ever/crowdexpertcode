package evaluateWorkers;

import config.Mainconfig;
import config.Mainconfig.ListDatasets;
import factorypattern.AlgorithmFactory;
import feedback.FeedBackModel;

/**
 * @author Lamtran3101
 * <h4>Decription</h4>
 * This class is not used anymore. Please run {@link experiments.Runner this} 
 */
public abstract class Main {
	public static void main(String[] args) {
		Mainconfig.getInstance().initialized();
		AlgorithmFactory algo = new AlgorithmFactory();
		int iter = Integer.parseInt(Mainconfig.getInstance().getListConfig()
				.get("Iterator"));
		for (int i = 0; i < iter; i++) {
			for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
				Mainconfig.getInstance().setData(data);
				FeedBackModel model = new FeedBackModel();
				EvaluateWorker eval = null;
				String[] algorithms = Mainconfig.getInstance().getAlgorithms();
				for (String algorithm : algorithms) {
					eval = algo.createAlgorithm(algorithm, model);
					runOneAlgorithm(eval);
				}
			}
		}

	}

	protected abstract EvaluateWorker createAlgorithm(String type,
			FeedBackModel model);

	private static void runOneAlgorithm(EvaluateWorker runner) {
		runner.run();
		Output output = new Output(runner.getClass().getName(), Mainconfig
				.getInstance().getDataset().toString());
		output.setWorkers(runner.getWorkersResult());
		output.setQuestions(runner.getQuestionsResult());
		output.setCompletionTime(runner.getCompetiontime());
		output.exportResult();
		// output.exportWorkersDetail("worker.csv");
	}

}
