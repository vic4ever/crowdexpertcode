package evaluateWorkers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import question.PossibleAnswer;
import question.Question;
import tools.io.MyCsvWriter;
import user.Worker;
import config.Mainconfig;
import data.PossibleCategories;
import feedback.FeedBack;

/**
 * @author Lamtran3101 <h4>Description</h4> This class is used for handling and
 *         exporting output
 */
public class Output {
	private String dataset;
	private String algorithm;
	private List<Worker> workers;
	private List<Question> questions;
	private double completionTime;
	/*private String[] default_titles = { "Dataset", "Algorithm", "Enviroment",
			"FeedbackModel", "Number of Workers", "Number of Question",
			"Worker accuray", "Correspondences Accuracy", "Completion time",
			"True postive rate", "False positive rate",
			"NumberofTruthFulWorker", "Average of correct trappedquestion", "Remaining Questions", "after" };
*/
	private String[] default_titles = { "Dataset", "Algorithm", "Enviroment",
			"FeedbackModel", "Number of Workers", "Number of Question",
			"Worker accuray", "Correspondences Accuracy", "Completion time" };

	/*
	 * private String[] default_titles = { "Algorithm", "Enviroment",
	 * "FeedbackModel", "Number of Workers", "Number of Question",
	 * "Worker accuray", "Correspondences Accuracy", "Completion time"};
	 */

	private Map<String, Object> extendedResult = null;

	public Output(String algorithm, String dataset) {
		this.dataset = dataset;
		this.algorithm = algorithm;
	}

	public void exportResult() {
		MyCsvWriter.getInstance().WriteToFile(generateData(), "statistic.csv");
	}

	public void exportResult(String filename) {
		MyCsvWriter.getInstance().WriteToFile(generateData(), filename);
	}

	public void exportResult(String filename, Map<String, Object> extendedResult) {
		this.extendedResult = extendedResult;
		MyCsvWriter.getInstance().WriteToFile(generateData(), filename);
	}

	public void exportWorkersDetail(String filename) {
		MyCsvWriter.getInstance()
				.WriteToFile(generateWorkersDetail(), filename);
	}

	private String getContext() {
		return getAverageQuality() + "";
	}

	private String generateWorkersDetail() {
		StringBuffer sb = new StringBuffer();

		String[] workerstitles = { "Worker", "Reliability", "Est.Reliability",
				"NumberOfFeedbacks", "Type" };
		sb.append(workerstitles[0]);
		for (int i = 1; i < workerstitles.length; i++) {
			sb.append("\t" + workerstitles[i]);
		}
		sb.append("\n");
		for (Worker worker : workers) {
			sb.append(worker.getWID() + "\t"
					+ worker.getChar().getReliableDegree() + "\t"
					+ worker.getEstChar().getReliableDegree() + "\t"
					+ worker.getListAnswers().size() + "\t"
					+ worker.getChar().getClassName() + "\n");
		}
		return sb.toString();
	}
	
	public String generateTitles(){
		StringBuffer sb = new StringBuffer();
		sb.append(default_titles[0]);
		for (int i = 1; i < default_titles.length; i++) {
			sb.append("\t" + default_titles[i]);
		}

		if (extendedResult != null) {
			for (String key : extendedResult.keySet()) {
				sb.append("\t" + key);
			}
		}
		sb.append("\n");
		return sb.toString();
	}

	private String generateData() {

		StringBuffer sb = new StringBuffer();
		sb.append(default_titles[0]);
		for (int i = 1; i < default_titles.length; i++) {
			sb.append("\t" + default_titles[i]);
		}

		if (extendedResult != null) {
			for (String key : extendedResult.keySet()) {
				sb.append("\t" + key);
			}
		}
		sb.append("\n");
		double[] ROC = getROC();
		String FeedbackModel = Mainconfig.getInstance().getListConfig()
				.get("feedbackModel");

		/*
		 * sb.append(algorithm + "\t" + getContext() + "\t" + FeedbackModel +
		 * "\t" + workers.size() + "\t" + questions.size() + "\t" +
		 * getWorkersAccuracyRatio() + "\t" + getCorrespondencesAccuracy() +
		 * "\t" + getCompletionTime());
		 */
		double[] hp_result = getListofTruthfulWorkers();
		sb.append(dataset + "\t" + algorithm + "\t" + getContext() + "\t"
				+ FeedbackModel + "\t" + workers.size() + "\t"
				+ questions.size() + "\t" + getWorkersAccuracyRatio() + "\t"
				+ getCorrespondencesAccuracy() + "\t" + getCompletionTime());
				/*+ "\t" 
				
				+ ROC[0] + "\t" + ROC[1] + "\t"
				+ hp_result[0] + "\t"
				+ hp_result[1] + "\t"
				+ hp_result[2] + "\t"
				+ hp_result[3]);*/

		if (extendedResult != null) {
			for (String key : extendedResult.keySet()) {
				sb.append("\t" + extendedResult.get(key));
			}
		}
		sb.append("\n");

		System.out.println("dataset: " + dataset);
		System.out.println("algorithm: " + algorithm);
		System.out.println("workers: " + workers.size());
		System.out.println("questions: " + questions.size());
		System.out.println("Worker Accuracy ratio: "
				+ getWorkersAccuracyRatio());
		System.out.println("Correspondences Accuracy ratio: "
				+ getCorrespondencesAccuracy());
		System.out.println("Completetion time: " + getCompletionTime());
		return sb.toString();
	}

	private double[] getROC() {
		double[] figures = new double[4];
		for (int i = 0; i < figures.length; i++) {
			figures[i] = 0;
		}
		for (Question question : questions) {
			PossibleCategories estResult = question.getCorrespondence()
					.getEstResult();
			PossibleCategories groundtruth = question.getCorrespondence()
					.getGroundTruth();
			switch (estResult) {
			case True:
				if (groundtruth == PossibleCategories.True) {
					figures[0]++;
				} else if (groundtruth == PossibleCategories.False) {
					figures[3]++;
				}
				break;
			case False:
				if (groundtruth == PossibleCategories.True) {
					figures[1]++;
				} else if (groundtruth == PossibleCategories.False) {
					figures[2]++;
				}
				break;
			default:
				break;
			}
		}
		double[] result = new double[2];
		result[0] = figures[0] / (figures[0] + figures[1]);
		result[1] = figures[3] / (figures[2] + figures[3]);
		return result;
	}

	private double getWorkersAccuracyRatio() {
		double k = 0;
		for (Worker worker : workers) {
			double denta = Math.abs(worker.getChar().getReliableDegree()
					- worker.getEstChar().getReliableDegree());
			k = k + denta;
		}
		return k / workers.size();
	}

	private double getCorrespondencesAccuracy() {
		double k = 0;
		for (Question question : questions) {
			String groundtruth = question.getCorrespondence().getGroundTruth()
					.toString();
		
			String estResult = question.getCorrespondence().getEstResult()
					.toString();
			//System.out.println("groundtruth: " + groundtruth + ", estResult: " + estResult);
			//System.out.println("value: " + question.getCorrespondence().getValue() + ", groundtruth :" + groundtruth + ", estResult: " + estResult + ", mode : " + question.getCorrespondence().getMode());
			if (groundtruth.equals(estResult))
				k++;
		}
		double size = questions.size();

		return k / size;
	}

	private double getAverageQuality() {
		double average = 0;
		for (Worker worker : workers) {
			average += worker.getReliability();
		}
		return average / workers.size();
	}
	
	private double[] getListofTruthfulWorkers() {
		double[] result = new double[4];
		Map<String, Question> listQuestions = new HashMap<String, Question>(); ;
		result[0] = 0;
		result[1] = 0;
		result[2] = 0;
		result[3] = 0;
		double total = 0;
		double totalFeedbacks = 0;
		double threshold = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("evaluatedThreshold"));
		for (Worker worker : workers) {
			double correctAnswer = 0;
			for (String value : worker.getListAnswers().keySet()) {
				FeedBack feedback = worker.getListAnswers().get(value);
				if (feedback.getQuestion().getCorrespondence().getMode() == 1) {
					PossibleAnswer answer = feedback.getAnswer();
					PossibleAnswer correct = feedback.getQuestion().getCorrectAnswer();
					if (answer == correct) {
						correctAnswer++;
					}
				}
			} 
			if(correctAnswer >= threshold) {
				result[0]++;
				total+= correctAnswer;
				for(String value : worker.getListAnswers().keySet()){
					FeedBack feedback = worker.getListAnswers().get(value);
					if (feedback.getQuestion().getCorrespondence().getMode() != 1) {
						listQuestions.put(feedback.getQuestion().getCorrespondence().getValue(), feedback.getQuestion());
					}
				}
			}
			totalFeedbacks+= worker.getListAnswers().size();
		}
		result[1] = total/result[0];
		result[2] = listQuestions.size();
		result[3] = totalFeedbacks;
		return result;
	}

	private double getWorkerEstiamteErrorRate() {
		double result = 0;
		double threshold = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("SDAcceptable"));
		for (Worker worker : workers) {
			double denta = Math.abs(worker.getChar().getReliableDegree()
					- worker.getEstChar().getReliableDegree());
			if (denta >= threshold)
				result++;
		}
		return result / workers.size();
	}

	private String getSpammersDetectionErrorRate() {
		double result = 0;
		double size = 0;
		double threshold = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("SpammerThreshold"));
		for (Worker worker : workers) {
			if (worker.getChar().getReliableDegree() <= threshold)
				size++;
			if (worker.getEstChar().getReliableDegree() <= threshold)
				result++;
		}
		if (size != 0)
			return "Ratio:" + result / size;
		else
			return "No init spammers: " + result;
	}

	/*
	 * private double getHoneyPotRatio() { return
	 * Double.parseDouble(Mainconfig.getInstance().getListConfig()
	 * .get("evaluatedThreshold")); }
	 * 
	 * private double getfeedbacksPerQuestions() { double sum = 0; for (Question
	 * question : questions) { sum += question.getListFeedbacks().size(); }
	 * return sum / questions.size(); }
	 * 
	 * private double getfeedbacksPerWorkers() { double sum = 0; for (Worker
	 * worker : workers) { sum += worker.getListAnswers().size(); } return sum /
	 * workers.size(); }
	 */

	/**
	 * Return a list which contains 4 numbers: result[0]: true positive(TP),
	 * (answer)True->True(ground-truth) result[1]: true negative(TN),
	 * False->False result[2]: false positive(FP), True->False result[3]: false
	 * negative(FN), False->True
	 * 
	 * @return result
	 */
	/*
	 * private int[] getTruePositive() { int[] result = new int[4]; for (int num
	 * : result) { num = 0; } for (Question question : questions) { String
	 * answer = question.getCorrespondence().getEstResult() .toString(); String
	 * groundtruth = question.getCorrectAnswer().toString(); if
	 * (answer.equals("True")) { if (groundtruth.equals("True")) {
	 * 
	 * } else if (groundtruth.equals("False")) {
	 * 
	 * } } else if (answer.equals("False")) { if (groundtruth.equals("True")) {
	 * 
	 * } else if (groundtruth.equals("False")) {
	 * 
	 * } } }
	 * 
	 * return result; }
	 */

	/*
	 * private int getNumberOfEstSpammers(){ int i = 0; for(Worker worker:
	 * workers){ if
	 * (worker.getEstChar().getClass().getName().equals("user.Spammer")) i++; }
	 * 
	 * return i; }
	 */

	/*
	 * private int getNumberOfRealSpammers(){ int i = 0; for(Worker worker:
	 * workers){ System.out.println(worker.getChar().getClass().getName()); if
	 * (worker.getChar().getClass().getName().equals("user.Spammer")) i++; }
	 * return i; }
	 */

	public double getCompletionTime() {
		return completionTime;
	}

	public void setCompletionTime(double completionTime) {
		this.completionTime = completionTime;
	}

	public List<Worker> getWorkers() {
		return workers;
	}

	public void setWorkers(List<Worker> workers) {
		this.workers = workers;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

}
