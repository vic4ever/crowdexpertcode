package evaluateWorkers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import config.Mainconfig;

import data.PossibleCategories;

import feedback.FeedBack;
import feedback.FeedBackModel;

import question.Question;
import question.PossibleAnswer;
import user.Characteristic;
import user.TruthFulWorker;
import user.Worker;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * This algorithm based on paper
 *         "Supervised learning from multiple experts: Whom to trust when everyone lies a bit"
 */
public class SLME extends EvaluateWorker {

	private Map<String, Double> softlabels;
	private double positiveClasses;
	private Map<Integer, Double> sensitivities;
	private Map<Integer, Double> specificities;
	private double iter = 10;
	private double threshold = 0.5;

	private Map<Integer, double[]> workersParameters;
	private double[] prevalencePositiveClass;

	public SLME(FeedBackModel model) {
		super(model);
		initialized();
	}

	private void initialized() {
		softlabels = new HashMap<String, Double>();

		positiveClasses = 0;
		sensitivities = new HashMap<Integer, Double>();
		specificities = new HashMap<Integer, Double>();
		iter = Double.parseDouble(Mainconfig.getInstance().getListConfig()
				.get("SLME_iter"));
		threshold = Double.parseDouble(Mainconfig.getInstance().getListConfig()
				.get("SLME_threshold"));
	}

	@Override
	public void execute() {
		Mainconfig.getInstance().getTimer().start();
		initSoftLabels();
		initParameters();
		int i = 0;
		/* while (i < iter) { */
		while (i < iter) {
			estResult();
			estWorkers();
			estPositiveClass();
			updateSoftLabel();
			updateParam();
			i++;
		}
		estResult();
		Mainconfig.getInstance().getTimer().stop();
		this.setCompetiontime(Mainconfig.getInstance().getTimer()
				.getElapsedTime());

		estWorkersQuality();

	}

	private void initParameters() {
		prevalencePositiveClass = new double[2];
		for (int i = 0; i < prevalencePositiveClass.length; i++) {
			prevalencePositiveClass[i] = 1;
		}
		workersParameters = new HashMap<Integer, double[]>();
		for (Worker worker : model.getListWorkers()) {
			int ID = worker.getWID();
			double[] params = new double[4];
			for (int i = 0; i < params.length; i++) {
				params[i] = 1;
			}
			workersParameters.put(ID, params);
		}
	}

	private void updateParam() {
		for(Question question : model.getListQuestions()){
			String key = question.getCorrespondence().getValue();
			double softLabeli = softlabels.get(key);
			prevalencePositiveClass[0] += softLabeli;
			prevalencePositiveClass[1] += (1 - softLabeli);
		}
		
		
		for (Worker worker : model.getListWorkers()) {
			int ID = worker.getWID();
			double[] param = workersParameters.get(ID);
			for (String key : worker.getListAnswers().keySet()) {
				FeedBack feedback = worker.getListAnswers().get(key);
				double answer = getAnswerValue(feedback);
				String value = feedback.getQuestion().getCorrespondence()
						.getValue();
				double softlabeli = softlabels.get(value);
				param[0] += (softlabeli * answer);
				param[1] += (softlabeli * (1 - answer));
				param[2] += ((1 - softlabeli) * (1 - answer));
				param[3] += ((1 - softlabeli) * answer);
			}
			workersParameters.put(ID, param);
		}
	}

	private void estWorkers() {

		for (Worker worker : model.getListWorkers()) {
			double[] figures = getNumbers(worker);
			double SensitivityMean = figures[0];
			double SensitivityVariance = figures[1];
			double SpecificityMean = figures[2];
			double SpecificityVariance = figures[3];
			/*
			 * double[] a = computeParameters(a1, a2); double[] b =
			 * computeParameters(b1, b2);
			 */
			/*
			 * System.out.println("Worker: " + worker.getWID() + ", mean: " +
			 * a[0] + ", variance: " + a[1]);
			 */
			// System.out.println("ID:" + worker.getWID() + " " + a1 + " " + a2
			// + " " + b1 + " " + b2);
			addSensitivity(worker, SensitivityMean, SensitivityVariance);
			addSpecificity(worker, SpecificityMean, SpecificityVariance);
		}
	}

	private double[] computeParameters(double mean, double variance) {
		double[] figures = new double[2];

		double a1 = (-1 * Math.pow(mean, 3) + Math.pow(mean, 2) - mean
				* variance)
				/ variance;
		double a2 = a1 * (1 - mean) / mean;

		figures[0] = a1;
		figures[1] = a2;
		return figures;
	}

	private void addSensitivity(Worker worker, double mean, double variance) {

		/*
		 * double a1 = (-1 * Math.pow(mean, 3) + Math.pow(mean, 2) - mean
		 * variance) / variance; double a2 = a1 * (1 - mean) / mean;
		 */
		// System.out.println("Worker: " + worker.getWID() + ", mean: " + a1 +
		// ", variance: " + a2);

		double a1 = mean;
		double a2 = variance;

		double numerator = a1 - 1;
		double denominator = a1 + a2 - 2;
		for (String value : worker.getListAnswers().keySet()) {
			FeedBack feedback = worker.getListAnswers().get(value);
			double softLabeli = softlabels.get(feedback.getQuestion()
					.getCorrespondence().getValue());
			double answer = getAnswerValue(feedback);
			numerator += (softLabeli * answer);
			denominator += softLabeli;
		}

		sensitivities.put(worker.getWID(), numerator / denominator);
	}

	private void addSpecificity(Worker worker, double mean, double variance) {

		/*
		 * double b1 = (-Math.pow(mean, 3) + Math.pow(mean, 2) - mean *
		 * variance) / variance; double b2 = b1 * (1 - mean) / mean;
		 */
		double b1 = mean;
		double b2 = variance;

		double numerator = b1 - 1;
		double denominator = b1 + b2 - 2;
		for (String value : worker.getListAnswers().keySet()) {
			FeedBack feedback = worker.getListAnswers().get(value);
			double softLabeli = softlabels.get(feedback.getQuestion()
					.getCorrespondence().getValue());
			double answer = getAnswerValue(feedback);
			numerator += ((1 - softLabeli) * (1 - answer));
			denominator += (1 - softLabeli);
		}
		specificities.put(worker.getWID(), numerator / denominator);
	}

	private void estPositiveClass() {
		/*
		 * double noYes = 0; double noNo = 0; double size =
		 * model.getListQuestions().size(); double numerator = 0; double
		 * denominator = model.getListQuestions().size(); for (Question question
		 * : model.getListQuestions()) { String value =
		 * question.getCorrespondence().getValue(); double softLabeli =
		 * softlabels.get(value); if (softLabeli < threshold) noNo++; else
		 * noYes++; numerator += softLabeli; }
		 */
		double size = model.getListQuestions().size();
		double mean = 0;
		double variance = 0;
		double numerator = 0;
		double denominator = model.getListQuestions().size();
		for (Question question : model.getListQuestions()) {
			String value = question.getCorrespondence().getValue();
			double softLabeli = softlabels.get(value);
			numerator += softLabeli;
			mean += softLabeli;
		}
		mean = mean / size;
		for (Question question : model.getListQuestions()) {
			String value = question.getCorrespondence().getValue();
			double softLabeli = softlabels.get(value);
			variance = Math.pow(mean - softLabeli, 2);
		}
		variance = variance / size;

		double[] p = computeParameters(mean, variance);
		/*
		 * double p1 = (-Math.pow(p[0], 3) + Math.pow(p[0], 2) - p[0] * p[1]) /
		 * p[1]; double p2 = p1 * (1 - p[0]) / p[0];
		 */
		p[0] = prevalencePositiveClass[0];
		p[1] = prevalencePositiveClass[1];
		numerator += p[0] - 1;
		denominator += p[0] + p[1] - 2;
		positiveClasses = numerator / denominator;
	}

	private void updateSoftLabel() {
		for (Question question : model.getListQuestions()) {
			String value = question.getCorrespondence().getValue();
			double ai = computeai(question);
			double bi = computebi(question);
			double numerator = ai * positiveClasses;
			double denominator = numerator + bi * (1 - positiveClasses);
			// System.out.println("ai: " + ai + ", positiveClasses: " +
			// positiveClasses);
			double softLabeli = numerator / denominator;
			softlabels.put(value, softLabeli);
		}
	}

	private double computeai(Question question) {
		double result = 1;
		for (Integer value : question.getListFeedbacks().keySet()) {
			FeedBack feedback = question.getListFeedbacks().get(value);
			double answer = getAnswerValue(feedback);
			double sensitivity = sensitivities.get(value);
			result = result * Math.pow(sensitivity, answer)
					* Math.pow(1 - sensitivity, 1 - answer);
			// System.out.println(sensitivity);
		}
		// System.exit(0);
		return result;
	}

	private double computebi(Question question) {
		double result = 1;
		for (Integer value : question.getListFeedbacks().keySet()) {
			FeedBack feedback = question.getListFeedbacks().get(value);
			double answer = getAnswerValue(feedback);
			double specificity = specificities.get(value);
			result = result * Math.pow(specificity, 1 - answer)
					* Math.pow(1 - specificity, answer);
		}
		return result;
	}

	private void estResult() {
		/* double correct = model.getListQuestions().size(); */
		for (Question question : model.getListQuestions()) {
			String value = question.getCorrespondence().getValue();
			double softLabeli = softlabels.get(value);
			question.getCorrespondence().setEstResult(PossibleCategories.True);
			if (softLabeli < threshold) {
				/* correct--; */
				question.getCorrespondence().setEstResult(
						PossibleCategories.False);
			}
		}
		// System.out.println("correct: " + correct + ", size: " +
		// model.getListQuestions().size());
	}

	private void estWorkersQuality() {
		for (Worker worker : model.getListWorkers()) {
			double numCorrectAnswers = 0;
			double totalAnswers = worker.getListAnswers().size();
			for (String value : worker.getListAnswers().keySet()) {
				FeedBack feedback = worker.getListAnswers().get(value);
				PossibleAnswer assignedLabel = feedback.getAnswer();
				PossibleAnswer correctLabeli = feedback.getQuestion()
						.getCorrespondence().getEstResult().getAnswer();
				if (assignedLabel == correctLabeli) {
					numCorrectAnswers++;
				}
			}
			/*
			 * int id = worker.getWID(); double sensitivity =
			 * sensitivities.get(id); double specificity =
			 * specificities.get(id); double quality = Math.log(sensitivity) +
			 * Math.log(specificity);
			 */
			Characteristic c = new TruthFulWorker();
			// c.setReliableDegree(quality);
			/* System.out.println(numCorrectAnswers / totalAnswers); */
			c.setReliableDegree(numCorrectAnswers / totalAnswers);
			worker.setEstChar(c);
			/*
			 * System.out.println("Worker: " + worker.getWID() + ", real: " +
			 * worker.getChar().getReliableDegree() + ", est: " +
			 * c.getReliableDegree());
			 */
		}
	}

	private void initSoftLabels() {
		for (Question question : model.getListQuestions()) {
			double size = question.getListFeedbacks().size();
			double sum = 0;
			for (Integer value : question.getListFeedbacks().keySet()) {
				FeedBack feedback = question.getListFeedbacks().get(value);
				sum += getAnswerValue(feedback);
			}
			double softLabeli = sum / size;
			softlabels.put(question.getCorrespondence().getValue(), softLabeli);
		}
	}

	private double getAnswerValue(FeedBack feedback) {
		switch (feedback.getAnswer()) {
		case Yes:
			return 1;
		case No:
			return 0;
		default:
			return 0.5;
		}

	}

	/**
	 * @param worker
	 * @return array of number contain 4 elements A[0] : yes-> yes A[1] : no ->
	 *         yes A[2] : no -> no a[3] : yes -> no
	 */
	private double[] getNumbers(Worker worker) {
		/*
		 * double[] figures = new double[4]; for (int i = 0; i < figures.length;
		 * i++) { figures[i] = 0; }
		 */
		/*
		 * double sensitivityMean = 0; double sensitivityVariance = 0; double
		 * specificityMean = 0; double specificityVariance = 0;
		 * 
		 * double noYes = 0; double noNo = 0;
		 * 
		 * for (Question question : model.getListQuestions()) { String value =
		 * question.getCorrespondence().getValue(); double softLabeli =
		 * softlabels.get(value); if (softLabeli < threshold) { specificityMean
		 * += (1 - softLabeli); noNo++; } else { sensitivityMean += softLabeli;
		 * noYes++; } } if (noYes != 0) sensitivityMean = sensitivityMean /
		 * noYes; if (noNo != 0) specificityMean = specificityMean / noNo; for
		 * (Question question : model.getListQuestions()) { String value =
		 * question.getCorrespondence().getValue(); double softLabeli =
		 * softlabels.get(value); if (softLabeli < threshold) {
		 * specificityVariance += Math .pow(specificityMean - (1 - softLabeli ),
		 * 2); } else { sensitivityVariance += Math .pow(sensitivityMean -
		 * softLabeli, 2); } } if (noYes != 0) sensitivityVariance =
		 * sensitivityVariance / noYes; if (noNo != 0) specificityVariance =
		 * specificityVariance / noNo;
		 * 
		 * for (String key : worker.getListAnswers().keySet()) { FeedBack
		 * feedback = worker.getListAnswers().get(key); PossibleAnswer answer =
		 * feedback.getAnswer(); String value =
		 * feedback.getQuestion().getCorrespondence() .getValue(); double
		 * softLabeli = softlabels.get(value); if (answer == PossibleAnswer.Yes)
		 * { if (softLabeli > 0.5) { sensitivityMean++; } else {
		 * specificityVariance++; } } else if (answer == PossibleAnswer.No) { if
		 * (softLabeli > 0.5) { sensitivityVariance++; } else {
		 * specificityMean++; } } }
		 * 
		 * figures[0] = sensitivityMean; figures[1] = sensitivityVariance;
		 * figures[2] = specificityMean; figures[3] = specificityVariance;
		 */

		double[] param = workersParameters.get(worker.getWID());

		return param;
	}

	/*
	 * private PossibleCategories convertAnswer(PossibleAnswer answer) { switch
	 * (answer) { case Yes: return PossibleCategories.True; case No: return
	 * PossibleCategories.False; default: return PossibleCategories.Unknown; } }
	 */

	@Override
	public List<Worker> getWorkersResult() {
		return model.getListWorkers();
	}

	@Override
	public List<Question> getQuestionsResult() {
		return model.getListQuestions();
	}

}
