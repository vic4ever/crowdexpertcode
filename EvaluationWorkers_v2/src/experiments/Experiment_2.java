package experiments;

import java.util.LinkedHashMap;
import java.util.Map;


import question.Question;
import user.Worker;

import config.Mainconfig;

public class Experiment_2 extends Experiments{
	private int index;
	public Experiment_2() {
		super();
		filename = "experiment_2.csv";
		currentPath = "experiments/";
		fileConfig = "experiment_2.xml";
		step = 1;
		index = 0;
	}

	@Override
	void increaseStep() {
		if ((index % 20) == 0) {
			Long y = Mainconfig.getInstance().getRand().nextLong();
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			Mainconfig.getInstance().getListConfig().put("total", "30");
		}
		else{
			Mainconfig.getInstance().getListConfig().put("total",  Mainconfig.getInstance().getTotalWorkers() + step + "");
		}
		double total = Mainconfig.getInstance().getTotalWorkers();
		Mainconfig.getInstance().getListConfig().put("total", total + "");
		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("Seed", Mainconfig.getInstance().getListConfig().get("seed"));
		data.put("Answers Per Worker", computeAnswersPerWorker());
		data.put("Feedbacks Per Qestion", computeAnswersPerQuestion());
		return data;
	}



}
