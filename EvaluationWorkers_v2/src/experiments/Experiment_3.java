package experiments;

import java.util.LinkedHashMap;
import java.util.Map;


import question.Question;
import user.Worker;
import config.Mainconfig;

public class Experiment_3 extends Experiments {

	public Experiment_3() {
		super();
		filename = "experiment_3.csv";
		currentPath = "experiments/";
		fileConfig = "experiment_3.xml";
		step = 1;
		index = 0;
	}

	@Override
	void increaseStep() {
		
		if ((index % 20) == 0) {
			index = 0;
			Long y = Mainconfig.getInstance().getRand().nextLong();
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			Mainconfig.getInstance().getListConfig().put("feedbacksPerQuestion", "1");
		}
		else{
			Mainconfig.getInstance().getListConfig().put("feedbacksPerQuestion",  Integer.parseInt(Mainconfig.getInstance().getListConfig().get("feedbacksPerQuestion")) + (int) step + "");
		}
		int total = Integer.parseInt(Mainconfig.getInstance().getListConfig().get("feedbacksPerQuestion"));
		System.out.println("---------" + total);
		Mainconfig.getInstance().getListConfig().put("feedbacksPerQuestion", total + "");
		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("Seed", Mainconfig.getInstance().getListConfig().get("seed"));
		String key = "Feedbacks Per Qestion";
		double value = computeAnswersPerQuestion();
		data.put(key, value);

		data.put("ratio", value / workers.size());
		data.put("Answers per Worker", computeAnswersPerWorker());
		data.put("index", index);
		return data;
	}

}
