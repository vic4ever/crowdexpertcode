package experiments;

import java.util.LinkedHashMap;
import java.util.Map;


import question.Question;
import user.Worker;

import config.Mainconfig;

public class Experiment_4 extends Experiments{
	private int index;
	public Experiment_4() {
		super();
		filename = "experiment_4.csv";
		currentPath = "experiments/";
		fileConfig = "experiment_4.xml";
		step = 1;
		index = 0;
	}

	@Override
	void increaseStep() {
		if ((index % 11) == 0) {
			index = 0;
			Long y = Mainconfig.getInstance().getRand().nextLong();
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			Mainconfig.getInstance().getListConfig().put("total", "30");
			Mainconfig.getInstance().getListConfig().put("workersRatio", "fix(2);fix(10);fix(8)");
		}
		else{
			Mainconfig.getInstance().getListConfig().put("total",  Mainconfig.getInstance().getTotalWorkers() + step + "");
			int novicesW = 10 + index;
			Mainconfig.getInstance().getListConfig().put("workersRatio", "fix(2);fix(" + novicesW + ");fix(8)");
		}
		double total = Mainconfig.getInstance().getTotalWorkers();
		Mainconfig.getInstance().getListConfig().put("total", total + "");
		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("index",index );
		data.put("Answers Per Worker", computeAnswersPerWorker());
		data.put("Feedbacks Per Qestion", computeAnswersPerQuestion());
		data.put("workersRatio", Mainconfig.getInstance().getListConfig().get("workersRatio"));
		return data;
	}



}
