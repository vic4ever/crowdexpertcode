package experiments;

import java.util.LinkedHashMap;
import java.util.Map;

import config.Mainconfig;
public class Experiment_HoneyPot extends Experiments {

	private String param = "";

	public Experiment_HoneyPot(){
		index = 0;
		setupEvaluationThreshold();
	}
	
	private void setupEvaluationQuestion() {
		filename = "honeypot/experiment_honeypot.csv";
		currentPath = "experiments/honeypot/";
		fileConfig = "experiment_honeypot.xml";
		param = "HoneyPotRatio";
		step = 2;
	}

	private void setupEvaluationMinimum() {
		filename = "honeypot/experiment_honeypot_2.csv";
		currentPath = "experiments/honeypot/";
		fileConfig = "experiment_honeypot_2.ini";
		param = "evaluatedMinimum";
		step = 5;
	}

	private void setupEvaluationThreshold() {
		filename = "honeypot/experiment_honeypot_md_ThresHold.csv";
		currentPath = "experiments/honeypot/";
		fileConfig = "experiment_honeypot.ini";
		param = "evaluatedThreshold";
		step = 1;
	}

	@Override
	void increaseStep() {
		if ((index % 10) == 0) {
			index = 0;
			Long y = Mainconfig.getInstance().getRand().nextLong();
			System.out.println(y);
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			Mainconfig.getInstance().getListConfig().put(param, "1");
		}
		else{
			Mainconfig.getInstance().getListConfig().put(param,  Integer.parseInt(Mainconfig.getInstance().getListConfig().get(param)) + (int) step + "");
		}
		int total = Integer.parseInt(Mainconfig.getInstance().getListConfig().get(param));
		System.out.println("---------" + total);
		Mainconfig.getInstance().getListConfig().put(param, total + "");
		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put(param, Mainconfig.getInstance().getListConfig()
				.get(param));
		data.put("index", index);
		data.put("Answers per Worker", computeAnswersPerWorker());
		return data;
	}
	
	/*private double getNumberofTruthfulWorker(){
		
	}*/
	

}
