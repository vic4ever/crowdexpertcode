package experiments;

import java.util.LinkedHashMap;
import java.util.Map;

import config.Mainconfig;

public class Experiment_InputData  extends Experiments{

	private double decrease = 0.05;
	
	public Experiment_InputData() {
		super();
		filename = "experiment_inputData.csv";
		currentPath = "experiments/";
		fileConfig = "experiment_inputData.xml";
		step = 1;
		index = 0;
	}
	
	@Override
	void increaseStep() {
		if ((index % 21) == 0) {
			index = 0;
			Long y = Mainconfig.getInstance().getRand().nextLong();
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			Mainconfig.getInstance().getListConfig().put("InputDataRatio", "1");
		}
		else{
			double ratio = Double.parseDouble(Mainconfig.getInstance().getListConfig().get("InputDataRatio"));
			ratio = ratio - decrease;
			Mainconfig.getInstance().getListConfig().put("InputDataRatio", ratio + "");
		}
		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("index",index );
		data.put("InputDataRatio", Mainconfig.getInstance().getListConfig().get("InputDataRatio"));
		data.put("Answers Per Worker", computeAnswersPerWorker());
		data.put("Feedbacks Per Qestion", computeAnswersPerQuestion());
		return data;
	}

}
