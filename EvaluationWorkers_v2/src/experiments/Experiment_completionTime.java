package experiments;

import java.util.LinkedHashMap;
import java.util.Map;

import config.Mainconfig;

public class Experiment_completionTime extends Experiments{

	public Experiment_completionTime() {
		super();
		filename = "completiontime/experiment_time_100_300.csv";
		currentPath = "experiments/";
		fileConfig = "experiment_time.ini";
		step = 1;
		index = 0;
	}

	@Override
	void increaseStep() {
//		if ((index % 20) == 0) {
//			index = 0;
//			Long y = Mainconfig.getInstance().getRand().nextLong();
//			Mainconfig.getInstance().getListConfig().put("seed", y + "");
//			Mainconfig.getInstance().getListConfig().put("total", "20");
//		}
//		else{
//			Mainconfig.getInstance().getListConfig().put("total",  Mainconfig.getInstance().getTotalWorkers() + step + "");
//		}
//		double total = Mainconfig.getInstance().getTotalWorkers();
//		Mainconfig.getInstance().getListConfig().put("total", total + "");
//		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
//		data.put("index",index );
		data.put("Answers Per Worker", computeAnswersPerWorker());
		data.put("Feedbacks Per Qestion", computeAnswersPerQuestion());
		return data;
	}

}
