package experiments;

import java.util.LinkedHashMap;
import java.util.Map;


import question.Question;
import user.Worker;
import config.Mainconfig;

public class Experiment_multiC extends Experiments {

	public Experiment_multiC() {
		super();
		filename = "NEW/experiment_test.csv";
		currentPath = "experiments/";
		fileConfig = "experiment_multiC.xml";
		step = 2;
		index = 0;
	}
	@Override
	void increaseStep() {
		if ((index % 24) == 0) {
			index = 0;
			Long y = Mainconfig.getInstance().getRand().nextLong();
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			Mainconfig.getInstance().getListConfig().put("total", "22");
		}
		else{
			Mainconfig.getInstance().getListConfig().put("total",  Mainconfig.getInstance().getTotalWorkers() + step + "");
		}
		double total = Mainconfig.getInstance().getTotalWorkers();
		Mainconfig.getInstance().getListConfig().put("total", total + "");
		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("Seed", Mainconfig.getInstance().getListConfig().get("seed"));
		String key = "Feedbacks Per Qestion";
		double value = computeAnswersPerQuestion();
		data.put(key, value);

		data.put("ratio", value / workers.size());
		data.put("Answers per Worker", computeAnswersPerWorker());
		data.put("index", index);
		return data;
	}

}
