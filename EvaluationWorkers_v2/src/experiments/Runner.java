package experiments;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * We use this class for running one specify experiment or set of experiemnts.
 * In general, there're two important parameters:
 * <ul>
 * <li>currentpath : the folder that contains all setting files</li>
 * <li>filename : the general setting file that must not be changed</li>
 * </ul>
 */
public class Runner {

	private List<Experiments> holder;
	private String currentpath = "experiments/";
	private String filename = "experiment_setup.ini";
	private List<String> lines;

	public Runner() {
		holder = new ArrayList<Experiments>();
		lines = new ArrayList<String>();
		readfile();
		prepareExperiments();
	}

	private void run() {
		for (Experiments experiment : holder) {
			experiment.run();
		}
	}

	public static void main(String[] args) {
		Runner experiment = new Runner();
		experiment.run();
	}

	private void prepareExperiments() {
		for (String line : lines) {
			Experiments experiment = null;
			switch (line) {
			case "Experiment_ELICE":
				experiment = new Experiment_ELICE();
				break;
			case "Experiment_GLAD":
				experiment = new Experiment_GLAD();
				break;
			case "Experiment_HoneyPot":
				experiment = new Experiment_HoneyPot();
				break;
			case "Experiment_Iter":
				experiment = new Experiment_Iter();
				break;
			case "Experiment_SLME":
				experiment = new Experiment_SLME();
				break;
			case "Experiment_1":
				experiment = new Experiment_1();
				break;
			case "Experiment_1b":
				experiment = new Experiment_1b();
				break;
			case "Experiment_2":
				experiment = new Experiment_2();
				break;

			case "Experiment_3":
				experiment = new Experiment_3();
				break;
			case "Experiment_4":
				experiment = new Experiment_4();
				break;

			case "Experiment_5":
				experiment = new Experiment_5();
				break;
			case "Experiment_6":
				experiment = new Experiment_6();
				break;
			case "Experiment_7":
				experiment = new Experiment_7();
				break;
			case "Experiment_8":
				experiment = new Experiment_8();
				break;
			case "Experiment_9":
				experiment = new Experiment_9();
				break;
			case "Experiment_10":
				experiment = new Experiment_10();
				break;
			case "Experiment_init":
				experiment = new Experiment_init();
				break;
			case "Experiment_EM":
				experiment = new Experiment_EM();
				break;
			case "Experiment_inputData":
				experiment = new Experiment_InputData();
				break;
			case "Experiment_multiC":
				experiment = new Experiment_multiC();
				break;
			case "Experiment_time":
				experiment = new Experiment_completionTime();
				break;	
			default:
				break;
			}
			if (experiment != null)
				holder.add(experiment);
		}
	}

	public void readfile() {
		String path = currentpath + filename;
		try {
			BufferedReader dataInput = new BufferedReader(new FileReader(
					new File(path)));
			String line;

			while ((line = dataInput.readLine()) != null) {
				// buffer.append(cleanLine(line.toLowerCase()));
				line = line.replaceAll(" ", "");
				line = line.replaceAll("\"", "");
				lines.add(line);
			}
			dataInput.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
