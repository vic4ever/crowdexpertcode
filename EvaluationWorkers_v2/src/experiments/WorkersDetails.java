package experiments;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Lamtran3101
 * We use this class for exporting workers' detail .
 */
public class WorkersDetails {

	private List<Experiments> holder;
	private String currentpath = "experiments/";
	private String filename = "experiment_setup.ini";
	private List<String> lines;

	public WorkersDetails() {
		holder = new ArrayList<Experiments>();
		lines = new ArrayList<String>();
		readfile();
		prepareExperiments();
	}

	private void run() {
		for (Experiments experiment : holder) {
			experiment.workersExport();
		}
	}

	public static void main(String[] args) {
		WorkersDetails experiment = new WorkersDetails();
		experiment.run();
	}

	private void prepareExperiments() {
		for (String line : lines) {
			Experiments experiment = null;
			switch (line) {
			case "Experiment_ELICE":
				experiment = new Experiment_ELICE();
				break;
			case "Experiment_HoneyPot":
				experiment = new Experiment_HoneyPot();
				break;
			case "Experiment_Iter":
				experiment = new Experiment_Iter();
				break;
			case "Experiment_1":
				experiment = new Experiment_1();
				break;
			case "Experiment_1b":
				experiment = new Experiment_1b();
				break;
			case "Experiment_2":
				experiment = new Experiment_2();
				break;	
			/*
			 * case "Experiment_3": experiment = new Experiment_3(); break; case
			 * "Experiment_4": experiment = new Experiment_4(); break;
			 */
			case "Experiment_5":
				experiment = new Experiment_5();
				break;		
			case "Experiment_6":
				experiment = new Experiment_6();
				break;
			case "Experiment_7":
				experiment = new Experiment_7();
				break;	
			case "Experiment_8":
				experiment = new Experiment_8();
				break;		
			default:
				break;
			}
			if (experiment != null)
				holder.add(experiment);
		}
	}

	public void readfile() {
		String path = currentpath + filename;
		try {
			BufferedReader dataInput = new BufferedReader(new FileReader(
					new File(path)));
			String line;

			while ((line = dataInput.readLine()) != null) {
				// buffer.append(cleanLine(line.toLowerCase()));
				line = line.replaceAll(" ", "");
				line = line.replaceAll("\"", "");
				lines.add(line);
			}
			dataInput.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
