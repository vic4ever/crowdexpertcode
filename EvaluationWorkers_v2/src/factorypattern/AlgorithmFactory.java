package factorypattern;

import evaluateWorkers.ELICE;
import evaluateWorkers.EM;
import evaluateWorkers.EvaluateWorker;
import evaluateWorkers.GLAD;
import evaluateWorkers.HoneyPot;
import evaluateWorkers.IterativeLearning;
import evaluateWorkers.Main;
import evaluateWorkers.MajorityDecision;
import evaluateWorkers.SLME;
import feedback.FeedBackModel;

/**
 * @author Lamtran3101 <h4>Description</h4> This class has a responsibility for
 *         creating correct algorithm.
 */
public class AlgorithmFactory extends Main {

	private static AlgorithmFactory instance;

	public static AlgorithmFactory getInstance() {
		if (instance == null) {
			instance = new AlgorithmFactory();
		}
		return instance;
	}

	public AlgorithmFactory() {

	}

	@Override
	public EvaluateWorker createAlgorithm(String type, FeedBackModel model) {
		EvaluateWorker eval = null;
		/* System.out.println(type); */
		switch (type) {
		case ("EM"):
			eval = new EM(model);
			break;
		case ("HoneyPot"):
			eval = new HoneyPot(model);
			break;
		case ("ELICE"):
			eval = new ELICE(model);
			break;
		case ("SLME"):
			eval = new SLME(model);
			break;
		case ("IterativeLearning"):
			eval = new IterativeLearning(model);
			break;
		case ("GLAD"):
			eval = new GLAD(model);
			break;
		default:
			eval = new MajorityDecision(model);
			break;
		}
		return eval;
	}

}
