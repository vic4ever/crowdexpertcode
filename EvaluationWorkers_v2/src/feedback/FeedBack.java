package feedback;

import question.PossibleAnswer;
import question.Question;
import user.Worker;

/**
 * @author Lamtran3101 <h4>Description</h4> Each feedback is a mapping of one
 *         worker to one question. There's {@link #getAnswer() a method} for
 *         quickly getting worker's answer.
 */
public abstract class FeedBack {
	private Question question;
	private Worker worker;
	private PossibleAnswer answer;

	public FeedBack() {

	}

	public void setAnswer(PossibleAnswer ans) {
		answer = ans;
	}

	public PossibleAnswer getAnswer() {
		return this.answer;
	}

	public void setWorker(Worker w) {
		worker = w;
	}

	public Worker getWorker() {
		return this.worker;
	}

	public void setQuestion(Question q) {
		question = q;
	}

	public Question getQuestion() {
		return this.question;
	}

	@Override
	public FeedBack clone() throws CloneNotSupportedException {
		return (FeedBack) super.clone();
	}
}
