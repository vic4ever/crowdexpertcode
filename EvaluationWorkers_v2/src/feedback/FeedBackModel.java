package feedback;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import question.GenerateQuestions;
import question.PossibleAnswer;
import question.Question;
import user.TruthFulWorker;
import user.UserCommunity;
import user.Worker;
import config.Mainconfig;
import data.PossibleCategories;

/**
 * @author Lamtran3101 <h4>Description</h4> This class is the core module of
 *         Feedback component. We will generate crowds, questions and feedbacks
 *         here.
 *
 */
public class FeedBackModel {

    private List<Worker> Workers;
    private List<Question> Questions;
    private List<FeedBack> FeedBacks;
    private HashMap<String, Question> questionsMap;
    private HashMap<Integer, Worker> workersMap;

    public FeedBackModel() {
        initialized();
    }

    /**
     * Before given data to one algorithm, we need to clean all estimate data
     * assign to questions, workers and feedbacks by previous algorithm.
     */
    public void refreshData() {
        for (Worker worker : Workers) {
            worker.setEstChar(new TruthFulWorker());
            worker.getEstChar().refreshData();
        }
        for (Question question : Questions) {
            question.getCorrespondence().refreshData();
        }
    }

    private void generateQuestion() {
        GenerateQuestions questions = new GenerateQuestions();
        questions.generateQuestion(Mainconfig.getInstance()
                .getQuestionDistributor());
        Questions = questions.getList();
        questionsMap = new HashMap<>();
        for (Question question : Questions) {
            questionsMap.put(question.getCorrespondence().getValue(), question);
        }
    }

    private void generateWorker() {
        UserCommunity Users = new UserCommunity();
        Users.generateWorker(Mainconfig.getInstance().getWorkerDsitributor());
        Workers = Users.getListOfWorker();
        //		for (Worker worker : Workers) {
        //			System.out.println(worker.getChar().getClassName());
        //		}
        workersMap = new HashMap<>();
        for (Worker worker : Workers) {
            workersMap.put(worker.getWID(), worker);
        }
    }

    private void generateFeedBack() {
        GenerateFeedBacks Feedbacks = new GenerateFeedBacks(Workers, Questions);
        Feedbacks.generateFeedBacks(Mainconfig.getInstance()
                .getFeedBackDistributor());
        this.FeedBacks = Feedbacks.getFeedBacks();
        // displayFeedBack();
    }

    public void displayFeedBack() {

        int i = 0;
        for (FeedBack f : FeedBacks) {
            i++;
            System.out.println("Feedback: " + i);
            displayFeedback(f);
        }
    }

    public void displayFeedback(FeedBack f) {
        Worker w = f.getWorker();
        //		System.out.println("Worker: " + w.getWID() + " is a "				+ w.getChar().getClassName());
        System.out.println("Worker: " + w.getWID() + " is a "				+ w.getReliability());

        Question q = f.getQuestion();
        System.out.println("Question: " + q.getCorrespondence().getValue()
                + " is " + q.getCorrespondence().getGroundTruth());

        System.out.println("Answer: " + f.getAnswer() + " " + f.getAnswer().getCategory());
        System.out.println("----------------");
    }

    public void printFeedbacks(){
        for (FeedBack f : FeedBacks) {
            System.out.print(String.format("%d\t%d\t%s\n", f.getWorker().getWID(), f.getQuestion().getId(), f.getAnswer()));
        }
    }

    public void generateGALInputFiles(String labelFileName, String catFileName, String goldFileName, String workerFile) throws IOException{
        FileWriter labelFile = new FileWriter(labelFileName);
        BufferedWriter labelWriter = new BufferedWriter(labelFile);

        Collections.sort(FeedBacks, new Comparator<FeedBack>(){
            public int compare(FeedBack m1, FeedBack m2) {
                return m1.getQuestion().getId() - m2.getQuestion().getId();
            }
        });
        for (FeedBack f : FeedBacks) {
            if(f.getAnswer().toString().equals("Yes")){
                labelWriter.write(String.format("%d\t%d\t%d\n", f.getWorker().getWID(), f.getQuestion().getId(), 1));
            }else{
                labelWriter.write(String.format("%d\t%d\t%d\n", f.getWorker().getWID(), f.getQuestion().getId(), 0));
            }

        }
        labelWriter.close();

        FileWriter catFIle = new FileWriter(catFileName);
        BufferedWriter catWriter = new BufferedWriter(catFIle);
        Set<Integer> cats = new HashSet<Integer>();
        for(FeedBack f: FeedBacks){
            PossibleAnswer cat = f.getAnswer();
            if(f.getAnswer().toString().equals("Yes")){
                cats.add(1);
            }else{
                cats.add(0);
            }
        }

        for (Integer possibleCategories : cats) {
            catWriter.write(String.format("%d\n",possibleCategories));
            //			System.out.println(String.format("%s\n",possibleCategories));
        }
        catWriter.close();

        FileWriter goldFile = new FileWriter(goldFileName);
        BufferedWriter goldWriter = new BufferedWriter(goldFile);
        for (Question question : Questions) {
            if(question.getCorrectAnswer().toString().equals("Yes")){
                goldWriter.write(String.format("%s\t%d\n", question.getId(),1));
            }else{
                goldWriter.write(String.format("%s\t%d\n", question.getId(),0));
            }
        }
        goldWriter.close();


        FileWriter workerF = new FileWriter(workerFile);
        BufferedWriter workerWriter = new BufferedWriter(workerF);
        Collections.sort(Workers, new Comparator<Worker>() {
            @Override public int compare(final Worker o1, final Worker o2) {
                if(o1.getReliability() - o2.getReliability() >0)
                    return -11;
                else if(o1.getReliability() - o2.getReliability() == 0)
                    return 0;
                else return 1;
            }
        });
        for(Worker w: Workers){
            workerWriter.write(String.format("%d\t%f\t-1\n", w.getWID(),w.getReliability()));
        }
        workerWriter.close();
    }


    public void generateGALInputFilesMulti(String labelFileName, String catFileName, String goldFileName, String workerFile) throws IOException{
        FileWriter labelFile = new FileWriter(labelFileName);
        BufferedWriter labelWriter = new BufferedWriter(labelFile);

        Map<String,Integer> catMap = new HashMap<String,Integer>();
        catMap.put("A", 0);
        catMap.put("B", 1);
        catMap.put("C", 2);
        catMap.put("D", 3);
        catMap.put("E", 4);
        catMap.put("F", 5);
        catMap.put("G", 6);
        catMap.put("H", 7);
        catMap.put("Yes", 1);
        catMap.put("No", 0);

        Collections.sort(FeedBacks, new Comparator<FeedBack>(){
            public int compare(FeedBack m1, FeedBack m2) {
                return m1.getQuestion().getId() - m2.getQuestion().getId();
            }
        });
        for (FeedBack f : FeedBacks) {
            Integer val = catMap.get(f.getAnswer().toString());
            labelWriter.write(String.format("%d\t%d\t%d\n", f.getWorker().getWID(), f.getQuestion().getId(), val));
        }
        labelWriter.close();

        FileWriter catFIle = new FileWriter(catFileName);
        BufferedWriter catWriter = new BufferedWriter(catFIle);
        Set<Integer> cats = new HashSet<Integer>();
        for(FeedBack f: FeedBacks){
            Integer cat = catMap.get(f.getAnswer().toString());
            cats.add(cat);
        }

        for (Integer possibleCategories : cats) {
            catWriter.write(String.format("%d\n",possibleCategories));
            //			System.out.println(String.format("%s\n",possibleCategories));
        }
        catWriter.close();

        FileWriter goldFile = new FileWriter(goldFileName);
        BufferedWriter goldWriter = new BufferedWriter(goldFile);
        for (Question question : Questions) {
            Integer gold = catMap.get(question.getCorrectAnswer().toString());
            goldWriter.write(String.format("%s\t%d\n", question.getId(),gold));
        }
        goldWriter.close();


        FileWriter workerF = new FileWriter(workerFile);
        BufferedWriter workerWriter = new BufferedWriter(workerF);
        Collections.sort(Workers, new Comparator<Worker>() {
            @Override public int compare(final Worker o1, final Worker o2) {
                if(o1.getReliability() - o2.getReliability() >0)
                    return -1;
                else if(o1.getReliability() - o2.getReliability() == 0)
                    return 0;
                else return 1;
            }
        });
        for(Worker w: Workers){
            workerWriter.write(String.format("%d\t%f\t-1\n", w.getWID(),w.getReliability()));
        }
        workerWriter.close();
    }

    private void initialized() {
        generateQuestion();
        generateWorker();
        generateFeedBack();

        filterQuestions();
        filterWorkers();

        System.out.println("Before update");
        for (Worker worker : Workers) {
            System.out.println(worker.getWID()+ " " + worker.getChar().getClassName());
        }

        updateWorkersQuality();
		/*
		 * ValidatorFeedBacks validator = new ValidatorFeedBacks(Workers,
		 * Questions, FeedBacks); validator.exportWorkersAnswers();
		 * validator.exportQuestionsAnswers();
		 */
        // System.exit(0);
        System.out.println("Print feedback");
        for (Worker worker : Workers) {
            System.out.println(worker.getWID()+ " " + worker.getChar().getClassName());
        }
        //		displayFeedBack();
    }

    private void updateWorkersQuality() {
        String inputType = Mainconfig.getInstance().getListConfig()
                .get("InputDataType");
        if (inputType.equals("Binary")) {
            updateBinary();
        } else {
            updateMultiple();
        }
    }

    private void updateMultiple() {
        for (Worker worker : Workers) {
            double reliability = getWorkerQuality_2(worker);
            worker.getChar().setReliableDegree(reliability);
        }
    }

    private void updateBinary() {
        for (Worker worker : Workers) {
            double[] figures = getWorkerQuality(worker);
            double reliability = (figures[0] + figures[2])
                    / worker.getListAnswers().size();
            double sensitivity = figures[0] / (figures[0] + figures[1]);
            double specificity = figures[2] / (figures[2] + figures[3]);
			/*
			 * System.out.println("ID: " + worker.getWID() + " " + reliability +
			 * sensitivity + specificity); System.out.println("ID: " +
			 * worker.getWID() + " " + worker.getReliability() + " " +
			 * worker.getSensitivity() + " " + worker.getSpecificity());
			 */
            worker.getChar().setReliableDegree(reliability);
            worker.getChar().setSensitivity(sensitivity);
            worker.getChar().setSpecificity(specificity);
        }
    }

    private double getWorkerQuality_2(Worker worker) {
        double reliability = 0;
        for (String value : worker.getListAnswers().keySet()) {
            FeedBack feedback = worker.getListAnswers().get(value);
            PossibleAnswer answer = feedback.getAnswer();
            PossibleAnswer correctAnswer = feedback.getQuestion()
                    .getCorrectAnswer();
            if (correctAnswer == answer)
                reliability++;
        }

        return reliability / worker.getListAnswers().size();
    }

    private double[] getWorkerQuality(Worker worker) {
        double[] figures = new double[4];
        for (int i = 0; i < figures.length; i++) {
            figures[i] = 0;
        }
        for (String value : worker.getListAnswers().keySet()) {
            FeedBack feedback = worker.getListAnswers().get(value);
            PossibleAnswer answer = feedback.getAnswer();
            PossibleAnswer correctAnswer = feedback.getQuestion()
                    .getCorrectAnswer();
            switch (answer) {
                case Yes:
                    if (correctAnswer == PossibleAnswer.Yes) {
                        figures[0]++;
                    } else if (correctAnswer == PossibleAnswer.No) {
                        figures[3]++;
                    }

                    break;
                case No:
                    if (correctAnswer == PossibleAnswer.Yes) {
                        figures[1]++;
                    } else if (correctAnswer == PossibleAnswer.No) {
                        figures[2]++;
                    }
                    break;
                default:
                    break;
            }
        }

        return figures;
    }

    /**
     * This method removes any workers that didn't answer any question
     */
    private void filterWorkers() {
        workersMap.clear();
		/*
		 * for (Worker worker : Workers) { if (worker.getListAnswers().size() ==
		 * 0) { Workers.remove(worker); } else { workersMap.put(worker.getWID(),
		 * worker); }
		 * 
		 * }
		 */

        for (Iterator<Worker> itr = Workers.iterator(); itr.hasNext();) {
            Worker worker = itr.next();
            if (worker.getListAnswers().size() == 0) {
                itr.remove();
            } else {
                workersMap.put(worker.getWID(), worker);
            }
        }

    }

    /**
     * This method removes any question that nobody answered.
     */
    private void filterQuestions() {
        questionsMap.clear();
        // System.out.println(Questions.size());
		/*
		 * for (Question question : Questions) { if
		 * (question.getListFeedbacks().size() == 0) {
		 * Questions.remove(question); } else {
		 * questionsMap.put(question.getCorrespondence().getValue(), question);
		 * }
		 * 
		 * }
		 */

        for (Iterator<Question> itr = Questions.iterator(); itr.hasNext();) {
            Question question = itr.next();
            if (question.getListFeedbacks().size() == 0) {
                itr.remove();
            } else {
                questionsMap.put(question.getCorrespondence().getValue(),
                        question);
            }
        }

    }

    public List<Worker> getListWorkers() {
        return Workers;
    }

    public List<Question> getListQuestions() {
        return Questions;
    }

    public List<FeedBack> getListFeedBacks() {
        return FeedBacks;
    }

    public HashMap<String, Question> getQuestionsMap() {
        return questionsMap;
    }

    public HashMap<Integer, Worker> getWorkersMap() {
        return workersMap;
    }
}
