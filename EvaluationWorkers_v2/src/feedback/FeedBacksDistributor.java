package feedback;

import java.util.List;


import question.Question;
import user.Worker;

public abstract class FeedBacksDistributor {

	/**
	 * This method distributes the reliability of given workers
	 * 
	 * @param w
	 *            the input workers to be distributed
	 * @param q
	 *            the input questions to be distributed
	 */
	public abstract void distribute(List<FeedBack> feedbacks, List<Worker> workers,
			List<Question> questions);
}
