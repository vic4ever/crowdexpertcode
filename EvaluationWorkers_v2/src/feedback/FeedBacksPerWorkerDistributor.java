package feedback;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;


import question.Question;
import user.Worker;
import config.Mainconfig;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * this class generates feedbacks with only one constrains: number of answer per worker
 */
public class FeedBacksPerWorkerDistributor extends FeedBacksDistributor {
	private double ratio = 0.8;
	private int feedbacksPerWorker = 0;

	public FeedBacksPerWorkerDistributor() {
		ratio = Double.parseDouble(Mainconfig.getInstance().getListConfig()
				.get("feedbackRatio"));
		feedbacksPerWorker = Integer.parseInt(Mainconfig.getInstance()
				.getListConfig().get("feedbacksPerWorker"));
	}

	@Override
	public void distribute(List<FeedBack> feedbacks, List<Worker> workers,
			List<Question> questions) {
		if (setFeedBacksperWorkerContrains()) {
			ratio = (double) feedbacksPerWorker / questions.size();
			if (ratio > 1)
				ratio = 1;
		}
		for (Worker worker : workers) {
			for (Question question : questions) {
				Random luck = Mainconfig.getInstance().getRand();
				if (luck.nextDouble() < ratio) {
					FeedBack feedback = worker.generateAnswer(question);
					feedbacks.add(feedback);
				}

			}
		}

		if (setFeedBacksperWorkerContrains()) {
			// force answer to make sure all workers answer above the given
			// threshold
			for (Worker worker : workers) {
				Collections.shuffle(questions);
				int rest = feedbacksPerWorker - worker.getListAnswers().size();
				if (rest <= 0) rest = 0;
				for (Question question : questions){
					if (rest == 0) break;
					String value = question.getCorrespondence().getValue();
					if (!checkExistedAnswer(value, worker.getListAnswers())){
						FeedBack feedback = worker.generateAnswer(question);
						feedbacks.add(feedback);
						rest--;
					}
				}
			}
		}

	}
	
	private boolean checkExistedAnswer(String value, Map<String, FeedBack> listAnswers){
		for(String content : listAnswers.keySet()){
			if (content.equals(value)) return true;
		}
		return false;
	}

	private boolean setFeedBacksperWorkerContrains() {
		if (feedbacksPerWorker == 0)
			return false;
		else
			return true;
	}

}
