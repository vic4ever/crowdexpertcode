package feedback;

import java.util.ArrayList;
import java.util.List;


import question.Question;
import user.Worker;

public class GenerateFeedBacks {
	private List<Worker> workers;
	private List<Question> questions;
	private List<FeedBack> feedbacks;

	public GenerateFeedBacks(List<Worker> workers, List<Question> questions) {
		feedbacks = new ArrayList<FeedBack>();
		this.workers = workers;
		this.questions = questions;
	}

	public List<FeedBack> getFeedBacks() {
		return this.feedbacks;
	}

	public void generateFeedBacks(FeedBacksDistributor distributor) {
		distributor.distribute(feedbacks, workers, questions);
	}

}
