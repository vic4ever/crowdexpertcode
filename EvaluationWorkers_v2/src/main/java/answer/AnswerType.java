package main.java.answer;

import java.util.List;

import main.java.question.PossibleAnswer;

public abstract class AnswerType {
	public abstract List<PossibleAnswer> getPossibleAnswers();
}
