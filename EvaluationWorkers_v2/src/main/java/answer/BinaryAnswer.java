package main.java.answer;

import java.util.ArrayList;
import java.util.List;

import main.java.question.PossibleAnswer;

/**
 * @author Lamtran3101 
 * <h4>Description</h4>
 * Contain Yes/No answer. Unknown will be only use for initialization.
 */
public class BinaryAnswer extends AnswerType {

	public BinaryAnswer() {
	}

	@Override
	public List<PossibleAnswer> getPossibleAnswers() {
		List<PossibleAnswer> list = new ArrayList<>();
		list.add(PossibleAnswer.Yes);
		list.add(PossibleAnswer.No);
		list.add(PossibleAnswer.Unknown);
		return list;
	}

}
