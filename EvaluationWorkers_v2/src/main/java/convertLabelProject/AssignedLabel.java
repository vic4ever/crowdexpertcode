package main.java.convertLabelProject;

public class AssignedLabel {

	private String workerName;
	private String objectName;
	private String categoryName;

	public AssignedLabel(String w, String d, String c) {
		this.workerName = w;
		this.objectName = d;
		this.categoryName = c;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((objectName == null) ? 0 : objectName.hashCode());
		result = prime * result
				+ ((workerName == null) ? 0 : workerName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AssignedLabel))
			return false;
		AssignedLabel other = (AssignedLabel) obj;
		if (objectName == null) {
			if (other.objectName != null)
				return false;
		} else if (!objectName.equals(other.objectName))
			return false;
		if (workerName == null) {
			if (other.workerName != null)
				return false;
		} else if (!workerName.equals(other.workerName))
			return false;
		return true;
	}

	public String getWorkerName() {

		return workerName;
	}

	public String getObjectName() {

		return objectName;
	}

	public String getCategoryName() {

		return categoryName;
	}

}
