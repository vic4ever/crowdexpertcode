package main.java.evaluateWorkers;

import java.util.List;

import main.java.config.Mainconfig;

import main.java.question.Question;
import main.java.user.Worker;
import main.java.aggregateResult.EMbase;
import main.java.feedback.FeedBackModel;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * This algorithm based on paper "Quality Management on Amazon Mechanical Turk"
 */
public class EM extends EvaluateWorker {

	/*
	 * private String category; private String input; private String
	 * correctFile; private String costfile;
	 */

	public EM(FeedBackModel model) {
		super(model);
	}

	@Override
	public void execute() {
		EMbase result = new EMbase(model.getListFeedBacks(),
				model.getListWorkers(), model.getListQuestions());
		int iter = Integer.parseInt(Mainconfig.getInstance().getListConfig()
				.get("Em_iter"));
		result.execute(iter);
		this.setCompetiontime(result.getCompetiontime());

		/*
		 * updateWorkersQuality(); updateData();
		 */
	}

	/*
	 * private void updateData(){ TxtReader reader =new TxtReader();
	 * reader.readfile("object-probabilities"); String[] content =
	 * reader.getContent().split("\n"); int size = content.length; int start =
	 * 1; while (start < size ){ String[] data = content[start].split("\t");
	 * String category = data[3]; String value = data[0]; for(Question question
	 * : model.getListQuestions()){
	 * if(question.getCorrespondence().getValue().equals(value)){
	 * question.getCorrespondence
	 * ().setEstResult(PossibleCategories.valueOf(value)); } } } }
	 * 
	 * private void updateWorkersQuality(){ TxtReader reader =new TxtReader();
	 * reader.readfile("worker-statistics-detailed"); String[] content =
	 * reader.getContent().split("\n"); int size = content.length; int start =
	 * 0; while (start < size){ String ID = content[start].split(" ")[1];
	 * 
	 * String errorRate = content[start + 1].split(" ")[3].split("%")[0]; double
	 * r = 0; if (!errorRate.equals("---")){ r = 1 -
	 * Double.parseDouble(errorRate)/100; }
	 * 
	 * //System.out.println("Worker: " + ID + " Error: " + errorRate + " r= " +
	 * r ); start += 16; user.Worker worker = setWorkers.get(ID);
	 * System.out.println(ID); System.out.println(errorRate); user.Worker worker
	 * = null; for(user.Worker oneworker : model.getListWorkers()){
	 * if(ID.equals(oneworker.getWID()+ "")) worker = oneworker; }
	 * Characteristic user = new TruthFulWorker(); if ( r <
	 * Mainconfig.getInstance().getReliabilityThreshold()){ user = new
	 * Spammer(); }
	 * 
	 * user.setReliableDegree(r); worker.setEstChar(user);
	 * //System.out.println(worker.getChar()); } }
	 */

	@Override
	public List<Worker> getWorkersResult() {
		return model.getListWorkers();
	}

	@Override
	public List<Question> getQuestionsResult() {
		return model.getListQuestions();
	}
}
