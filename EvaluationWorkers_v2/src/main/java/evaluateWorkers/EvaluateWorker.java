package main.java.evaluateWorkers;

import java.util.List;

import main.java.question.Question;
import main.java.user.Worker;
import main.java.feedback.FeedBackModel;

public abstract class EvaluateWorker {

	protected FeedBackModel model;
	protected double competiontime;

	

	public EvaluateWorker(FeedBackModel model) {
		this.model = model;
	}
	
	public void run(){
		
		this.execute();
		
		
		/*Mainconfig.getInstance().getTimer().start();
		this.execute();
		Mainconfig.getInstance().getTimer().stop();
		this.setCompetiontime(Mainconfig.getInstance().getTimer().getElapsedTime());*/
	}

	public abstract void execute();

	public abstract List<Worker> getWorkersResult();

	public abstract List<Question> getQuestionsResult();
	
	public double getCompetiontime() {
		return competiontime;
	}

	public void setCompetiontime(double competiontime) {
		this.competiontime = competiontime;
	}

}
