package main.java.evaluateWorkers;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.java.question.PossibleAnswer;
import main.java.question.Question;
import main.java.tools.io.TxtReader;
import main.java.tools.io.TxtWriter;
import main.java.user.Characteristic;
import main.java.user.TruthFulWorker;
import main.java.user.Worker;
import ch.epfl.lsir.nisb.manager.UtilManager;
import main.java.config.Mainconfig;
import main.java.data.PossibleCategories;
import main.java.feedback.FeedBack;
import main.java.feedback.FeedBackModel;

/**
 * @author Lamtran3101 <h4>Description</h4> This algorithm based on paper
 *         "Whose vote should count more: Optimal integration of labels from labelers of unknown expertise"
 */
public class GLAD extends EvaluateWorker {

	private double threshhold = 0.5;

	private Map<String, Integer> getQuestionID;
	private Map<Integer, Question> getQuestionbyID;

	public GLAD(FeedBackModel model) {
		super(model);

		String tmp = Mainconfig.getInstance().getListConfig()
				.get("GLAD_threshold");
		if (!tmp.trim().isEmpty()) {
			threshhold = Double.parseDouble(tmp);
		}
	}

	@Override
	public void execute() {
		initialized();
		writeDatatoFile("data");
		exportResult();

		double time = getDuration();
		this.setCompetiontime(time);

		estimateLabels();
		estimateWorkers();
		/* System.exit(0); */
	}

	private double getDuration() {
		double time = 0;
		TxtReader reader = new TxtReader();
		reader.setCurrentPath("");
		reader.readfile("time");
		String[] lines = reader.getContent().split("\n");
		for (String line : lines) {
			String[] content = line.split("\t");
			time = Double.parseDouble(content[0]);
		}
		return time;
	}

	private void estimateLabels() {
		TxtReader reader = new TxtReader();
		reader.setCurrentPath("");
		reader.readfile("question");
		String[] lines = reader.getContent().split("\n");
		for (String line : lines) {
			String[] content = line.split("\t");
			int id = Integer.parseInt(content[0]);
			double value = Double.parseDouble(content[2]);
			// System.out.println(value);
			if (value > threshhold)
				value = 1;
			else
				value = 0;
			Question question = getQuestionbyID.get(id);
			PossibleCategories estResult = estLabel((int) value);
			question.getCorrespondence().setEstResult(estResult);
		}
	}

	private void estimateWorkers() {
		for (Worker worker : model.getListWorkers()) {
			double numCorrectAnswers = 0;
			double totalAnswers = worker.getListAnswers().size();
			for (String value : worker.getListAnswers().keySet()) {
				FeedBack feedback = worker.getListAnswers().get(value);
				PossibleAnswer assignedLabel = feedback.getAnswer();
				PossibleAnswer correctLabeli = feedback.getQuestion()
						.getCorrespondence().getEstResult().getAnswer();
				if (assignedLabel == correctLabeli) {
					numCorrectAnswers++;
				}
			}
			Characteristic c = new TruthFulWorker();
			c.setReliableDegree(numCorrectAnswers / totalAnswers);
			worker.setEstChar(c);
		}
	}

	private void exportResult() {
		try {
			String cmd = "Cexe" + File.separator + "em.exe" + " data.txt";
			UtilManager.consoleUtil().exec(cmd);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Worker> getWorkersResult() {
		return model.getListWorkers();
	}

	@Override
	public List<Question> getQuestionsResult() {
		return model.getListQuestions();
	}

	private void writeDatatoFile(String filename) {
		StringBuffer sb = new StringBuffer();
		String title = getTotalFeedbacks() + "\t" + getTotalWorkers() + "\t"
				+ getTotalQuestion() + "\t" + getPrior() + "\n";
		sb.append(title);
		for (FeedBack feedback : model.getListFeedBacks()) {
			String oneRecord = getQuestionID.get(feedback.getQuestion()
					.getCorrespondence().getValue())
					+ "\t"
					+ feedback.getWorker().getWID()
					+ "\t"
					+ classifyObject(feedback.getAnswer()) + "\n";
			sb.append(oneRecord);
		}
		// System.out.println(sb.toString());
		TxtWriter.getInstance().WriteToFile(sb.toString(), filename);
	}

	private int classifyObject(PossibleAnswer answer) {
		switch (answer) {
		case No:
			return 0;
		case Yes:
			return 1;
		default:
			return 0;
		}
	}

	private PossibleCategories estLabel(int value) {
		switch (value) {
		case 0:
			return PossibleCategories.False;
		case 1:
			return PossibleCategories.True;
		default:
			return PossibleCategories.Unknown;
		}
	}

	private void initialized() {
		initializedQuestions();
	}

	private void initializedQuestions() {
		getQuestionID = new HashMap<String, Integer>();
		getQuestionbyID = new HashMap<Integer, Question>();
		int id = 0;
		for (Question question : model.getListQuestions()) {
			String key = question.getCorrespondence().getValue();
			getQuestionID.put(key, id);
			getQuestionbyID.put(id, question);
			id++;
		}
	}

	private double getPrior() {
		return 0.5;
	}

	private int getTotalQuestion() {
		return model.getListQuestions().size();
	}

	private int getTotalWorkers() {
		return model.getListWorkers().size();
	}

	private int getTotalFeedbacks() {
		int totalFeedBacks = 0;
		for (Worker worker : model.getListWorkers()) {
			totalFeedBacks += worker.getListAnswers().size();
		}
		return totalFeedBacks;
	}

}
