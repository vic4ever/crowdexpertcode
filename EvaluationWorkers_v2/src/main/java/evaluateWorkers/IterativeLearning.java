package main.java.evaluateWorkers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.distribution.NormalDistribution;

import main.java.question.PossibleAnswer;
import main.java.question.Question;
import main.java.user.Characteristic;
import main.java.user.TruthFulWorker;
import main.java.user.Worker;
import main.java.config.Mainconfig;
import main.java.data.PossibleCategories;
import main.java.feedback.FeedBack;
import main.java.feedback.FeedBackModel;

/**
 * @author Lamtran3101 
 * <h4>Description</h4>
 * This algorithm based on paper
 *         "Iterative learning for reliable crowdsourcing systems"
 */
public class IterativeLearning extends EvaluateWorker {

	private double mean = 1;
	private double sd = 1;

	private int iter = 10;
	private Map<Integer, Map<String, Double>> setWorkers2Questions;
	private Map<String, Map<Integer, Double>> setQuestions2Workers;

	public IterativeLearning(FeedBackModel model) {
		super(model);
		initialized();
	}

	private void initialized() {
		iter = Integer.parseInt(Mainconfig.getInstance().getListConfig()
				.get("IterativeLearning_iter"));
		setWorkers2Questions = new HashMap<Integer, Map<String, Double>>();
		setQuestions2Workers = new HashMap<String, Map<Integer, Double>>();
		distributeReliability();
		initQuestions2Workers();
	}

	private void distributeReliability() {
		NormalDistribution gen = new NormalDistribution(mean, sd);
		for (Worker worker : model.getListWorkers()) {
			int id = worker.getWID();
			Map<String, Double> questions = new HashMap<String, Double>();
			for (String value : worker.getListAnswers().keySet()) {
				double prob = gen.sample();
				/*
				 * if (prob > 1) { prob = 1; } else if (prob < 0) { prob = 0; }
				 */
				questions.put(value, prob);
			}
			setWorkers2Questions.put(id, questions);
		}
	}

	private void initQuestions2Workers() {
		for (Question question : model.getListQuestions()) {
			String value = question.getCorrespondence().getValue();
			Map<Integer, Double> workers = new HashMap<Integer, Double>();
			for (Integer id : question.getListFeedbacks().keySet()) {
				workers.put(id, 0.0);
			}
			setQuestions2Workers.put(value, workers);
		}
	}

	private double getAnswervalue(FeedBack feedback) {
		switch (feedback.getAnswer()) {
		case Yes:
			return 1;
		case No:
			return -1;
		default:
			return 0;
		}
	}

	@Override
	public void execute() {
		Mainconfig.getInstance().getTimer().start();
		for (int i = 0; i < iter; i++) {
			System.out.println(i);
			System.out.println("computeX");
			computeX();
			System.out.println("computeY");
			computeY();
			System.out.println("Done: " + i);
		}
		computeHardLabels();
		Mainconfig.getInstance().getTimer().stop();
		this.setCompetiontime(Mainconfig.getInstance().getTimer()
				.getElapsedTime());

		estWorkersQuality();
	}

	private void computeX() {
		for (Question question : model.getListQuestions()) {
			String value = question.getCorrespondence().getValue();
			Map<Integer, Double> workers = setQuestions2Workers.get(value);
			for (Integer id : workers.keySet()) {
				double result = 0;
				for (Integer otherId : question.getListFeedbacks().keySet()) {
					if (otherId != id) {
						FeedBack feedback = question.getListFeedbacks().get(
								otherId);
						double answer = getAnswervalue(feedback);
						double reliability = setWorkers2Questions.get(otherId)
								.get(value);
						result += (answer * reliability);
					}
				}
				workers.put(id, result);
			}
			setQuestions2Workers.put(value, workers);
		}
	}

	private void computeY() {
		for (Worker worker : model.getListWorkers()) {
			int id = worker.getWID();
			Map<String, Double> questions = setWorkers2Questions.get(id);
			for (String value : questions.keySet()) {
				double result = 0;
				for (String otherValue : worker.getListAnswers().keySet()) {
					if (!otherValue.equals(value)) {
						FeedBack feedback = worker.getListAnswers().get(
								otherValue);
						double answer = getAnswervalue(feedback);
						double reliability = setQuestions2Workers.get(
								otherValue).get(id);
						result += (answer * reliability);
					}
				}
				questions.put(value, result);
			}
			setWorkers2Questions.put(id, questions);
		}
	}

	private void computeHardLabels() {
		for (Question question : model.getListQuestions()) {
			double result = 0;
			String value = question.getCorrespondence().getValue();
			question.getCorrespondence().setEstResult(PossibleCategories.True);
			for (Integer id : question.getListFeedbacks().keySet()) {
				FeedBack feedback = question.getListFeedbacks().get(id);
				double answer = getAnswervalue(feedback);
				double reliability = setWorkers2Questions.get(id).get(value);
				result += (answer * reliability);
			}
			if (result < 0)
				question.getCorrespondence().setEstResult(
						PossibleCategories.False);
		}
	}

	private void estWorkersQuality() {
		for (Worker worker : model.getListWorkers()) {
			double numCorrectAnswers = 0;
			double totalAnswers = worker.getListAnswers().size();
			for (String value : worker.getListAnswers().keySet()) {
				FeedBack feedback = worker.getListAnswers().get(value);
				PossibleAnswer assignedLabel = feedback.getAnswer();
				PossibleAnswer correctLabeli = feedback.getQuestion()
						.getCorrespondence().getEstResult().getAnswer();
				if (assignedLabel == correctLabeli) {
					numCorrectAnswers++;
				}
			}
			Characteristic c = new TruthFulWorker();
			c.setReliableDegree(numCorrectAnswers / totalAnswers);
			worker.setEstChar(c);
		}
	}

	/*
	 * private PossibleCategories convertAnswer(PossibleAnswer answer) { switch
	 * (answer) { case Yes: return PossibleCategories.True; case No: return
	 * PossibleCategories.False; default: return PossibleCategories.Unknown; } }
	 */

	@Override
	public List<Worker> getWorkersResult() {
		return model.getListWorkers();
	}

	@Override
	public List<Question> getQuestionsResult() {
		return model.getListQuestions();
	}

}
