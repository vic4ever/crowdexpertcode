package main.java.evaluateWorkers;

import java.util.List;

import main.java.question.Question;
import main.java.user.Worker;
import main.java.aggregateResult.MajorityVoting;
import main.java.feedback.FeedBackModel;

/**
 * @author Lamtran3101 
 * <h4>Description</h4> 
 * This algorithm based on paper
 *         "Cost-Optimal Validation Mechanisms and Cheat-Detection for Crowdsourcing Platforms"
 */
public class MajorityDecision extends EvaluateWorker {

	/* private String[] categories; */
	/*
	 * private DawidSkene ds; private Set<Category> categories; private
	 * Set<AssignedLabel> labels; private Set<CorrectLabel> correct; private
	 * Set<CorrectLabel> evaluation;
	 */

	public MajorityDecision(FeedBackModel model) {
		super(model);
	}

	@Override
	public void execute() {
		MajorityVoting result = new MajorityVoting(model.getListFeedBacks(),
				model.getListWorkers(), model.getListQuestions());
		result.execute();
		this.setCompetiontime(result.getTime());
	}

	@Override
	public List<Worker> getWorkersResult() {
		return model.getListWorkers();
	}

	@Override
	public List<Question> getQuestionsResult() {
		return model.getListQuestions();
	}

}
