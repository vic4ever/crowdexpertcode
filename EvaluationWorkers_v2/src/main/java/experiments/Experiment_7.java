package main.java.experiments;

import java.util.LinkedHashMap;
import java.util.Map;

import question.Question;
import user.Worker;

import config.Mainconfig;

public class Experiment_7 extends Experiments {

	public Experiment_7() {
		super();
		filename = "multiC/8labels_randomSpammers.csv";
		currentPath = "experiments/";
		fileConfig = "experiment_7.ini";
		step = 1;
		index = 0;
	}

	@Override
	void increaseStep() {
		if ((index % 26) == 0) {
			index = 0;
			Long y = Mainconfig.getInstance().getRand().nextLong();
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			Mainconfig.getInstance().getListConfig().put("total", "27");
		}
		else{
			Mainconfig.getInstance().getListConfig().put("total",  Mainconfig.getInstance().getTotalWorkers() + step + "");
		}
		double total = Mainconfig.getInstance().getTotalWorkers();
		Mainconfig.getInstance().getListConfig().put("total", total + "");
		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("index",index );
		data.put("Answers Per Worker", computeAnswersPerWorker());
		data.put("Feedbacks Per Qestion", computeAnswersPerQuestion());
		return data;
	}

}
