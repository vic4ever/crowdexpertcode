package main.java.experiments;

import java.util.LinkedHashMap;
import java.util.Map;

import question.Question;

import user.Worker;

import config.Mainconfig;

/**
 * @author lamtran3101
 * in this experiment, with n =100, we will increase bad labelers to see their effect in the accuracy
 *.
 */
public class Experiment_ELICE extends Experiments {

	private int start = 0;
	public Experiment_ELICE() {
		step = 1;
		filename = "elice/experiment_elice.csv";
		currentPath = "experiments/elice/";
		fileConfig = "experiment_elice.xml";
	}

	@Override
	void increaseStep() {
		double total = 0;
		if(start == 0){
			total = (Double.parseDouble(Mainconfig.getInstance()
					.getListConfig().get("total")));
		}
		else if ((start % 20) == 0){
			Mainconfig.getInstance().getListConfig().put("total", "10");
			total = (Double.parseDouble(Mainconfig.getInstance()
					.getListConfig().get("total")));
		}
		else{
			total = (Double.parseDouble(Mainconfig.getInstance()
					.getListConfig().get("total")) + step);
		}
		Mainconfig.getInstance().getListConfig().put("total", total + "");
		start++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("ratioSubset", Mainconfig.getInstance()
				.getListConfig().get("ratioSubset"));
		data.put("Feedbacks Per Qestion", computeAnswersPerQuestion());
		data.put("Answers per Worker", computeAnswersPerWorker());
		
		return data;

	}
	
	private double getRatioExperts(){
		double num = 0;
		for(Worker worker : workers){
			if (worker.getReliability() > 0.7) num++;
		}
		return num/workers.size();
	}
	

}
