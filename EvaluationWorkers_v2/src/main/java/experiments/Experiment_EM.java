package main.java.experiments;

import java.util.LinkedHashMap;
import java.util.Map;

import config.Mainconfig;

public class Experiment_EM extends Experiments {
	public Experiment_EM() {
		super();
		filename = "experiment_em.csv";
		currentPath = "experiments/";
		fileConfig = "experiment_em.xml";
		step = 1;
		index = 0;
	}

	@Override
	void increaseStep() {
			Long y = Mainconfig.getInstance().getRand().nextLong();
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			if (index % 41 == 0) index= 0;
			index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("index",index );
		data.put("Answers Per Worker", computeAnswersPerWorker());
		data.put("Feedbacks Per Qestion", computeAnswersPerQuestion());
		return data;
	}
}
