package main.java.experiments;

import java.util.LinkedHashMap;
import java.util.Map;

import question.Question;

import user.Worker;

import config.Mainconfig;

/**
 * @author lamtran3101
 * in this experiment, with n =100, we will increase bad labelers to see their effect in the accuracy
 *.
 */
public class Experiment_GLAD extends Experiments {

	public Experiment_GLAD() {
		step = 10;
		filename = "glad/experiment_glad.csv";
		currentPath = "experiments/glad/";
		fileConfig = "experiment_glad.xml";
		index = 0;
	}

	@Override
	void increaseStep() {
		if ((index % 20) == 0) {
			index = 0;
			Long y = Mainconfig.getInstance().getRand().nextLong();
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			Mainconfig.getInstance().getListConfig().put("total", "30");
		}
		else{
			Mainconfig.getInstance().getListConfig().put("total",  Mainconfig.getInstance().getTotalWorkers() + step + "");
		}
		double total = Mainconfig.getInstance().getTotalWorkers();
		Mainconfig.getInstance().getListConfig().put("total", total + "");
		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("Seed", Mainconfig.getInstance().getListConfig().get("seed"));
		data.put("Answers Per Worker", computeAnswersPerWorker());
		data.put("Feedbacks Per Qestion", computeAnswersPerQuestion());
		return data;
	}

}
