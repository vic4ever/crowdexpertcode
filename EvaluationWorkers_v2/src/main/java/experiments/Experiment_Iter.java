package main.java.experiments;

import java.util.LinkedHashMap;
import java.util.Map;

import user.Worker;

import config.Mainconfig;

/**
 * @author lamtran3101
 * in this experiment, with n =100, we will increase bad labelers to see their effect in the accuracy
 *.
 */
public class Experiment_Iter extends Experiments {

	public Experiment_Iter() {
		super();
		filename = "Iter/experiment_Iter.csv";
		currentPath = "experiments/Iter/";
		fileConfig = "experiment_Iter.ini";
		step = 5;
		index = 0;
	}

	@Override
	void increaseStep() {
		
		if ((index % 10) == 0) {
			index = 0;
			Long y = Mainconfig.getInstance().getRand().nextLong();
			System.out.println(y);
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			Mainconfig.getInstance().getListConfig().put("IterativeLearning_iter", "5");
		}
		else{
			Mainconfig.getInstance().getListConfig().put("IterativeLearning_iter",  Integer.parseInt(Mainconfig.getInstance().getListConfig().get("IterativeLearning_iter")) + (int) step + "");
		}
		int total = Integer.parseInt(Mainconfig.getInstance().getListConfig().get("IterativeLearning_iter"));
		System.out.println("---------" + total);
		Mainconfig.getInstance().getListConfig().put("IterativeLearning_iter", total + "");
		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("Seed", Mainconfig.getInstance().getListConfig().get("seed"));
		String key = "Feedbacks Per Qestion";
		double value = computeAnswersPerQuestion();
		data.put(key, value);

	/*	data.put("ratio", value / workers.size());*/
		data.put("Answers per Worker", computeAnswersPerWorker());
		data.put("index", index);
		data.put("Iter", Mainconfig.getInstance().getListConfig().get("IterativeLearning_iter"));
		return data;
	}

}
