package main.java.experiments;

import java.util.LinkedHashMap;
import java.util.Map;

import config.Mainconfig;

public class Experiment_SLME extends Experiments{

	public Experiment_SLME() {
		step = 1;
		filename = "slme/experiment_slme.csv";
		currentPath = "experiments/slme/";
		fileConfig = "experiment_slme.xml";
		index = 0;
	}
	
	@Override
	void increaseStep() {
		
		if ((index % 20) == 0) {
			index = 0;
			Long y = Mainconfig.getInstance().getRand().nextLong();
			System.out.println(y);
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			Mainconfig.getInstance().getListConfig().put("feedbacksPerQuestion", "1");
		}
		else{
			Mainconfig.getInstance().getListConfig().put("feedbacksPerQuestion",  Integer.parseInt(Mainconfig.getInstance().getListConfig().get("feedbacksPerQuestion")) + (int) step + "");
		}
		int total = Integer.parseInt(Mainconfig.getInstance().getListConfig().get("feedbacksPerQuestion"));
		System.out.println("---------" + total);
		Mainconfig.getInstance().getListConfig().put("feedbacksPerQuestion", total + "");
		index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("Seed", Mainconfig.getInstance().getListConfig().get("seed"));
		String key = "Feedbacks Per Qestion";
		double value = computeAnswersPerQuestion();
		data.put(key, value);

		data.put("ratio", value / workers.size());
		data.put("Answers per Worker", computeAnswersPerWorker());
		data.put("index", index);
		return data;
	}

}
