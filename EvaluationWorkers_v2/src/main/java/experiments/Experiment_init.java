package main.java.experiments;

import java.util.LinkedHashMap;
import java.util.Map;

import config.Mainconfig;

public class Experiment_init extends Experiments {
	public Experiment_init() {
		super();
		filename = "experiment_init.csv";
		currentPath = "experiments/";
		fileConfig = "experiment_init.ini";
		step = 1;
		index = 0;
	}

	@Override
	void increaseStep() {
			Long y = Mainconfig.getInstance().getRand().nextLong();
			Mainconfig.getInstance().getListConfig().put("seed", y + "");
			if (index % 41 == 0) index= 0;
			index++;
	}

	@Override
	Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("index",index );
		data.put("Answers Per Worker", computeAnswersPerWorker());
		data.put("Feedbacks Per Qestion", computeAnswersPerQuestion());
		return data;
	}
}
