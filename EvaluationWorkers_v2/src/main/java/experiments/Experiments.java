package main.java.experiments;

import java.util.List;
import java.util.Map;

import question.Question;
import tools.io.ConfigReader;
import tools.io.MyCsvWriter;
import user.Worker;
import config.Mainconfig;
import config.Mainconfig.ListDatasets;
import evaluateWorkers.EvaluateWorker;
import evaluateWorkers.Output;
import factorypattern.AlgorithmFactory;
import feedback.FeedBackModel;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * The abstract file that other experiment will inherit.
 * {@link #run() Here} is the flowchart of one experiement
 */
public abstract class Experiments {
	protected List<Worker> workers;
	protected List<Question> questions;
	protected ConfigReader reader = new ConfigReader();
	protected Map<String, String> listConfig;
	protected double step = 0;
	protected double index =0;
	protected String filename = null;
	protected String currentPath = null;
	protected String fileConfig = null;

	public Experiments() {
		index = 0;
		/*workers = new ArrayList<>();
		questions = new ArrayList<>();*/
	}

	/**
	 * For each experiment, this flowchart will be applied. <br/>
	 * After loading the general configuration, it will load a particular {@link #customizeConfig() custom configured}  file for each experiment.<br/>
	 * At each step, it will {@link #reinitialize() reinitialize}  some parameters before given the data to one algorithm <br/>
	 * For each iteration, first it will load the holder that contains data sets. <br/>
	 * With a specific data set, it will give to all algorithms.
	 */
	public void run() {
		customizeConfig();
		for (String key : Mainconfig.getInstance().getListConfig().keySet()) {
			System.out.println(key + ": " +  Mainconfig.getInstance().getListConfig().get(key));
		}
		//System.out.println(Mainconfig.getInstance().getListConfig().get("Iterator "));
		int iter = Integer.parseInt(Mainconfig.getInstance().getListConfig().get("Iterator"));
		AlgorithmFactory algo = new AlgorithmFactory();
		for (int i = 0; i < iter; i++) {
			reinitialize();
			System.out.println("-------------------" + iter + ", " + i);
			for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
				Mainconfig.getInstance().setData(data);
				FeedBackModel model = new FeedBackModel();
				EvaluateWorker eval = null;
				String[] algorithms = Mainconfig.getInstance().getAlgorithms();
				for (String algorithm : algorithms) {
					model.refreshData();
					System.out.println("totalF: " + model.getListFeedBacks().size());
					eval = algo.createAlgorithm(algorithm, model);
					runOneAlgorithm(eval);
				}
			}
		}
	}

	public void workersExport() {
		String[] parseName = this.getClass().getName().split("[.]");
		String experiment = parseName[parseName.length - 1];
		/* String EstResult = "EstResult" + experiment + "_" + dataset; */
		customizeConfig();
		int iter = Integer.parseInt(Mainconfig.getInstance().getListConfig()
				.get("Iterator"));
		//AlgorithmFactory algo = new AlgorithmFactory();
		for (int i = 0; i < iter; i++) {
			reinitialize();
			for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
				Mainconfig.getInstance().setData(data);
				FeedBackModel model = new FeedBackModel();
				setQuestions(model.getListQuestions());
				setWorkers(model.getListWorkers());
				String dataset = data.name();
				String Expertise = "Expertise_" + experiment + "_" + dataset + ".csv";
				MyCsvWriter.getInstance().WriteToFile(generateData(), Expertise);
			}
		}

	}
	
	private String generateData(){
		StringBuffer sb = new StringBuffer();
		sb.append("Worker \t Expertise \n");
		for(Worker worker : workers){
			String oneRecord = worker.getWID() + "\t" + worker.getReliability() + "\n";
			sb.append(oneRecord);
		}
		return sb.toString();
	}

	/**
	 * This function changes the value of some specific parameters in {@link Experiments#increaseStep()}
	 */
	private void reinitialize() {
		Mainconfig.getInstance().initialized();
		increaseStep();
		Mainconfig.getInstance().initialized();
	}

	abstract void increaseStep();

	abstract Map<String, Object> getExtendedResult();

	private void runOneAlgorithm(EvaluateWorker runner) {
		runner.run();
		Output output = new Output(runner.getClass().getName(), Mainconfig
				.getInstance().getDataset().toString());
		output.setWorkers(runner.getWorkersResult());
		output.setQuestions(runner.getQuestionsResult());
		output.setCompletionTime(runner.getCompetiontime());
		setQuestions(runner.getQuestionsResult());
		setWorkers(runner.getWorkersResult());
		output.exportResult(filename, getExtendedResult());
		//output.exportWorkersDetail("Worker.csv");
	}

	
	/**
	 * After read the general setting, it will load a particular setting and override all parameters which are specify in another file setting
	 */
	private void customizeConfig() {
		reader.setCurrentPth(currentPath);
		reader.readfile(fileConfig);
		listConfig = reader.getConfig();
		for (String key : listConfig.keySet()) {
			Mainconfig.getInstance().getListConfig()
					.put(key, listConfig.get(key));
		}
	}
	
	protected double computeAnswersPerWorker() {
		double sum = 0;
		for (Worker worker : workers) {
			sum += worker.getListAnswers().size();
		}
		return sum / workers.size();
	}

	protected double computeAnswersPerQuestion() {
		double sum = 0;
		for (Question question : questions) {
			sum += question.getListFeedbacks().size();
		}
		return sum / questions.size();
	}

	public List<Worker> getWorkers() {
		return workers;
	}

	public void setWorkers(List<Worker> workers) {
		this.workers = workers;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
}
