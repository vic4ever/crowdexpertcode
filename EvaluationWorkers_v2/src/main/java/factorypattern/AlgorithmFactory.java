package main.java.factorypattern;

import main.java.evaluateWorkers.ELICE;
import main.java.evaluateWorkers.EM;
import main.java.evaluateWorkers.EvaluateWorker;
import main.java.evaluateWorkers.GLAD;
import main.java.evaluateWorkers.HoneyPot;
import main.java.evaluateWorkers.IterativeLearning;
import main.java.evaluateWorkers.Main;
import main.java.evaluateWorkers.MajorityDecision;
import main.java.evaluateWorkers.SLME;
import main.java.feedback.FeedBackModel;

/**
 * @author Lamtran3101 <h4>Description</h4> This class has a responsibility for
 *         creating correct algorithm.
 */
public class AlgorithmFactory extends Main {

	private static AlgorithmFactory instance;

	public static AlgorithmFactory getInstance() {
		if (instance == null) {
			instance = new AlgorithmFactory();
		}
		return instance;
	}

	public AlgorithmFactory() {

	}

	@Override
	public EvaluateWorker createAlgorithm(String type, FeedBackModel model) {
		EvaluateWorker eval = null;
		/* System.out.println(type); */
		switch (type) {
		case ("EM"):
			eval = new EM(model);
			break;
		case ("HoneyPot"):
			eval = new HoneyPot(model);
			break;
		case ("ELICE"):
			eval = new ELICE(model);
			break;
		case ("SLME"):
			eval = new SLME(model);
			break;
		case ("IterativeLearning"):
			eval = new IterativeLearning(model);
			break;
		case ("GLAD"):
			eval = new GLAD(model);
			break;
		default:
			eval = new MajorityDecision(model);
			break;
		}
		return eval;
	}

}
