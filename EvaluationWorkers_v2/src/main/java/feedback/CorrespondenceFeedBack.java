package main.java.feedback;

/**
 * @author Lamtran3101 <h4>Description</h4> This class implements the feedback
 *         wrapping the correspondence.
 */
public class CorrespondenceFeedBack extends FeedBack {
	public CorrespondenceFeedBack() {

	}

	@Override
	public CorrespondenceFeedBack clone() throws CloneNotSupportedException {
		CorrespondenceFeedBack clone = new CorrespondenceFeedBack();
		clone.setAnswer(this.getAnswer());
		clone.setQuestion(this.getQuestion());
		clone.setWorker(this.getWorker());
		return clone;
	}
}
