package main.java.feedback;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import main.java.question.Question;
import main.java.user.Worker;
import main.java.config.Mainconfig;

/**
 * @author Lamtran3101 
 * <h4>Description</h4>
 *  There are several constrains when we generate feedbacsk
 *         such as: minimum answers per workers, minimum feedbacks per question,
 *         etc. <br/> Thus, we use this class to ensure our generated feedbacks
 *         satisfies all given constrains.
 */
public class FeedBacksConstraintDistributor extends FeedBacksDistributor {

	private boolean WorkerConstraint = false;
	private boolean QuestionConstraint = false;
	private boolean HoneyPotConstraint = false;
	private int NumOfAnswersPerWorker = 0;
	private int NumOfFeedbacksPerQuestion = 0;
	private double eliceRatio = 0.4;
	private boolean checkEliceRatio = false;
	private double ratio = 0.8;

	public FeedBacksConstraintDistributor() {
		WorkerConstraint = checkWorkerConstraint();
		QuestionConstraint = checkQuestionConstraint();
		ratio = Double.parseDouble(Mainconfig.getInstance().getListConfig()
				.get("feedbackRatio"));
		eliceRatio = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("ratioSubset"));
		checkEliceRatio = checkEliceConstraint();
		HoneyPotConstraint = checkHPConstraint();
		
	}

	@Override
	public void distribute(List<FeedBack> feedbacks, List<Worker> workers,
			List<Question> questions) {

		/*
		 * for(Worker worker: workers){ System.out.println("ID: " +
		 * worker.getWID() + " " + worker.getReliability() + " " +
		 * worker.getSensitivity() + " " + worker.getSpecificity()); }
		 */

		if (checkEliceRatio) {
			double size = eliceRatio;
			for (Worker worker : workers) {
				int start = 0;
				while (start < size) {
					Question question = questions.get(start);
					FeedBack feedback = worker.generateAnswer(question);

					feedbacks.add(feedback);
				
					start++;
				}
			}
		}
		
		if (HoneyPotConstraint) {
			for (Worker worker : workers) {
				for(Question question :questions){
					int mode = question.getCorrespondence().getMode();
					if (mode == 1){
						if (!checkExistedAnswer(question.getCorrespondence()
								.getValue(), worker.getListAnswers())) {
							FeedBack feedback = worker.generateAnswer(question);
							feedbacks.add(feedback);
						}
					}
				}
			}
		}
		
		
		/*
		 * for (Worker worker : workers) { System.out.println("Id: " +
		 * worker.getWID() + ", number: " + worker.getListAnswers().size()); }
		 */

		if (WorkerConstraint) {
			for (Worker worker : workers) {
				Collections.shuffle(questions);
				int index = 0;
				int rest = NumOfAnswersPerWorker
						- worker.getListAnswers().size();
				while (index < questions.size()) {
					Question question = questions.get(index);
					if (!checkExistedAnswer(question.getCorrespondence()
							.getValue(), worker.getListAnswers())) {
						if (rest <= 0)
							break;
						FeedBack feedback = worker.generateAnswer(question);
						feedbacks.add(feedback);
						rest--;
					}
					index++;
				}
			}
		}

		if (QuestionConstraint) {
			for (Question question : questions) {
				Collections.shuffle(workers);
				int rest = NumOfFeedbacksPerQuestion
						- question.getListFeedbacks().size();
				int index = 0;
				while (index < workers.size()) {
					Worker worker = workers.get(index);
					if (!(checkExistedAnswer(worker.getWID(),
							question.getListFeedbacks()))) {
						if (rest <= 0)
							break;
						FeedBack feedback = worker.generateAnswer(question);
						feedbacks.add(feedback);
						rest--;

					}
					index++;
				}
			}
		}

		if (WorkerConstraint) {
			ratio = (double) NumOfAnswersPerWorker / questions.size();
			if (ratio > 1)
				ratio = 1;
			/* System.out.println(ratio); */
		}
		for (Worker worker : workers) {
			for (Question question : questions) {
				if (Mainconfig.getInstance().getRand().nextDouble() < ratio) {
					if (!checkExistedAnswer(question.getCorrespondence()
							.getValue(), worker.getListAnswers())) {
						FeedBack feedback = worker.generateAnswer(question);
						feedbacks.add(feedback);
					}
				}
			}
		}

	}

	private boolean checkExistedAnswer(String value,
			Map<String, FeedBack> listAnswers) {
		for (String content : listAnswers.keySet()) {
			if (content.equals(value))
				return true;
		}
		return false;
	}

	private boolean checkExistedAnswer(int id,
			Map<Integer, FeedBack> listAnswers) {
		for (Integer content : listAnswers.keySet()) {
			if (content == id)
				return true;
		}
		return false;
	}

	
	/**
	 * ELICE constrains make sure minimum n questions that all workers need to answer.
	 * @return true if we want to apply this constrains
	 */
	private boolean checkEliceConstraint() {
		double check = Integer.parseInt(Mainconfig.getInstance()
				.getListConfig().get("checkRatioSubset"));
		if (check == 1)
			return true;
		return false;
	}
	
	/**
	 * HP constrains make sure all n trapped questions that all workers need to answer.
	 * @return true if we want to apply this constrains
	 */
	private boolean checkHPConstraint() {
		double check = Integer.parseInt(Mainconfig.getInstance()
				.getListConfig().get("checkHPConstraint"));
		if (check == 1)
			return true;
		return false;
	}
	
	
	

	/**
	 * This constrains ensures minimum answers per worker. 
	 * @return true if we want to apply this constrains.
	 */
	private boolean checkWorkerConstraint() {
		NumOfAnswersPerWorker = Integer.parseInt(Mainconfig.getInstance()
				.getListConfig().get("feedbacksPerWorker"));
		if (NumOfAnswersPerWorker > 0)
			return true;
		return false;
	}

	/**
	 * This constrains ensures minimum feedbacks per question. 
	 * @return true if we want to apply this constrains.
	 */
	private boolean checkQuestionConstraint() {

		NumOfFeedbacksPerQuestion = Integer.parseInt(Mainconfig.getInstance()
				.getListConfig().get("feedbacksPerQuestion"));

		if (NumOfFeedbacksPerQuestion > 0)
			return true;
		return false;
	}

}
