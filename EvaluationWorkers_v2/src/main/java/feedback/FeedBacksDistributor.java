package main.java.feedback;

import java.util.List;

import main.java.question.Question;
import main.java.user.Worker;

public abstract class FeedBacksDistributor {

	/**
	 * This method distributes the reliability of given workers
	 * 
	 * @param w
	 *            the input workers to be distributed
	 * @param q
	 *            the input questions to be distributed
	 */
	public abstract void distribute(List<FeedBack> feedbacks, List<Worker> workers,
			List<Question> questions);
}
