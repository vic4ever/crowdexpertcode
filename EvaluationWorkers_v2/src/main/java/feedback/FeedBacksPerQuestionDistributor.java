package main.java.feedback;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import main.java.config.Mainconfig;

import main.java.question.Question;
import main.java.user.Worker;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * This class generate feedbacks with only one constrains : number of feedbacks per question
 */
public class FeedBacksPerQuestionDistributor extends FeedBacksDistributor {
	private double ratio = 0.8;
	private int feedbacksPerQuestion = 0;

	public FeedBacksPerQuestionDistributor() {
		ratio = Double.parseDouble(Mainconfig.getInstance().getListConfig()
				.get("feedbackRatioQuestion"));
		feedbacksPerQuestion = Integer.parseInt(Mainconfig.getInstance()
				.getListConfig().get("feedbacksPerQuestion"));
	}

	@Override
	public void distribute(List<FeedBack> feedbacks, List<Worker> workers,
			List<Question> questions) {
		if (setFeedBacksperQuestionContrains()) {
			ratio = (double) feedbacksPerQuestion / workers.size();
			if (ratio > 1)
				ratio = 1;
		}
		for (Question question : questions) {
			for (Worker worker : workers) {
				Random luck = Mainconfig.getInstance().getRand();
				if (luck.nextDouble() < ratio) {
					FeedBack feedback = worker.generateAnswer(question);
					feedbacks.add(feedback);
				}

			}
		}
		
		if (setFeedBacksperQuestionContrains()) {
			// force answer to make sure all workers answer above the given
			// threshold
			for (Question question : questions) {
				Collections.shuffle(workers);
				int rest = feedbacksPerQuestion - question.getListFeedbacks().size();
				if (rest <= 0) rest = 0;
				for (Worker worker : workers){
					if (rest == 0) break;
					int id = worker.getWID();
					if (!checkExistedAnswer(id, question.getListFeedbacks())){
						FeedBack feedback = worker.generateAnswer(question);
						feedbacks.add(feedback);
						rest--;
					}
				}
			}
		}
		
	}
	
	private boolean checkExistedAnswer(int value, Map<Integer, FeedBack> listAnswers){
		for(Integer content : listAnswers.keySet()){
			if (content == value) return true;
		}
		return false;
	}

	private boolean setFeedBacksperQuestionContrains() {
		if (feedbacksPerQuestion == 0)
			return false;
		else
			return true;
	}

}
