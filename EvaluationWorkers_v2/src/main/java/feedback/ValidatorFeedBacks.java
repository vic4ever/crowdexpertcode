package main.java.feedback;

import java.util.List;

import main.java.question.Question;
import main.java.tools.io.MyCsvWriter;
import main.java.user.Worker;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * This class is used for testing only. 
 * Using this class, we will verify the correctness of generated feedbacks.
 */
public class ValidatorFeedBacks {
	private List<Worker> Workers;
	private List<Question> Questions;
	private List<FeedBack> FeedBacks;

	public ValidatorFeedBacks(List<Worker> workers, List<Question> questions,
			List<FeedBack> feedBacks) {
		super();
		Workers = workers;
		Questions = questions;
		setFeedBacks(feedBacks);
	}

	public void exportWorkersAnswers() {
		MyCsvWriter.getInstance().WriteToFile(generateWorkersData(),
				"workersAnswers.csv");
	}

	public void exportQuestionsAnswers() {
		MyCsvWriter.getInstance().WriteToFile(generateQuestionsData(),
				"questionsAnswers.csv");
	}

	private String generateQuestionsData() {
		StringBuffer data = new StringBuffer();
		data.append("Question \t NumberOfAnswers \t ratio \n");
		for (Question question : Questions) {
			data.append(getOneQuestionAnswers(question));
		}
		return data.toString();
	}

	private String getOneQuestionAnswers(Question question) {
		String value = question.getCorrespondence().getValue();
		int number = question.getListFeedbacks().size();
		double ratio = (double) number / Workers.size();
		return value + "\t" + number + "\t" + ratio + "\n";
	}

	private String generateWorkersData() {
		StringBuffer data = new StringBuffer();
		data.append("Worker \t NumberOfAnswers  \t ratio \n");
		for (Worker worker : Workers) {
			data.append(getOneWorkerAnswers(worker));
		}
		return data.toString();
	}

	private String getOneWorkerAnswers(Worker worker) {
		int id = worker.getWID();
		int number = worker.getListAnswers().size();
		double ratio = (double) number / Questions.size();
		return id + "\t" + number + "\t" + ratio + "\n";
	}

	public List<FeedBack> getFeedBacks() {
		return FeedBacks;
	}

	public void setFeedBacks(List<FeedBack> feedBacks) {
		FeedBacks = feedBacks;
	}

}
