package main.java.question;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * Binary Question only accepts Yes/no answers from workers.
 * However, in the initial state, its ground truth can be assign as unknown
 */
public class BinaryQuestion extends QuestionType {

	public BinaryQuestion() {

	}

	@Override
	public List<PossibleAnswer> getPossibleAnswers() {
		List<PossibleAnswer> res = new ArrayList<>();
		res.add(PossibleAnswer.Yes);
		res.add(PossibleAnswer.No);
		res.add(PossibleAnswer.Unknown);
		return res;
	}

}
