package main.java.question;

import main.java.data.PossibleCategories;

/**
 * @author Lamtran3101 <h4>Description</h4> This class supplies type of answers
 *         that one worker can give to one specific question.s Currently, there
 *         are only possible answers from Binary questions.
 */
public enum PossibleAnswer {
	Yes(PossibleCategories.True), No(PossibleCategories.False), Unknown(
			PossibleCategories.Unknown), A(PossibleCategories.A), B(
			PossibleCategories.B), C(PossibleCategories.C), D(
			PossibleCategories.D), E(PossibleCategories.E), F(
			PossibleCategories.F), G(PossibleCategories.G), H(
			PossibleCategories.H);

	private PossibleCategories category;

	PossibleAnswer(PossibleCategories category) {
		this.category = category;
	}

	public PossibleCategories getCategory() {
		return this.category;
	}
}
