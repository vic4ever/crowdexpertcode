package main.java.question;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.java.utility.IDGenerator;
import main.java.config.Mainconfig;
import main.java.data.Correspondence;
import main.java.feedback.FeedBack;

/**
 * @author Lamtran3101 <h4>Description</h4> Each question includes one
 *         Correspondence, type of question, the difficulty degree and list of
 *         workers who answered this question.
 */
public class Question {
	private int id = 0;
	private Correspondence corr;
	private QuestionType type;
	private Map<Integer, FeedBack> listWorkersAnswers;
	private double difficultDegree;

	public Question() {
		id = IDGenerator.nextQuestionID();
		corr = new Correspondence();
		String inputType = Mainconfig.getInstance().getListConfig()
				.get("InputDataType");
		if (inputType.equals("Binary")) {
			type = new BinaryQuestion();
		} else {
			type = new MultiChoiceQuestion();
		}

		listWorkersAnswers = new HashMap<Integer, FeedBack>();
		difficultDegree = 0;
	}

	public double getDifficultDegree() {
		return difficultDegree;
	}

	public void setDifficultDegree(double difficultDegree) {
		this.difficultDegree = difficultDegree;
	}

	public PossibleAnswer getCorrectAnswer() {
		return this.corr.getGroundTruth().getAnswer();
	}

	public PossibleAnswer getEstimateAnswer() {
		return this.corr.getEstResult().getAnswer();

	}

	public void addWorker(FeedBack feedback) {
		listWorkersAnswers.put(feedback.getWorker().getWID(), feedback);
	}

	public Map<Integer, FeedBack> getListFeedbacks() {
		return this.listWorkersAnswers;
	}

	public Correspondence getCorrespondence() {
		return corr;
	}

	public List<?> getPossibleAnswer() {
		return type.getPossibleAnswers();
	}

	@Override
	public Question clone() throws CloneNotSupportedException {
		Question clone = new Question();
		clone.id = this.id;
		clone.difficultDegree = this.difficultDegree;
		clone.corr = this.corr.clone();
		clone.listWorkersAnswers = new HashMap<Integer, FeedBack>();
		for (Integer key : this.listWorkersAnswers.keySet()) {
			clone.listWorkersAnswers.put(key, this.listWorkersAnswers.get(key)
					.clone());
		}
		return clone;
	}

	public void displayWorkers() {
		String res = "Workers answer this question " + id + ": ";
		for (Integer wid : listWorkersAnswers.keySet()) {
			res += wid + " ";
		}
		System.out.println(res);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Question " + id;
	}
}
