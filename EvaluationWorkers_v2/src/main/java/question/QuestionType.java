package main.java.question;

import java.util.List;

public abstract class QuestionType {

	public abstract List<PossibleAnswer> getPossibleAnswers();
}
