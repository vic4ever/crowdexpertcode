package main.java.transformdata;

import java.util.ArrayList;

import main.java.tools.io.TxtWriter;

public class CategoriesWriter extends TransFormData {

	ArrayList<String> list;

	public CategoriesWriter(ArrayList<String> list) {
		this.list = list;
	}

	@Override
	public String transform() {
		return TxtWriter.getInstance().WriteToFile(getString(), "category");

	}

	private String getString() {
		StringBuffer sb = new StringBuffer();
		for (String s : list) {
			sb.append(s + "\n");
		}
		return sb.toString();
	}

}
