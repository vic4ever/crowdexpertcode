package main.java.transformdata;


/**
 * @author lamtran3101
 * This class is used to wrap our data, make them become the inputs of EM algorithm.
 *
 */
public abstract class TransFormData {
	public abstract String transform();
}
