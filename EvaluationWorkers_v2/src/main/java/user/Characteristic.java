package main.java.user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import main.java.question.PossibleAnswer;
import main.java.question.Question;
import main.java.config.Mainconfig;
import main.java.feedback.FeedBack;

/**
 * @author Lamtran3101 <h4>Description</h4> Each characteristic has three
 *         numbers.
 *         <ul>
 *         <li>reliabilitiDegree : Follow one coin model.</li>
 *         <li>sensitivity and specificity : Follow two coin model.</li>
 *         </ul>
 */
public abstract class Characteristic {
	protected double reliabilitiDegree;
	protected double sensitivity;
	protected double specificity;
	protected List<PossibleAnswer> answers;

	abstract FeedBack answer(Question question, Worker worker);

	protected PossibleAnswer getAnswerBaseOnCharacter(Question question) {
		String InputType = Mainconfig.getInstance().getListConfig()
				.get("InputDataType");
		if (InputType.equals("Binary")) {
			return BinaryAnswer(question);
		} else {
			initListAnswers();
			return MultipleAnswer(question);
		}
	}

	protected abstract PossibleAnswer BinaryAnswer(Question question);

	protected abstract PossibleAnswer MultipleAnswer(Question question);

	public Characteristic() {
		setDegree();
		String InputType = Mainconfig.getInstance().getListConfig()
				.get("InputDataType");
		if (!InputType.equals("Binary")) {
			initListAnswers();
		}
	}

	protected void setDegree() {
		reliabilitiDegree = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("default_reliability"));
		sensitivity = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("default_sensitivity"));
		specificity = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("default_specificity"));
	}

	public void setDegreeNum(double value, int pos) {
		switch (pos) {
		case 1:
			setSensitivity(value);
			break;
		case 2:
			setSpecificity(value);
			break;

		default:
			setReliableDegree(value);
			break;
		}
	}

	public void refreshData() {
		reliabilitiDegree = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("default_reliability"));
		sensitivity = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("default_sensitivity"));
		specificity = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("default_specificity"));
	}

	public void setReliableDegree(double n) {
		reliabilitiDegree = n;
	}

	public double getReliableDegree() {
		return reliabilitiDegree;
	}

	public void setSpecificity(double s) {
		specificity = s;
	}

	public void setSensitivity(double s) {
		sensitivity = s;
	}

	public double getSpecificity() {
		return specificity;
	}

	public double getSensitivity() {
		return sensitivity;
	}

	/**
	 * revert the answer. E.g. Yes-> No and vice versa.
	 * 
	 * @param answer
	 * @return
	 */
	protected PossibleAnswer revertAnswer(PossibleAnswer answer) {
		switch (answer) {
		case Yes:
			return PossibleAnswer.No;
		default:
			return PossibleAnswer.Yes;
		}
	}

	protected PossibleAnswer wrongAnswer(PossibleAnswer correctAnswer) {

		if (answers != null) {
			if (answers.size() > 1) {
				Collections.shuffle(answers);
				PossibleAnswer answer = answers.get(0);
				while (answer == correctAnswer) {
					Collections.shuffle(answers);
					answer = answers.get(0);
				}
				return answer;
			}
			System.out.println("not null " + answers.size());
			System.exit(0);
			return null;

		}
		System.out.println("not null " + answers.size());
		System.exit(0);
		return null;
	}

	private void initListAnswers() {
		int num = Integer.parseInt(Mainconfig.getInstance().getListConfig()
				.get("InputNumberLabels"));
		PossibleAnswer[] fullList = { PossibleAnswer.A, PossibleAnswer.B,
				PossibleAnswer.C, PossibleAnswer.D, PossibleAnswer.E,
				PossibleAnswer.F, PossibleAnswer.G, PossibleAnswer.H };
		answers = new ArrayList<>();
		int start = 0;
		while (start < num) {
			answers.add(fullList[start]);
			start++;
		}
	}

	public String getClassName() {
		return this.getClass().getName();
	}

	@Override
	public Characteristic clone() throws CloneNotSupportedException {
		return (Characteristic) super.clone();
	}
}
