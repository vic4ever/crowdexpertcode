package main.java.user;

import java.util.ArrayList;
import java.util.List;

import main.java.config.Mainconfig;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * This class will combine other distribution for distributing workers' expertises.
 * Most of the time, we use this class for creating complex pool workers. 
 */
public class MixedDistributor extends WorkerDistributor {

	private String[] ratio;
	private String[] typeOfDistributor;
	private List<String[]> distributors;

	public MixedDistributor() {
		ratio = Mainconfig.getInstance().getListConfig().get("workersRatio")
				.split(";");

		distributors = new ArrayList<>();
	}

	@Override
	public void distribute(List<Worker> Workers, int pos) {
		switch (pos) {
		case 1:
			typeOfDistributor = Mainconfig.getInstance().getListConfig()
					.get("typeOfDistributorSensitivity").split(";");
			break;
		case 2:
			typeOfDistributor = Mainconfig.getInstance().getListConfig()
					.get("typeOfDistributorSpecificity").split(";");
			break;
		default:
			typeOfDistributor = Mainconfig.getInstance().getListConfig()
					.get("typeOfDistributor").split(";");
			break;
		}
		distributors.clear();
		for (int i = 0; i < ratio.length; i++) {
			String[] pair = new String[2];
			pair[0] = ratio[i];
			pair[1] = typeOfDistributor[i];
			distributors.add(pair);
		}

		int size = Workers.size();
		int length = distributors.size();
		int index = 0;
		int rest = size;
		for (; index < length; index++) {
			String[] values = distributors.get(index)[0].split("[(,)%]");
			int num = 0;
			switch (values[0]) {
			case "fix":
				num = Integer.parseInt(values[1]);
				break;
			default:
				num = (int) (size * Double.parseDouble(values[0]) / 100);
				break;
			}

			if (index == (length - 1) || num > rest)
				num = rest;

			
			// System.out.println("size: " + size + ", index: " + index + ", num: " + num + ", rest: " + rest + ", value: " + distributors.get(index)[1] + ", pos" + pos);
			 

			List<Worker> subWorkers = getSubWrkers(Workers, num, size - rest);
			rest = rest - num;
			// System.out.println("-------" + distributors.get(index)[1]);
			String[] content = distributors.get(index)[1].split("[(,)]");
			WorkerDistributor distributor = null;
			
			String normalDistribution = "NormalDistribution".toString();
			String uniDistribution = "UniformDistribution".toString();
			
			String ccc = content[0];
			
			if(content[0].equals(normalDistribution)){
				double mean = Double.parseDouble(content[1]);
				double sd = Double.parseDouble(content[2]);

				distributor = new NormalDistributor(mean, sd);
			} else if (content[0].equals(uniDistribution)){
				double lowBound = Double.parseDouble(content[1]);
				double upBound = Double.parseDouble(content[2]);
				distributor = new UniformDistributor(lowBound, upBound);
			}else {
				double reliability = Double.parseDouble(content[0]);
				distributor = new FixedDistributor(reliability);
			}

//			switch (content[0]) {
//			case "NormalDistribution":
//				double mean = Double.parseDouble(content[1]);
//				double sd = Double.parseDouble(content[2]);
//
//				distributor = new NormalDistributor(mean, sd);
//				break;
//			case "UniformDistribution":
//				double lowBound = Double.parseDouble(content[1]);
//				double upBound = Double.parseDouble(content[2]);
//				distributor = new UniformDistributor(lowBound, upBound);
//				break;
//			default:
//				//System.out.println(content[0]);
//				double reliability = Double.parseDouble(content[0]);
//				distributor = new FixedDistributor(reliability);
//				break;
//			}
			distributor.distribute(subWorkers, pos);
			/*
			 * System.out.println("Here: " + content[0] + " "+ content[1] + " "+
			 * content[2]);
			 */
		}
	}

	private List<Worker> getSubWrkers(List<Worker> Workers, int size, int pos) {
		List<Worker> subWorkers = new ArrayList<>();
//		 System.out.println("get: " + size + ", " + pos); 
		for (int i = pos; i < (pos + size); i++) {
			subWorkers.add(Workers.get(i));
		}
		return subWorkers;
	}

}
