package main.java.user;

import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;

import config.Mainconfig;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * This class distributes workers' reliability based on normal distribution.
 * It has two important factor: mean and standard deviation (sd).
 */
public class NormalDistributor extends WorkerDistributor {

	private double mean = 0.1;
	private double sd = 0.3;

	public NormalDistributor() {

		mean = Double.parseDouble(Mainconfig.getInstance().getListConfig()
				.get("mean"));
		sd = Double.parseDouble(Mainconfig.getInstance().getListConfig()
				.get("sd"));

	}

	public NormalDistributor(double mean, double sd) {
		this.mean = mean;
		this.sd = sd;
	}

	@Override
	public void distribute(List<Worker> workers, int pos) {
		NormalDistribution gen = new NormalDistribution(mean, sd);
		for (Worker worker : workers) {
			
			double prob = gen.sample();
			while (prob > 1 || prob < 0) {
				prob = gen.sample();
			}
			worker.getChar().setDegreeNum(prob, pos);
		}
		
		
	}

}
