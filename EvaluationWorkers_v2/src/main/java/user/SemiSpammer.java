package main.java.user;

import java.util.Collections;

import main.java.question.PossibleAnswer;
import main.java.question.Question;
import main.java.config.Mainconfig;

/**
 * @author Lamtran3101 <h4>Description</h4> SemiSpammers are those whose answer
 *         partial questions randomly, the rest of questions will be answer
 *         correctly.
 */
public class SemiSpammer extends Spammer {

	public SemiSpammer() {
		style = AnswerStyle.Semi;
	}

	@Override
	protected PossibleAnswer BinaryAnswer(Question question) {
		double ratio = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("semiSpammerRatio"));
		if (Mainconfig.getInstance().getRand().nextDouble() < ratio) {
			return question.getCorrectAnswer();
		} else {
			if (Mainconfig.getInstance().getRand().nextDouble() < 0.5) {
				return PossibleAnswer.Yes;
			} else
				return PossibleAnswer.No;
		}
	}

	@Override
	protected PossibleAnswer MultipleAnswer(Question question) {
		double ratio = Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("semiSpammerRatio"));
		if (Mainconfig.getInstance().getRand().nextDouble() < ratio) {
			return question.getCorrectAnswer();
		} else {
			Collections.shuffle(answers);
			return answers.get(0);
		}
	}

	@Override
	public SemiSpammer clone() throws CloneNotSupportedException {
		SemiSpammer clone = new SemiSpammer();
		return (SemiSpammer) genClone(clone);
	}
}
