package main.java.user;

import java.util.ArrayList;

import main.java.question.PossibleAnswer;
import main.java.question.Question;
import main.java.feedback.CorrespondenceFeedBack;
import main.java.feedback.FeedBack;

public abstract class Spammer extends Characteristic {

	protected enum AnswerStyle {
		Yes, No, Rand, Semi, A, B, C, D, E, F, G, H
	};

	protected AnswerStyle style;

	public Spammer() {
		super();
		/* initialized(); */
	}

	/*
	 * private void initialized(){ AnswerStyle[] list = AnswerStyle.values();
	 * Random rand = new Random(); style = list[rand.nextInt(list.length)]; }
	 */

	@Override
	FeedBack answer(Question question, Worker worker) {
		FeedBack feedback = new CorrespondenceFeedBack();
		feedback.setWorker(worker);
		feedback.setQuestion(question);
		feedback.setAnswer(getAnswerBaseOnCharacter(question));
		return feedback;
	}

	@Override
	public String getClassName() {
		return this.getClass().getName() + ", I always answer " + style;
	}

	/*
	 * @Override protected abstract PossibleAnswer
	 * getAnswerBaseOnCharacter(Question question);
	 *//*
		 * { switch (style){ case Yes: return PossibleAnswer.Yes; case No:
		 * return PossibleAnswer.No; default: Random rand = new Random(); double
		 * luck = rand.nextDouble(); if(luck < 0.5) return PossibleAnswer.Yes;
		 * else return PossibleAnswer.No ; } }
		 */
	protected Spammer genClone(Spammer clone) {
		clone.setSpecificity(this.getSpecificity());
		clone.setSensitivity(this.getSensitivity());
		clone.setReliableDegree(this.getReliableDegree());
		clone.style = this.style;
		clone.answers = new ArrayList<PossibleAnswer>(this.answers);
		return clone;
	}
}
