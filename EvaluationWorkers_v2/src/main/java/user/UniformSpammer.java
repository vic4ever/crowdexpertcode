package main.java.user;

import java.util.Collections;

import main.java.question.PossibleAnswer;
import main.java.question.Question;
import main.java.config.Mainconfig;

/**
 * @author Lamtran3101 <h4>Description</h4> Uniform Spammers are those who give
 *         the same answer for all questions, without concerning about content
 *         of questions.
 */
public class UniformSpammer extends Spammer {

	public UniformSpammer() {
		String inputType = Mainconfig.getInstance().getListConfig()
				.get("InputDataType");
		if (inputType.equals("Binary")) {
			binaryStyle();
		} else {
			MultipleStyle();
		}

	}

	private void binaryStyle() {
		style = AnswerStyle.No;
		if (Mainconfig.getInstance().getRand().nextDouble() < 0.5) {
			style = AnswerStyle.Yes;
		}
	}

	private void MultipleStyle() {
		Collections.shuffle(answers);
		PossibleAnswer answer = answers.get(0);
		switch (answer) {
		case B:
			style = AnswerStyle.B;
			break;
		case C:
			style = AnswerStyle.C;
			break;
		case D:
			style = AnswerStyle.D;
			break;
		case E:
			style = AnswerStyle.E;
			break;
		case F:
			style = AnswerStyle.F;
			break;
		case G:
			style = AnswerStyle.G;
			break;
		case H:
			style = AnswerStyle.H;
			break;
		default:
			style = AnswerStyle.A;
		}
	}

	@Override
	protected PossibleAnswer BinaryAnswer(Question question) {
		if (style == AnswerStyle.Yes)
			return PossibleAnswer.Yes;
		else
			return PossibleAnswer.No;
	}

	@Override
	protected PossibleAnswer MultipleAnswer(Question question) {
		switch (style) {
		case B:
			return PossibleAnswer.B;
		case C:
			return PossibleAnswer.C;
		case D:
			return PossibleAnswer.D;
		case E:
			return PossibleAnswer.E;
		case F:
			return PossibleAnswer.F;
		case G:
			return PossibleAnswer.G;
		case H:
			return PossibleAnswer.H;
		default:
			return PossibleAnswer.A;
		}
	}

	@Override
	public UniformSpammer clone() throws CloneNotSupportedException {
		UniformSpammer clone = new UniformSpammer();
		return (UniformSpammer) genClone(clone);
	}
}
