package main.java.user;

import java.util.ArrayList;
import java.util.List;

import main.java.config.Mainconfig;

/**
 * @author Lamtran3101 <h4>Description</h4> This class has responsibility for
 *         creating crowd workers, including spammers.
 */
public class UserCommunity {
	private List<Worker> workers;
	private double total;
	private double truthfulWorker = 0;
	private double spammers = 0;

	public UserCommunity() {
		initilized();
	}

	/**
	 * Construct list of workers base on MainConfig
	 */
	private void initilized() {
		workers = new ArrayList<>();
		total = Mainconfig.getInstance().getTotalWorkers();
		String[] ratioType = Mainconfig.getInstance().getListConfig()
				.get("WorkerTypeRatio").split("[(,)%]");
		String typename = Mainconfig.getInstance().getListConfig()
				.get("WorkerType").split("[(,)%]")[0];
		switch (typename) {
		case "spammer":
			spammers = getNumOfSpecificWorker(ratioType);
			truthfulWorker = total - spammers;
			break;

		default:
			truthfulWorker = getNumOfSpecificWorker(ratioType);
			spammers = total - truthfulWorker;
			break;
		}

	}

	/**
	 * initialize reliability degree of each worker base on a distribute
	 * function.
	 * 
	 * @param distributor
	 */
	public void generateWorker(WorkerDistributor distributor) {
		String feedbackModel = Mainconfig.getInstance().getListConfig()
				.get("feedbackModel");
		if (truthfulWorker != 0) {
			for (int i = 0; i < truthfulWorker; i++) {
				Worker worker = new Worker();
				worker.setChar(new TruthFulWorker());
				workers.add(worker);
			}
		}
		if (spammers != 0) {
			double[] listSpammers = getListSpammers();
			String[] spammersType = Mainconfig.getInstance().getListConfig()
					.get("spammersType").split(";");
			for (int j = 0; j < spammers; j++) {
				// int id = (int) (j + truthfulWorker);
				Worker worker = new Worker();
				Spammer spammer = null;

				if (j < listSpammers[0]) {
					spammer = addSpammers(spammersType[0]);
				} else if (j < listSpammers[0] + listSpammers[1]) {
					spammer = addSpammers(spammersType[1]);
				} else {
					spammer = addSpammers(spammersType[2]);
				}
				worker.setChar(spammer);
				workers.add(worker);
			}
		}

		switch (feedbackModel) {
		case "TwoCoin":
			distributor.distribute(workers, 1);
			distributor.distribute(workers, 2);
			break;
		default:
			distributor.distribute(workers, 0);
			break;
		}
	}

	private Spammer addSpammers(String type) {
		switch (type) {
		case "semi":
			return new SemiSpammer();
		case "random":
			return new RandomSpammer();
		default:
			return new UniformSpammer();
		}
	}

	private double[] getListSpammers() {
		double[] list = new double[3];
		for (int i = 0; i < list.length; i++) {
			list[i] = 0;
		}
		String[] content = Mainconfig.getInstance().getListConfig()
				.get("spammersRatio").split(";");
		for (int j = 0; j < content.length; j++) {
			list[j] = getNumOfSpecificWorker(content[j].split("[(,)%]"));
		}

		// random spammer list[0]
		double rest = spammers;
		if (rest < list[0]) {
			list[0] = rest;
			list[1] = 0;
			list[2] = 0;
		} else {
			rest = rest - list[0];
			if (rest < list[1]) {
				list[1] = rest;
				list[2] = 0;
			} else {
				rest = rest - list[1];
				if (rest != list[2]) {
					list[2] = rest;
				}
			}
		}

		return list;
	}

	private int getNumOfSpecificWorker(String[] ratioType) {
		int num = 0;
		switch (ratioType[0]) {
		case "fix":
			num = Integer.parseInt(ratioType[1]);
			break;
		default:
			num = (int) (total / 100 * Double.parseDouble(ratioType[0]));
			break;
		}
		return num;
	}

	public double getTotalWorkers() {
		return total;
	}

	public List<Worker> getListOfWorker() {
		return workers;
	}

}
