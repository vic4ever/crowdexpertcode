package main.java.user;

import java.util.List;

/**
 * @author lamtran3101
 * <h4>Description</h4>
 * This abstract class is used to distribute reliability degree among workers based on a distribute function
 */
public abstract class WorkerDistributor {
	public abstract void distribute(List<Worker> Workers, int pos);
}
