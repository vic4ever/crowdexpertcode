package main.java.utility;

public class IDGenerator {
	private static int wID = -1;
	private static int qID = -1;

	public static int nextWorkerID() {
		wID++;
		return wID;
	}

	public static int nextQuestionID() {
		qID++;
		return qID;
	}

	public static void resetWorkerID() {
		wID = 0;
	}
	
	public static void resetWorkerID(int i) {
		wID = i;
	}

	public static void resetQuestionID() {
		qID = 0;
	}

	public static void resetQuestionID(int i) {
		qID = i;
	}
}
