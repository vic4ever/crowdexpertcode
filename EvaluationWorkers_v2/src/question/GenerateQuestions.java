package question;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import utility.IDGenerator;
import ch.epfl.lsir.nisb.data.graph.GeneralGraph;
import ch.epfl.lsir.nisb.data.graph.MappingIterator;
import ch.epfl.lsir.nisb.data.graph.mapping.MappingTarget;
import ch.epfl.lsir.nisb.dataAccess.DataModel;
import ch.epfl.lsir.nisb.tools.GraphUtil;
import config.Mainconfig;
import data.PossibleCategories;

/**
 * @author Lamtran3101 <h4>Description</h4> This class control number of Yes and
 *         No questions which will be generated.
 */
public class GenerateQuestions {
	private ArrayList<Question> questions;
	private int totalQuestion = 50;
	private int NumOfYes = 0;
	private int NumOfNo = 0;
	private boolean checkContrains;

	public GenerateQuestions() {
		questions = new ArrayList<Question>();
		totalQuestion = Integer.parseInt(Mainconfig.getInstance()
				.getListConfig().get("NumOfQuestion"));
		String inputType = Mainconfig.getInstance().getListConfig()
				.get("InputDataType");
		if (inputType.equals("Binary")) {
			generateBinaryC();
		} else {
			generateMultipleC();
		}

	}

	public ArrayList<Question> getList() {
		return questions;
	}

	public void generateQuestion(QuestionDistributor dQ) {
		// System.out.println(questions + "nulllll");
		dQ.distribute(questions);
	}

	private void generateMultipleC() {
		int numLabels = Integer.parseInt(Mainconfig.getInstance()
				.getListConfig().get("InputNumberLabels"));
		PossibleCategories[] fullList = { PossibleCategories.A,
				PossibleCategories.B, PossibleCategories.C,
				PossibleCategories.D, PossibleCategories.E,
				PossibleCategories.F, PossibleCategories.G,
				PossibleCategories.H };
		List<PossibleCategories> answers = new ArrayList<>();
		int start = 0;
		while (start < numLabels) {
			answers.add(fullList[start]);
			start++;
		}

		int[] ratio = labelsRatio(numLabels);
		int index = 0;
		int Qid = 0;
		while (index < ratio.length) {
			int size = ratio[index];
			PossibleCategories cat = answers.get(index);
			// System.out.println("PossibleCategories: " + cat);
			for (int i = 0; i < size; i++) {
				Question question = new Question();
				question.getCorrespondence().setValue(Qid + "");
				question.getCorrespondence().setGroundTruth(cat);
				// System.out.println(cat.getAnswer());
				// System.out.println(cat.getAnswer());
				questions.add(question);
				// System.out.println("question: " +
				// question.getCorrespondence().getValue() + " " +
				// question.getCorrectAnswer());
				Qid++;
			}
			index++;
		}
		Collections.shuffle(questions);
	}

	private int[] labelsRatio(int numlabels) {
		int[] ratio = new int[numlabels];
		for (int i = 0; i < numlabels; i++) {
			ratio[i] = 0;
		}
		int rest = totalQuestion;

		String[] InputRatio = Mainconfig.getInstance().getListConfig()
				.get("InputRatioLabels").split(";");

		for (int i = 0; i < InputRatio.length; i++) {
			String[] values = InputRatio[i].split("[(,)%]");
			int num = 0;
			switch (values[0]) {
			case "fix":
				num = Integer.parseInt(values[1]);
				break;
			default:
				num = (int) (totalQuestion * Double.parseDouble(values[0]) / 100);
				break;
			}
			if (rest <= num)
				num = rest;
			else
				rest = rest - num;
			ratio[i] = num;
		}

		return ratio;
	}

	private void generateBinaryC() {
		checkContrains = checkInputConstrains();
		if (checkContrains) {
			double ratio = Double.parseDouble(Mainconfig.getInstance()
					.getListConfig().get("InputDataRatio"));
			NumOfYes = (int) (totalQuestion * ratio);
			NumOfNo = totalQuestion - NumOfYes;
		}
		init();
	}

	private void init() {
		int start = 0;
		int numYes = 0;
		int numNo = 0;
		int iter = 0;
		while (true) {
			if (start >= totalQuestion)
				break;
			Boolean check = randomBinaryGroundTruth();
			Question question = new Question();
			question.getCorrespondence().setValue(iter + "");
			if (check) {
				numYes++;
				question.getCorrespondence().setGroundTruth(
						PossibleCategories.True);
			} else {
				numNo++;
				question.getCorrespondence().setGroundTruth(
						PossibleCategories.False);
			}
			boolean checkQuestion = checkRequirments(question, numYes, numNo);
			if (checkQuestion) {
				start++;
				questions.add(question);
				iter++;
			} else {
				IDGenerator.resetQuestionID(iter - 1);
			}
		}
	}

	private boolean randomBinaryGroundTruth() {
		double rand = Mainconfig.getInstance().getRand().nextDouble();
		if (rand < 0.5)
			return false;
		return true;
	}

	private void init_bk() {
		DataModel dataModel = DataModel.getInstance(Mainconfig.getInstance()
				.getDataset());
		GeneralGraph graph = dataModel.getGenGraph();
		GeneralGraph golden = dataModel.getGoldenGraph();
		MappingIterator mItr = graph.mappingIterator();
		int start = 0;
		int numYes = 0;
		int numNo = 0;
		while (mItr.hasNext()) {
			if (start >= totalQuestion)
				break;
			MappingTarget corr = mItr.next();
			String mappingFullID = corr.getMappingTargetFullID();

			Boolean check = GraphUtil.isCorrectWithGroupBothWay(golden,
					mappingFullID);
			Question question = new Question();
			question.getCorrespondence().setValue(mappingFullID);
			if (check) {
				numYes++;
				question.getCorrespondence().setGroundTruth(
						PossibleCategories.True);
			} else {
				numNo++;
				question.getCorrespondence().setGroundTruth(
						PossibleCategories.False);
			}

			boolean checkQuestion = checkRequirments(question, numYes, numNo);
			if (checkQuestion) {
				start++;
				questions.add(question);
			}

		}
	}

	private boolean checkRequirments(Question question, int numYes, int numNo) {
		if (checkContrains == true) {
			boolean result = false;
			PossibleCategories label = question.getCorrespondence()
					.getGroundTruth();
			switch (label) {
			case False:
				if (numNo <= NumOfNo)
					result = true;
				break;
			case True:
				if (numYes <= NumOfYes)
					result = true;
				break;
			default:
				break;
			}
			return result;

		} else
			return true;
	}

	private boolean checkInputConstrains() {
		String check = Mainconfig.getInstance().getListConfig()
				.get("InputDataConstrains");
		if (check.equals("true"))
			return true;
		else
			return false;
	}

	/*
	 * private void init() { DataModel dataModel =
	 * DataModel.getInstance(Mainconfig.getInstance() .getDataset());
	 * GeneralGraph graph = dataModel.getGenGraph(); GeneralGraph golden =
	 * dataModel.getGoldenGraph(); MappingIterator mItr =
	 * graph.mappingIterator(); int start = 0; while (mItr.hasNext()) { if(start
	 * >= totalQuestion) break; MappingTarget corr = mItr.next(); String
	 * MappingTargetID = corr.getMappingTargetID();
	 * corr.getMappingTargetFullID() Map<String, MappingInfo> candidateTargets =
	 * corr .getCandidateTargets(); for (String value :
	 * candidateTargets.keySet()) { if(start >= totalQuestion) break; String
	 * mappingFullID = GraphUtil.generateFullMappingID( MappingTargetID, value);
	 * Boolean check = GraphUtil.isCorrectWithGroupBothWay(golden,
	 * mappingFullID); Question question = new Question();
	 * question.getCorrespondence().setValue(mappingFullID); if (check) {
	 * question.getCorrespondence().setGroundTruth( PossibleCategories.True); }
	 * else { question.getCorrespondence().setGroundTruth(
	 * PossibleCategories.False); } start++; questions.add(question); } } }
	 */

}
