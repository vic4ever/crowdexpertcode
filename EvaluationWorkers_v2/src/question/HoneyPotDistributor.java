package question;

import java.util.ArrayList;
import java.util.Collections;

import config.Mainconfig;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * This class is used for Honey Pot algorithm. It controls number of "trap questions" that we'll use in experiemnts
 */
public class HoneyPotDistributor extends QuestionDistributor {

	private double t;

	public HoneyPotDistributor() {
		Mainconfig.getInstance().getRand();
		t = Double.parseDouble(Mainconfig.getInstance().getListConfig()
				.get("HoneyPotRatio"));
	}

	@Override
	public void distribute(ArrayList<Question> questions) {
		int index = 0;
		for (Question question : questions) {
			/*if (rand.nextFloat() < t) {
				question.getCorrespondence().setMode(1);
			}*/
			if(index >= t) break;
			question.getCorrespondence().setMode(1);
			index++;
		}
		Collections.shuffle(questions);

		/*
		 * System.out.println("Honey Spot");
		 */}

}
