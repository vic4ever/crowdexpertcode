package question;

import java.util.ArrayList;
import java.util.List;

public class MultiChoiceQuestion extends QuestionType{

	public MultiChoiceQuestion() {

	}
	
	@Override
	public List<PossibleAnswer> getPossibleAnswers() {
		List<PossibleAnswer> res = new ArrayList<>();
		res.add(PossibleAnswer.A);
		res.add(PossibleAnswer.B);
		res.add(PossibleAnswer.C);
		res.add(PossibleAnswer.D);
		res.add(PossibleAnswer.E);
		res.add(PossibleAnswer.F);
		res.add(PossibleAnswer.G);
		res.add(PossibleAnswer.H);
		return res;
	}

}
