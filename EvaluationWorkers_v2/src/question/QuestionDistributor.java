package question;

import java.util.ArrayList;

public abstract class QuestionDistributor {
	public abstract void distribute(ArrayList<Question> questions);
}
