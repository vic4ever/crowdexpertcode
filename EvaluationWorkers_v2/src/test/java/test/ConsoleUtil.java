package test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Observable;

public class ConsoleUtil extends Observable {
	// private String content;
	private static final PrintStream printStreamOriginal = System.out;

	public String exec(String cmd) throws IOException {
		Process proc = Runtime.getRuntime().exec(cmd);

		/* Wait for this process stop */
		BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		String content = "";
		String line;
		long start = System.currentTimeMillis();
		do {
			line = in.readLine();
			if (line != null) {
				content += line + "\n";
			}
			else {
				long end = System.currentTimeMillis();
				if (end - start > 5000){
					break;
				}
			}
		} while (true);
		// while ((in.readLine()) != null) {// waiting for this process stop
		// System.out.println();
		// }
		return content;
	}

	public void exec(String cmd, String outputFile) throws IOException {
		PrintWriter w = new PrintWriter(new BufferedWriter(new FileWriter(outputFile, false)), true);

		Process proc = Runtime.getRuntime().exec(cmd);

		/* Wait for this process stop */
		BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		String line;
		do {
			line = in.readLine();
			if (line != null) {
				w.println(line);

				this.setChanged();
				this.notifyObservers(line);
			}
			else {
				break;
			}
		} while (true);
	}

	// public String getContent() {
	// return content;
	// }
	
	public void redirectSystemOut(String logFile) {
		try {
			System.setOut(new PrintStream(new File(logFile)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void resetSystemStdOut() {
		System.setOut(printStreamOriginal);
	}

}
