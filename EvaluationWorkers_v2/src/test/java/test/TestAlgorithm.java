package test;

import java.io.IOException;

import config.Mainconfig;
import config.Mainconfig.ListDatasets;
import evaluateWorkers.EvaluateWorker;
import evaluateWorkers.Output;
import factorypattern.AlgorithmFactory;
import feedback.FeedBackModel;

public class TestAlgorithm {

	/**
	 * @param args
	 */
	private static String algorithm = "EM";
	// private static String algorithm = "SLME";
	// private static String algorithm = "IterativeLearning";
	// private static String algorithm = "ELICE";

	private static String filename = algorithm + ".csv";
	static AlgorithmFactory algo = new AlgorithmFactory();;
	static EvaluateWorker eval = null;

	public static void main(String[] args) throws IOException {
		Mainconfig.getInstance().initialized();
		for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
			Mainconfig.getInstance().setData(data);
		}
		FeedBackModel model = new FeedBackModel();
//		model.printFeedbacks();
//		model.displayFeedBack();
		//model.generateGALInputFiles("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt");
//		model.generateGALInputFiles("C:/Users/Asus/workspace/FeedbackProcess/input/labels3.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/categories3.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/gold3.txt");
//		model.generateGALInputFiles("C:/Users/Asus/workspace/FeedbackProcess/input/labelsM3.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/categoriesM3.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/goldM3.txt");
//		model.generateGALInputFiles("C:/Users/Asus/workspace/FeedbackProcess/input/labels10.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/categories10.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/gold10.txt");
//		model.generateGALInputFiles("C:/Users/Asus/workspace/FeedbackProcess/input/labels15b.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/categories10.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/gold15b.txt");
//		model.generateGALInputFiles("C:/Users/Asus/workspace/FeedbackProcess/input/lFGlow.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/cFGlow.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/gFGlow.txt");
//		model.generateGALInputFiles("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt","C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/wexp3.txt");		
//		model.generateGALInputFiles("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/lexp3.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/cexp3.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/gexp3.txt","C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/wexp3.txt");
//		model.generateGALInputFiles("C:/Users/Asus/workspace/FeedbackProcess/input/lllexp3.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/lllcexp3.txt", "C:/Users/Asus/workspace/FeedbackProcess/input/lllgexp3.txt","C:/Users/Asus/workspace/FeedbackProcess/input/lllwexp3.txt");
		String folder = "C:/Users/Asus/workspace/FeedbackProcess/input/RunningTime/test/";
//		model.generateGALInputFiles(folder + "label.txt", folder + "category.txt", folder + "gold.txt",folder + "worker.txt");
		model.generateGALInputFilesMulti(folder + "label.txt", folder + "category.txt", folder + "gold.txt",folder + "worker.txt");
//		eval = algo.createAlgorithm(algorithm, model);
//		Mainconfig.getInstance().getTimer().start();
//		eval.execute();
//		Mainconfig.getInstance().getTimer().stop();
//		Output output = new Output(eval.getClass().getName(), Mainconfig
//				.getInstance().getDataset().toString());
//		output.setWorkers(eval.getWorkersResult());
//		output.setQuestions(eval.getQuestionsResult());
//		output.setCompletionTime(Mainconfig.getInstance().getTimer()
//				.getElapsedTime());
//		output.exportWorkersDetail(filename);
//		output.exportResult();
	}

}
