package test;

import java.util.List;

import tools.io.MyCsvWriter;
import user.UserCommunity;
import user.Worker;
import config.Mainconfig;

public class TestCommunity {

	/**
	 * @param args
	 */
	static UserCommunity com;
	public static void displayName(){
		for(int i=0; i < com.getTotalWorkers(); i++){
			System.out.println("worker:" + i + " " + com.getListOfWorker().get(i).getChar().getClassName());		
		}
	}
	
	private static String generateStatistic(List<Worker> workers){
		StringBuffer sb = new StringBuffer();
		for(Worker worker: workers){
			sb.append(worker.getWID() + "\t" + worker.getChar().getReliableDegree() + "\n" );
		}
		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		Mainconfig.getInstance().initialized();
		com = new UserCommunity();
/*		com.setTotalWorkers(Mainconfig.getInstance().getTotalWorkers());*/
		com.generateWorker(Mainconfig.getInstance().getWorkerDsitributor());
		/*MyCsvWriter.getInstance().WriteToFile(generateStatistic(com.getListOfWorker()), "worker.csv", true, false);*/
		//MyCsvWriter.getInstance().WriteToFile(generateStatistic(com.getListOfWorker()), "worker_context1.csv", true, false);
		displayName();
		
	}

}
