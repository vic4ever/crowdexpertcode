package test;

import java.util.Map;

import ch.epfl.lsir.nisb.data.graph.GeneralGraph;
import ch.epfl.lsir.nisb.data.graph.MappingIterator;
import ch.epfl.lsir.nisb.data.graph.mapping.MappingTarget;
import ch.epfl.lsir.nisb.data.graph.mapping.MappingTarget.MappingInfo;
import ch.epfl.lsir.nisb.dataAccess.DataModel;
import ch.epfl.lsir.nisb.dataAccess.dataset.DatasetProvider.DatasetCollection;
import ch.epfl.lsir.nisb.tools.GraphUtil;

public class TestCorrespondences {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		testcase1();
	}

	public static void testcase1() {
		DataModel dataModel = DataModel
				.getInstance(DatasetCollection.BUSINESS_PARTNER);
		// TestMassCollaborationMajorityVoting $this = new
		// TestMassCollaborationMajorityVoting();
		// $this.run(dataModel);

		// generate micro mapping
		GeneralGraph graph = dataModel.getGenGraph();
		GeneralGraph golden = dataModel.getGoldenGraph();
		MappingIterator mItr = graph.mappingIterator();
		while (mItr.hasNext()) {
			/* System.out.println(mItr.next()); */
			MappingTarget corr = mItr.next();
			String MappingTargetID = corr.getMappingTargetID();

			/*
			 * Boolean check = g.isCorrectWithGroupBothWay(golden,
			 * mappingFUllID);
			 */
			Map<String, MappingInfo> candidateTargets = corr
					.getCandidateTargets();
			for (String value : candidateTargets.keySet()) {
				String mappingFullID = GraphUtil.generateFullMappingID(
						MappingTargetID, value);
				System.out.println(mappingFullID);
				Boolean check = GraphUtil.isCorrectWithGroupBothWay(golden,
						mappingFullID);
				System.out.println(check);

			}

			/* System.out.println(mItr.next().getCandidateTargets()); */

			System.out.println("-----");

		}
		// golden graph
		/*
		 * GeneralGraph golden = dataModel.getGoldenGraph(); MappingIterator
		 * mItr2 = golden.mappingIterator(); int j = 0; while (mItr2.hasNext()){
		 * j++; Question ques= new Question(mItr2.next());
		 * System.out.println(ques);
		 * 
		 * } System.out.println(j);
		 */

	}

}
