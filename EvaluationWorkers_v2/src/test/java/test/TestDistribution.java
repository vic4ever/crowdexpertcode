package test;

import user.MixedDistributor;
import user.UserCommunity;
import user.Worker;
import user.WorkerDistributor;
import config.Mainconfig;

public class TestDistribution {

	public void testFixedDistributor() {
		Mainconfig.getInstance().initialized();
		WorkerDistributor dist = new MixedDistributor();
		UserCommunity userComm = new UserCommunity();
		userComm.generateWorker(dist);
		for (Worker worker : userComm.getListOfWorker()) {
			System.out.println("reliability: " + worker.getReliability()
					+ ", sensitivity: " + worker.getSensitivity()
					+ ", specificity: " + worker.getSpecificity());
		}

		/*
		 * for (Worker worker : userComm.getListOfWorker()){ double r =
		 * worker.getChar().getReliableDegree(); AssertJUnit.assertEquals(r,
		 * Double
		 * .parseDouble(Mainconfig.getInstance().getListConfig().get("fixed")));
		 * }
		 */
	}
}
