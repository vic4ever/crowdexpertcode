package test;

import java.util.Map;

import config.Mainconfig;
import config.Mainconfig.ListDatasets;
import question.Question;
import tools.io.ConfigReader;
import tools.io.MyCsvWriter;
import user.Worker;
import evaluateWorkers.EvaluateWorker;
import experiments.Experiments;
import factorypattern.AlgorithmFactory;
import feedback.FeedBack;
import feedback.FeedBackModel;

public class TestFeedBack {
	private FeedBackModel model;
	private ConfigReader reader = new ConfigReader();
	private Map<String, String> listConfig;
	private double step = 0;
	private double index = 0;
	private String filename = null;
	private String currentPath = null;
	private String fileConfig = null;

	public TestFeedBack() {
		initialized();

	}

	private void initialized() {
		filename = "experiment_em.csv";
		currentPath = "";
		fileConfig = "config.ini";
		step = 1;
		index = 0;
	}

	private void displayFeedBack() {
		int i = 0;
		for (FeedBack feedback : model.getListFeedBacks()) {
			i++;
			System.out.println("Feedback: " + i);
			Worker w = feedback.getWorker();
			System.out.println("Worker: " + w.getWID() + " is a "
					+ w.getChar().getClassName());

			Question q = feedback.getQuestion();
			System.out.println("Question: " + q.getCorrespondence().getValue()
					+ " is " + q.getCorrespondence().getGroundTruth());

			System.out.println("Answer: " + feedback.getAnswer());
			System.out.println("----------------");
		}
	}

	private String generateDetail() {
		StringBuffer sb = new StringBuffer();

		String[] workerstitles = { "QuestionID", "Groundtruth", "WorkersID", "type", "Answers", "Exp" };
		sb.append(workerstitles[0]);
		for (int i = 1; i < workerstitles.length; i++) {
			sb.append("\t" + workerstitles[i]);
		}
		sb.append("\n");
		/*for (Question question : model.getListQuestions()) {
			String oneRecord = question.getCorrespondence()
					.getValue()
					+ "\t"
					+ generateListID(question)
					+ "\t"
					+ index + "\n";
			sb.append(oneRecord);
		}*/
		for (FeedBack feedback : model.getListFeedBacks()) {
			String oneRecord = feedback.getQuestion().getCorrespondence()
					.getValue()
					+ "\t"
					+ feedback.getQuestion().getCorrespondence().getGroundTruth()
					+ "\t"
					+ feedback.getWorker().getWID()
					+ "\t"
					+ feedback.getWorker().getChar().getClassName()
					+ "\t"
					+ feedback.getAnswer()
					+ "\t"
					+ index + "\n";
			sb.append(oneRecord);
		}
		//System.out.println("content: " + sb.toString());

		return sb.toString();
	}
	
	private String generateListID(Question question){
		String list= "";
		for(Integer id : question.getListFeedbacks().keySet()){
			list = list  + "," + id;
		}
		return list;
	}

	public void exportFeedbacksDetail(String filename) {
		MyCsvWriter.getInstance().WriteToFile(generateDetail(), filename);
	}

	public void run() {
		customizeConfig();
		int iter = Integer.parseInt(Mainconfig.getInstance().getListConfig()
				.get("Iterator"));
		System.out.println("iter: " + iter);
		for (int i = 0; i < iter; i++) {
			Mainconfig.getInstance().initialized();
			index = i;		
			for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
				Mainconfig.getInstance().setData(data);
				model = new FeedBackModel();
				exportFeedbacksDetail("FeedBacks.csv");
			}
		}
	}

	public static void main(String[] args) {
		TestFeedBack model = new TestFeedBack();
		model.run();
	}

	private void customizeConfig() {
		reader.setCurrentPth(currentPath);
		reader.readfile(fileConfig);
		listConfig = reader.getConfig();
		for (String key : listConfig.keySet()) {
			Mainconfig.getInstance().getListConfig()
					.put(key, listConfig.get(key));
		}
	}

}
