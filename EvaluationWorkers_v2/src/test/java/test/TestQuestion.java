package test;

import java.util.ArrayList;

import question.GenerateQuestions;
import question.Question;

import config.Mainconfig;
import config.Mainconfig.ListDatasets;
import data.PossibleCategories;

public class TestQuestion {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Mainconfig.getInstance().initialized();
		for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
			Mainconfig.getInstance().setData(data);
		}
		GenerateQuestions questions = new GenerateQuestions();
		questions.generateQuestion(Mainconfig.getInstance().getQuestionDistributor());	
		ArrayList<Question> listquestions = questions.getList();
		//displaydetails(listquestions);
		//displayGeneral(listquestions);
		//displayQuatity(listquestions);
		displaySummary(listquestions);

	}
	
	private static void displayQuatity(ArrayList<Question> questions){
		int numYes= 0;
		int numNo = 0;
		for(Question question : questions){
			PossibleCategories label = question.getCorrespondence().getGroundTruth();
			if (label == PossibleCategories.True) numYes++;
			else if (label == PossibleCategories.False) numNo++;
		}		
		System.out.println("Total: " + questions.size());
		System.out.println("Yes: " + numYes);
		System.out.println("No: " + numNo);
	}
	
	private static void displaydetails(ArrayList<Question> questions){
		int i = 0;
		for( Question question : questions){
			i++;
			System.out.println("Question: " + i);
			
			System.out.println("Value: " + question.getCorrespondence().getValue());
			System.out.println("Groundtruh: " + question.getCorrespondence().getGroundTruth());
			System.out.println("Correct Answer: " + question.getCorrectAnswer());
			System.out.println("Est Result: " + question.getCorrespondence().getEstResult());
			System.out.println("Mode: " + question.getCorrespondence().getMode());			
			System.out.println("-------------------------------");
		}
	}
	
	private static void displaySummary(ArrayList<Question> questions){
		int num = Integer.parseInt(Mainconfig.getInstance().getListConfig().get("InputNumberLabels"));
		int[] listLabels = new int[8];
		for(int i=0; i<listLabels.length; i++){
			listLabels[i] = 0;
		}
		for(Question question : questions){
			switch (question.getCorrectAnswer()) {
			case A:
				listLabels[0]++;
				break;
			case B:
				listLabels[1]++;
				break;
			case C:
				listLabels[2]++;
				break;
			case D:
				listLabels[3]++;
				break;
			case E:
				listLabels[4]++;
				break;
			case F:
				listLabels[5]++;
				break;
			case G:
				listLabels[6]++;
				break;
			case H:
				listLabels[7]++;
				break;	
			default:
				break;
			}
		}
		System.out.println("numLabels: " + num);
		System.out.println("A: " + listLabels[0]);
		System.out.println("B: " + listLabels[1]);
		System.out.println("C: " + listLabels[2]);
		System.out.println("D: " + listLabels[3]);
		System.out.println("E: " + listLabels[4]);
		System.out.println("F: " + listLabels[5]);
		System.out.println("G: " + listLabels[6]);
		System.out.println("H: " + listLabels[7]);
		
	}
	private static void displayGeneral(ArrayList<Question> questions){
		int numberEvaluteQuestions = 0;
		for(Question question : questions){
			if(question.getCorrespondence().getMode() == 1){
				numberEvaluteQuestions++;
			}
		}
		
		System.out.println("Number of questions: " + questions.size());
		System.out.println("Number of evaluated questions: " + numberEvaluteQuestions);
	}

}
