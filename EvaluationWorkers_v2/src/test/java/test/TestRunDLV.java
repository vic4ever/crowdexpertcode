package test;

import java.io.File;
import java.io.IOException;

import ch.epfl.lsir.nisb.manager.UtilManager;

public class TestRunDLV {
	private static final String dlv = "console" + File.separator + "dlv.mingw.exe";
	private static final String outputDir = "test/prolog/output/solution/";
	private static final String plDir = "test/prolog/output/clusters/";
	private static final String rlFile = "console/dlv-rules-v7c.txt";

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		TestRunDLV $this = new TestRunDLV();
//		$this.testcase(dlv, plDir, rlFile, outputDir);
//		$this.testcase(dlv, "test/prolog/output/clusters_2schemas/", rlFile, "test/prolog/output/solution_2schemas/");
//		$this.testcase(dlv, "test/hypertree/output/clusters/", rlFile, "test/hypertree/output/solution/");
//		$this.testcase(dlv, "test/repair2/output/clusters/", rlFile, "test/repair2/output/solution");
		$this.testcase(dlv, "dataset/purchaseorder/output/clusters/", rlFile, "dataset/purchaseorder/output/solution");
//		$this.testDetection(dlv, "test/violation/example.txt", Constant.RULEFILE_InCircleViolation, "test/violation/output/vio-example.txt");
//		$this.testDetection(dlv, "test/violation/example1.txt", Constant.RULEFILE_InCircleViolation, "test/violation/output/detect-example1.txt");
//		$this.testViolation(dlv, "test/violation/sample.txt", Constant.RULEFILE_ViolateInclude, "test/violation/output/vio-sample.txt");
//		$this.testWitness(dlv, "dataset/purchaseorder/output/clusters/cluster1005.pl", Constant.RULE_FILE_NO_TRANS, "log/asp/witness.txt");
//		$this.testDetectionCircle(dlv, "test/violation/po-golden.pl", Constant.RULEFILE_InCircleViolation, "test/violation/output/vioCir-po-golden.txt");
//		$this.testDetection11(dlv, "test/violation/po-golden.pl", Constant.RULEFILE_11Violation, "test/violation/output/vio11-po-golden.txt");
	}
	
	public void testcase(String dlv, String plDir, String rlFile, String outputDir){
		UtilManager.aspUtil(true).runDLVBatch(dlv, plDir, rlFile, outputDir);
//		UtilManager.aspUtil().runDLV(dlv, StringUtil.combinePaths(plDir, "cluster0.pl"), rlFile,
//				StringUtil.combinePaths(outputDir, "out-" + "cluster0" + ".txt"));
//		UtilManager.aspUtil().runDLVFilterWitness(dlv,
//				StringUtil.combinePaths(plDir, "cluster01.pl"), rlFile,
//				StringUtil.combinePaths(outputDir, "out-" + "cluster01" + ".txt"));
//		UtilManager.aspUtil().runDLV(dlv,
//				"E:\\Java\\NisBv6.1\\test\\prolog\\output\\graph_LeafOnly.pl", rlFile,
//				StringUtil.combinePaths(outputDir, "out-" + "graphLeafOnly" + ".txt"));
	}
	
	public void testDetectionCircle(String dlv, String inputFile, String rlFile, String outputFile){
		UtilManager.aspUtil().detectCircleViolation(dlv, inputFile, rlFile, outputFile, 1000);
	}
	
	public void testDetection11(String dlv, String inputFile, String rlFile, String outputFile){
		UtilManager.aspUtil().detect11Violation(dlv, inputFile, rlFile, outputFile);
	}
	
	public void testViolation(String dlv, String inputFile, String rlFile, String outputFile){
		UtilManager.aspUtil().findViolationAgainstInclude(dlv, inputFile, rlFile, outputFile);
	}
	
	public void testWitness(String dlv, String inputFile, String rlFile, String outputFile){
		UtilManager.aspUtil().runDLVFilterWitness(dlv, inputFile, rlFile, outputFile);
	}
	
}
