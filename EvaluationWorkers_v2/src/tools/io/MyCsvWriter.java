package tools.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import ch.epfl.lsir.nisb.tools.FileUtil;

import com.csvreader.CsvWriter;

import config.Mainconfig;

public class MyCsvWriter extends Writer {
	private static MyCsvWriter singleton;
	private String defaultPath = "EM/output/";;

	private MyCsvWriter() {
		defaultPath = Mainconfig.getInstance().getListConfig().get("CSVoutput");
	}

	private MyCsvWriter(String path) {
		defaultPath = path;
	}

	public static MyCsvWriter getInstance() {
		if (singleton == null) {
			singleton = new MyCsvWriter();
		}
		return singleton;
	}

	@Override
	public String WriteToFile(String input, String FileName) {
		return WriteToFile(input, FileName, true, false);
	}

	public String WriteToFile(String input, String filename, boolean withTitle,
			boolean rawdata) {
		String FileName = defaultPath + filename;
		String[] lines = input.split("\n");
		if (rawdata) {
			saveRawFile(FileName, lines, withTitle);
		} else {
			saveGeneral(FileName, withTitle, lines);
		}
		return FileName;
	}
	
	public void removeExistFile(String filename){
			//CsvWriter csvOutput = new CsvWriter(new FileWriter(filename, false), ',');
			File file = new File(filename);
			file.delete();
			//csvOutput.close();
	}

	private void saveRawFile(String outputFile, String[] lines,
			boolean withTitle) {
		boolean alreadyExists = new File(outputFile).exists();
		try {
			CsvWriter csvOutput = new CsvWriter(
					new FileWriter(outputFile, true), ',');
			int size = lines.length;
			int start = 0;
			if (!withTitle)
				start = 1;
			if (alreadyExists) {
				start = 1;
			}
			for (; start < size; start++) {
				String line = lines[start];
				csvOutput.write(line);
				csvOutput.endRecord();
			}
			csvOutput.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveGeneral(String outputFile, boolean withTitle,
			String[] lines) {
		boolean alreadyExists = new File(outputFile).exists();
		try {
			CsvWriter csvOutput = new CsvWriter(
					new FileWriter(outputFile, true), ',');
			int size = lines.length;
			int start = 0;
			if (!withTitle)
				start = 1;
			if (alreadyExists) {
				start = 1;
			}
			for (; start < size; start++) {
				String line = lines[start];
				String[] content = line.split("\t");
				for (String piece : content) {
					csvOutput.write(piece);
				}
				csvOutput.endRecord();
			}
			csvOutput.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
