package tools.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import config.Mainconfig;

public class TxtWriter extends Writer {

	private static TxtWriter singleton;
	private static String outputPath;

	private TxtWriter() {
		outputPath = Mainconfig.getInstance().getListConfig().get("Output");
	}

	public static TxtWriter getInstance() {
		if (singleton == null) {
			singleton = new TxtWriter();
		}
		return singleton;
	}

	@Override
	public String WriteToFile(String input, String filename) {
		File outfile = new File(outputPath + filename + ".txt");
		if (filename.contains("question")) {
			System.out.println();
		}
		try {

			/*
			 * if (!(new File(outfile.getParent()).exists())) { (new
			 * File(outfile.getParent())).mkdirs(); }
			 */
			BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
			bw.write(input);
			bw.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return outfile.getAbsolutePath();
	}

}
