package transformdata;

import tools.io.TxtWriter;

public class CorrectedFileWriter extends TransFormData {

	public CorrectedFileWriter() {

	}

	@Override
	public String transform() {
		return TxtWriter.getInstance().WriteToFile(getString(), "corrected");
	}

	private String getString() {
		StringBuffer sb = new StringBuffer();
		/*
		 * for(FeedBack feedback : list){ int worker =
		 * feedback.getWorker().getWID(); String content =
		 * feedback.getQuestion().getCorrespondence().getValue(); PossibleAnswer
		 * answer = feedback.getAnswer(); sb.append(worker + "\t" + content +
		 * "\t" + answer + "\n"); }
		 */
		sb.append("\n");
		return sb.toString();
	}

}
