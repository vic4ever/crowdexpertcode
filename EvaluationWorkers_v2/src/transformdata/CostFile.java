package transformdata;

import java.util.ArrayList;

import tools.io.TxtWriter;

public class CostFile extends TransFormData {

	ArrayList<String> list;

	public CostFile(ArrayList<String> list) {
		this.list = list;
	}

	@Override
	public String transform() {
		return TxtWriter.getInstance().WriteToFile(getString(), "cost");
	}

	private String getString() {
		StringBuffer sb = new StringBuffer();
		for (String col1 : list) {
			for (String col2 : list) {
				int cost = 1;
				if (col1.equals(col2)) {
					cost = 0;
				}
				sb.append(col1 + "\t" + col2 + "\t" + cost + "\n");
			}
		}
		return sb.toString();
	}

}
