package transformdata;

import java.util.List;

import tools.io.TxtWriter;
import data.PossibleCategories;
import feedback.FeedBack;

public class InputWriter extends TransFormData {

	private List<FeedBack> list;

	public InputWriter(List<FeedBack> FeedBack) {
		this.list = FeedBack;
	}

	@Override
	public String transform() {
		return TxtWriter.getInstance().WriteToFile(getString(), "input");
	}

	private String getString() {
		StringBuffer sb = new StringBuffer();
		for (FeedBack feedback : list) {
			int worker = feedback.getWorker().getWID();
			String content = feedback.getQuestion().getCorrespondence()
					.getValue();
			PossibleCategories answer = feedback.getAnswer().getCategory();
			sb.append(worker + "\t" + content + "\t" + answer + "\n");
		}
		return sb.toString();

	}

}
