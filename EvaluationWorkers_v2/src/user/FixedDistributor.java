package user;

import java.util.List;

import config.Mainconfig;

/**
 * @author Lamtran3101
 * <h4>Description</h4>
 * This class will assign a fix number of reliability to a particular worker.
 */
public class FixedDistributor extends WorkerDistributor{
	private double value;

	
	public FixedDistributor(){
		this.value = Double.parseDouble(Mainconfig.getInstance().getListConfig().get("fixed"));
	}
	
	public FixedDistributor(double value) {
		super();
		this.value = value;
	}

	@Override
	public void distribute(List<Worker> workers, int pos) {
		for(Worker worker : workers){
			worker.getChar().setDegreeNum(this.value, pos);
		}	
	}
}
