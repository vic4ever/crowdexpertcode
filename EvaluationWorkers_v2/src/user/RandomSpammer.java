package user;

import java.util.Collections;

import question.PossibleAnswer;
import question.Question;
import config.Mainconfig;

/**
 * @author Lamtran3101 <h4>Description</h4> Random Spammers are thoes who answer
 *         questions randomly.
 */
public class RandomSpammer extends Spammer {

	public RandomSpammer() {
		style = AnswerStyle.Rand;
	}

	@Override
	protected PossibleAnswer BinaryAnswer(Question question) {
		if (Mainconfig.getInstance().getRand().nextDouble() < 0.5) {
			return PossibleAnswer.Yes;
		} else
			return PossibleAnswer.No;
	}

	@Override
	protected PossibleAnswer MultipleAnswer(Question question) {
		Collections.shuffle(answers);
		return answers.get(0);
	}

	@Override
	public RandomSpammer clone() throws CloneNotSupportedException {
		RandomSpammer clone = new RandomSpammer();
		return (RandomSpammer) genClone(clone);
	}

}
