package user;

import question.PossibleAnswer;
import question.Question;
import config.Mainconfig;
import feedback.CorrespondenceFeedBack;
import feedback.FeedBack;

/**
 * @author Lamtran3101 <h4>Description</h4> Truthful Workers are those who
 *         anwser question based on their reliability.
 * 
 */
public class TruthFulWorker extends Characteristic {

	public TruthFulWorker() {
		super();
	}

	@Override
	FeedBack answer(Question question, Worker worker) {
		FeedBack feedback = new CorrespondenceFeedBack();
		feedback.setWorker(worker);
		feedback.setQuestion(question);
		feedback.setAnswer(getAnswerBaseOnCharacter(question));
		// System.out.println("Feedback: " + feedback.getAnswer() );
		return feedback;
	}

	@Override
	public String getClassName() {
		return this.getClass().getName()
				+ ", I always answer with reliablity degree "
				+ reliabilitiDegree;
	}

	@Override
	protected PossibleAnswer BinaryAnswer(Question question) {
		String modelAnswer = Mainconfig.getInstance().getListConfig()
				.get("feedbackModel");
		double pior = 0.5;
		PossibleAnswer correctanswer = question.getCorrectAnswer();
		switch (modelAnswer) {
		case "TwoCoin":
			switch (correctanswer) {
			case Yes:
				pior = sensitivity;
				break;
			case No:
				pior = specificity;
				break;
			default:
				break;
			}
			break;
		default:
			pior = reliabilitiDegree;
		}
		if (Mainconfig.getInstance().getRand().nextDouble() < pior)
			return correctanswer;
		else
			return revertAnswer(correctanswer);
	}

	@Override
	protected PossibleAnswer MultipleAnswer(Question question) {
		double pior = 0.5;
		pior = reliabilitiDegree;
		if (Mainconfig.getInstance().getRand().nextDouble() < pior) {
			return question.getCorrectAnswer();
		} else
			return wrongAnswer(question.getCorrectAnswer());
	}

	@Override
	public Characteristic clone() throws CloneNotSupportedException {
		TruthFulWorker clone = new TruthFulWorker();
		clone.setSpecificity(this.getSpecificity());
		clone.setSensitivity(this.getSensitivity());
		clone.setReliableDegree(this.getReliableDegree());
		return clone;
	}

}
