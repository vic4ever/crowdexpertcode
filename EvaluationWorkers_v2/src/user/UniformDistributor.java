package user;

import java.util.List;

import org.apache.commons.math3.distribution.UniformRealDistribution;

import config.Mainconfig;

/**
 * @author lamtran3101 
 * <h4>Description</h4>
 * This class uses Uniform distribute function from math3
 * library. It has two parameters: lowBound and upBound. 
 * <ul>
 * <li>lowBound : the minimum reliability that can assign to a worker. </li>
 * <li>upBound : the maximum reliability that can assign to a worker.</li>
 * </ul>
 */
public class UniformDistributor extends WorkerDistributor {

	private double lowBound = 0;
	private double upBound = 1;

	public UniformDistributor() {
		lowBound = Double.parseDouble(Mainconfig.getInstance().getListConfig()
				.get("lowBound"));
		upBound = Double.parseDouble(Mainconfig.getInstance().getListConfig()
				.get("upBound"));
	}

	public UniformDistributor(double lowBound, double upBound) {
		super();
		this.lowBound = lowBound;
		this.upBound = upBound;
	}

	@Override
	public void distribute(List<Worker> workers, int pos) {
		UniformRealDistribution gen = new UniformRealDistribution(lowBound,
				upBound);
		for (Worker worker : workers) {
			worker.getChar().setDegreeNum(gen.sample(), pos);
		}

	}

}
