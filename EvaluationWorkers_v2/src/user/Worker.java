package user;

import java.util.HashMap;
import java.util.Map;

import question.Question;
import utility.IDGenerator;
import feedback.FeedBack;

/**
 * @author lamtran3101 <h4>Description</h4> Each worker has a unique ID, his
 *         characteristic and his estimate characteristic which is judged by one
 *         specific algorithm.Besides, there's a list of answers that this user
 *         answered so far.
 */
public class Worker {
	private int WID;
	private Characteristic c;
	private Characteristic estc;
	private Map<String, FeedBack> listAnswers;

	public Worker() {
		WID = IDGenerator.nextWorkerID();
		listAnswers = new HashMap<>();
	}

	/**
	 * how one worker answer a question, the output of this function is a
	 * feedback contain information about his worker, his question and answer.
	 * 
	 * @param question
	 * @return
	 */
	public FeedBack generateAnswer(Question question) {
		FeedBack feedback = this.getChar().answer(question, this);

		question.addWorker(feedback);
		listAnswers.put(question.getCorrespondence().getValue(), feedback);
		return feedback;
	}

	public FeedBack getAnswer(Question question) {
		return listAnswers.get(question.getCorrespondence().getValue());
	}

	public Map<String, FeedBack> getListAnswers() {
		return this.listAnswers;
	}

	public int getWID() {
		return WID;
	}

	public void setChar(Characteristic n) {
		c = n;
	}

	public void setEstChar(Characteristic n) {
		estc = n;
	}

	// Facade
	public double getReliability() {
		return c.getReliableDegree();
	}

	public double getSensitivity() {
		return c.getSensitivity();
	}

	public double getSpecificity() {
		return c.getSpecificity();
	}

	public Characteristic getChar() {
		return c;
	}

	public Characteristic getEstChar() {
		return estc;
	}

	/**
	 * Clone a new worker with a new id, other information is the same as
	 * original worker
	 */
	public Worker clone() throws CloneNotSupportedException {
		Worker clone = new Worker();
		if (this.c != null)
			clone.c = this.c.clone();
		if (this.estc != null)
			clone.estc = this.estc.clone();
		if (this.listAnswers != null) {
			clone.listAnswers = new HashMap<String, FeedBack>();
			for (String key : this.listAnswers.keySet()) {
				FeedBack fb = this.listAnswers.get(key).clone();
				fb.setWorker(clone);
				Question q = fb.getQuestion();
				q.addWorker(fb);
				clone.listAnswers.put(key, fb);
			}
		}
		return clone;
	}

	public void setWID(int wID) {
		WID = wID;
	}

	public void display() {
		System.out.println("Worker: " + WID + " is a "
				+ this.getChar().getClassName());

		for (FeedBack f : listAnswers.values()) {
			Question q = f.getQuestion();
			System.out.println("Question: " + q.getCorrespondence().getValue()
					+ " is " + q.getCorrespondence().getGroundTruth());
			q.displayWorkers();
			System.out.println("Answer: " + f.getAnswer());
			System.out.println("----------------");
		}
	}
}
