graph [

  directed 0

  node [
    id 1
    label "{R5, R9}    {a1b2, b2c1, a2b2, a2c2, a2c1}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 2
    label "{R2, R4, R9}    {a1b1, a1b2, a2b1, b2c1, a2b2, a2c1}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 3
    label "{R3, R4, R8}    {a1b1, a1c1, b2c1, a2b2, a2c1}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  edge [
    source 1
    target 2
  ]

  edge [
    source 2
    target 3
  ]

]
