graph [

  directed 0

  node [
    id 1
    label "{r23, r54, r64, r75, r99, r220, r276, r587}    {c15, c16, c32, c36, c37, c38, c65, c66, c68, c69, c70, c75, c110, c119, c121, c122, c132, c143, c154, c162}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 2
    label "{r343}    {c222, c223}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 3
    label "{r730}    {c252, c253}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 4
    label "{r835}    {c256, c257}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 5
    label "{r733}    {c179, c254}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 6
    label "{r146}    {c178, c179}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 7
    label "{r1026}    {c269, c270}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 8
    label "{r488}    {c232, c237}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 9
    label "{r403}    {c231, c232}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 10
    label "{r475}    {c210, c236}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 11
    label "{r493}    {c210, c239}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 12
    label "{r252}    {c209, c210}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 13
    label "{r990}    {c212, c265}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 14
    label "{r271}    {c211, c212}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 15
    label "{r880}    {c136, c212}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 16
    label "{r670}    {c136, c250}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 17
    label "{r823}    {c136, c255}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 18
    label "{r523}    {c136, c241}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 19
    label "{r68}    {c135, c136}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 20
    label "{r514}    {c212, c240}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 21
    label "{r729}    {c212, c251}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 22
    label "{r938}    {c261, c262}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 23
    label "{r565}    {c244, c245}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 24
    label "{r312, r330}    {c216, c217, c218, c221}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 25
    label "{r312, r1045}    {c185, c216, c218, c221}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 26
    label "{r312, r1069}    {c184, c185, c218, c226}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 27
    label "{r159, r312}    {c183, c184, c185, c218}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 28
    label "{r316, r577}    {c46, c47, c219, c246}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 29
    label "{r666}    {c219, c249}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 30
    label "{r963}    {c219, c264}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 31
    label "{r1017}    {c219, c267}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 32
    label "{r896}    {c219, c259}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 33
    label "{r1018}    {c219, c268}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 34
    label "{r582}    {c219, c247}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 35
    label "{r906}    {c219, c260}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 36
    label "{r15}    {c45, c46, c47}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 37
    label "{r400}    {c47, c219, c230}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 38
    label "{r419, r577}    {c46, c233, c246}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 39
    label "{r1015}    {c62, c266}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 40
    label "{r492}    {c62, c229, c238}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 41
    label "{r591}    {c229, c248}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 42
    label "{r397}    {c228, c229}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 43
    label "{r839}    {c229, c258}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 44
    label "{r524, r528}    {c62, c229, c242, c243}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 45
    label "{r524, r1049}    {c62, c225, c242, c243}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 46
    label "{r20, r358}    {c60, c61, c62, c225}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 47
    label "{r947}    {c60, c263}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 48
    label "{r158, r760, r772, r806, r814, r943, r1067}    {c17, c18, c26, c28, c29, c33, c34, c96, c97, c98, c120, c144, c149, c172, c202, c205, c235}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 49
    label "{r158, r185, r209, r434, r456, r532, r814}    {c17, c18, c25, c26, c28, c29, c33, c34, c96, c98, c120, c144, c149, c172, c187, c202, c205, c235}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 50
    label "{r158, r164, r197, r209, r415, r434, r532, r921}    {c17, c18, c25, c26, c28, c29, c33, c34, c96, c98, c108, c109, c144, c149, c172, c187, c200, c202, c205, c235}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 51
    label "{r158, r164, r185}    {c17, c18, c33, c96, c98, c109, c144, c187, c198}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 52
    label "{r11, r158, r383}    {c17, c33, c109, c144, c198, c227}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 53
    label "{r39, r76, r209, r235, r244, r456}    {c26, c28, c29, c33, c34, c107, c108, c109, c144, c149, c200, c202, c205, c207, c235}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 54
    label "{r39, r949}    {c26, c28, c29, c107, c214}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 55
    label "{r8, r150, r244, r456}    {c26, c29, c33, c34, c107, c108, c149, c181, c200, c235}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 56
    label "{r8, r79, r150, r1065}    {c26, c29, c33, c34, c149, c181, c220}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 57
    label "{r9, r11, r39, r79}    {c26, c27, c28, c29, c34, c107, c149}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 58
    label "{r3, r6, r24, r31, r47, r60, r96, r314, r426}    {c6, c8, c9, c10, c11, c12, c19, c20, c21, c22, c23, c24, c39, c40, c53, c54, c55, c58, c71, c72, c73, c77, c87, c88, c99, c100, c112, c113, c116, c117, c118, c134, c151, c160}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 59
    label "{r3, r26, r34, r135, r382}    {c6, c10, c12, c22, c58, c73, c77, c87, c88, c113, c134, c195}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 60
    label "{r3, r7, r26, r34, r66, r135}    {c6, c10, c12, c22, c23, c58, c73, c77, c87, c88, c95, c113, c134}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 61
    label "{r17, r43, r58, r173, r284, r426, r432, r599, r673, r1029}    {c6, c7, c8, c10, c11, c12, c19, c20, c21, c22, c23, c39, c40, c52, c53, c54, c55, c58, c72, c73, c77, c87, c88, c99, c100, c101, c112, c113, c116, c117, c118, c134, c151, c160}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 62
    label "{r6, r7, r17, r19, r36, r48, r58, r237, r599, r824}    {c6, c7, c8, c10, c11, c12, c19, c20, c21, c22, c23, c39, c40, c52, c53, c54, c55, c57, c58, c72, c73, c77, c87, c88, c99, c100, c101, c112, c113, c116, c117, c134, c151, c160}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 63
    label "{r7, r17, r18, r24, r36, r48, r237, r273}    {c6, c8, c10, c11, c12, c22, c23, c40, c52, c53, c54, c55, c56, c57, c58, c72, c73, c77, c87, c88, c99, c100, c113, c116, c117, c134}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 64
    label "{r24, r31, r42, r269, r387}    {c8, c12, c52, c53, c55, c56, c72, c88, c100, c113, c213}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 65
    label "{r7, r17, r24, r36, r48, r237, r273, r853}    {c6, c8, c10, c11, c12, c22, c23, c53, c54, c56, c58, c72, c73, c77, c87, c88, c99, c100, c113, c114, c116, c117, c134}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 66
    label "{r24, r273, r556, r715, r853, r950}    {c6, c10, c11, c12, c22, c23, c54, c56, c58, c72, c73, c77, c87, c88, c113, c114, c116, c117, c133, c134}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 67
    label "{r370, r381, r615, r698, r715, r853}    {c10, c11, c12, c22, c23, c54, c56, c58, c72, c73, c77, c113, c114, c116, c117, c133, c175}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 68
    label "{r163, r169, r359}    {c11, c12, c58, c73, c77, c114, c116, c175, c189}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 69
    label "{r7, r58, r163, r359}    {c11, c12, c23, c58, c73, c77, c114, c116, c175, c186}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 70
    label "{r2, r58, r61, r217, r356, r613, r624}    {c5, c6, c7, c8, c10, c11, c12, c19, c20, c21, c22, c39, c40, c52, c53, c57, c58, c73, c87, c99, c100, c101, c112, c113, c116, c151, c160}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 71
    label "{r2, r13, r48, r52, r81, r87, r356, r615}    {c5, c6, c7, c10, c11, c12, c19, c20, c21, c22, c39, c40, c52, c53, c57, c58, c73, c87, c99, c100, c101, c112, c116, c125, c151, c160}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 72
    label "{r6, r19, r61, r87, r372, r754, r1071}    {c6, c7, c10, c11, c19, c20, c21, c22, c39, c40, c52, c53, c57, c58, c73, c87, c99, c100, c124, c125, c151, c160}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 73
    label "{r6, r19, r52, r57, r87, r283, r664}    {c6, c7, c10, c11, c20, c21, c22, c39, c40, c53, c57, c58, c59, c73, c87, c124, c125, c151}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 74
    label "{r13, r57, r81, r522, r594}    {c7, c20, c21, c39, c40, c53, c59, c73, c124, c125, c151, c204}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 75
    label "{r90, r129, r433, r438, r706, r1062}    {c2, c3, c4, c43, c44, c79, c82, c85, c92, c93, c94, c106, c111, c123, c126, c146, c147, c148, c152, c164, c166, c171, c173, c208, c234}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 76
    label "{r129, r224, r606, r689, r706, r787, r826, r902, r989}    {c2, c3, c4, c43, c44, c79, c80, c81, c82, c85, c89, c90, c91, c92, c93, c94, c102, c103, c106, c111, c126, c127, c137, c138, c146, c147, c148, c152, c164, c166, c171, c173, c206, c208, c234}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 77
    label "{r1, r14, r93, r129, r781}    {c2, c3, c4, c44, c90, c91, c106, c126, c148, c171, c173, c199}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 78
    label "{r14, r22, r78, r93, r129, r204}    {c2, c3, c4, c44, c67, c90, c91, c106, c126, c146, c148, c171, c173}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 79
    label "{r28, r30, r32, r33, r59, r78, r129, r218}    {c2, c3, c4, c43, c44, c79, c80, c81, c82, c85, c86, c89, c90, c91, c93, c94, c103, c106, c126, c146, c147, c148, c164, c166, c171, c173}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 80
    label "{r27, r30, r78, r145, r224}    {c44, c79, c81, c84, c85, c86, c89, c93, c103, c148, c171}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 81
    label "{r27, r32, r38, r78, r90, r129, r465, r973}    {c2, c3, c4, c42, c43, c44, c79, c86, c89, c90, c91, c93, c94, c103, c106, c126, c146, c147, c148, c164, c166, c171, c173}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 82
    label "{r38, r78, r90, r93, r129, r973}    {c1, c2, c3, c4, c42, c43, c44, c86, c90, c91, c93, c106, c126, c146, c147, c148, c164, c166, c171, c173}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 83
    label "{r1, r59, r78, r205, r973, r1030}    {c1, c2, c3, c42, c43, c44, c86, c93, c106, c126, c146, c147, c148, c164, c165, c166, c173}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 84
    label "{r14, r176, r190}    {c2, c41, c42, c43, c44, c126, c164, c165, c173}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 85
    label "{r14, r78, r190, r265}    {c2, c42, c43, c44, c126, c146, c164, c165, c173, c174}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 86
    label "{r69, r116, r348, r373, r533, r603, r736, r919, r998}    {c2, c3, c4, c43, c44, c79, c80, c81, c82, c89, c91, c92, c94, c102, c103, c104, c106, c111, c126, c127, c137, c138, c148, c152, c164, c166, c171, c206, c208, c234}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 87
    label "{r69, r116, r310, r348, r468, r533, r818, r919}    {c2, c3, c4, c43, c44, c79, c80, c81, c82, c89, c91, c92, c102, c103, c104, c106, c126, c127, c137, c138, c148, c152, c164, c170, c171, c206, c208, c234}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 88
    label "{r37, r116, r141, r310, r442, r487, r850, r931}    {c2, c3, c4, c43, c44, c79, c80, c82, c89, c91, c92, c102, c103, c104, c106, c126, c127, c137, c138, c148, c152, c164, c170, c201, c206, c208}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 89
    label "{r116, r487, r586, r850, r917, r919, r931}    {c2, c3, c4, c43, c44, c79, c80, c82, c91, c92, c102, c104, c106, c126, c127, c137, c138, c152, c164, c170, c194, c201, c208}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 90
    label "{r37, r38, r110, r116, r120, r196, r487}    {c2, c3, c4, c43, c78, c79, c80, c82, c91, c92, c102, c106, c126, c127, c138, c152, c170, c194}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 91
    label "{r32, r37, r306, r340, r1027}    {c2, c78, c79, c82, c92, c102, c127, c138, c152, c170, c194, c215}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 92
    label "{r38, r115, r205, r607}    {c2, c3, c4, c43, c44, c82, c93, c105, c106, c126, c147, c148, c173}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 93
    label "{r12, r16, r99, r222, r448, r467, r955}    {c14, c15, c16, c30, c32, c35, c36, c37, c38, c48, c49, c65, c66, c69, c70, c74, c75, c110, c122, c132, c143, c150, c157, c158, c161, c162}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 94
    label "{r396, r401, r437, r448, r629, r663, r671, r707, r867}    {c14, c15, c16, c30, c32, c35, c37, c38, c48, c49, c50, c51, c65, c66, c69, c70, c74, c75, c83, c110, c122, c132, c140, c141, c143, c150, c153, c157, c158, c161, c162, c167, c190}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 95
    label "{r21, r46, r70, r74, r327}    {c15, c32, c37, c38, c66, c74, c75, c83, c110, c122, c140, c176}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 96
    label "{r21, r46, r70, r74, r88}    {c15, c32, c37, c38, c66, c74, c75, c83, c110, c122, c140, c156}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 97
    label "{r21, r25, r46, r54, r182}    {c15, c32, c37, c38, c66, c74, c75, c83, c110, c122, c140, c169}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 98
    label "{r21, r46, r70, r74, r86}    {c15, c32, c37, c38, c66, c74, c75, c83, c110, c122, c140, c155}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 99
    label "{r10, r12, r21, r25, r46, r54, r92}    {c15, c30, c31, c32, c35, c37, c38, c66, c74, c75, c83, c110, c122, c140}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 100
    label "{r12, r147, r347, r437, r567, r587, r601, r663, r1068}    {c13, c14, c15, c16, c30, c32, c35, c37, c38, c48, c49, c50, c51, c65, c66, c69, c70, c74, c75, c83, c110, c122, c132, c140, c143, c153, c157, c161, c162, c167, c168, c190}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 101
    label "{r4, r92, r179, r494}    {c15, c65, c66, c70, c75, c83, c110, c140, c168, c197}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 102
    label "{r4, r92, r179, r226}    {c15, c65, c66, c70, c75, c83, c110, c140, c168, c196}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 103
    label "{r144, r226, r376, r396, r508, r737, r946, r1028}    {c13, c14, c15, c30, c32, c35, c37, c38, c48, c49, c50, c65, c66, c69, c70, c74, c75, c83, c110, c122, c132, c140, c153, c157, c167, c168, c203}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 104
    label "{r29, r83, r144, r226, r878}    {c13, c14, c32, c50, c66, c69, c74, c153, c167, c180, c203}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 105
    label "{r12, r64, r70, r74, r347, r448, r467, r900}    {c14, c15, c30, c32, c35, c37, c38, c48, c49, c50, c64, c65, c66, c69, c74, c75, c83, c110, c122, c132, c140, c153, c157, c203}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 106
    label "{r10, r12, r64, r276, r291, r508, r900}    {c15, c30, c32, c35, c37, c38, c48, c64, c65, c66, c69, c74, c75, c83, c110, c115, c122, c132, c140, c157, c203}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 107
    label "{r89, r497, r663, r793, r832, r900}    {c30, c32, c35, c37, c38, c48, c64, c65, c66, c69, c75, c110, c115, c122, c132, c157, c182, c203}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 108
    label "{r12, r100, r143, r152}    {c35, c64, c65, c66, c75, c110, c122, c157, c177, c182}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 109
    label "{r10, r21, r152, r719}    {c30, c64, c65, c66, c75, c110, c122, c157, c159, c182}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 110
    label "{r10, r100, r152, r855}    {c30, c64, c65, c66, c75, c110, c122, c157, c182, c224}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 111
    label "{r10, r21, r100, r152}    {c30, c63, c64, c65, c66, c75, c110, c122, c157, c182}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 112
    label "{r72, r144, r448, r567, r680, r808, r957, r1052}    {c13, c14, c15, c16, c32, c37, c38, c48, c49, c50, c51, c65, c66, c69, c70, c74, c75, c83, c110, c122, c128, c129, c140, c143, c153, c157, c161, c162, c168, c190}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 113
    label "{r534, r540, r561, r567, r690, r707, r1038, r1052}    {c13, c14, c15, c16, c32, c37, c38, c49, c50, c51, c65, c66, c69, c70, c74, c75, c83, c110, c128, c129, c140, c143, c145, c153, c157, c161, c162, c168, c190}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 114
    label "{r80, r369, r680, r1025, r1057}    {c14, c15, c38, c50, c51, c65, c66, c70, c75, c110, c140, c145, c153, c163}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 115
    label "{r62, r83, r144, r437, r448, r467, r473, r567, r1057}    {c13, c14, c15, c16, c32, c37, c38, c49, c50, c51, c65, c66, c69, c70, c74, c75, c76, c83, c110, c128, c129, c130, c140, c142, c143, c145, c153, c157, c161, c168, c190}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 116
    label "{r62, r77, r83, r207, r249, r437, r448, r473, r490, r567}    {c14, c15, c16, c32, c37, c38, c49, c50, c51, c65, c66, c69, c70, c74, c75, c76, c83, c110, c128, c129, c130, c140, c142, c143, c145, c153, c157, c161, c168, c190, c193}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 117
    label "{r44, r62, r72, r75, r172, r385, r448, r473, r567, r680}    {c14, c15, c16, c32, c37, c38, c50, c51, c65, c66, c70, c75, c76, c83, c110, c128, c129, c130, c131, c140, c142, c143, c145, c153, c157, c161, c168, c190, c193}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 118
    label "{r12, r519, r521, r567, r680, r750, r857}    {c14, c15, c16, c37, c38, c51, c65, c70, c75, c76, c83, c110, c130, c131, c140, c143, c145, c153, c168, c190, c192}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 119
    label "{r16, r239, r680, r1004}    {c15, c38, c51, c75, c83, c130, c140, c145, c153, c188, c192}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 120
    label "{r4, r63, r172, r174, r546}    {c14, c16, c70, c76, c110, c130, c131, c143, c145, c190, c191, c192}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 121
    label "{r4, r62, r63, r70, r77, r519}    {c14, c16, c32, c38, c50, c65, c76, c128, c131, c139, c140, c145, c153, c190}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  edge [
    source 1
    target 2
  ]

  edge [
    source 1
    target 3
  ]

  edge [
    source 1
    target 4
  ]

  edge [
    source 1
    target 5
  ]

  edge [
    source 5
    target 6
  ]

  edge [
    source 1
    target 7
  ]

  edge [
    source 1
    target 8
  ]

  edge [
    source 8
    target 9
  ]

  edge [
    source 1
    target 10
  ]

  edge [
    source 10
    target 11
  ]

  edge [
    source 10
    target 12
  ]

  edge [
    source 1
    target 13
  ]

  edge [
    source 13
    target 14
  ]

  edge [
    source 13
    target 15
  ]

  edge [
    source 15
    target 16
  ]

  edge [
    source 15
    target 17
  ]

  edge [
    source 15
    target 18
  ]

  edge [
    source 15
    target 19
  ]

  edge [
    source 13
    target 20
  ]

  edge [
    source 13
    target 21
  ]

  edge [
    source 1
    target 22
  ]

  edge [
    source 1
    target 23
  ]

  edge [
    source 1
    target 24
  ]

  edge [
    source 24
    target 25
  ]

  edge [
    source 25
    target 26
  ]

  edge [
    source 26
    target 27
  ]

  edge [
    source 1
    target 28
  ]

  edge [
    source 28
    target 29
  ]

  edge [
    source 28
    target 30
  ]

  edge [
    source 28
    target 31
  ]

  edge [
    source 28
    target 32
  ]

  edge [
    source 28
    target 33
  ]

  edge [
    source 28
    target 34
  ]

  edge [
    source 28
    target 35
  ]

  edge [
    source 28
    target 36
  ]

  edge [
    source 28
    target 37
  ]

  edge [
    source 28
    target 38
  ]

  edge [
    source 1
    target 39
  ]

  edge [
    source 39
    target 40
  ]

  edge [
    source 40
    target 41
  ]

  edge [
    source 40
    target 42
  ]

  edge [
    source 40
    target 43
  ]

  edge [
    source 40
    target 44
  ]

  edge [
    source 44
    target 45
  ]

  edge [
    source 45
    target 46
  ]

  edge [
    source 46
    target 47
  ]

  edge [
    source 1
    target 48
  ]

  edge [
    source 48
    target 49
  ]

  edge [
    source 49
    target 50
  ]

  edge [
    source 50
    target 51
  ]

  edge [
    source 51
    target 52
  ]

  edge [
    source 50
    target 53
  ]

  edge [
    source 53
    target 54
  ]

  edge [
    source 53
    target 55
  ]

  edge [
    source 55
    target 56
  ]

  edge [
    source 53
    target 57
  ]

  edge [
    source 1
    target 58
  ]

  edge [
    source 58
    target 59
  ]

  edge [
    source 58
    target 60
  ]

  edge [
    source 58
    target 61
  ]

  edge [
    source 61
    target 62
  ]

  edge [
    source 62
    target 63
  ]

  edge [
    source 63
    target 64
  ]

  edge [
    source 63
    target 65
  ]

  edge [
    source 65
    target 66
  ]

  edge [
    source 66
    target 67
  ]

  edge [
    source 67
    target 68
  ]

  edge [
    source 67
    target 69
  ]

  edge [
    source 62
    target 70
  ]

  edge [
    source 70
    target 71
  ]

  edge [
    source 71
    target 72
  ]

  edge [
    source 72
    target 73
  ]

  edge [
    source 73
    target 74
  ]

  edge [
    source 1
    target 75
  ]

  edge [
    source 75
    target 76
  ]

  edge [
    source 76
    target 77
  ]

  edge [
    source 76
    target 78
  ]

  edge [
    source 76
    target 79
  ]

  edge [
    source 79
    target 80
  ]

  edge [
    source 79
    target 81
  ]

  edge [
    source 81
    target 82
  ]

  edge [
    source 82
    target 83
  ]

  edge [
    source 83
    target 84
  ]

  edge [
    source 83
    target 85
  ]

  edge [
    source 76
    target 86
  ]

  edge [
    source 86
    target 87
  ]

  edge [
    source 87
    target 88
  ]

  edge [
    source 88
    target 89
  ]

  edge [
    source 89
    target 90
  ]

  edge [
    source 90
    target 91
  ]

  edge [
    source 75
    target 92
  ]

  edge [
    source 1
    target 93
  ]

  edge [
    source 93
    target 94
  ]

  edge [
    source 94
    target 95
  ]

  edge [
    source 94
    target 96
  ]

  edge [
    source 94
    target 97
  ]

  edge [
    source 94
    target 98
  ]

  edge [
    source 94
    target 99
  ]

  edge [
    source 94
    target 100
  ]

  edge [
    source 100
    target 101
  ]

  edge [
    source 100
    target 102
  ]

  edge [
    source 100
    target 103
  ]

  edge [
    source 103
    target 104
  ]

  edge [
    source 103
    target 105
  ]

  edge [
    source 105
    target 106
  ]

  edge [
    source 106
    target 107
  ]

  edge [
    source 107
    target 108
  ]

  edge [
    source 107
    target 109
  ]

  edge [
    source 107
    target 110
  ]

  edge [
    source 107
    target 111
  ]

  edge [
    source 100
    target 112
  ]

  edge [
    source 112
    target 113
  ]

  edge [
    source 113
    target 114
  ]

  edge [
    source 113
    target 115
  ]

  edge [
    source 115
    target 116
  ]

  edge [
    source 116
    target 117
  ]

  edge [
    source 117
    target 118
  ]

  edge [
    source 118
    target 119
  ]

  edge [
    source 118
    target 120
  ]

  edge [
    source 117
    target 121
  ]

]
