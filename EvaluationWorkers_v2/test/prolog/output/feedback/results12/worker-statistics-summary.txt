Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	58.4%	9%	17%	10	0
user1	46.27%	1%	7%	10	0
user10	66.11%	18%	32%	8	0
user11	15.93%	47%	68%	6	0
user12	50.0%	---	---	3	0
user13	50.0%	---	---	3	0
user14	50.0%	---	---	3	0
user15	9.89%	67%	80%	3	0
user2	50.0%	---	---	10	0
user3	26.94%	29%	46%	10	0
user4	57.84%	8%	16%	10	0
user5	14.77%	54%	70%	10	0
user6	26.03%	31%	48%	10	0
user7	62.61%	7%	25%	10	0
user8	61.23%	5%	22%	10	0
user9	25.79%	32%	48%	10	0
