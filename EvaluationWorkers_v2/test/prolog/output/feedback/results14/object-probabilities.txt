Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CityName-CORR-MDM.Record.Addresses.City	0.98908	0.01092	
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.FloorID-CORR-MDM.Record.Addresses.Floor	0.0111	0.9889	
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressDataKey.AddressKeyGUID-CORR-SRM.PartnerInformation.AddressInformation.Address.AddressDataKey.AddressKeyGUID	0.00102	0.99898	
1-TO-0-attrib-MDM.Record.MainAddress.City-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CityName	3.5E-4	0.99965	
1-TO-2-attrib-MDM.Record.Addresses.Floor-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.FloorID	0.99067	0.00933	
1-TO-2-attrib-MDM.Record.MainAddress.Building-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.BuildingID	0.99067	0.00933	
1-TO-2-attrib-MDM.Record.MainAddress.FaxCompleteNumber-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CompleteFacsimileNumber	0.98908	0.01092	
2-TO-0-attrib-SRM.PartnerInformation.AddressInformation.Address.AddressDataKey.AddressKeyGUID-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressDataKey.AddressKeyGUID	0.00113	0.99887	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.FloorID-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.FloorID	0.97927	0.02073	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.BuildingID-CORR-MDM.Record.Addresses.Building	0.98908	0.01092	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CompleteFacsimileNumber-CORR-MDM.Record.Addresses.FaxCompleteNumber	0.98908	0.01092	
