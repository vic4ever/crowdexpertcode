Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CountryCode-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CountryCode	0.00111	0.99889	
0-TO-2-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.AcademicTitleCode1-CORR-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.PersonData.AcademicTitleCode1	0.98299	0.01701	
0-TO-2-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.GivenName-CORR-SRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.GivenName	0.9996	4.0E-4	
1-TO-0-attrib-MDM.Record.MainAddress.Country-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CountryCode	0.99936	6.4E-4	
1-TO-2-attrib-MDM.Record.Addresses.Building-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.BuildingID	4.7E-4	0.99953	
1-TO-2-attrib-MDM.Record.PartnerTypeCode-CORR-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.CentralData.PartnerTypeCode	0.9986	0.0014	
2-TO-0-attrib-SRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.GivenName-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.GivenName	0.99945	5.5E-4	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.PersonData.AcademicTitleCode1-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.AcademicTitleCode1	0.99112	0.00888	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.BuildingID-CORR-MDM.Record.Addresses.Building	0.9996	4.0E-4	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CountryCode-CORR-MDM.Record.Addresses.Country	7.7E-4	0.99923	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.CentralData.PartnerTypeCode-CORR-MDM.Record.PartnerTypeCode	0.00173	0.99827	
