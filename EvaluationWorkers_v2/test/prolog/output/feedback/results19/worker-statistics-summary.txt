Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	61.7%	9%	23%	10	0
user1	32.91%	12%	34%	10	0
user10	50.0%	---	---	2	0
user11	88.79%	60%	78%	2	0
user2	31.47%	19%	37%	10	0
user3	54.77%	1%	10%	10	0
user4	47.13%	1%	6%	10	0
user5	28.1%	20%	44%	8	0
user6	90.24%	66%	80%	8	0
user7	34.2%	19%	32%	8	0
user8	50.0%	---	---	5	0
user9	72.25%	25%	45%	3	0
