Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	25.44%	28%	49%	10	0
user1	11.31%	63%	77%	10	0
user10	99.96%	100%	100%	3	0
user2	21.34%	33%	57%	10	0
user3	41.49%	3%	17%	10	0
user4	68.35%	14%	37%	10	0
user5	24.84%	28%	50%	7	0
user6	50.0%	---	---	2	0
user7	100.0%	100%	100%	3	0
user8	50.0%	---	---	2	0
user9	100.0%	100%	100%	2	0
