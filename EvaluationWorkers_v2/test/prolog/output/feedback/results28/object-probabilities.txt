Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.CommunicationInformation.SMTPInformation.SMTP.Contact.ContactData.EMailAddress-CORR-MDM.Record.Addresses.EMailAddress	0.01139	0.98861	
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CountryCode-CORR-MDM.Record.Addresses.Country	0.99012	0.00988	
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID-CORR-MDM.Record.Addresses.HouseNumber	0.99808	0.00192	
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode-CORR-MDM.Record.Addresses.Region	0.98988	0.01012	
0-TO-2-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.NationalityCode-CORR-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.PersonData.NationalityCode	0.00268	0.99732	
1-TO-0-attrib-MDM.Record.MainAddress.Country-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CountryCode	0.98848	0.01152	
1-TO-0-attrib-MDM.Record.MainAddress.EMailAddress-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.CommunicationInformation.SMTPInformation.SMTP.Contact.ContactData.EMailAddress	0.99382	0.00618	
1-TO-2-attrib-MDM.Record.Addresses.Region-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode	0.05416	0.94584	
1-TO-2-attrib-MDM.Record.MainAddress.HouseNumber-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID	0.00661	0.99339	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID	0.01354	0.98646	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode	0.99481	0.00519	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.PersonData.NationalityCode-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.NationalityCode	0.02175	0.97825	
