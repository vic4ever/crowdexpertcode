Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	43.48%	2%	13%	12	0
user1	54.05%	1%	8%	12	0
user2	27.3%	23%	45%	12	0
user3	43.24%	3%	14%	12	0
user4	33.21%	13%	34%	12	0
user5	54.3%	3%	9%	12	0
user6	65.37%	15%	31%	12	0
user7	41.02%	4%	18%	11	0
