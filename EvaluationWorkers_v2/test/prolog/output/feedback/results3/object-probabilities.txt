Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.FloorID-CORR-MDM.Record.Addresses.Floor	0.98628	0.01372	
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode-CORR-MDM.Record.Addresses.Region	0.0246	0.9754	
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.StreetName-CORR-MDM.Record.Addresses.Street	0.0922	0.9078	
0-TO-1-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.BirthName-CORR-MDM.Record.BirthName	0.0246	0.9754	
0-TO-2-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.ControlData.NumberRangeGroupCode-CORR-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.ControlData.NumberRangeGroupCode	3.0E-5	0.99997	
1-TO-0-attrib-MDM.Record.Addresses.Region-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode	0.99656	0.00344	
1-TO-0-attrib-MDM.Record.BirthName-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.BirthName	0.92094	0.07906	
1-TO-0-attrib-MDM.Record.MainAddress.Floor-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.FloorID	0.98435	0.01565	
1-TO-2-attrib-MDM.Record.MainAddress.Street-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.StreetName	0.92094	0.07906	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.StreetName-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.StreetName	0.99656	0.00344	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.ControlData.NumberRangeGroupCode-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.ControlData.NumberRangeGroupCode	3.0E-5	0.99997	
