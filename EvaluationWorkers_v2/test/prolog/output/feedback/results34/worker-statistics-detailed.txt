Worker: user0
Error Rate: 69.98%
Quality (Expected): 23%
Quality (Optimized): 40%
Number of Annotations: 12
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=58.043%	P[yes->no]=41.957%	
P[no->yes]=98.004%	P[no->no]=1.996%	

Worker: user1
Error Rate: 28.52%
Quality (Expected): 20%
Quality (Optimized): 43%
Number of Annotations: 12
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=84.897%	P[yes->no]=15.103%	
P[no->yes]=41.947%	P[no->no]=58.053%	

Worker: user10
Error Rate: 34.43%
Quality (Expected): 17%
Quality (Optimized): 31%
Number of Annotations: 5
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=32.798%	P[yes->no]=67.202%	
P[no->yes]=1.659%	P[no->no]=98.341%	

Worker: user11
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user12
Error Rate: 89.51%
Quality (Expected): 63%
Quality (Optimized): 79%
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=7.324%	P[yes->no]=92.676%	
P[no->yes]=86.339%	P[no->no]=13.661%	

Worker: user2
Error Rate: 40.7%
Quality (Expected): 10%
Quality (Optimized): 19%
Number of Annotations: 12
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.49%	P[yes->no]=0.51%	
P[no->yes]=80.886%	P[no->no]=19.114%	

Worker: user3
Error Rate: 29.25%
Quality (Expected): 19%
Quality (Optimized): 41%
Number of Annotations: 12
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=84.257%	P[yes->no]=15.743%	
P[no->yes]=42.76%	P[no->no]=57.24%	

Worker: user4
Error Rate: 61.62%
Quality (Expected): 6%
Quality (Optimized): 23%
Number of Annotations: 12
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=56.906%	P[yes->no]=43.094%	
P[no->yes]=80.148%	P[no->no]=19.852%	

Worker: user5
Error Rate: 23.14%
Quality (Expected): 29%
Quality (Optimized): 54%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=83.813%	P[yes->no]=16.187%	
P[no->yes]=30.095%	P[no->no]=69.905%	

Worker: user6
Error Rate: 61.76%
Quality (Expected): 11%
Quality (Optimized): 24%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=72.927%	P[yes->no]=27.073%	
P[no->yes]=96.445%	P[no->no]=3.555%	

Worker: user7
Error Rate: 37.09%
Quality (Expected): 12%
Quality (Optimized): 26%
Number of Annotations: 7
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=96.683%	P[yes->no]=3.317%	
P[no->yes]=70.861%	P[no->no]=29.139%	

Worker: user8
Error Rate: 6.49%
Quality (Expected): 76%
Quality (Optimized): 87%
Number of Annotations: 5
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=97.659%	P[yes->no]=2.341%	
P[no->yes]=10.633%	P[no->no]=89.367%	

Worker: user9
Error Rate: 41.45%
Quality (Expected): 3%
Quality (Optimized): 17%
Number of Annotations: 5
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=67.066%	P[yes->no]=32.934%	
P[no->yes]=49.972%	P[no->no]=50.028%	

