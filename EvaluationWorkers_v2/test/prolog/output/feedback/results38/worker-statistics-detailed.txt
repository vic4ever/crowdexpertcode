Worker: user0
Error Rate: 38.21%
Quality (Expected): 13%
Quality (Optimized): 24%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.389%	P[yes->no]=0.611%	
P[no->yes]=75.805%	P[no->no]=24.195%	

Worker: user1
Error Rate: 73.92%
Quality (Expected): 27%
Quality (Optimized): 48%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=45.571%	P[yes->no]=54.429%	
P[no->yes]=93.42%	P[no->no]=6.58%	

Worker: user2
Error Rate: 15.3%
Quality (Expected): 52%
Quality (Optimized): 69%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=97.866%	P[yes->no]=2.134%	
P[no->yes]=28.465%	P[no->no]=71.535%	

Worker: user3
Error Rate: 58.96%
Quality (Expected): 4%
Quality (Optimized): 18%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=57.086%	P[yes->no]=42.914%	
P[no->yes]=75.0%	P[no->no]=25.0%	

Worker: user4
Error Rate: 58.82%
Quality (Expected): 6%
Quality (Optimized): 18%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=74.352%	P[yes->no]=25.648%	
P[no->yes]=91.998%	P[no->no]=8.002%	

Worker: user5
Error Rate: 38.38%
Quality (Expected): 13%
Quality (Optimized): 23%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.625%	P[yes->no]=0.375%	
P[no->yes]=76.393%	P[no->no]=23.607%	

Worker: user6
Error Rate: 15.51%
Quality (Expected): 48%
Quality (Optimized): 69%
Number of Annotations: 8
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=79.811%	P[yes->no]=20.189%	
P[no->yes]=10.841%	P[no->no]=89.159%	

Worker: user7
Error Rate: 64.63%
Quality (Expected): 15%
Quality (Optimized): 29%
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=67.517%	P[yes->no]=32.483%	
P[no->yes]=96.773%	P[no->no]=3.227%	

Worker: user8
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 5
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user9
Error Rate: 33.56%
Quality (Expected): 11%
Quality (Optimized): 33%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=70.663%	P[yes->no]=29.337%	
P[no->yes]=37.788%	P[no->no]=62.212%	

