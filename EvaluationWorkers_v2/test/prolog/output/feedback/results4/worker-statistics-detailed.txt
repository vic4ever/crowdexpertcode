Worker: user0
Error Rate: 46.59%
Quality (Expected): 0%
Quality (Optimized): 7%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=57.039%	P[yes->no]=42.961%	
P[no->yes]=50.213%	P[no->no]=49.787%	

Worker: user1
Error Rate: 44.72%
Quality (Expected): 2%
Quality (Optimized): 11%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=85.674%	P[yes->no]=14.326%	
P[no->yes]=75.11%	P[no->no]=24.89%	

Worker: user10
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user11
Error Rate: 0.0%
Quality (Expected): 100%
Quality (Optimized): 100%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user12
Error Rate: 97.74%
Quality (Expected): 91%
Quality (Optimized): 95%
Number of Annotations: 3
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=4.513%	P[yes->no]=95.487%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user2
Error Rate: 13.27%
Quality (Expected): 58%
Quality (Optimized): 73%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.847%	P[yes->no]=0.153%	
P[no->yes]=26.393%	P[no->no]=73.607%	

Worker: user3
Error Rate: 25.86%
Quality (Expected): 31%
Quality (Optimized): 48%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.33%	P[yes->no]=0.67%	
P[no->yes]=51.041%	P[no->no]=48.959%	

Worker: user4
Error Rate: 44.72%
Quality (Expected): 2%
Quality (Optimized): 11%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=85.674%	P[yes->no]=14.326%	
P[no->yes]=75.11%	P[no->no]=24.89%	

Worker: user5
Error Rate: 45.48%
Quality (Expected): 1%
Quality (Optimized): 9%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=42.731%	P[yes->no]=57.269%	
P[no->yes]=33.692%	P[no->no]=66.308%	

Worker: user6
Error Rate: 24.36%
Quality (Expected): 27%
Quality (Optimized): 51%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=85.62%	P[yes->no]=14.38%	
P[no->yes]=34.333%	P[no->no]=65.667%	

Worker: user7
Error Rate: 4.32%
Quality (Expected): 84%
Quality (Optimized): 91%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.983%	P[yes->no]=0.017%	
P[no->yes]=8.618%	P[no->no]=91.382%	

Worker: user8
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user9
Error Rate: 17.2%
Quality (Expected): 48%
Quality (Optimized): 66%
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=66.559%	P[yes->no]=33.441%	
P[no->yes]=0.955%	P[no->no]=99.045%	

