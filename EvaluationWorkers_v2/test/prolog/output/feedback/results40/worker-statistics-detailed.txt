Worker: user0
Error Rate: 57.09%
Quality (Expected): 8%
Quality (Optimized): 14%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=85.671%	P[yes->no]=14.329%	
P[no->yes]=99.86%	P[no->no]=0.14%	

Worker: user1
Error Rate: 68.94%
Quality (Expected): 20%
Quality (Optimized): 38%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=58.135%	P[yes->no]=41.865%	
P[no->yes]=96.018%	P[no->no]=3.982%	

Worker: user2
Error Rate: 41.16%
Quality (Expected): 4%
Quality (Optimized): 18%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=85.465%	P[yes->no]=14.535%	
P[no->yes]=67.779%	P[no->no]=32.221%	

Worker: user3
Error Rate: 19.79%
Quality (Expected): 37%
Quality (Optimized): 60%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=81.898%	P[yes->no]=18.102%	
P[no->yes]=21.482%	P[no->no]=78.518%	

Worker: user4
Error Rate: 18.66%
Quality (Expected): 41%
Quality (Optimized): 63%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=69.846%	P[yes->no]=30.154%	
P[no->yes]=7.174%	P[no->no]=92.826%	

Worker: user5
Error Rate: 63.34%
Quality (Expected): 14%
Quality (Optimized): 27%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=71.435%	P[yes->no]=28.565%	
P[no->yes]=98.106%	P[no->no]=1.894%	

Worker: user6
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user7
Error Rate: 31.42%
Quality (Expected): 15%
Quality (Optimized): 37%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=83.546%	P[yes->no]=16.454%	
P[no->yes]=46.378%	P[no->no]=53.622%	

Worker: user8
Error Rate: 33.27%
Quality (Expected): 11%
Quality (Optimized): 33%
Number of Annotations: 5
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=70.018%	P[yes->no]=29.982%	
P[no->yes]=36.559%	P[no->no]=63.441%	

Worker: user9
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

