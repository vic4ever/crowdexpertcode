Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	25.62%	28%	49%	10	0
user1	66.81%	12%	34%	10	0
user2	16.12%	48%	68%	10	0
user3	41.63%	3%	17%	10	0
user4	24.48%	26%	51%	10	0
user5	71.2%	24%	42%	8	0
user6	14.22%	51%	72%	4	0
user7	50.0%	---	---	2	0
