Worker: user0
Error Rate: 30.66%
Quality (Expected): 15%
Quality (Optimized): 39%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=65.876%	P[yes->no]=34.124%	
P[no->yes]=27.195%	P[no->no]=72.805%	

Worker: user1
Error Rate: 62.38%
Quality (Expected): 7%
Quality (Optimized): 25%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=50.003%	P[yes->no]=49.997%	
P[no->yes]=74.765%	P[no->no]=25.235%	

Worker: user2
Error Rate: 15.92%
Quality (Expected): 50%
Quality (Optimized): 68%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=97.297%	P[yes->no]=2.703%	
P[no->yes]=29.136%	P[no->no]=70.864%	

Worker: user3
Error Rate: 21.81%
Quality (Expected): 32%
Quality (Optimized): 56%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=82.907%	P[yes->no]=17.093%	
P[no->yes]=26.522%	P[no->no]=73.478%	

Worker: user4
Error Rate: 38.5%
Quality (Expected): 6%
Quality (Optimized): 23%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=49.371%	P[yes->no]=50.629%	
P[no->yes]=26.365%	P[no->no]=73.635%	

Worker: user5
Error Rate: 41.94%
Quality (Expected): 3%
Quality (Optimized): 16%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=66.517%	P[yes->no]=33.483%	
P[no->yes]=50.394%	P[no->no]=49.606%	

Worker: user6
Error Rate: 34.2%
Quality (Expected): 10%
Quality (Optimized): 32%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=66.318%	P[yes->no]=33.682%	
P[no->yes]=34.713%	P[no->no]=65.287%	

Worker: user7
Error Rate: 5.8%
Quality (Expected): 79%
Quality (Optimized): 88%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=98.298%	P[yes->no]=1.702%	
P[no->yes]=9.903%	P[no->no]=90.097%	

Worker: user8
Error Rate: 2.52%
Quality (Expected): 90%
Quality (Optimized): 95%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=97.824%	P[yes->no]=2.176%	
P[no->yes]=2.874%	P[no->no]=97.126%	

