Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CityName-CORR-MDM.Record.Addresses.City	1.0	0.0	
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CommunicationTypeCode-CORR-MDM.Record.Addresses.CommType	0.99675	0.00325	
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID	0.99991	9.0E-5	
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.TaxJurisdictionCode-CORR-SRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.TaxJurisdictionCode	1.0	0.0	
1-TO-0-attrib-MDM.Record.MainAddress.HouseNumber-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID	0.00241	0.99759	
1-TO-0-attrib-MDM.Record.MainAddress.TaxJurCode-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.TaxJurisdictionCode	0.99947	5.3E-4	
1-TO-2-attrib-MDM.Record.Addresses.City-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CityName	0.99998	2.0E-5	
1-TO-2-attrib-MDM.Record.MainAddress.CommType-CORR-SRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CommunicationTypeCode	1.0E-5	0.99999	
2-TO-0-attrib-SRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CommunicationTypeCode-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CommunicationTypeCode	0.99962	3.8E-4	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CityName-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CityName	1.0E-5	0.99999	
2-TO-1-attrib-SRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.TaxJurisdictionCode-CORR-MDM.Record.Addresses.TaxJurCode	0.99971	2.9E-4	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID-CORR-MDM.Record.Addresses.HouseNumber	0.97038	0.02962	
