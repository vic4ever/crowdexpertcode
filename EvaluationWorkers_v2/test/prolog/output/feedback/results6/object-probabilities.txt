Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.AddressUsageInformation.AddressUsage.AddressUsageDataKey.AddressTypeCode-CORR-MDM.Record.Roles.RoleCode	4.0E-5	0.99996	
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode-CORR-MDM.Record.Addresses.Region	0.94594	0.05406	
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CityName-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CityName	0.95288	0.04712	
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CommunicationTypeCode-CORR-SRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CommunicationTypeCode	0.97258	0.02742	
1-TO-0-attrib-MDM.Record.Addresses.CommType-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CommunicationTypeCode	0.00146	0.99854	
1-TO-0-attrib-MDM.Record.MainAddress.Region-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode	0.97258	0.02742	
1-TO-0-attrib-MDM.Record.Roles.RoleCode-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.AddressUsageInformation.AddressUsage.AddressUsageDataKey.AddressTypeCode	6.9E-4	0.99931	
1-TO-2-attrib-MDM.Record.Addresses.HouseNumber-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID	0.05902	0.94098	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CityName-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CityName	0.97258	0.02742	
2-TO-1-attrib-SRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CommunicationTypeCode-CORR-MDM.Record.Addresses.CommType	0.97258	0.02742	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID-CORR-MDM.Record.Addresses.HouseNumber	0.95288	0.04712	
