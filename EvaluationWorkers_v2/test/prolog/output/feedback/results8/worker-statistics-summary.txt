Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	54.46%	1%	9%	11	0
user1	63.5%	13%	27%	11	0
user10	6.73%	75%	87%	2	0
user2	31.78%	14%	36%	11	0
user3	47.02%	0%	6%	11	0
user4	39.96%	5%	20%	11	0
user5	20.82%	34%	58%	11	0
user6	47.78%	0%	4%	9	0
user7	69.22%	23%	38%	9	0
user8	7.62%	72%	85%	8	0
user9	7.04%	75%	86%	5	0
