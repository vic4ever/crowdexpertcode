package test;

import config.Mainconfig;
import config.Mainconfig.ListDatasets;
import evaluateWorkers.EvaluateWorker;
import evaluateWorkers.Output;
import factorypattern.AlgorithmFactory;
import feedback.FeedBackModel;

public class TestAlgorithm {

	/**
	 * @param args
	 */
	private static String algorithm = "EM";
	// private static String algorithm = "SLME";
	// private static String algorithm = "IterativeLearning";
	// private static String algorithm = "ELICE";

	private static String filename = algorithm + ".csv";
	static AlgorithmFactory algo = new AlgorithmFactory();;
	static EvaluateWorker eval = null;

	public static void main(String[] args) {
		Mainconfig.getInstance().initialized();
		for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
			Mainconfig.getInstance().setData(data);
		}
		FeedBackModel model = new FeedBackModel();
		model.displayFeedBack();
		eval = algo.createAlgorithm(algorithm, model);
		Mainconfig.getInstance().getTimer().start();
		eval.execute();
		Mainconfig.getInstance().getTimer().stop();
		Output output = new Output(eval.getClass().getName(), Mainconfig
				.getInstance().getDataset().toString());
		output.setWorkers(eval.getWorkersResult());
		output.setQuestions(eval.getQuestionsResult());
		output.setCompletionTime(Mainconfig.getInstance().getTimer()
				.getElapsedTime());
		output.exportWorkersDetail(filename);
		output.exportResult();
	}

}
