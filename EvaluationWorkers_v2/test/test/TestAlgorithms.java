package test;

import evaluateWorkers.EvaluateWorker;
import evaluateWorkers.HoneyPot;
import feedback.FeedBackModel;

public class TestAlgorithms {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FeedBackModel model = new FeedBackModel();
		/*
		 * EvaluateWorker em = new EM(model); em.execute();
		 */
		/*
		 * EvaluateWorker md = new MajorityDecision(model); md.execute();
		 */
		EvaluateWorker hs = new HoneyPot(model);
		hs.execute();
	}

}
