package test;


import evaluateWorkers.EM;
import evaluateWorkers.EvaluateWorker;
import feedback.FeedBackModel;

public class TestEm {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FeedBackModel model = new FeedBackModel();
		EvaluateWorker em = new EM(model);
		em.execute();
	}

}
