package test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TestLinkHashMap {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Map a = new HashMap<String, String>();
		Map b = new LinkedHashMap<String, String>();
		a.put("b", "this is b");
		a.put("a", "this is a");
		a.put("d", "this is d");
		a.put("c", "this is c");
		
		b.put("b", "this is b");
		b.put("a", "this is a");
		b.put("d", "this is d");
		b.put("c", "this is c");
		
		display(a);
		display(b);
	}
	
	private static void display(Map<String, String> a){
		for(String key : a.keySet()){
			System.out.println("key: " + key + ", value: " + a.get(key));
		}
	}

}
