package test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import question.Question;

import tools.io.ConfigReader;
import tools.io.MyCsvWriter;
import user.Worker;
import config.Mainconfig;
import config.Mainconfig.ListDatasets;
import evaluateWorkers.EvaluateWorker;
import evaluateWorkers.Output;
import factorypattern.AlgorithmFactory;
import feedback.FeedBackModel;

public abstract class Test_SLME {

	protected static List<Worker> workers;
	protected static List<Question> questions;
	protected static ConfigReader reader = new ConfigReader();
	protected static Map<String, String> listConfig;
	protected static double step = 0;
	protected static String filename = null;
	protected static String currentPath = null;
	protected static String fileConfig = null;
	protected static int index = 0;
	
	
	public static void main(String[] args) {
		run();	
	}
	public Test_SLME() {
	}

	public static void run() {
		step = 1;
		filename = "slme/experiment_slme.csv";
		currentPath = "experiments/slme/";
		fileConfig = "experiment_slme.ini";
		customizeConfig();
		int iter = Integer.parseInt(Mainconfig.getInstance().getListConfig()
				.get("Iterator"));
		AlgorithmFactory algo = new AlgorithmFactory();
		for (int i = 0; i < iter; i++) {
			reinitialize();
			System.out.println("-------------------" + iter + ", " + i);
			for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
				Mainconfig.getInstance().setData(data);
				FeedBackModel model = new FeedBackModel();
				EvaluateWorker eval = null;
				String[] algorithms = Mainconfig.getInstance().getAlgorithms();
				while(index <40){
					double total = (Double.parseDouble(Mainconfig.getInstance()
							.getListConfig().get("iter")) + step);
					Mainconfig.getInstance().getListConfig().put("iter", total + "");
					
					for (String algorithm : algorithms) {
						eval = algo.createAlgorithm(algorithm, model);
						runOneAlgorithm(eval);
					}
					index++;
				}
				
			}
		}
	}

	public void workersExport() {
		String[] parseName = this.getClass().getName().split("[.]");
		String experiment = parseName[parseName.length - 1];
		/* String EstResult = "EstResult" + experiment + "_" + dataset; */
		customizeConfig();
		int iter = Integer.parseInt(Mainconfig.getInstance().getListConfig()
				.get("Iterator"));
		AlgorithmFactory algo = new AlgorithmFactory();
		for (int i = 0; i < iter; i++) {
			reinitialize();
			for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
				Mainconfig.getInstance().setData(data);
				FeedBackModel model = new FeedBackModel();
				setQuestions(model.getListQuestions());
				setWorkers(model.getListWorkers());
				String dataset = data.name();
				String Expertise = "Expertise_" + experiment + "_" + dataset + ".csv";
				MyCsvWriter.getInstance().WriteToFile(generateData(), Expertise);
			}
		}

	}
	
	private String generateData(){
		StringBuffer sb = new StringBuffer();
		sb.append("Worker \t Expertise \n");
		for(Worker worker : workers){
			String oneRecord = worker.getWID() + "\t" + worker.getReliability() + "\n";
			sb.append(oneRecord);
		}
		return sb.toString();
	}

	private static void reinitialize() {
		increaseStep();
		Mainconfig.getInstance().initialized();
	}

	static void increaseStep() {
		double total = (Double.parseDouble(Mainconfig.getInstance()
				.getListConfig().get("total")) + step);
		Mainconfig.getInstance().getListConfig().put("total", total + "");
	}

	static Map<String, Object> getExtendedResult() {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("index", index);
		return data;
	}

	private static void runOneAlgorithm(EvaluateWorker runner) {
		Mainconfig.getInstance().getTimer().start();
		runner.execute();
		Mainconfig.getInstance().getTimer().stop();
		Output output = new Output(runner.getClass().getName(), Mainconfig
				.getInstance().getDataset().toString());
		output.setWorkers(runner.getWorkersResult());
		output.setQuestions(runner.getQuestionsResult());
		setQuestions(output.getQuestions());
		setWorkers(output.getWorkers());

		output.setCompletionTime(Mainconfig.getInstance().getTimer()
				.getElapsedTime());
		Map<String, Object> extendedResult = getExtendedResult();
		output.exportResult(filename, extendedResult);
	}

	private static void customizeConfig() {
		reader.setCurrentPth(currentPath);
		reader.readfile(fileConfig);
		listConfig = reader.getConfig();
		for (String key : listConfig.keySet()) {
			Mainconfig.getInstance().getListConfig()
					.put(key, listConfig.get(key));
		}
	}

	public List<Worker> getWorkers() {
		return workers;
	}

	public static void setWorkers(List<Worker> workers) {
		workers = workers;
	}

	public static List<Question> getQuestions() {
		return questions;
	}

	public static void setQuestions(List<Question> questions) {
		questions = questions;
	}
}
