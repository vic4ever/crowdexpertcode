{start(a1), inCirc(a1,b2), inCirc(b2,a2), end(a2)}
{start(b1), inCirc(a1,b2), inCirc(b1,a1), end(b2)}
{start(a1), inCirc(a1,b1), inCirc(b1,c1), inCirc(c1,a2), end(a2)}
{start(b1), inCirc(a2,b2), inCirc(b1,c1), inCirc(c1,a2), end(b2)}
{start(b1), inCirc(a2,b2), inCirc(b1,a2), end(b2)}
{start(a1), inCirc(a1,b1), inCirc(b1,a2), end(a2)}

attr(a1,sa).
attr(a2,sa).
attr(b1,sb).
attr(b2,sb).
attr(c1,sc).

cor(a1,b1).
cor(a1,b2).

cor(a2,b1).
cor(a2,b2).

cor(a2,c1).
cor(b1,c1).