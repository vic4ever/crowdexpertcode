package data.converter;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ComparisonChain;

import au.com.bytecode.opencsv.CSVWriter;


public class Stock2GAL {
	private static final String nasdaq100 = "'aapl','adbe','adp','adsk','akam','altr','alxn','amat','amgn','amzn','apol','atvi','bbby','bidu','biib','bmc','brcm','ca','celg','cern','chkp','chrw','cmcsa','cost','csco','ctrp','ctsh','ctxs','dell','dltr','dtv','ebay','erts','esrx','expd','expe','fast','ffiv','fisv','flex','flir','fslr','gild','gmcr','goog','grmn','hpq','hsic','ilmn','infy','intc','intu','isrg','joyg','klac','life','linta','lltc','lrcx','mat','mchp','mrvl','msft','mu','mxim','myl','nflx','nihd','ntap','nvda','nwsa','orcl','orly','payx','pcar','pcln','qcom','qgen','rimm','rost','sbux','shld','sial','sndk','spls','srcl','stx','symc','teva','urbn','vmed','vod','vrsn','vrtx','wcrx','wfm','wynn','xlnx','xray','yhoo'";
	private static final String nasdaq20 = "'aapl','adbe','adp','adsk','akam','altr','alxn','amat','amgn','amzn','apol','atvi','bbby','bidu','biib','bmc','brcm','ca','celg','cern'";
	private static final String nasdaq10 = "'aapl','adbe','adp','adsk','akam','altr','alxn','amat','amgn','amzn'";
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	//(A column, a symbol, a day) -> a data item
	
	//        ColumnName,(Symbol,Day),Source,Value of ColumnIndex 
	private Map<String,Map<String,Map<String,String>>> db = new HashMap<String, Map<String,Map<String,String>>>();
	
	//          Column	   SymbolDay  Values
	private Map<String,Map<String,Set<String>>> columnSDValset = new HashMap<String, Map<String,Set<String>>>();
	
	//         Column     SymbolDay  Value	 Category
	private Map<String,Map<String,Map<String,Integer>>> columnSDValCat = new HashMap<String, Map<String,Map<String,Integer>>>();
	
	//          Source Source Ind
	private Map<String,Integer> srcMap = new HashMap<String, Integer>();
	
	//          Column     SymbolDay DtItmInd
	private Map<String,Map<String,Integer>> dtItmMap = new HashMap<String, Map<String,Integer>>();
	
	private List<String[]> outputData = new ArrayList<String[]>();
	private List<String[]> outputGold = new ArrayList<String[]>();
	
	private int maxCategory = 0;
	private int numDataItem = -1;
	
	private void buildGALInputs(String labelFile, String goldFile, String catFile) throws IOException{
		
		for(String columnNme : db.keySet()){
			Map<String, Map<String, String>> SDSrcVal = db.get(columnNme);
			Map<String, Map<String, Integer>> SDValCat = columnSDValCat.get(columnNme);
			Map<String, Integer> SDDtInd = dtItmMap.get(columnNme);
			
			for(String SD: SDSrcVal.keySet()){
				int dtInd = SDDtInd.get(SD);
				Map<String, String> SrcVal = SDSrcVal.get(SD);
				Map<String, Integer> ValCat = SDValCat.get(SD);
				for(String srcStr : SrcVal.keySet()){
					String val = SrcVal.get(srcStr);
					int srcInd = -1;
					if(!srcStr.contains("gold")){
						srcInd = srcMap.get(srcStr);					
					}
					int cat = ValCat.get(val);
					if(srcStr.contains("gold")){
						String[] outGold = new String[2];
						outGold[0] = dtInd+""; 
						outGold[1] = cat+"";
						outputGold.add(outGold);
					}else{
						String[] outData = new String[3];
						outData[0] = srcInd+ "";
						outData[1] = dtInd+ "";
						outData[2] = cat + "";
						outputData.add(outData);
					}
				}
			}
		}
	
		Collections.sort(outputGold, new Comparator<String[]>(){
			@Override
			public int compare(String[] arg0, String[] arg1) {
				return Integer.parseInt(arg0[0]) - Integer.parseInt(arg1[0]);
			}
			
		});
		
		
		CSVWriter goldLabelWriter = new CSVWriter(new FileWriter(goldFile),'\t',CSVWriter.NO_QUOTE_CHARACTER);
		for(String[] outG : outputGold){
			goldLabelWriter.writeNext(outG);
		}
		goldLabelWriter.close();
		
		Collections.sort(outputData, new Comparator<String[]>(){
			@Override
			public int compare(String[] arg0, String[] arg1) {
				return ComparisonChain.start().compare(Integer.parseInt(arg0[1]),Integer.parseInt(arg1[1])).compare(Integer.parseInt(arg0[0]),Integer.parseInt(arg1[0])).result();
			}
			
		});
		CSVWriter labelWriter = new CSVWriter(new FileWriter(labelFile),'\t',CSVWriter.NO_QUOTE_CHARACTER);
		for(String[] outD : outputData){
			labelWriter.writeNext(outD);
		}
		labelWriter.close();
		
		FileWriter catWriter = new FileWriter(catFile);
		for(int i=0; i<maxCategory; i++){
			catWriter.write(i+"\n");
		}
		catWriter.close();
		
	}

	private void writeResultSet(ResultSet resultSet) throws SQLException {
		ResultSetMetaData rsmd = resultSet.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		
		// ResultSet is initially before the first data set
		while (resultSet.next()) {
			// It is possible to get the columns via name
			// also possible to get the columns via the column number
			// which starts at 1
			// e.g. resultSet.getSTring(2);
			String symbol = resultSet.getString("Symbol");
			int day = resultSet.getInt("Day");
			
			String symbolAndDay = symbol + "," + Integer.toString(day);
			String source = resultSet.getString("Source");
			if(!srcMap.containsKey(source)){
				if(!source.contains("gold")){
					srcMap.put(source, srcMap.size());
				}
			}
			
			for(int columnInd = 4; columnInd <= columnsNumber; columnInd ++ ){
				String value = resultSet.getString(columnInd);
				if(value == null){
					continue;
				}
				
				String columnName = rsmd.getColumnName(columnInd);
				Map<String, Map<String, String>> symbolDaySrcValue = db.get(columnName);
				Map<String, Set<String>> SDValset = columnSDValset.get(columnName);
				Map<String, Map<String, Integer>> SDValCat = columnSDValCat.get(columnName);
				Map<String, Integer> SDInd = dtItmMap.get(columnName);
				
				if(symbolDaySrcValue == null){
					symbolDaySrcValue = new HashMap<String, Map<String,String>>();
				}
				if(SDValset == null){
					SDValset = new HashMap<String, Set<String>>();
				}
				if(SDValCat == null){
					SDValCat = new HashMap<String, Map<String,Integer>>();
				}
				if(SDInd == null){
					SDInd = new HashMap<String, Integer>();
				}
				
				Map<String, String> srcValue = symbolDaySrcValue.get(symbolAndDay);
				Set<String> valSet = SDValset.get(symbolAndDay);
				Map<String, Integer> valCat = SDValCat.get(symbolAndDay);
				
				if(srcValue == null){
					srcValue = new HashMap<String, String>();
				}
				if(valSet == null){
					valSet = new HashSet<String>();
				}
				if(valCat == null){
					valCat = new HashMap<String, Integer>();
				}
				if(!SDInd.containsKey(symbolAndDay)){
					numDataItem++;
					SDInd.put(symbolAndDay, numDataItem);
				}
				
				
				if(srcValue.containsKey(source)){
					throw new SQLException("Src already contains" + source + value + symbolAndDay);
				}else{
					srcValue.put(source, value);
				}
				
				if(!valCat.containsKey(value)){
					assert !valSet.contains(value);
					valSet.add(value);
					valCat.put(value, valSet.size() - 1);
					if(valSet.size() > maxCategory){
						maxCategory = valSet.size();
					}
					SDValset.put(symbolAndDay, valSet);
					SDValCat.put(symbolAndDay, valCat);
				}
				
				symbolDaySrcValue.put(symbolAndDay, srcValue);
				
				columnSDValCat.put(columnName, SDValCat);
				columnSDValset.put(columnName, SDValset);
				dtItmMap.put(columnName, SDInd);
				db.put(columnName, symbolDaySrcValue);
			}
		}
	}
	


	private void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (statement != null) {
				statement.close();
			}

			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {

		}
	}
	

	public void readDataBase(String columnnn, String srcNoGold) throws Exception {
//		String columnnn = "Volume";
		//36,88,1,36,32
//		String srcNoGold = "'yahoo-finance','raymond-james','streetinsider-com','smartmoney','advfn'";
		
//		String columnnn = "TodayHigh";
		//85,95,95,68,92
//		String srcNoGold = "'yahoo-finance','easystockalterts','streetinsider-com','scroli','barchart'"; //1.0 all
		
		//String columnnn = "PreviousClose";
		//97,97,97,71,97
		//String srcNoGold = "'yahoo-finance','easystockalterts','streetinsider-com','scroli','barchart'"; //1.0 all
		
		//String columnnn = "ChangePercent";
		//97,44,72,70,34
//		String srcNoGold = "'yahoo-finance','easystockalterts','streetinsider-com','scroli','stocktwits'";
		
//		String columnnn = "OpenPrice";
		//33,78,78,36,65
//		String srcNoGold = "'yahoo-finance','barchart','insidestocks','personal-wealth-biz','scroli'";
		
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connect = DriverManager
					.getConnection("jdbc:mysql://localhost/test?"
							+ "user=root&password=toor");
			statement = connect.createStatement();
			
//			String srcGold = "'gold_nasdaq','yahoo-finance','barchart','insidestocks','personal-wealth-biz','scroli'";
			String srcGold = "'gold_nasdaq'," + srcNoGold ;

//			String noGoldView = "create or replace view test.withoutGold as select d.Symbol, count(distinct(d."+ columnnn +")) as countPrice from test.stock d where d.Source IN (" + srcNoGold+ ") and Symbol IN (" + nasdaq10 +") and Day = 0 group by Symbol";
//			String goldView = "create or replace view test.withGold as select d.Symbol, count(distinct(d."+ columnnn +")) as countPrice from test.stock d where d.Source IN (" + srcGold + ") and Symbol IN (" + nasdaq10 +") and Day = 0 group by Symbol";			
//			statement.executeUpdate(noGoldView);
//			statement.executeUpdate(goldView);
			
//			String noGoldView = "create or replace view test.withoutGold as select d.Symbol, count(distinct(d."+ columnnn +")) as countPrice from test.stock d where d.Source IN (" + srcNoGold+ ") and Symbol IN (" + nasdaq100 +") and Day = 0 group by Symbol";
//			String goldView = "create or replace view test.withGold as select d.Symbol, count(distinct(d."+ columnnn +")) as countPrice from test.stock d where d.Source IN (" + srcGold + ") and Symbol IN (" + nasdaq100 +") and Day = 0 group by Symbol";			
//			statement.executeUpdate(noGoldView);
//			statement.executeUpdate(goldView);
			
			String noGoldView20 = "create or replace view test.withoutGold as select d.Symbol, count(distinct(d."+ columnnn +")) as countPrice from test.stock d where d.Source IN (" + srcNoGold+ ") and Symbol IN (" + nasdaq20 +") and Day = 0 group by Symbol";
			String goldView20 = "create or replace view test.withGold as select d.Symbol, count(distinct(d."+ columnnn +")) as countPrice from test.stock d where d.Source IN (" + srcGold + ") and Symbol IN (" + nasdaq20 +") and Day = 0 group by Symbol";
			statement.executeUpdate(noGoldView20);
			statement.executeUpdate(goldView20);
			
//			preparedStatement = connect
//					.prepareStatement("select d.Source, d.Symbol, d.Day, d.OpenPrice from test.stock d where d.Source IN ('gold_nasdaq','yahoo-finance','barchart','insidestocks','personal-wealth-biz','scroli') and Symbol IN ('aapl','adbe','adp','adsk','akam','altr','alxn','amat','amgn','amzn','apol','atvi','bbby','bidu','biib','bmc','brcm','ca','celg','cern','chkp','chrw','cmcsa','cost','csco','ctrp','ctsh','ctxs','dell','dltr','dtv','ebay','erts','esrx','expd','expe','fast','ffiv','fisv','flex','flir','fslr','gild','gmcr','goog','grmn','hpq','hsic','ilmn','infy','intc','intu','isrg','joyg','klac','life','linta','lltc','lrcx','mat','mchp','mrvl','msft','mu','mxim','myl','nflx','nihd','ntap','nvda','nwsa','orcl','orly','payx','pcar','pcln','qcom','qgen','rimm','rost','sbux','shld','sial','sndk','spls','srcl','stx','symc','teva','urbn','vmed','vod','vrsn','vrtx','wcrx','wfm','wynn','xlnx','xray','yhoo') and Day = 0 order by Symbol");
			preparedStatement = connect
					.prepareStatement("select d.Source, d.Symbol, d.Day, d."+ columnnn +" from test.stock d where d.Source IN ("+ 
									srcGold +") and Symbol IN(select t.Symbol from test.withGold t left join test.withoutGold o on t.Symbol = o.Symbol where t.countPrice = o.countPrice) and d.Day = 0 order by d.Symbol");
			resultSet = preparedStatement.executeQuery();
			writeResultSet(resultSet);

		} catch (Exception e) {
			throw e;
		} finally {
			close();
		}
	}
	
	public void run(String labelFile,String goldFile,String catFile) throws Exception{
		readDataBase("Volume","'yahoo-finance','raymond-james','streetinsider-com','smartmoney','advfn'");
//		readDataBase("OpenPrice","'advfn','barchart','bloomberg','cio-com','cnn-money','easystockalterts','eresearch-fidelity-com','financial-content','finapps-forbes-com','finviz','fool','howthemarketworks','investopedia','investorguide','marketintellisearch','marketwatch','msn-money','nasdaq-com','optimum','pc-quote','predictwallstreet','raymond-james','scroli','simple-stock-quotes','stocksmart','stocktwits','streetinsider-com','thecramerreport','thestree','tickerspy','tmx-quotemedia','wallstreetsurvivor','yahoo-finance','zacks'");
		//NoCopy
//		readDataBase("OpenPrice","'advfn','barchart','bloomberg','cio-com','cnn-money','easystockalterts','eresearch-fidelity-com','financial-content','finapps-forbes-com','fool','howthemarketworks','investopedia','investorguide','marketintellisearch','marketwatch','msn-money','nasdaq-com','optimum','pc-quote','raymond-james','scroli','stocksmart','streetinsider-com','thecramerreport','thestree','tickerspy','tmx-quotemedia','wallstreetsurvivor','yahoo-finance'");
		//Copy
//		readDataBase("OpenPrice","'advfn','barchart','bloomberg','boston-com','bostonmerchant','business-insider','chron','cio-com','cnn-money','easystockalterts','eresearch-fidelity-com','finance-abc7-com','finance-abc7chicago-com','financial-content','finapps-forbes-com','fool','howthemarketworks','hpcwire','investopedia','investorguide','marketintellisearch','marketwatch','minyanville','msn-money','nasdaq-com','optimum','paidcontent','pc-quote','raymond-james','renewable-energy-world','screamingmedia','scroli','stocknod','stocksmart','streetinsider-com','thecramerreport','thestree','tickerspy','tmx-quotemedia','wallstreetsurvivor','yahoo-finance'");
		
//		readDataBase("ChangePercent", "'yahoo-finance','easystockalterts','streetinsider-com','scroli','stocktwits'");
		buildGALInputs(labelFile,goldFile,catFile);
		System.out.println(srcMap);
//		System.out.println(maxCategory);
	}
	
	public static void main(String[] args){
		Stock2GAL converter = new Stock2GAL();
		try {
			converter.run("./stock/labels.txt","./stock/golds.txt","./stock/categories.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
