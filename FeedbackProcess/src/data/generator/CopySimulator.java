package data.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import utils.Permute;
import utils.Utils;

import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;

public class CopySimulator {

    public enum COPY_FALSE_STRATEGY {COPY_ALL, COPY_PARTLY, COPY_PARTLY_CHANGE}

    ;

    public enum COPY_TRUE_STRATEGY {COPY_ALL, COPY_PARTLY, COPY_NONE, COPY_PARTLY_CHANGE}

    ;
    private DataSet dataSet;

    private double copyRatio = 0.75;

    private double copyFalseRatio = 0.8;

    private double copyTrueRatio = 0.2;

    private double changeAnswerProbCorrect = 0.4;

    private double changeAnserProbIncorrect = 0.4;

    public CopySimulator(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    public void genCopierRandomly(COPY_FALSE_STRATEGY falseStrat, COPY_TRUE_STRATEGY trueStrat) {
        List<DataSource> srcs = dataSet.getDataSources(true);
        List<DataSource> copiedSrcs = new ArrayList<DataSource>();

        //Create copied srcs
        //# The first srcInd
        int newSrcInd = srcs.size();
        //# Number of copied srcs to be created
        int numCopiers = (int) (srcs.size() * getCopyRatio());
        Random random = new Random(System.nanoTime());
        for (int i = 0; i < numCopiers; i++) {
            //Select a random src
            int srcInd = random.nextInt(srcs.size());
            DataSource src = dataSet.getSource(srcInd);
            //Create the new src
            assert src!=null;
            assert dataSet!=null;
            List<Assignment> copiedAsses = copyAssignmentList(src.getAssignments(), newSrcInd, dataSet.getDataItems(), falseStrat, trueStrat);
            DataSource copiedSrc = src.clone(copiedAsses);
            copiedSrc.setOriginalSource(src.getDataSourceIndex());
            copiedSrc.setDataSourceIndex(newSrcInd);
            assert copiedSrc.getDataSourceIndex() != src.getDataSourceIndex();
            assert copiedSrc.isCopier() == true;

            //Add to src trustworthiness
            dataSet.sourceTrustworthiness.put(copiedSrc.getDataSourceIndex(), copiedSrc.getEstimatedReliability());
            //Add to dataset
            copiedSrc.setReliability(calculateSrcReliability(copiedSrc));
            copiedSrcs.add(copiedSrc);
            newSrcInd++;
        }
        copiedSrcs.addAll(srcs);
        dataSet.setDataSources(copiedSrcs);
    }

    public void genCopierFromWorstSrc(COPY_FALSE_STRATEGY falseStrat, COPY_TRUE_STRATEGY trueStrat) {
        List<DataSource> srcs = dataSet.getDataSources(true);

        //Get the worst src
        List<DataSource> copiedSrcs = new ArrayList<DataSource>();
        copiedSrcs.addAll(srcs);
        Collections.sort(copiedSrcs, new Comparator<DataSource>() {
            @Override
            public int compare(DataSource arg0, DataSource arg1) {
                if (arg1.getReliability() - arg0.getReliability() > 0) {
                    return -11;
                } else {
                    if (arg1.getReliability() - arg0.getReliability() < 0) {
                        return 1;
                    } else {
                        return 0;
                    }

                }
            }
        });
        DataSource worstSrc = copiedSrcs.get(0);
        assert worstSrc.getReliability() <= copiedSrcs.get(1).getReliability();
        //Get the list of assignments in the worst src
        List<Assignment> toCopyAsses = worstSrc.getAssignments();

        //Create copied srcs
        //# The first srcInd
        int newSrcInd = srcs.size();
        //# Number of copied srcs to be created
        int numCopiers = (int) (srcs.size() * getCopyRatio());

        for (int i = 0; i < numCopiers; i++) {
            //# create new copied src

            //			// The list of new assignments for this src
            //			List<Assignment> asses = new ArrayList<Assignment>();
            //			//TODO: copy all. If change then change here to copy only a part
            //			for (Assignment assignment : toCopyAsses) {
            //				//Create new assignment
            //				Assignment copiedAss = assignment.clone();
            //				copiedAss.setSourceIndex(newSrcInd);
            //				//Need to add to data item also
            //				//TODO: check if this is correct
            //				DataItem itm = dataSet.getDataItems().get(assignment.getDataItemIndex());
            //				itm.addAssignment(copiedAss);
            //				//Add to src
            //				asses.add(copiedAss);
            //			}
            //			assert asses.size() == toCopyAsses.size();

            List<Assignment> asses = copyAssignmentList(toCopyAsses, newSrcInd, dataSet.getDataItems(), falseStrat, trueStrat);
            if (asses.size() == 0) {
                continue;
            }

            DataSource copySrc = worstSrc.clone(asses);
            copySrc.setOriginalSource(worstSrc.getDataSourceIndex());
            copySrc.setDataSourceIndex(newSrcInd);
            newSrcInd++;
            assert copySrc.getDataSourceIndex() != worstSrc.getDataSourceIndex();
            assert copySrc.isCopier() == true;
            copySrc.setReliability(calculateSrcReliability(copySrc));
            srcs.add(copySrc);
            //Add to sourceTrustworthiness
            dataSet.sourceTrustworthiness.put(copySrc.getDataSourceIndex(), worstSrc.getEstimatedReliability());
        }
        assert srcs.size() == newSrcInd;
        dataSet.setDataSources(srcs);
    }

    private List<Assignment> copyAssignmentList(List<Assignment> toCopyAsses, int newSrcInd, List<DataItem> items, COPY_FALSE_STRATEGY falseStrat, COPY_TRUE_STRATEGY trueStrat) {
        List<Assignment> retVal = new ArrayList<Assignment>();
        //Select a list of assignment to be copied
        List<Assignment> toCopiedAsses = genToCopyAsses(toCopyAsses, items, falseStrat, trueStrat);
        //Create a list of copied assignments from this list
        for (Assignment assignment : toCopiedAsses) {
            //Create new assignment
            Assignment copiedAss = assignment.clone();
            copiedAss.setSourceIndex(newSrcInd);
            //Add to data item
            //TODO: check if this is correct
            DataItem itm = items.get(assignment.getDataItemIndex());
            itm.addAssignment(copiedAss);
            //Add to src
            retVal.add(copiedAss);
        }
        //		assert toCopyAsses.size() == retVal.size();
        return retVal;
    }

    private List<Assignment> genToCopyAsses(List<Assignment> toCopyAsses, List<DataItem> items, COPY_FALSE_STRATEGY falseStrat, COPY_TRUE_STRATEGY trueStrat) {
        List<List<Assignment>> allAsses = copyIncorrectAssignments(toCopyAsses, items);
        List<Assignment> correct = allAsses.get(0);
        List<Assignment> incorrect = allAsses.get(1);

        List<Assignment> retVal = new ArrayList<Assignment>();

        switch (falseStrat) {
            case COPY_ALL:
                retVal.addAll(incorrect);
                break;
            case COPY_PARTLY: {
                List<Integer> original = Utils.makeSequence(0, incorrect.size());
                long seed = System.nanoTime();
                Collections.shuffle(original, new Random(seed));
                int numFalseAss = (int) (incorrect.size() * getCopyFalseRatio());
                for (int i = 0; i < numFalseAss; i++) {
                    int assInd = original.get(i);
                    Assignment asss = incorrect.get(assInd);
                    retVal.add(asss);
                }
                break;
            }
            case COPY_PARTLY_CHANGE:
                List<Integer> original = Utils.makeSequence(0, incorrect.size());
                long seed = System.nanoTime();
                Collections.shuffle(original, new Random(seed));
                int numFalseAss = (int) (incorrect.size() * getCopyFalseRatio());
                for (int i = 0; i < numFalseAss; i++) {
                    int assInd = original.get(i);
                    Assignment asss = incorrect.get(assInd);
                    Assignment assss = asss.clone();

                    //Decide whether to change
                    Random rand = new Random(System.nanoTime());
                    boolean change = false;
                    if (rand.nextDouble() < changeAnserProbIncorrect) {
                        change = true;
                    }

                    if (change) {
                        //Select the category to change to
                        Set<Integer> catSet = dataSet.getDataItems().get(0).categoryList;
                        List<Integer> catList = new ArrayList<Integer>(catSet);
                        Random rand2 = new Random(System.nanoTime());
                        int catInd = rand2.nextInt(catList.size());
                        Integer cat = catList.get(catInd);
                        while (cat == asss.getValue()) {
                            catInd = rand2.nextInt(catList.size());
                            cat = catList.get(catInd);
                        }
                        assss.setValue(cat);
                    }

                    retVal.add(assss);
                }
                break;
        }

        switch (trueStrat) {
            case COPY_ALL:
                retVal.addAll(correct);
                break;
            case COPY_PARTLY: {
                int numTrueAss = (int) (correct.size() * getCopyTrueRatio());
                List<Integer> original = Utils.makeSequence(0, correct.size());
                long seed = System.nanoTime();
                Collections.shuffle(original, new Random(seed));
                for (int i = 0; i < numTrueAss; i++) {
                    int assInd = original.get(i);
                    Assignment asss = correct.get(assInd);
                    retVal.add(asss);
                }
                break;
            }
            case COPY_PARTLY_CHANGE: {
                int numTrueAss = (int) (correct.size() * getCopyTrueRatio());
                List<Integer> original = Utils.makeSequence(0, correct.size());
                long seed = System.nanoTime();
                Collections.shuffle(original, new Random(seed));
                for (int i = 0; i < numTrueAss; i++) {
                    int assInd = original.get(i);
                    Assignment asss = correct.get(assInd);
                    Assignment assss = asss.clone();
                    //Decide whether to change
                    Random rand = new Random(System.nanoTime());
                    double prob = rand.nextDouble();
                    boolean change = false;
                    if (prob < changeAnswerProbCorrect) {
                        change = true;
                    }
                    if (change) {
                        //Decide the category to be changed to
                        Set<Integer> catSet = dataSet.getDataItems().get(0).categoryList;
                        List<Integer> catList = new ArrayList<Integer>(catSet);
                        Random rand2 = new Random(System.nanoTime());
                        int catInd = rand2.nextInt(catList.size());
                        Integer cat = catList.get(catInd);
                        while (cat == asss.getValue()) {
                            catInd = rand2.nextInt(catList.size());
                            cat = catList.get(catInd);
                        }
                        assss.setValue(cat);
                    }
                    retVal.add(assss);
                }
                break;
            }
            case COPY_NONE:
                break;
        }

        return retVal;
    }

    private List<List<Assignment>> copyIncorrectAssignments(List<Assignment> toCopyAsses, List<DataItem> items) {
        List<Assignment> incorrectAsses = new ArrayList<Assignment>();
        List<Assignment> correctAsses = new ArrayList<Assignment>();
        //Copy all assignments
        for (Assignment assignment : toCopyAsses) {
            //TODO: check if this is correct
            DataItem itm = items.get(assignment.getDataItemIndex());
            assert itm.getDataItemIndex() == assignment.getDataItemIndex();
            if (assignment.getValue() == itm.getGroundTruth()) {
                correctAsses.add(assignment);
            } else {
                incorrectAsses.add(assignment);
            }
        }
        List<List<Assignment>> retVal = new ArrayList<List<Assignment>>();
        retVal.add(correctAsses);
        retVal.add(incorrectAsses);
        return retVal;
    }

    private double calculateSrcReliability(DataSource src) {
        List<Assignment> assignments = src.getAssignments();
        int all = 0;
        int correct = 0;
        for (Assignment assignment : assignments) {
            DataItem itm = dataSet.getDataItems().get(assignment.getDataItemIndex());
            assert itm.getDataItemIndex() == assignment.getDataItemIndex();
            if (assignment.getValue() == itm.getGroundTruth()) {
                correct++;
            }
            all++;
        }
        double retVal = (double) ((double) correct / (double) all);
        return retVal;
    }

    public double getCopyRatio() {
        return copyRatio;
    }

    public void setCopyRatio(double copyRatio) {
        this.copyRatio = copyRatio;
    }

    public double getCopyTrueRatio() {
        return copyTrueRatio;
    }

    public void setCopyTrueRatio(double copyTrueRatio) {
        this.copyTrueRatio = copyTrueRatio;
    }

    public double getCopyFalseRatio() {
        return copyFalseRatio;
    }

    public void setCopyFalseRatio(double copyFalseRatio) {
        this.copyFalseRatio = copyFalseRatio;
    }
}
