package data.model;

public class Assignment {
	private int sourceIndex;
	private Integer value;
	private int dataItemIndex;
	private boolean feedback = false;
	
	public Assignment(int sourceIndex,int dataItemIndex,  int value, boolean feedback){
		this.sourceIndex = sourceIndex;
		this.value = value;
		this.dataItemIndex = dataItemIndex;
		this.feedback = feedback;
	}
	
	public int getSourceIndex() {
		return sourceIndex;
	}
	public void setSourceIndex(int sourceIndex) {
		this.sourceIndex = sourceIndex;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	@Override
	public String toString(){
		String retVal = "";
		retVal += sourceIndex + " " + dataItemIndex +" " +value;
		return retVal;
	}

	public int getDataItemIndex() {
		return dataItemIndex;
	}

	public void setDataItemIndex(int dataItemIndex) {
		this.dataItemIndex = dataItemIndex;
	}
	
	public Assignment clone(){
		Assignment cloned = new Assignment(this.sourceIndex, this.dataItemIndex, new Integer(this.value), this.feedback);
		return cloned;
	}
}
