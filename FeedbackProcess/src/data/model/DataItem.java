package data.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class DataItem {
	private int groundTruth; 
	private int dataItemIndex;
	//sourceIndex - value
	private List<Assignment> assignments;
	//Category - Number of categories assigned by Sources
	private SortedMap<Integer,Integer> valueCount;
	//Map between category and probability
	private SortedMap<Integer,Double> valueProb;
	protected double entropy;
	protected boolean feedbacked = false;

	public Set<Integer> categoryList;

    public DataItem clone(){
        DataItem cloned = new DataItem();

        //Copy assignments
        List<Assignment> asses = new ArrayList<Assignment>();
        for (Assignment assignment : this.assignments) {
            Assignment clonedAss = assignment.clone();
            asses.add(clonedAss);
        }
        cloned.setAssignments(asses);

        //Copy ground truth
        cloned.setGroundTruth(this.groundTruth);

        // Copy dataItem index
        cloned.setDataItemIndex(this.getDataItemIndex());

        // Copy feedbacked
        cloned.setFeedbacked(this.feedbacked);

        // Copy categories
        Set<Integer> clonedCat = new HashSet<Integer>();
        for(Integer cat : this.categoryList){
            clonedCat.add(new Integer(cat.intValue()));
        }
        cloned.categoryList = clonedCat;

        //Copy valueCount
        SortedMap<Integer,Integer> clonedCnt = new TreeMap<Integer,Integer>();
        for (Integer val : this.valueCount.keySet()) {
            int cnt = this.valueCount.get(val);
            clonedCnt.put(new Integer(val), cnt);
        }
        cloned.setValueCount(clonedCnt);
        //Copy valueProb
        SortedMap<Integer,Double> clonedProb = new TreeMap<Integer,Double>();
        for(Integer val: this.valueProb.keySet()){
            double prob = this.valueProb.get(val);
            clonedProb.put(new Integer(val), prob);
        }
        cloned.setValueProb(clonedProb);
        //Copy entropy
        cloned.entropy = this.entropy;
        return cloned;
    }

	public DataItem(){
		valueCount = new TreeMap<Integer, Integer>();
		valueProb = new TreeMap<Integer, Double>();
		categoryList = new HashSet<Integer>();
	}

	public void addAssignment(Assignment assignment){
		if(assignments == null){
			assignments = new ArrayList<Assignment>();
		}
		assignments.add(assignment);
	}


	public void calculateCategoryList(){
//		for(Assignment assignment: assignments){
		//	categoryList.add(assignment.getValue());		
			//}
	}


	//	public int getNumDatasources() {
	//		return numDatasources;
	//	}

	//	public void setNumDatasources(int numDatasources) {
	//		this.numDatasources = numDatasources;
	//	}

	public double calculateEntropy() {
		entropy = 0;
		Set<Integer> values = valueProb.keySet();
		for (Integer integer : values) {
			double prob = valueProb.get(integer);
			if(prob == 0){
				continue;
			}
			entropy += -prob*Math.log(prob);
		}
		return entropy;
	}

	public boolean isFeedbacked() {
		return feedbacked;
	}

	public void setFeedbacked(boolean feedbacked) {
		this.feedbacked = feedbacked;
	}

	public int getGroundTruth() {
		return groundTruth;
	}

	public void setGroundTruth(int groundTruth) {
		this.groundTruth = groundTruth;
	}

	@Override
	public String toString(){
		String retVals = "";
		for (Assignment assignment : this.getAssignments()) {
			retVals += assignment.toString() + " Pr "+ this.valueProb.get(assignment.getValue()) + "\n";
		}
		retVals += "Entropy: "+ calculateEntropy() + "\n";
		retVals += "GroundTruth " + groundTruth;
		return retVals;
	}

	public List<Assignment> getAssignments() {
		return assignments;
	}

	public void setAssignments(List<Assignment> assignments) {
		this.assignments = assignments;
	}

	public int getDataItemIndex() {
		return dataItemIndex;
	}

	public void setDataItemIndex(int dataItemIndex) {
		this.dataItemIndex = dataItemIndex;
	}


	public SortedMap<Integer,Integer> getValueCount() {
		calculateValueCount();
		return valueCount;
	}


	public void setValueCount(SortedMap<Integer,Integer> valueCount) {
		this.valueCount = valueCount;
	}
	
	
	private void calculateValueCount(){
		valueCount.clear();
		for(Assignment ass:this.assignments){
			int val = ass.getValue();
			if(valueCount.containsKey(val)){
				int cnt = valueCount.get(val);
				cnt++;
				valueCount.put(val, cnt);
			}else{
				valueCount.put(val, 1);
			}
		}
	}
	
	public void majorityProbAssign(){
		calculateValueCount();
		for (Assignment assignment : getAssignments()) {
			int count = getValueCount().get(assignment.getValue());
			double prob = (double) count / (double) getAssignments().size();
			valueProb.put(assignment.getValue(), prob);
		}
	}

//	@Deprecated
//	public void initAssignmentProbability() {
//		// Generate value - count
//		setValueCount(new TreeMap<Integer, Integer>());
//		for (Assignment assignment : getAssignments()) {
//			Integer key = assignment.getValue();
//			Integer value = getValueCount().get(key);
//			if(value != null){
//				getValueCount().put(key, value+1);
//			}else{
//				getValueCount().put(key,1);
//			}
//		}
//		// Calculate probability
//		int numAss = getAssignments().size();
//		for (Assignment assignment : getAssignments()) {
//			int count = getValueCount().get(assignment.getValue());
//			double prob = (double) count / (double) numAss;
//			assignment.setProbability(prob);
//		}
//	}

	public SortedMap<Integer,Double> getValueProb() {
		return valueProb;
	}

	public void setValueProb(SortedMap<Integer,Double> valueProb) {
		this.valueProb = valueProb;
	}

	public int getSelectiveValue(){
		if(this.isFeedbacked()){
			return this.groundTruth;
		}
		Map.Entry<Integer, Double> maxEntry = null;

		for (Map.Entry<Integer, Double> entry : valueProb.entrySet())
		{
			if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
			{
				maxEntry = entry;
			}
		}
		return maxEntry.getKey();
	}

    public int getEstimatedSelectiveValue(){
        Map.Entry<Integer, Double> maxEntry = null;

        for (Map.Entry<Integer, Double> entry : valueProb.entrySet())
        {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
            {
                maxEntry = entry;
            }
        }
        return maxEntry.getKey();
    }


	//	public List<T> getDataValues() {
	//		return dataValues;
	//	}


	//	public void setDataValues(List<T> dataValues) {
	//		this.dataValues = dataValues;
	//	}
	//
	//
	//	public int getNumDataValues() {
	//		return this.dataValues.size();
	//	}


}
