package data.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class DataItemFactory{
	private HashMap registeredDataItems = new HashMap();
	
	public void registerDataItem(String dataItemName,Class dataItemClass){
		registeredDataItems.put(dataItemName, dataItemClass);
	}
	
	public DataItem createDataItem(String dataItemName) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException{
		Class dataItemClass = (Class)registeredDataItems.get(dataItemName);
		//System.out.println(dataItemClass);
		Constructor dataItemConstructor = dataItemClass.getDeclaredConstructor(new Class[] { });
		return (DataItem) dataItemConstructor.newInstance(new Object[]{});
	}
}
