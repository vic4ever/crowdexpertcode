package data.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;


public class DataSet {
    //TODO: when add a new field, need to modify clone
    private int numSources;
    private int numDataItems;
    //private int numAssignments;
    private List<DataItem> dataItems;
    private List<DataSource> dataSources;
    public SortedMap<Integer, Double> sourceTrustworthiness;
    //	private SortedMap<Integer,TupleR<Integer,Integer>> columnIndexMap;
    private boolean feedbacked = false;

    public DataSet clone() {
        DataSet cloned = new DataSet(this.getNumSources(), this.numDataItems);

        //Copy dataitems
        List<DataItem> clonedDtItms = new ArrayList<DataItem>();
        for (DataItem dataItem : this.dataItems) {
            clonedDtItms.add(dataItem.clone());
        }
        cloned.setDataItems(clonedDtItms);
        //Copy srcTruthworthiness
        SortedMap<Integer, Double> clonedSrcTrustWorth = new TreeMap<Integer, Double>();
        for (Integer val : this.sourceTrustworthiness.keySet()) {
            double trust = this.sourceTrustworthiness.get(val);
            clonedSrcTrustWorth.put(val, trust);
        }
        cloned.setTrustworthiness(clonedSrcTrustWorth);
        //Copy feedbacked
        cloned.setFeedbacked(this.feedbacked);
//		//Copy numAss
//		cloned.setNumAssignments(this.getNumAssignments());

        //TODO: Smell bad
        //Copy datasource
        ////Generate list of cloned assignments for each source
        Map<Integer, List<Assignment>> srcIndAndAssList = new HashMap<Integer, List<Assignment>>();
        for (DataItem clonedDtItm : clonedDtItms) {
            List<Assignment> clonedAsses = clonedDtItm.getAssignments();
            for (Assignment clonedAss : clonedAsses) {
                if (srcIndAndAssList.containsKey(clonedAss.getSourceIndex())) {
                    List<Assignment> listt = srcIndAndAssList.get(clonedAss.getSourceIndex());
                    listt.add(clonedAss);
                    srcIndAndAssList.put(clonedAss.getSourceIndex(), listt);
                } else {
                    List<Assignment> nList = new ArrayList<Assignment>();
                    nList.add(clonedAss);
                    srcIndAndAssList.put(clonedAss.getSourceIndex(), nList);
                }
            }
        }
        ////Create new datasource
        List<DataSource> clonedSrcs = new ArrayList<DataSource>();
        for (DataSource dataSrc : dataSources) {
            List<Assignment> asses = srcIndAndAssList.get(dataSrc.getDataSourceIndex());
            DataSource clonedSrc = dataSrc.clone(asses);
            clonedSrcs.add(clonedSrc);
        }
        cloned.setDataSources(clonedSrcs);

        return cloned;
    }

    public DataSet() {
        dataItems = new ArrayList<DataItem>();
        sourceTrustworthiness = new TreeMap<Integer, Double>();
    }

    public DataSet(int numSources, int numDataItems) {
        this.numDataItems = numDataItems;
        this.numSources = numSources;
        //		factory = new DataItemFactory();
        dataItems = new ArrayList<DataItem>();
        sourceTrustworthiness = new TreeMap<Integer, Double>();

        //factory.registerDataItem("SimpleDataItem", SimpleDataItem.class);
    }

    //	public void generateData() throws Exception{
    //		for(int j=0; j<numDataItems; j++){
    //			DataItem dataItem = factory.createDataItem("SimpleDataItem");
    //			dataItem.setNumDatasources(getNumSources());
    ////			dataItem.generateAssignments();
    //			dataItem.initAssignmentProbability();
    //			dataItems.add(dataItem);
    //		}
    //	}

    //Init the src reliability to be 0.5 (maximum entropy principle)
    public void initTrustworthiness() {
        for (int i = 0; i < getNumSources(); i++) {
            sourceTrustworthiness.put(i, 0.5);
        }
    }

    //Caculate entropy of the dataset
    public double calculateEntropy() {
        double retVal = 0;
        for (DataItem dataItem : dataItems) {
            retVal += dataItem.calculateEntropy();
        }
        return retVal;
    }


    public int getNumSources() {
        numSources = dataSources.size();
        return numSources;
    }

    public int getNumSources(boolean onlyAvail) {
        int retVal = 0;
        for (DataSource ds : this.dataSources) {
            if (onlyAvail) {
                if (!ds.isDeleted()) {
                    retVal++;
                }
            } else {
                retVal++;
            }
        }
        return retVal;
    }

//	public void setNumSources(int numSources) {
//		this.numSources = numSources;
//	}

    public int getNumDataItems() {
        numDataItems = dataItems.size();
        return numDataItems;
    }

	/*
//	public void setNumDataItems(int numDataItems) {
//		this.numDataItems = numDataItems;
//	}
	*/

    public List<DataItem> getDataItems() {
        return dataItems;
    }

    public void setDataItems(List<DataItem> dataItems) {
        this.dataItems = dataItems;
    }

/*	public double[] getSourceTrustworthiness(){
        int sourceSize = sourceTrustworthiness.size();
		if(sourceSize == 0){
			this.initTrustworthiness();
		}

		double[] retVals = new double[sourceSize];
		for(int i=0; i< sourceSize; i++){
			retVals[i] = sourceTrustworthiness.get(i);
		}

		return retVals;
	}*/

	/*public double[] calculateItemValueProb(){
        int numValues = countNumDataValues();
		double[] retVals = new double[numValues];
		Iterator<Integer> iterator = getColumnIndexMap().keySet().iterator();
		while(iterator.hasNext()){
			Integer columnIndex = iterator.next();
			TupleR<Integer,Integer> tuple= getColumnIndexMap().get(columnIndex);
			Integer dataItemIndex = tuple.first;
			DataItem dataItem = dataItems.get(dataItemIndex);
			int denominator = dataItem.getAssignments().size();
			Integer value = tuple.second;
			int numerator = dataItem.getValueCount().get(value);
			double prob = (double) numerator/(double) denominator;
			if(dataItem.getValueProb() == null){
				dataItem.setValueProb(new TreeMap<Integer,Double>());
			}
			dataItem.getValueProb().put(value, prob);
			retVals[columnIndex] = prob;
			//System.out.println(dataItemIndex + " " + value + " " + prob);
		}
		return retVals;
	}*/

	/*@Deprecated
    public double[][] generateMapTable(boolean isWeighted){
		double[][] retVals = new double[numDataItems][numSources];
		for(double[] row:retVals){
			Arrays.fill(row, 0);
		}
		int rowCount = -1;
		for(DataItem dataItem: dataItems){
			rowCount++;
			List<Assignment> assignments = dataItem.getAssignments();
			for (Assignment assignment : assignments) {
				if (isWeighted){
					retVals[rowCount][assignment.getSourceIndex()] = assignment.getProbability();
				}else{
					retVals[rowCount][assignment.getSourceIndex()] = 1.0;
				}
			}
		}
		return retVals;
	}
	*/

	/*public double[][] generateMapTable(){
        int numValues = countNumDataValues();
		//		System.out.println("Numval:" + numValues);
		double[][] retVals = null;
		if(isFeedbacked()){
			retVals= new double[numSources][numValues];
		}
		else{
			//retVals = new double[numSources+numSurrogates][numValues];	
		}
		for(double[] row:retVals){
			Arrays.fill(row, 0);
		}
		setColumnIndexMap(new TreeMap<Integer, TupleR<Integer,Integer>>());
		int columnCount = -1;
		for(DataItem dataItem: dataItems){
			//System.out.println(dataItem.getDataItemIndex());
			Set<Integer> dataValues = dataItem.getValueCount().keySet();
			//			System.out.println(dataValues.size());
			List<Assignment> assignments = dataItem.getAssignments();
			for (Integer object : dataValues) {
				//				System.out.println("Size: " + dataValues.size());
				//				System.out.println(dataItem.getDataItemIndex());
				columnCount++;
				TupleR<Integer,Integer> tuple = new TupleR<Integer, Integer>(dataItem.getDataItemIndex(), object);
				getColumnIndexMap().put(columnCount,tuple);
				//TODO:chi tao table tu non-feedback
				for (Assignment assignment : assignments) {
					if(assignment.getValue() == object){
						//						System.out.println(numValues);
						//						System.out.println(dataItem.getDataItemIndex());
						//						System.out.println(rowCount);
						//						System.out.println(assignment.getSourceIndex());
						//System.out.println(assignment.getSourceIndex() + " " +columnCount);
						retVals[assignment.getSourceIndex()][columnCount] = 1;
					}
				}
			}
		}
		return retVals;
	}
	*/

	/*private int countNumDataValues(){
        int retVal = 0;
		for (DataItem dataItem : dataItems) {
			retVal +=dataItem.getValueCount().size();
		}
		return retVal;
	}*/


    //Set src reliability
    public void setTrustworthiness(Map<Integer, Double> srcTrust) {
        for (Integer i : srcTrust.keySet()) {
            sourceTrustworthiness.put(i, srcTrust.get(i));
        }
    }

    public int getNumLabels() {
        return dataItems.get(0).categoryList.size();
    }

    //Set src reliability
    public void setTrustworthiness(double[] values) {
        //System.out.println(numSources);
        for (int i = 0; i < this.getNumSources(); i++) {
            sourceTrustworthiness.put(i, values[i]);
        }
    }

    //Calculate the src entropy based on src reliability
    public double calculateSourceEntropy() {
        Set<Integer> srcIndices = sourceTrustworthiness.keySet();
        double retVal = 0;
        for (Integer srcInd : srcIndices) {
            Double srcProb = sourceTrustworthiness.get(srcInd);
            assert !srcProb.isNaN();
            if (srcProb <= 0) {
                continue;
            }
            retVal += srcProb * Math.log(srcProb);
        }
        return 0 - retVal;
    }

    @Override
    public String toString() {
        String retVal = "";
        for (int i = 0; i < this.getNumSources(); i++) {
            retVal = retVal + "S" + i + " Tr: " + sourceTrustworthiness.get(i) + "\n";
        }
        retVal += "============================\n";
        for (int i = 0; i < numDataItems; i++) {
            retVal += dataItems.get(i).toString() + "\n";
            retVal += "-----\n";
        }
        retVal += "Uncertainty " + this.calculateEntropy();
        return retVal;
    }

	/*public int getNumAssignments() {
        return numAssignments;
	}

	public void setNumAssignments(int numAssignments) {
		this.numAssignments = numAssignments;
	}
	*/

	/*
//	public SortedMap<Integer,TupleR<Integer,Integer>> getColumnIndexMap() {
//		return columnIndexMap;
//	}
//
//	public void setColumnIndexMap(SortedMap<Integer,TupleR<Integer,Integer>> columnIndexMap) {
//		this.columnIndexMap = columnIndexMap;
//	}
 * 
 */

/*	public void addAssignment(int sourceIndex, int dataItemIndex, int value){
		//TODO: nho set feedbacked
		DataItem dataItem = dataItems.get(dataItemIndex);
		this.numAssignments++;
		Assignment assignment = new Assignment(sourceIndex, dataItemIndex ,value , isFeedbacked());
		dataItem.addAssignment(assignment);
		assert dataItem != null;
		assert dataItem.getValueCount() != null;
		System.out.println(dataItem.getDataItemIndex());
		System.out.println(dataItem.getValueCount());
		int count = dataItem.getValueCount().get(value);
		dataItem.getValueCount().put(value, count + 1);
		//TODO: neu value add vao ko chua trong value set cua data Item => phai them column, count ....
		if(!dataItem.getValueProb().keySet().contains(value)){

		}
	}*/

/*	public void addUserFeedback(int value,int dataItemIndex){
		if(isFeedbacked()){
			setFeedbacked(true);
			DataItem dataItem = dataItems.get(dataItemIndex);
			for(int i=0; i< numSurrogates; i++){
				Assignment assignment = new Assignment(numSources + i,dataItemIndex, value,true);
				dataItem.addAssignment(assignment);

				//Chi khi nao value moi moi add. Ko thi thoi.
				//TODO
				if(!dataItem.getValueProb().keySet().contains(value)){

				}
			}
		}else{
			DataItem dataItem = dataItems.get(dataItemIndex);
		}

	}*/

    public Map<Integer, List<Assignment>> getSourceAssignments() {
        Map<Integer, List<Assignment>> retVal = new HashMap<Integer, List<Assignment>>();
        for (DataItem dtItm : dataItems) {
            for (Assignment ass : dtItm.getAssignments()) {
                int srcInd = ass.getSourceIndex();
                if (retVal.containsKey(srcInd)) {
                    List<Assignment> list = retVal.get(srcInd);
                    list.add(ass);
                    retVal.put(srcInd, list);
                } else {
                    List<Assignment> l = new ArrayList<Assignment>();
                    l.add(ass);
                    retVal.put(srcInd, l);
                }
            }
        }
        return retVal;
    }

    public boolean isFeedbacked() {
        return feedbacked;
    }

    public void setFeedbacked(boolean feedbacked) {
        this.feedbacked = feedbacked;
    }

    public void printValueProb() {
        for (DataItem dataItem : dataItems) {
            SortedMap<Integer, Double> sth = dataItem.getValueProb();
            Set<Integer> probs = sth.keySet();
            for (Integer integer : probs) {
                Double prob = sth.get(integer);
                System.out.println(String.format("D%d Val %d P %f ", dataItem.getDataItemIndex(), integer, prob));
            }
        }
    }

    //Pretty printing the dataset data
    public void prettyPrint() {
        for (int i = 0; i < this.getNumSources(); i++) {
            if (i > 10)
                System.out.printf("S%d ", i);
            else
                System.out.printf("S%-1d ", i);
        }
        System.out.println();

        for (DataItem dataItem : dataItems) {
            List<Assignment> assignments = dataItem.getAssignments();
            for (int i = 0; i < this.getNumSources(); i++) {
                if (i < 10) {
                    boolean flag = false;
                    for (Assignment assignment : assignments) {
                        if (assignment.getSourceIndex() == i) {
                            System.out.printf("%-3d", assignment.getValue());
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        System.out.printf("%-3s", "X");
                    }
                } else {
                    boolean flag = false;
                    for (Assignment assignment : assignments) {
                        if (assignment.getSourceIndex() == i) {
                            System.out.printf("%-4d", assignment.getValue());
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        System.out.printf("%-4s", "X");
                    }
                }
            }
            System.out.println();
        }
    }

    //Print reliability of src
    public void printTrust() {
        Set<Integer> sources = this.sourceTrustworthiness.keySet();
        for (Integer integer : sources) {
            Double trust = sourceTrustworthiness.get(integer);
            System.out.println(String.format("S%d T %f", integer, trust));
        }
    }

    //Get the list of selective values
    public List<Integer> getSelectiveValues() {
        List<Integer> retVal = new ArrayList<Integer>();
        for (DataItem item : dataItems) {
            retVal.add(item.getDataItemIndex(), item.getSelectiveValue());
        }
        return retVal;
    }

    //Calculate the precision of the selective set based on ground truth
    public double calculatePrecision() {
        double retVal = 0;
        int sum = 0;
        for (DataItem item : dataItems) {
            if (item.getSelectiveValue() == item.getGroundTruth()) {
                sum++;
            }
        }
        retVal = (double) sum / dataItems.size();
        return retVal;
    }

    //Estimate src reliability based on selective category and feedback category
    public Map<Integer, Double> calculateSrcTrust() {
        Map<Integer, Double> retVal = new HashMap<Integer, Double>();

        Map<Integer, Integer> numAsses = new HashMap<Integer, Integer>();
        Map<Integer, Integer> numCorrect = new HashMap<Integer, Integer>();
        int comparedValue = -1;
        for (DataItem dataItem : dataItems) {

            if (!dataItem.isFeedbacked()) {
                if (dataItem.getValueProb().keySet().size() == 0) {
                    continue;
                }
                comparedValue = dataItem.getSelectiveValue();
            } else {
                comparedValue = dataItem.getGroundTruth();
            }

            List<Assignment> assignments = dataItem.getAssignments();
            for (Assignment assignment : assignments) {
                int srcIndex = assignment.getSourceIndex();

                if (numAsses.containsKey(srcIndex)) {
                    int numAss = numAsses.get(srcIndex);
                    numAss++;
                    numAsses.put(srcIndex, numAss);
                } else {
                    numAsses.put(srcIndex, 1);
                }


                if (numCorrect.containsKey(srcIndex)) {
                    if (assignment.getValue() == comparedValue) {
                        int numGood = numCorrect.get(srcIndex);
                        numGood++;
                        numCorrect.put(srcIndex, numGood);
                    }
                } else {
                    if (assignment.getValue() == comparedValue) {
                        numCorrect.put(srcIndex, 1);
                    } else {
                        numCorrect.put(srcIndex, 0);
                    }
                }
            }
        }


        Set<Integer> srcIndices = numAsses.keySet();
        for (Integer srcIndex : srcIndices) {
            int numAss = numAsses.get(srcIndex);
            int numGood = numCorrect.get(srcIndex);
            double value = (double) ((double) numGood / (double) numAss);
            retVal.put(srcIndex, value);
        }

        return retVal;
    }

    public DataSource deleteSource(int srcInd) throws Exception {
        DataSource src = this.getSource(srcInd);
        src.setDeleted(true);
        assert src.getDataSourceIndex() == srcInd;
        List<Assignment> asses = src.getAssignments();

        //Delete assignments of this src from all data items
        for (Assignment assignment : asses) {
            DataItem itm = dataItems.get(assignment.getDataItemIndex());
            assert itm.getDataItemIndex() == assignment.getDataItemIndex();
            boolean removalRet = itm.getAssignments().remove(assignment);
            if (removalRet == false) {
                throw new Exception("Assignment not found in dataitem " + itm.getDataItemIndex() + " of src " + srcInd);
            }
        }

        //Remove from dataSources
        dataSources.remove(src);

        //Remove from trustworthiness
        this.sourceTrustworthiness.remove(srcInd);

        return src;
    }

    public List<DataSource> getDataSources(boolean alsoDeleted) {
        if (!alsoDeleted) {
            return dataSources;
        } else {
            List<DataSource> retVal = new ArrayList<DataSource>();
            for (DataSource ds : this.dataSources) {
                if (!ds.isDeleted()) {
                    retVal.add(ds);
                }
            }
            return retVal;
        }
    }

    public void setDataSources(List<DataSource> dataSources) {
        this.dataSources = dataSources;
    }

    public boolean isSrcAvailable(int srcInd) {
        for (DataSource src : this.dataSources) {
            if (src.getDataSourceIndex() == srcInd) {
                return true;
            }
        }
        return false;
    }

    public DataSource getSource(int srcInd) {
        for (DataSource src : this.dataSources) {
            if (src.getDataSourceIndex() == srcInd) {
                return src;
            }
        }
        return null;
    }

    public int getNumCopiers(boolean alsoDeleted) {
        int retVal = 0;
        for (DataSource src : getDataSources(alsoDeleted)) {
            if (src.isCopier()) {
                retVal++;
            }
        }
        return retVal;
    }

    public int getNumUnreliable(double threshold, boolean alsoDeleted) {
        int retVal = 0;
        for (DataSource src : getDataSources(alsoDeleted)) {
            if (Math.abs(src.getReliability() - 0.5) <= threshold) {
                retVal++;
            }
        }
        return retVal;
    }

    public List<DataItem> getFeedbackedDataItems() {
        List<DataItem> retVal = new ArrayList<DataItem>();
        for (DataItem dataItem : dataItems) {
            if (dataItem.isFeedbacked()) {
                retVal.add(dataItem);
            }
        }
        return retVal;
    }

}
