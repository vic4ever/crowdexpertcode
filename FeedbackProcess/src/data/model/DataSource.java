package data.model;

import java.util.*;

public class DataSource {
    private int dataSourceIndex;
    //dataItemIndex - value
    private List<Assignment> assignments;
    private String tag = "";
    //Is this source a copier from other source ?
    private boolean isCopier = false;
    //The original source from which this src copies from
    // -1 means this is original
    private int originalSource = -1;
    private double estimatedReliability = Double.MIN_NORMAL;
    private double reliability = Double.MIN_NORMAL;
    private boolean isDeleted = false;

    public DataSource clone(List<Assignment> asses) {
        DataSource clonedSrc = new DataSource();
        //new DataSource(this.dataSourceIndex, asses, isCopier(), getOriginalSource(), tag, reliability, getEstimatedReliability());
        clonedSrc.setDataSourceIndex(this.dataSourceIndex);
        clonedSrc.setAssignmentList(asses);
        clonedSrc.setCopier(this.isCopier());
        clonedSrc.setOriginalSource(this.getOriginalSource());
        clonedSrc.tag = this.tag;
        clonedSrc.reliability = this.reliability;
        clonedSrc.setEstimatedReliability(this.getEstimatedReliability());
        clonedSrc.setDeleted(this.isDeleted);
        return clonedSrc;
    }

    public DataSource() {
        assignments = new ArrayList<Assignment>();
    }

    public DataSource(int srcIndex, List<Assignment> asses, boolean copi, int origin, String taag, double reliability, double estimatedReliability) {
        this.setDataSourceIndex(srcIndex);
        this.setAssignmentList(asses);
        this.setCopier(copi);
        this.setOriginalSource(origin);
        this.tag = taag;
        this.reliability = reliability;
        this.setEstimatedReliability(estimatedReliability);
    }

    public List<Assignment> getAssignments() {
        return assignments;
    }


    //From source ordering to data item ordering
    public void setAssignmentList(List<Assignment> asses) {
        if (assignments == null) {
            assignments = new ArrayList<Assignment>();
        }
        List<Assignment> cloned = new ArrayList<Assignment>();
        for (Assignment assignment : asses) {
            cloned.add(assignment);
        }
        Collections.sort(cloned, new Comparator<Assignment>() {

            @Override
            public int compare(Assignment arg0, Assignment arg1) {
                return arg0.getDataItemIndex() - arg1.getDataItemIndex();
            }

        });
//		assert asses.get(0) != cloned.get(0);
        //assert asses.get(0).getDataItemIndex() == 0;
        assignments = cloned;
    }

    public double getReliability() {
        return reliability;
    }

    public void setReliability(double reliability) {
        this.reliability = reliability;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getDataSourceIndex() {
        return dataSourceIndex;
    }

    public void setDataSourceIndex(int dataSourceIndex) {
        this.dataSourceIndex = dataSourceIndex;
    }

    public void addAssignment(Assignment ass) {
        if (this.assignments == null) {
            this.assignments = new ArrayList<Assignment>();
        }
        this.assignments.add(ass);
    }

    public double getEstimatedReliability() {
        return estimatedReliability;
    }

    public void setEstimatedReliability(double estimatedReliability) {
        this.estimatedReliability = estimatedReliability;
    }

    public int getOriginalSource() {
        return originalSource;
    }

    public void setOriginalSource(int originalSource) {
        this.originalSource = originalSource;
        if (this.originalSource != -1) {
            this.setCopier(true);
        } else {
            this.setCopier(false);
        }

    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isCopier() {
        return isCopier;
    }

    public void setCopier(boolean isCopier) {
        this.isCopier = isCopier;
    }

    public Assignment getAssignment(int dataItemIndex) {
        //TODO: assume 1 worker and 1 data item = 1 assignment
        for (Assignment assignment : this.assignments) {
            if (assignment.getDataItemIndex() == dataItemIndex) {
                return assignment;
            }
        }
        return null;
    }

}
