package data.reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;

public class GALDataReader {
    public static DataSet readGALLabels(String filename) throws Exception {
        @SuppressWarnings("resource")
        FileReader file = new FileReader(filename);
        @SuppressWarnings("resource")
        BufferedReader reader = new BufferedReader(file);

        DataSet dataSet = new DataSet();
        int numItems = 0;
        int numAss = 0;
        List<DataItem> items = new ArrayList<DataItem>(1001);
        Map<Integer, DataSource> srcs = new HashMap<Integer, DataSource>(1001);
        Set<Integer> dataItemIndices = new HashSet<Integer>();
        Set<Integer> sourceIndices = new HashSet<Integer>();
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] vals = line.split("\t");
            if (vals.length > 3) {
                throw new Exception("Number of information per line is more than 3 - Worker DataItem Value");
            }

            //Get the data source for this label. Create new if not exists
            int sourceIndex = Integer.parseInt(vals[0]);
            DataSource src = null;
            if (!srcs.containsKey((Integer) sourceIndex)) {
                src = new DataSource();
                src.setDataSourceIndex(sourceIndex);
                srcs.put(sourceIndex, src);
            } else {
                src = srcs.get(sourceIndex);
            }

            //Get the data item for this label. Create new if not exists
            int itemIndex = Integer.parseInt(vals[1]);
            DataItem item = null;
            if (!dataItemIndices.contains((Integer) itemIndex)) {
                numItems++;
                dataItemIndices.add(itemIndex);
                item = new DataItem();
                item.setDataItemIndex(itemIndex);
                items.add(itemIndex, item);
            } else {
                item = items.get(itemIndex);
            }

            //Create an assignment for this label
            int value = Integer.parseInt(vals[2]);
            Assignment assignment = new Assignment(sourceIndex, itemIndex, value, false);
            numAss++;
            //#Assign it to the correct data item
            item.addAssignment(assignment);
            Integer val = item.getValueCount().get(value);
            if (val == null) {
                item.getValueCount().put(value, 1);
            } else {
                item.getValueCount().put(value, val + 1);
            }
            //#Assign it to the correct data source
            src.addAssignment(assignment);
        }
//		dataSet.setNumSources(sourceIndices.size());
//		dataSet.setNumDataItems(numItems);
        dataSet.setDataItems(items);
//		dataSet.setNumAssignments(numAss);
        List<DataSource> srcss = new ArrayList<DataSource>(srcs.values());
        Collections.sort(srcss, new Comparator<DataSource>() {

            @Override
            public int compare(DataSource arg0, DataSource arg1) {
                return arg0.getDataSourceIndex() - arg1.getDataSourceIndex();
            }

        });
        dataSet.setDataSources(srcss);

        return dataSet;
    }

    public static DataSet readGALGoldLabels(String goldFileName, DataSet dataSet) throws IOException {
        FileReader file = new FileReader(goldFileName);
        @SuppressWarnings("resource")
        BufferedReader reader = new BufferedReader(file);

        List<DataItem> dataItems = dataSet.getDataItems();
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] vals = line.split("\t");
            int dataItemIndex = Integer.parseInt(vals[0]);
            int value = Integer.parseInt(vals[1]);
            DataItem dataItem = dataItems.get(dataItemIndex);
            dataItem.setGroundTruth(value);
        }
        return dataSet;
    }

    public static DataSet readCategories(String categoryFile, DataSet dataSet) throws IOException {
        FileReader file = new FileReader(categoryFile);
        BufferedReader reader = new BufferedReader(file);

        String line = "";
        Set<Integer> categories = new HashSet<Integer>();
        while ((line = reader.readLine()) != null) {
            int category = Integer.parseInt(line);
            categories.add(category);
        }
        reader.close();

        List<DataItem> dataItems = dataSet.getDataItems();
        for (DataItem dataItem : dataItems) {
            dataItem.categoryList = categories;
        }
        return dataSet;
    }

    public static DataSet readWorkerReliability(String workerFile, DataSet dataSet) throws IOException {
        FileReader file = new FileReader(workerFile);
        BufferedReader reader = new BufferedReader(file);

        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] vals = line.split("\t");
            int srcId = Integer.parseInt(vals[0]);
            DataSource src = dataSet.getSource(srcId);
            assert src.getDataSourceIndex() == srcId;
            double reliability = Double.parseDouble(vals[1]);
            src.setReliability(reliability);
            int originSrcId = Integer.parseInt(vals[2]);
            src.setOriginalSource(originSrcId);
            if (vals.length > 3) {
                String workerType = vals[3];
                src.setTag(workerType);
            } else {
                src.setTag("NA");
            }
        }
        reader.close();

        return dataSet;
    }

    public static DataSet readDatsetFromFolder(String folder) throws Exception {
        DataSet dataSet1 = GALDataReader.readGALLabels(folder + "/" + "label.txt");
        DataSet dataSet2 = GALDataReader.readGALGoldLabels(folder + "/" + "gold.txt", dataSet1);
        DataSet dataSet3 = GALDataReader.readCategories(folder + "/" + "category.txt", dataSet2);
        DataSet dataSet = GALDataReader.readWorkerReliability(folder + "/" + "worker.txt", dataSet3);
        return dataSet;
    }

}



