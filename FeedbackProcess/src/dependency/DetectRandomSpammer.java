package dependency;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.doublealgo.Transform;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;
import cern.colt.matrix.linalg.SingularValueDecomposition;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Ints;
import data.model.DataSet;
import data.model.DataSource;
//import edu.umbc.cs.maple.utils.ColtUtils;
import jsc.independentsamples.SmirnovTest;
import org.ejml.simple.SimpleMatrix;
import org.ejml.simple.SimpleSVD;
import utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Asus on 4/23/14.
 */
public class DetectRandomSpammer {
    private DataSet dataSet;
    double threshold;

    public DetectRandomSpammer(DataSet dataSet, double threshold) {
        this.dataSet = dataSet;
        this.threshold = threshold;
    }

    public Map<DataSource, Double> detectRandomSpammer() {

        Map<DataSource, Double> spammers = new HashMap<DataSource, Double>();
        for (DataSource src : dataSet.getDataSources(false)) {
            double val = calculateSpammerScore(src);
            //Near 0: random spammer
            spammers.put(src, val);
        }
        return spammers;
    }

    // See Eliminating Spammers and Ranking Annotators for Crowdsourced Labeling Tasks
    public double calculateSpammerScore(DataSource src) {
//        DoubleMatrix2D matrixE = ColtUtils.ones(dataSet.getNumLabels(), 1);

        int[][] confusionMatrix = Utils.generateConfusionMatrix(dataSet, src);
        if (allZeros(confusionMatrix)) {
            // Rank 0 matrix
            return 999999;
        }
        DoubleMatrix2D cm = new DenseDoubleMatrix2D(normalize(intArray2Double(confusionMatrix)));
        SingularValueDecomposition svd = new SingularValueDecomposition(cm);
        double[] vals = svd.getSingularValues();
        double sum = 0;
        for (int i = 1; i < dataSet.getNumLabels(); i++) {
            sum += vals[i] * vals[i];
        }

        return Math.sqrt(sum) / (dataSet.getNumLabels() - 1);

//        int[][] confusionMatrix = Utils.generateConfusionMatrix(dataSet, src);
//        double[][] temp = minusMeanOfEachRow(normalize(intArray2Double(confusionMatrix)));
//        double val = frobeniusNorm(temp);
//        return val * val / (dataSet.getNumLabels() * (dataSet.getNumLabels() - 1));
    }

//    public double calculateSpammerScore(DataSource src) {
//        return calculateScore(src);
//    }


    public double calculateScore(DataSource src) {
        int[][] confusionMatrix = Utils.generateConfusionMatrix(dataSet, src);
        SimpleMatrix matrix = new SimpleMatrix(intArray2Double(confusionMatrix));
        SimpleSVD svd = matrix.svd();
        SimpleMatrix W = svd.getW();
        double sum = 0;
        for (int i = 1; i < dataSet.getNumLabels(); i++) {
            sum += W.get(i, i);
        }
        return Math.sqrt(sum) / dataSet.getNumLabels();
    }

    public double frobeniusNorm(double[][] matrix) {
        double sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                sum += matrix[i][j] * matrix[i][j];
            }
        }
        return Math.sqrt(sum);
    }

    public double[][] normalize(final double[][] matrix) {
        double[][] m = new double[matrix.length][matrix.length];

        // Normalize
        for (int i = 0; i < matrix.length; i++) {
            double[] row = matrix[i];
            double sum = sum(row);
            for (int j = 0; j < matrix.length; j++) {
                double val = row[j];
                if (sum != 0) {
                    m[i][j] = val / sum;
                } else {
                    m[i][j] = 0;
                }
            }
        }
        return m;
    }

    public double[][] minusMeanOfEachRow(final double[][] m) {
        double[][] retVal = new double[m.length][m.length];
        // Minus mean
        for (int i = 0; i < m.length; i++) {
            double[] row = m[i];
            double mean = mean(row);
            for (int j = 0; j < m.length; j++) {
                double val = row[j];
                retVal[i][j] = val - mean;
            }
        }

        return retVal;
    }

    public double sum(double[] m) {
        double sum = 0;
        for (double d : m) {
            sum += d;
        }
        return sum;
    }

    public double mean(double[] m) {
        double sum = 0;
        for (double d : m) {
            sum += d;
        }
        return (double) ((double) sum / (double) m.length);
    }

    private double[][] intArray2Double(int[][] array) {
        double[][] ret = new double[array.length][array.length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                ret[i][j] = array[i][j];
            }
        }
        return ret;
    }

    private boolean allZeros(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i][j] != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    private DoubleMatrix2D generateMatrixE(int n) {
        double[][] m = new double[n][1];
        for (int i = 0; i < n; i++) {
            m[i][0] = 1;
        }
        DoubleMatrix2D retVal = new DenseDoubleMatrix2D(m);
        return retVal;
    }

}
