package dependency;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utils.TupleR;

import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;

public class Detector {
    /**
     * Prior probability that an answer of a copier is copied
     * Since a copier may not copy all but only c*100% from the original src
     */
    //Fullcopy
//    private double c = 1;
    //Partly cop
	private double c = 0.8;

    /**
     * Prior probability that two subjects are dependent
     */
    private double a = 0.2;

    /**
     * Number of false values in a domain of each question
     */
    private int n = 1;

    /**
     * Error rate - probability such that a subject (user) give wrong answers
     * 1 - reliability cua source
     */
    private double e = 0.25;

    private DataSet dataSet;

    public Detector() {

    }

    public Detector(DataSet dataSet) {
        this.setDataSet(dataSet);
        n = dataSet.getNumLabels() - 1;
    }

    public Map<TupleR<Integer, Integer>, Double> calculateDependentProbability() {
        Map<TupleR<Integer, Integer>, Double> retVal = new HashMap<TupleR<Integer, Integer>, Double>();
        List<DataSource> srcs = getDataSet().getDataSources(false);
        for (int i = 0; i < srcs.size(); i++) {
            DataSource src1 = srcs.get(i);
            for (int j = i + 1; j < srcs.size(); j++) {
                DataSource src2 = srcs.get(j);
                double dependentProb = this.calculateDependentProbability2Src(src1, src2);
                TupleR<Integer, Integer> tuple = new TupleR<Integer, Integer>(src1.getDataSourceIndex(), src2.getDataSourceIndex());
                retVal.put(tuple, dependentProb);
//				System.out.println(src1.getDataSourceIndex() +" " + src1.getOriginalSource() + " "+ src2.getDataSourceIndex() + " " +src2.getOriginalSource() +" " + dependentProb);
            }
        }
        return retVal;
    }

    public int getKT(DataSource s1, DataSource s2){
        int kt = 0;
        int kf = 0;
        int kd = 0;
        //Assume same number of category and only one correct answer
        setN(getDataSet().getDataItems().get(0).categoryList.size() - 1);
        List<Assignment> s1Asses = s1.getAssignments();
        List<Assignment> s2Asses = s2.getAssignments();
        for (Assignment assignment : s1Asses) {
            DataItem itm1 = getDataSet().getDataItems().get(assignment.getDataItemIndex());
            //Only check feedback item ????
            if (!itm1.isFeedbacked()) {
                continue;
            }

            int groundTruth = itm1.getGroundTruth();

            //comparedValue is the estimated gold. If use other than gold, need to remove check isFeedback
            int comparedValue = groundTruth;

            for (Assignment ass2 : s2Asses) {
                if (ass2.getDataItemIndex() == itm1.getDataItemIndex()) {
                    //Different
                    if (ass2.getValue() != assignment.getValue()) {
                        kd++;
                    } else {
                        //The same
                        if (ass2.getValue() == comparedValue) {
                            //The same and equal estimated gold
                            kt++;
                        } else {
                            //The same and different estimated gold
                            kf++;
                        }
                    }
                }
            }
        }
        return kt;
    }

    public int getKF(DataSource s1, DataSource s2){
        int kt = 0;
        int kf = 0;
        int kd = 0;
        //Assume same number of category and only one correct answer
        setN(getDataSet().getDataItems().get(0).categoryList.size() - 1);
        List<Assignment> s1Asses = s1.getAssignments();
        List<Assignment> s2Asses = s2.getAssignments();
        for (Assignment assignment : s1Asses) {
            DataItem itm1 = getDataSet().getDataItems().get(assignment.getDataItemIndex());
            //Only check feedback item ????
            if (!itm1.isFeedbacked()) {
                continue;
            }

            int groundTruth = itm1.getGroundTruth();

            //comparedValue is the estimated gold. If use other than gold, need to remove check isFeedback
            int comparedValue = groundTruth;

            for (Assignment ass2 : s2Asses) {
                if (ass2.getDataItemIndex() == itm1.getDataItemIndex()) {
                    //Different
                    if (ass2.getValue() != assignment.getValue()) {
                        kd++;
                    } else {
                        //The same
                        if (ass2.getValue() == comparedValue) {
                            //The same and equal estimated gold
                            kt++;
                        } else {
                            //The same and different estimated gold
                            kf++;
                        }
                    }
                }
            }
        }
        return kf;
    }

    public int getKD(DataSource s1, DataSource s2){
        int kt = 0;
        int kf = 0;
        int kd = 0;
        //Assume same number of category and only one correct answer
        setN(getDataSet().getDataItems().get(0).categoryList.size() - 1);
        List<Assignment> s1Asses = s1.getAssignments();
        List<Assignment> s2Asses = s2.getAssignments();
        for (Assignment assignment : s1Asses) {
            DataItem itm1 = getDataSet().getDataItems().get(assignment.getDataItemIndex());
            //Only check feedback item ????
            if (!itm1.isFeedbacked()) {
                continue;
            }

            int groundTruth = itm1.getGroundTruth();

            //comparedValue is the estimated gold. If use other than gold, need to remove check isFeedback
            int comparedValue = groundTruth;

            for (Assignment ass2 : s2Asses) {
                if (ass2.getDataItemIndex() == itm1.getDataItemIndex()) {
                    //Different
                    if (ass2.getValue() != assignment.getValue()) {
                        kd++;
                    } else {
                        //The same
                        if (ass2.getValue() == comparedValue) {
                            //The same and equal estimated gold
                            kt++;
                        } else {
                            //The same and different estimated gold
                            kf++;
                        }
                    }
                }
            }
        }
        return kd;
    }


    public double calculateDependentProbability2Src(DataSource s1, DataSource s2) {
        int kt = 0;
        int kf = 0;
        int kd = 0;
        //Assume same number of category and only one correct answer
        setN(getDataSet().getDataItems().get(0).categoryList.size() - 1);
        List<Assignment> s1Asses = s1.getAssignments();
        List<Assignment> s2Asses = s2.getAssignments();
        for (Assignment assignment : s1Asses) {
            DataItem itm1 = getDataSet().getDataItems().get(assignment.getDataItemIndex());
            //Only check feedback item ????
            if (!itm1.isFeedbacked()) {
                continue;
            }

            int groundTruth = itm1.getGroundTruth();

            //comparedValue is the estimated gold. If use other than gold, need to remove check isFeedback
            int comparedValue = groundTruth;

            for (Assignment ass2 : s2Asses) {
                if (ass2.getDataItemIndex() == itm1.getDataItemIndex()) {
                    //Different
                    if (ass2.getValue() != assignment.getValue()) {
                        kd++;
                    } else {
                        //The same
                        if (ass2.getValue() == comparedValue) {
                            //The same and equal estimated gold
                            kt++;
                        } else {
                            //The same and different estimated gold
                            kf++;
                        }
                    }
                }
            }
        }

        double dependProb = Double.NaN;
        if (c == 1) {
            dependProb = 1 / (1 + ((1 - a) / a)
                    * Math.pow((1 - e) / (1 - e + c * e), kt)
                    * Math.pow(e / (c * n + e - c * e), kf));
        } else {
            dependProb = 1 / (1 + ((1 - a) / a)
                    * Math.pow((1 - e) / (1 - e + c * e), kt)
                    * Math.pow(e / (c * n + e - c * e), kf)
                    * Math.pow(1 / (1 - c), kd));
        }
        return dependProb;
    }

    public DataSet getDataSet() {
        return dataSet;
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
}
