package dist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;

public class DistanceAware {

	private double delta = 1.0;
	private DataSet dataSet;

	public DistanceAware(DataSet ds, double del) {
		dataSet = ds;
		delta = del;
	}

	public void calculateTrustworthiness() {
		List<DataItem> dataItems = dataSet.getDataItems();

		for (DataItem dataItem : dataItems) {

			List<Assignment> assignments = dataItem.getAssignments();

			// Clustering
			assert assignments.size() >= 1;
			Assignment ass0 = assignments.get(0);
			List<List<Assignment>> clusters = new ArrayList<List<Assignment>>();
			List<Assignment> firstCluster = new ArrayList<Assignment>();
			firstCluster.add(ass0);
			clusters.add(firstCluster);

			for (int i = 1; i < assignments.size(); i++) {
				double val = (double) assignments.get(i).getValue();
				double minDist = Double.MAX_VALUE;
				List<Assignment> minCluster = null;

				for (List<Assignment> cluster : clusters) {
					double dist = cluster.get(0).getValue() - val;
					if (dist < minDist) {
						minDist = cluster.get(0).getValue() - val;
						minCluster = cluster;
					}
				}

				if (minDist < delta) {
					minCluster.add(assignments.get(i));
				} else {
					List<Assignment> cluster = new ArrayList<Assignment>();
					cluster.add(assignments.get(i));
					clusters.add(cluster);
				}
			}

			dataItem.majorityProbAssign();

			// Calculate probabilities
			for (List<Assignment> cluster : clusters) {
				double sim = calculateSim(cluster);
				double product = 1.0;
				for (Assignment assignment : cluster) {
					product *= (1 - dataItem.getValueProb().get(
							assignment.getValue())
							* sim);
				}
				double tf = 1 - product;

				for (Assignment assignment : cluster) {
					if (dataItem.isFeedbacked()) {
						if(assignment.getValue() == dataItem.getGroundTruth()){
							dataItem.getValueProb().put(assignment.getValue(), 1.0);	
						} else {
							dataItem.getValueProb().put(assignment.getValue(), 0.0);
						}
					} else {
						dataItem.getValueProb().put(assignment.getValue(), tf);
					}

				}
			}

		}

		// Calculate data source trustworthiness
		Map<Integer, Double> sourceTrust = new HashMap<Integer, Double>();
		for (DataSource src : dataSet.getDataSources(false)) {
			double srcProb = 0.0;
			List<Assignment> asses = src.getAssignments();
			for (Assignment assignment : asses) {
				DataItem item = dataSet.getDataItems().get(
						assignment.getDataItemIndex());
				double prob = item.getValueProb().get(assignment.getValue());
				srcProb += prob;
			}
			srcProb /= src.getAssignments().size();
			assert srcProb >= 0 && srcProb <= 1;
			sourceTrust.put(src.getDataSourceIndex(), srcProb);
		}
		dataSet.setTrustworthiness(sourceTrust);
	}

	private double calculateSim(List<Assignment> cluster) {
		double diameter = calculateDiameter(cluster);
		int size = cluster.size();
		return Math.exp(-diameter / size);
	}

	private double calculateDiameter(List<Assignment> cluster) {
		double diameter = Double.MIN_VALUE;
		for (int i = 0; i < cluster.size(); i++) {
			Assignment ass1 = cluster.get(i);
			double val1 = ass1.getValue();
			for (int j = i; j < cluster.size(); j++) {
				Assignment ass2 = cluster.get(j);
				double val2 = ass2.getValue();
				if (val2 - val1 > diameter) {
					diameter = val2 - val1;
				}
			}
		}
		return diameter;
	}
}
