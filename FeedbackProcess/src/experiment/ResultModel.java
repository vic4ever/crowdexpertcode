package experiment;

import java.util.List;

/**
 * Created by Asus on 7/23/14.
 */
public class ResultModel {
    private String ID;
    private String algorithm;
    private String time;
    private List<Double> precisions;
    private List<Double> entropies;
    private List<Integer> itemIndices;
    private String reliability;
    private String std;
    private String numSrc;
    private List<Double> spamPrec;
    private List<Double> spamRecall;
    private List<Double> copierPrec;
    private List<Double> copierRecall;
    private String percent1;
    private String percent2;
    private String reliability2;
    private String copyRatio;
    private String detectThreshold;
    private String removeSpammer;//
    private String removeCopier;//
    private String detectSpammer;//
    private String detectCopyThreshold;
    private List<Double> labelProbabilities;
    private List<Integer> correctLabels;
    private String maxIter;
    private String feedbackPerQuestion;
    private String budgetRatio;

    public String getBudgetRatio() {
        return budgetRatio;
    }

    public void setBudgetRatio(String budgetRatio) {
        this.budgetRatio = budgetRatio;
    }

    public String getFeedbackPerQuestion() {
        return feedbackPerQuestion;
    }

    public void setFeedbackPerQuestion(String feedbackPerQuestion) {
        this.feedbackPerQuestion = feedbackPerQuestion;
    }

    public String getMaxIter() {
        return maxIter;
    }

    public void setMaxIter(String maxIter) {
        this.maxIter = maxIter;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Double> getPrecisions() {
        return precisions;
    }

    public void setPrecisions(List<Double> precisions) {
        this.precisions = precisions;
    }

    public List<Double> getEntropies() {
        return entropies;
    }

    public void setEntropies(List<Double> entropies) {
        this.entropies = entropies;
    }

    public List<Integer> getItemIndices() {
        return itemIndices;
    }

    public void setItemIndices(List<Integer> itemIndices) {
        this.itemIndices = itemIndices;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getReliability() {
        return reliability;
    }

    public void setReliability(String reliability) {
        this.reliability = reliability;
    }

    public String getStd() {
        return std;
    }

    public void setStd(String std) {
        this.std = std;
    }

    public String getNumSrc() {
        return numSrc;
    }

    public void setNumSrc(String numSrc) {
        this.numSrc = numSrc;
    }

    public List<Double> getSpamPrec() {
        return spamPrec;
    }

    public void setSpamPrec(List<Double> spamPrec) {
        this.spamPrec = spamPrec;
    }

    public List<Double> getSpamRecall() {
        return spamRecall;
    }

    public void setSpamRecall(List<Double> spamRecall) {
        this.spamRecall = spamRecall;
    }

    public String getPercent1() {
        return percent1;
    }

    public void setPercent1(String percent1) {
        this.percent1 = percent1;
    }

    public String getPercent2() {
        return percent2;
    }

    public void setPercent2(String percent2) {
        this.percent2 = percent2;
    }

    public String getReliability2() {
        return reliability2;
    }

    public void setReliability2(String reliability2) {
        this.reliability2 = reliability2;
    }

    public String getCopyRatio() {
        return copyRatio;
    }

    public void setCopyRatio(String copyRatio) {
        this.copyRatio = copyRatio;
    }

    public String getDetectThreshold() {
        return detectThreshold;
    }

    public void setDetectThreshold(String detectThreshold) {
        this.detectThreshold = detectThreshold;
    }

    public String getRemoveSpammer() {
        return removeSpammer;
    }

    public void setRemoveSpammer(String removeSpammer) {
        this.removeSpammer = removeSpammer;
    }

    public String getDetectSpammer() {
        return detectSpammer;
    }

    public void setDetectSpammer(String detectSpammer) {
        this.detectSpammer = detectSpammer;
    }

    public String getRemoveCopier() {
        return removeCopier;
    }

    public void setRemoveCopier(String removeCopier) {
        this.removeCopier = removeCopier;
    }

    public String getDetectCopyThreshold() {
        return detectCopyThreshold;
    }

    public void setDetectCopyThreshold(String detectCopyThreshold) {
        this.detectCopyThreshold = detectCopyThreshold;
    }

    public List<Double> getCopierPrec() {
        return copierPrec;
    }

    public void setCopierPrec(List<Double> copierPrec) {
        this.copierPrec = copierPrec;
    }

    public List<Double> getCopierRecall() {
        return copierRecall;
    }

    public void setCopierRecall(List<Double> copierRecall) {
        this.copierRecall = copierRecall;
    }

    public List<Double> getLabelProbabilities() {
        return labelProbabilities;
    }

    public void setLabelProbabilities(List<Double> labelProbabilities) {
        this.labelProbabilities = labelProbabilities;
    }

    public List<Integer> getCorrectLabels() {
        return correctLabels;
    }

    public void setCorrectLabels(List<Integer> correctLabels) {
        this.correctLabels = correctLabels;
    }
}
