package experiment.stock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

import utils.ArrayUti;

import com.google.common.base.Joiner;

import data.converter.Stock2GAL;
import data.model.DataSet;
import data.reader.GALDataReader;
import feedback.depend.DependentSrcRemover;
import feedback.depend.NormalRemover;
import feedback.strategy.BaseLineFeedbackSequence;
import feedback.strategy.FeedbackSequence;
import feedback.strategy.IGDistFeedbackSequence;
import feedback.strategy.IGEMDependFeedbackSequence;
import feedback.strategy.IGFeedbackSequence;
import feedback.strategy.selection.CommonFalseBasedSelection;
import feedback.strategy.selection.IGEMDependSelection;
import feedback.strategy.selection.SelectionAlgorithm;

public class ExperimentEM {
	String label = "./stock/label.txt";
	String gold = "./stock/gold.txt";
	String cat = "./stock/category.txt";
	
	@Test
	public void IEMNoDelSequence() throws Exception{
		DataSet copiedDataSet = GALDataReader.readDatsetFromFolder("./copy");
		copiedDataSet.initTrustworthiness();
		FeedbackSequence noDelSequence = new IGFeedbackSequence(copiedDataSet.getNumDataItems());
		noDelSequence.setData(copiedDataSet);
		long startND = System.currentTimeMillis();		
		noDelSequence.feedbackLoop();
		long endND = System.currentTimeMillis();
		System.out.println("Timeeeee: " + Long.toString(endND - startND));
		System.out.println("Delllllllllllllllllllllllllllllllllll");
		System.out.println("Nodelllllllllllllllllllllllllllllllllll");
		System.out.println(Joiner.on('\t').join(noDelSequence.precisionList));
		System.out.println(Joiner.on('\t').join(noDelSequence.entropyList));
		System.out.println(Joiner.on('\t').join(noDelSequence.itemIndices));
//		FileUtils.writeStringToFile(IGEMF , Joiner.on('\t').join(noDelSequence.precisionList)+"\n", true);
//		FileUtils.writeStringToFile(IGEMF, Joiner.on('\t').join(noDelSequence.entropyList)+"\n", true);
//		FileUtils.writeStringToFile(IGEMF, Joiner.on('\t').join(noDelSequence.itemIndices)+"\n", true);
	}
	
	
	@Test
	public void IGEMDelSequence() throws Exception{
		String folder = "./copy/";
		String lab = folder + "label.txt";
		String gol = folder + "gold.txt";
		String cate = folder + "category.txt";
		Stock2GAL converter = new Stock2GAL();
		converter.run(lab, gol, cate);
		
		DataSet copyDataSet = GALDataReader.readDatsetFromFolder("./copy");
		System.out.println(copyDataSet.getNumDataItems());
		System.out.println(copyDataSet.getNumSources());

		//Deletion strategy
		DependentSrcRemover remover = new NormalRemover();
		remover.setThreshold(0.9);
		remover.DEBUG = false;

		//Selection strategy
		Map<Integer,SelectionAlgorithm> algors = new HashMap<Integer,SelectionAlgorithm>();
		SelectionAlgorithm IG = new IGEMDependSelection(copyDataSet,remover);
		//		SelectionAlgorithm noobEntropy = new FrequentEntropySelection(dataSet);
		SelectionAlgorithm noobEntropy= new CommonFalseBasedSelection(copyDataSet);
		//Has set not delete src after 5 iteration
		for(int i = 0; i< copyDataSet.getNumDataItems(); i++){
			//			if(i%4 ==0 || i%4 == 1 || i <= 5){
			if(i<=5){
				algors.put(i, noobEntropy);
			}else{
				algors.put(i, IG);
			}
		}


		DependentSrcRemover remover2 = new NormalRemover();
		remover2.setThreshold(0.9);
		remover2.DEBUG = true;

		//Feedback
		FeedbackSequence hitsSequence = new IGEMDependFeedbackSequence(copyDataSet.getNumDataItems(),algors);
		hitsSequence.setRemover(remover2);
		hitsSequence.setData(copyDataSet);
		long startD = System.currentTimeMillis();
		hitsSequence.feedbackLoop();
		long endD = System.currentTimeMillis();
		System.out.println("Timeeeee: " + Long.toString(endD - startD));
		System.out.println("Delllllllllllllllllllllllllllllllllll");
		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
//		FileUtils.writeStringToFile(IGEMF, Joiner.on('\t').join(hitsSequence.precisionList)+"\n", true);
//		FileUtils.writeStringToFile(IGEMF, Joiner.on('\t').join(hitsSequence.entropyList)+"\n", true);
//		FileUtils.writeStringToFile(IGEMF, Joiner.on('\t').join(hitsSequence.itemIndices)+"\n", true);
	}
	
	@Test
	public void randomCopySequence() throws Exception{
		List<Double> sP = new ArrayList<Double>();
		Collections.fill(sP, new Double(0));
		List<Double> sE = new ArrayList<Double>();
		Collections.fill(sE, new Double(0));
		System.out.println("Randommmmmmmmmmmmmmmmmmmmmmm");
		int numRun = 100;
		for(int i =0 ; i<numRun; i++){
			DataSet copiedDataSet2 = GALDataReader.readDatsetFromFolder("./rep");
			System.out.println(copiedDataSet2.getNumDataItems());
			copiedDataSet2.initTrustworthiness();
			FeedbackSequence randomSequence = new BaseLineFeedbackSequence(copiedDataSet2.getNumDataItems());
			randomSequence.setData(copiedDataSet2);
			randomSequence.feedbackLoop();
			sP = ArrayUti.sum(sP, randomSequence.precisionList);
			sE = ArrayUti.sum(sE, randomSequence.entropyList);
			
		}
		List<Double> averageP = ArrayUti.average(sP, numRun);
		List<Double> averageE = ArrayUti.average(sE, numRun);
		System.out.println(Joiner.on('\t').join(averageP));
		System.out.println(Joiner.on('\t').join(averageE));
	}
	
	@Test
	public void expReal() throws Exception{
		String folder = "./rep/";
		String lab = folder + "label.txt";
		String gol = folder + "gold.txt";
		String cate = folder + "category.txt";
		Stock2GAL converter = new Stock2GAL();
//		converter.run(label, gold, cat);
		converter.run(lab, gol, cate);
//		DataSet dataSet1 = GALDataReader.readGALLabels(label);
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels(gold, dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories(cat, dataSet2);
		DataSet dataSet = GALDataReader.readDatsetFromFolder("./rep");
		System.out.println(dataSet.getNumDataItems());
		FeedbackSequence hitsSequence = new IGFeedbackSequence(dataSet.getNumDataItems());
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
	}
	
	
	@Test
	public void expFG() throws Exception{
//		String folder = "./rep/";
//		String lab = folder + "label.txt";
//		String gol = folder + "gold.txt";
//		String cate = folder + "category.txt";
//		Stock2GAL converter = new Stock2GAL();
////		converter.run(label, gold, cat);
//		converter.run(lab, gol, cate);
////		DataSet dataSet1 = GALDataReader.readGALLabels(label);
////		DataSet dataSet2 = GALDataReader.readGALGoldLabels(gold, dataSet1);
////		DataSet dataSet =  GALDataReader.readCategories(cat, dataSet2);
//		DataSet dataSet = GALDataReader.readDatsetFromFolder("./rep");
//		System.out.println(dataSet.getNumDataItems());
//		FeedbackSequence hitsSequence = new FGIGFeedbackSequence(dataSet.getNumDataItems());
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
//		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
//		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
	}
	
	@Test
	public void expDist() throws Exception{
		String folder = "./rep/";
		String lab = folder + "label.txt";
		String gol = folder + "gold.txt";
		String cate = folder + "category.txt";
		Stock2GAL converter = new Stock2GAL();
//		converter.run(label, gold, cat);
		converter.run(lab, gol, cate);
//		DataSet dataSet1 = GALDataReader.readGALLabels(label);
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels(gold, dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories(cat, dataSet2);
		DataSet dataSet = GALDataReader.readDatsetFromFolder("./rep");
		System.out.println(dataSet.getNumDataItems());
		FeedbackSequence hitsSequence = new IGDistFeedbackSequence(dataSet.getNumDataItems(), 1.0);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
	}
	
	@Test
	public void expBL() throws Exception{
		String folder = "./rep/";
		String lab = folder + "label.txt";
		String gol = folder + "gold.txt";
		String cate = folder + "category.txt";
		Stock2GAL converter = new Stock2GAL();
//		converter.run(label, gold, cat);
		converter.run(lab, gol, cate);
//		DataSet dataSet1 = GALDataReader.readGALLabels(label);
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels(gold, dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories(cat, dataSet2);
		DataSet dataSet = GALDataReader.readDatsetFromFolder("./rep");
		dataSet.initTrustworthiness();
		System.out.println(dataSet.getNumDataItems());
		FeedbackSequence hitsSequence = new BaseLineFeedbackSequence(dataSet.getNumDataItems());
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
	}
	
	@Test
	public void exp3BL2() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels(label);
		DataSet dataSet2 = GALDataReader.readGALGoldLabels(gold, dataSet1);
		DataSet dataSet =  GALDataReader.readCategories(cat, dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new BaseLineFeedbackSequence(dataSet.getNumDataItems());
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
	}
	
	@Test
	public void expNearOptimalLow() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels(label);
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels(gold, dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories(cat, dataSet2);
////		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(dataSet.getNumDataItems(),true);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
//		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
//		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
	}
	
	@Test
	public void experiment() throws Exception{
		expReal();
		System.out.println();
		expNearOptimalLow();
		System.out.println();
		for(int i = 0; i< 5; i++){
			exp3BL2();
			System.out.println();
		}
		
	}
}
