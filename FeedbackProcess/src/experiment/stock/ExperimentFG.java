package experiment.stock;

import data.model.DataSet;
import data.reader.GALDataReader;
import feedback.strategy.BaseLineFeedbackSequence;
import feedback.strategy.FeedbackSequence;

public class ExperimentFG {
	String label = "labels.txt";
	String gold = "golds.txt";
	String cat = "categories.txt";
	
//	public void expReal() throws Exception{
////		Stock2GAL converter = new Stock2GAL();
////		converter.run(label, gold, cat);
//		DataSet dataSet1 = GALDataReader.readGALLabels(label);
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels(gold, dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories(cat, dataSet2);
//		dataSet.initTrustworthiness();
//		FeedbackSequence hitsSequence = new FGIGFeedbackSequence(dataSet.getNumDataItems());
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//	
	
	public void exp3BL2() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels(label);
		DataSet dataSet2 = GALDataReader.readGALGoldLabels(gold, dataSet1);
		DataSet dataSet =  GALDataReader.readCategories(cat, dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new BaseLineFeedbackSequence(dataSet.getNumDataItems());
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	
//	public void expNearOptimalLow() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels(label);
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels(gold, dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories(cat, dataSet2);
//		dataSet.initTrustworthiness();
////		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(dataSet.getNumDataItems(),false);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//	
//	@Test
//	public void experiment() throws Exception{
//		expReal();
////		System.out.println();
////		expNearOptimalLow();
////		System.out.println();
////		for(int i = 0; i< 5; i++){
////			exp3BL2();
////			System.out.println();
////		}
//		
//	}
	
//	public static void main(String[] args){
//		ExperimentFG fg = new ExperimentFG();
//		try {
//			PrintStream out = new PrintStream(new FileOutputStream("consoleOutput.txt"));
//			System.setOut(out);
//			fg.experiment();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
}
