package experiment.stock;

import org.testng.annotations.Test;

import com.google.common.base.Joiner;

import data.model.DataSet;
import data.reader.GALDataReader;
import feedback.strategy.BaseLineFeedbackSequence;
import feedback.strategy.FeedbackSequence;
import feedback.strategy.IGDistFeedbackSequence;
import feedback.strategy.IGFeedbackSequence;

public class ExperimentLinked {
	
	@Test
	public void expEMLinked() throws Exception{
		DataSet dataSet = GALDataReader.readDatsetFromFolder("./health");
		System.out.println(dataSet.getNumDataItems());
		FeedbackSequence hitsSequence = new IGFeedbackSequence(dataSet.getNumDataItems());
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
	}
	
//	@Test
//	public void expFG() throws Exception{
//		DataSet dataSet = GALDataReader.readDatsetFromFolder("./health");
//		dataSet.initTrustworthiness();
//		System.out.println(dataSet.getNumDataItems());
//		FeedbackSequence hitsSequence = new FGIGFeedbackSequence(dataSet.getNumDataItems());
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
//		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
//		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
//	}
	
	@Test
	public void expDist() throws Exception{
		DataSet dataSet = GALDataReader.readDatsetFromFolder("./health");
		System.out.println(dataSet.getNumDataItems());
		FeedbackSequence hitsSequence = new IGDistFeedbackSequence(dataSet.getNumDataItems(), 1.0);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
	}
	
	@Test
	public void expBL() throws Exception{
		DataSet dataSet = GALDataReader.readDatsetFromFolder("./health");
		dataSet.initTrustworthiness();
		System.out.println(dataSet.getNumDataItems());
		FeedbackSequence hitsSequence = new BaseLineFeedbackSequence(dataSet.getNumDataItems());
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
