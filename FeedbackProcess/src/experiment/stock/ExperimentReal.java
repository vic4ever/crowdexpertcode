package experiment.stock;

import data.model.DataSet;
import data.reader.GALDataReader;
import experiment.synthetic.ExperimentCombined;
import feedback.strategy.GreedySequence;
import feedback.strategy.IGEMRandomSequence;

import experiment.ResultModel;
import feedback.strategy.RandomEMRandomSequence;
import utils.ArrayUti;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Asus on 7/23/14.
 */
public class ExperimentReal {
    // "./dataset/bluebirds"
    public static ResultModel experimentIGEM(String inputFolder, double detectThreshold, double reviveThreshold) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();
        IGEMRandomSequence woIGEM = new IGEMRandomSequence(woDataSet.getNumDataItems(), detectThreshold, true, true, reviveThreshold);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setCopierPrec(woIGEM.spamPrecisionList);
        resultModel.setCopierRecall(woIGEM.spamRecallList);
        resultModel.setID(-1 + "");
        resultModel.setAlgorithm("IGEMRandomRemoveSequence");
        resultModel.setStd(-1 + "");
        resultModel.setReliability(-1 + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        resultModel.setLabelProbabilities(woIGEM.getProbabilityCorrectness());
        resultModel.setCorrectLabels(woIGEM.correctLabels);
        return resultModel;
    }

    public static ResultModel experimentGreedy(String inputFolder, double detectThreshold, double reviveThreshold) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();
        GreedySequence woIGEM = new GreedySequence(woDataSet.getNumDataItems());
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setCopierPrec(woIGEM.spamPrecisionList);
        resultModel.setCopierRecall(woIGEM.spamRecallList);
        resultModel.setID(-1 + "");
        resultModel.setAlgorithm("Greedy");
        resultModel.setStd(-1 + "");
        resultModel.setReliability(-1 + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public static ResultModel experimentRandomEM(String inputFolder, double detectThreshold, double reviveThreshold) throws Exception {
        DataSet originalDS = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();

        List<Double> sP = new ArrayList<Double>();
        List<Double> sE = new ArrayList<Double>();
        List<Double> spamP = new ArrayList<Double>();
        List<Double> spamR = new ArrayList<Double>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
//            DataSet woDataSet2 = GALDataReader
//                    .readDatsetFromFolder(inputFolder);
            DataSet woDataSet2 = originalDS.clone();
            woDataSet2.initTrustworthiness();
            RandomEMRandomSequence sequence = new RandomEMRandomSequence(woDataSet2.getNumDataItems(), detectThreshold, false, false);
            sequence.setData(woDataSet2);
            sequence.feedbackLoop();
            sP = ArrayUti.sum(sP, sequence.precisionList);
            sE = ArrayUti.sum(sE, sequence.entropyList);
            spamP = ArrayUti.sum(spamP, sequence.spamPrecisionList);
            spamR = ArrayUti.sum(spamR, sequence.spamRecallList);
        }
        long end = System.currentTimeMillis();
        long time = (end - start) / 5;

        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> indices2 = new ArrayList<Double>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices2.add(-1.0);
        }

        List<Double> aSpamP = ArrayUti.average(spamP, 5);
        List<Double> aSpamR = ArrayUti.average(spamR, 5);

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setPrecisions(averageP);
        resultModel.setSpamPrec(indices2);
        resultModel.setSpamRecall(indices2);
        resultModel.setCopierPrec(indices2);
        resultModel.setCopierRecall(indices2);
        resultModel.setID(-1 + "");
        resultModel.setAlgorithm("RandomEMDetectRandom");
        resultModel.setStd(-1 + "");
        resultModel.setReliability(-1 + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public static void main(String[] args) {
//        String input = "./dataset/valence2";
        String input = "./dataset/bluebirds";
        String dateTime = new SimpleDateFormat("MMddhhmm").format(new Date());
        String outputFilePath = "expResults/" + ExperimentReal.class.getSimpleName() + "_" + dateTime + ".csv";
        double detectThreshold = 0.01;
        double reviveThreshold = 0.5;

        ResultModel igem = null;
        ResultModel randomEM = null;
        ResultModel greedy = null;


        try {
            File file = new File(outputFilePath);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);

//            randomEM = ExperimentReal.experimentRandomEM(input, detectThreshold, reviveThreshold);
//            StringBuilder randomEMBuilder = generateStringBuilder(randomEM);
//            bw.write(randomEMBuilder.toString());
//            bw.flush();
            greedy = ExperimentReal.experimentGreedy(input, detectThreshold, reviveThreshold);
            StringBuilder greedyStringBuilder = generateStringBuilder(greedy);
            bw.write(greedyStringBuilder.toString());
            bw.flush();

            igem = ExperimentReal.experimentIGEM(input, detectThreshold, reviveThreshold);
            StringBuilder igemBuilder = generateStringBuilder(igem);
            bw.write(igemBuilder.toString());
            bw.flush();

            bw.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static <T> String listInt2String(List<T> list) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size() - 1; i++) {
            T t = list.get(i);
            builder.append(t.toString()).append(",");
        }
        builder.append(list.get(list.size() - 1));
        return builder.toString();
    }

    public static StringBuilder generateStringBuilder(ResultModel resultModel) {
        // DetectThreshold, DetectCopyThreshold, Reliablity1,Rel2,Percent1,Percent2,CopyRatio,Std,LoopID,Algor,#Src,Time,Prec1,Prec2,...,Precn,Ent1,Ent2,..,Entn,SP1,Sp2,...,SPn,SR1,SR2,...,SRn,Ind1,Ind2,..,Indn
        String comma = ",";
        StringBuilder builder = new StringBuilder();
        builder.append(resultModel.getDetectThreshold()).append(comma);//
        builder.append(resultModel.getDetectCopyThreshold()).append(comma);//
        builder.append(resultModel.getReliability()).append(comma);//
        builder.append(resultModel.getReliability2()).append(comma);//
        builder.append(resultModel.getPercent1()).append(comma);//
        builder.append(resultModel.getPercent2()).append(comma);//
        builder.append(resultModel.getCopyRatio()).append(comma);//
        builder.append(resultModel.getStd()).append(comma);//
        builder.append(resultModel.getID()).append(comma);//
        builder.append(resultModel.getAlgorithm()).append(comma);//
        builder.append(resultModel.getNumSrc()).append(comma);//
        builder.append(resultModel.getTime()).append(comma);//
//                builder.append(listInt2String(resultModel.getLabelProbabilities())).append(comma);//
        builder.append(comma);
        builder.append(listInt2String(resultModel.getPrecisions())).append(comma);//
        builder.append(comma);
        builder.append(listInt2String(resultModel.getEntropies())).append(comma);//
        builder.append(comma);
        if (resultModel.getSpamPrec().size() > 0) {
            builder.append(listInt2String(resultModel.getSpamPrec())).append(comma);//
            builder.append(comma);
            builder.append(listInt2String(resultModel.getSpamRecall())).append(comma);//
            builder.append(comma);
        }
        if (resultModel.getCopierPrec().size() > 0) {
            builder.append(listInt2String(resultModel.getCopierPrec())).append(comma);//
            builder.append(comma);
            builder.append(listInt2String(resultModel.getCopierRecall())).append(comma);//
            builder.append(comma);
        }
//                builder.append(comma);
//                builder.append(listInt2String(resultModel.getCorrectLabels())).append(comma);//
        builder.append(listInt2String(resultModel.getItemIndices())).append("\n");//
        return builder;
    }

}

