package experiment.synthetic;

public class ExperimentA {

//	public String configPath = "config.ini";
//	public String labelPath = "data/label.txt";
//	public String categoryPath = "data/category.txt";
//	public String goldPath = "data/gold.txt";
//	public String workerReliabilityPath = "data/worker.txt";
//	
//	public String outputPath = "output/";
//	public String outputMain = outputPath + "main.txt";
//	public String outputBL = outputPath + "baseline.txt";
//	public String outputTL = outputPath + "topline.txt";
//	
//	public void generateData() throws IOException{
//		Mainconfig.getInstance().initialized();
//		for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
//			Mainconfig.getInstance().setData(data);
//		}
//		IDGenerator.resetWorkerID(-1);
//		IDGenerator.resetQuestionID(-1);
//		FeedBackModel model = new FeedBackModel();
//		model.generateGALInputFiles(labelPath, categoryPath, goldPath,workerReliabilityPath);
//	}
//	
//	public void runExperiment(int numIter) throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels(labelPath);
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels(goldPath, dataSet1);
//		DataSet mainDataSet =  GALDataReader.readCategories(categoryPath, dataSet2);
//		FeedbackSequence hitsSequence = null;
//		
//		//Main run
//		hitsSequence = new IGFeedbackSequence(mainDataSet.getNumDataItems());
//		hitsSequence.setData(mainDataSet.clone());
//		hitsSequence.feedbackLoop();
//		FileUtils.writeStringToFile(new File(outputMain), numIter+ "," + ArrayUti.arrayToString(hitsSequence.precisionList)+"\n", true);
//		
//		//Near optimal
//		//TODO: true means EM
//		hitsSequence = new NearOptimalFeedbackSequence(mainDataSet.getNumDataItems(),true);
//		hitsSequence.setData(mainDataSet.clone());
//		hitsSequence.feedbackLoop();
//		FileUtils.writeStringToFile(new File(outputTL), numIter+ "," + ArrayUti.arrayToString(hitsSequence.precisionList) + "\n", true);
//		
//		//Baseline
//		hitsSequence = new BaseLineFeedbackSequence(mainDataSet.getNumDataItems());
//		hitsSequence.setData(mainDataSet.clone());
//		hitsSequence.feedbackLoop();
//		FileUtils.writeStringToFile(new File(outputBL), numIter+ "," + ArrayUti.arrayToString(hitsSequence.precisionList) + "\n", true);
//	}
//	
//	public void experimentLoop(int maxIter) throws Exception{
//		for(int i=0; i < maxIter; i++){
//			generateData();
//			runExperiment(i);
//		}
//	}
//	
//	public static void main(String[] args){
//		ExperimentA a = new ExperimentA();
//		try {
//			a.experimentLoop(2);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
}
