package experiment.synthetic;

import com.google.common.base.Joiner;
import main.java.config.Mainconfig;
import main.java.config.Mainconfig.ListDatasets;
import data.model.DataSet;
import data.reader.GALDataReader;
import main.java.feedback.FeedBackModel;
import feedback.depend.DependentSrcRemover;
import feedback.depend.NormalRemover;
import feedback.strategy.*;
import feedback.strategy.selection.CommonFalseBasedSelection;
import feedback.strategy.selection.IGEMDependSelection;
import feedback.strategy.selection.SelectionAlgorithm;
import main.java.utility.IDGenerator;
import org.apache.commons.io.FileUtils;
import utils.ArrayUti;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class ExperimentAll {

	public String input = "input/";

	public String inputPath = input + "data/";
	public String labelPath = inputPath + "label.txt";
	public String categoryPath = inputPath + "category.txt";
	public String goldPath = inputPath + "gold.txt";
	public String workerReliabilityPath = inputPath + "worker.txt";

	public String copyPath = input + "dataCopy/";
	public String copyLabelPath = copyPath + "label.txt";
	public String copyCategoryPath = copyPath + "category.txt";
	public String copyGoldPath = copyPath + "gold.txt";
	public String copyWorkerReliabilityPath = copyPath + "worker.txt";

	public String output = "output/";

	public String outputAll = output + "all.txt";
	public String outputPath = output + "output/";
	public String outputMain = outputPath + "main.txt";
	public String outputBL = outputPath + "baseline.txt";
	public String outputTL = outputPath + "topline.txt";
	public String outputFG = outputPath + "fg.txt";
	public String outputDist = outputPath + "dist.txt";

	public String copyOutputPath = output + "copy/";

	public String copyOutputDel = copyOutputPath + "del.txt";
	public String copyOutputNodel = copyOutputPath + "nodel.txt";
	public String copyOutputBL = copyOutputPath + "baseline.txt";

	public List<Double> mainPrecision = new ArrayList<Double>();
	public List<Double> mainEntropy = new ArrayList<Double>();

	public List<Double> IGFGPrecision = new ArrayList<Double>();
	public List<Double> IGFGEntropy = new ArrayList<Double>();

	public List<Double> BLPrecision = new ArrayList<Double>();
	public List<Double> BLEntropy = new ArrayList<Double>();

	public List<Double> delPrecision = new ArrayList<Double>();
	public List<Double> delEntropy = new ArrayList<Double>();

	public List<Double> noDelPrecision = new ArrayList<Double>();
	public List<Double> noDelEntropy = new ArrayList<Double>();

	public List<Double> copyBLPrecision = new ArrayList<Double>();
	public List<Double> copyBLEntropy = new ArrayList<Double>();

	public long timeMain = 0;
	public long timeIGFG = 0;
	public long timeDist = 0;

	public boolean runMain = false;
	public boolean runIGFG = false;
	public boolean runDist = false;
	public boolean runBL = false;

	public void generateData() throws IOException {
		Mainconfig.getInstance().initialized();
		for (ListDatasets data : Mainconfig.getInstance().getDatasets()) {
			Mainconfig.getInstance().setData(data);
		}
		System.out.println(Mainconfig.getInstance().getListConfig()
				.get("workersRatio"));
		System.out.println(Mainconfig.getInstance().getListConfig()
				.get("typeOfDistributor"));
		IDGenerator.resetWorkerID(-1);
		IDGenerator.resetQuestionID(-1);
		FeedBackModel model = new FeedBackModel();
		model.generateGALInputFilesMulti(labelPath, categoryPath, goldPath,
				workerReliabilityPath);
	}

	public void randomNormalSequence(String inputFolder) throws Exception {
		File outBL = new File(outputBL);
		FileUtils.writeStringToFile(outBL,
				"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n",
				true);
		System.out.println("Randommmmmmmmmmmm");
		List<Double> sP = new ArrayList<Double>();
		Collections.fill(sP, new Double(0));
		List<Double> sE = new ArrayList<Double>();
		Collections.fill(sE, new Double(0));
		for (int i = 0; i < 5; i++) {
			DataSet woDataSet2 = GALDataReader
					.readDatsetFromFolder(inputFolder);
			woDataSet2.initTrustworthiness();
			FeedbackSequence woRandom = new BaseLineFeedbackSequence(
					woDataSet2.getNumDataItems());
			woRandom.setData(woDataSet2);
			woRandom.feedbackLoop();
			sP = ArrayUti.sum(sP, woRandom.precisionList);
			sE = ArrayUti.sum(sE, woRandom.entropyList);
			System.out.println(Joiner.on('\t').join(woRandom.precisionList));
			System.out.println(Joiner.on('\t').join(woRandom.entropyList));
		}
		List<Double> averageP = ArrayUti.average(sP, 5);
		List<Double> averageE = ArrayUti.average(sE, 5);
		BLEntropy = ArrayUti.sum(BLEntropy, averageE);
		BLPrecision = ArrayUti.sum(BLPrecision, averageP);
		// FileUtils.writeStringToFile(outBL,
		// Joiner.on('\t').join(averageP)+"\n", true);
		// FileUtils.writeStringToFile(outBL,
		// Joiner.on('\t').join(averageE)+"\n", true);
	}

	public void IGEMNormalSequence(String inputFolder) throws Exception {
		File IGEMF = new File(outputMain);
		DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
		woDataSet.initTrustworthiness();
		FeedbackSequence woIGEM = new IGFeedbackSequence(
				woDataSet.getNumDataItems());
		woIGEM.setData(woDataSet);
		long start = System.currentTimeMillis();
		woIGEM.feedbackLoop();
		long end = System.currentTimeMillis();
		timeMain += end - start;
		System.out.println("Timeeeee: " + Long.toString(end - start));
		System.out.println(Joiner.on('\t').join(woIGEM.precisionList));
		System.out.println(Joiner.on('\t').join(woIGEM.entropyList));
		System.out.println(Joiner.on('\t').join(woIGEM.itemIndices));
		mainEntropy = ArrayUti.sum(mainEntropy, woIGEM.entropyList);
		mainPrecision = ArrayUti.sum(mainPrecision, woIGEM.precisionList);
		// FileUtils.writeStringToFile(IGEMF,
		// Joiner.on('\t').join(woIGEM.precisionList)+"\n", true);
		// FileUtils.writeStringToFile(IGEMF,
		// Joiner.on('\t').join(woIGEM.entropyList)+"\n", true);
		// FileUtils.writeStringToFile(IGEMF,
		// Joiner.on('\t').join(woIGEM.itemIndices)+"\n", true);
	}

//	public void IGFGNormalSequence(String inputFolder) throws Exception {
//		File IGEMF = new File(outputMain);
//		DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
//		woDataSet.initTrustworthiness();
//		FeedbackSequence woIGEM = new FGIGFeedbackSequence(
//				woDataSet.getNumDataItems());
//		woIGEM.setData(woDataSet);
//		long start = System.currentTimeMillis();
//		woIGEM.feedbackLoop();
//		long end = System.currentTimeMillis();
//		timeIGFG += end - start;
//		System.out.println("Timeeeee: " + Long.toString(end - start));
//		System.out.println(Joiner.on('\t').join(woIGEM.precisionList));
//		System.out.println(Joiner.on('\t').join(woIGEM.entropyList));
//		System.out.println(Joiner.on('\t').join(woIGEM.itemIndices));
//		IGFGEntropy = ArrayUti.sum(IGFGEntropy, woIGEM.entropyList);
//		IGFGPrecision = ArrayUti.sum(IGFGPrecision, woIGEM.precisionList);
//		// FileUtils.writeStringToFile(IGEMF,
//		// Joiner.on('\t').join(woIGEM.precisionList)+"\n", true);
//		// FileUtils.writeStringToFile(IGEMF,
//		// Joiner.on('\t').join(woIGEM.entropyList)+"\n", true);
//		// FileUtils.writeStringToFile(IGEMF,
//		// Joiner.on('\t').join(woIGEM.itemIndices)+"\n", true);
//	}

	public List<Double> IGDistPrecision = new ArrayList<Double>();
	public List<Double> IGDistEntropy = new ArrayList<Double>();

	public void IGDistNormalSequence(String inputFolder, double delta)
			throws Exception {
		File IGDistF = new File(outputMain);
		DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
		woDataSet.initTrustworthiness();
		FeedbackSequence woIGEM = new IGDistFeedbackSequence(
				woDataSet.getNumDataItems(), delta);
		woIGEM.setData(woDataSet);
		long start = System.currentTimeMillis();
		woIGEM.feedbackLoop();
		long end = System.currentTimeMillis();
		timeDist += end - start;
		System.out.println("Timeeeee: " + Long.toString(end - start));
		System.out.println(Joiner.on('\t').join(woIGEM.precisionList));
		System.out.println(Joiner.on('\t').join(woIGEM.entropyList));
		System.out.println(Joiner.on('\t').join(woIGEM.itemIndices));
		IGDistEntropy = ArrayUti.sum(IGDistEntropy, woIGEM.entropyList);
		IGDistPrecision = ArrayUti.sum(IGDistPrecision, woIGEM.precisionList);
	}

	public void IGEMDelSequence(String outputCopyFolder) throws Exception {
		DataSet copyDataSet = GALDataReader
				.readDatsetFromFolder(outputCopyFolder);

		// Deletion strategy
		DependentSrcRemover remover = new NormalRemover();
		remover.setThreshold(0.9);
		remover.DEBUG = false;

		// Selection strategy
		Map<Integer, SelectionAlgorithm> algors = new HashMap<Integer, SelectionAlgorithm>();
		SelectionAlgorithm IG = new IGEMDependSelection(copyDataSet, remover);
		// SelectionAlgorithm noobEntropy = new
		// FrequentEntropySelection(dataSet);
		SelectionAlgorithm noobEntropy = new CommonFalseBasedSelection(
				copyDataSet);
		// Has set not delete src after 5 iteration
		for (int i = 0; i < copyDataSet.getNumDataItems(); i++) {
			// if(i%4 ==0 || i%4 == 1 || i <= 5){
			if (i <= 5) {
				algors.put(i, noobEntropy);
			} else {
				algors.put(i, IG);
			}
		}

		DependentSrcRemover remover2 = new NormalRemover();
		remover2.setThreshold(0.9);
		remover2.DEBUG = true;

		File IGEMF = new File(copyOutputDel);
		// Feedback
		FeedbackSequence hitsSequence = new IGEMDependFeedbackSequence(
				copyDataSet.getNumDataItems(), algors);
		hitsSequence.setRemover(remover2);
		hitsSequence.setData(copyDataSet);
		long startD = System.currentTimeMillis();
		hitsSequence.feedbackLoop();
		long endD = System.currentTimeMillis();
		System.out.println("Timeeeee: " + Long.toString(endD - startD));
		System.out.println("Delllllllllllllllllllllllllllllllllll");
		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
		delPrecision = ArrayUti.sum(delPrecision, hitsSequence.precisionList);
		delEntropy = ArrayUti.sum(delEntropy, hitsSequence.entropyList);
		// FileUtils.writeStringToFile(IGEMF,
		// Joiner.on('\t').join(hitsSequence.precisionList)+"\n", true);
		// FileUtils.writeStringToFile(IGEMF,
		// Joiner.on('\t').join(hitsSequence.entropyList)+"\n", true);
		// FileUtils.writeStringToFile(IGEMF,
		// Joiner.on('\t').join(hitsSequence.itemIndices)+"\n", true);
	}

	public void IEMNoDelSequence(String copyFolder) throws Exception {
		DataSet copiedDataSet = GALDataReader.readDatsetFromFolder(copyFolder);
		copiedDataSet.initTrustworthiness();
		FeedbackSequence noDelSequence = new IGFeedbackSequence(
				copiedDataSet.getNumDataItems());
		noDelSequence.setData(copiedDataSet);
		long startND = System.currentTimeMillis();
		noDelSequence.feedbackLoop();
		long endND = System.currentTimeMillis();
		System.out.println("Timeeeee: " + Long.toString(endND - startND));
		System.out.println("Delllllllllllllllllllllllllllllllllll");
		System.out.println("Nodelllllllllllllllllllllllllllllllllll");
		System.out.println(Joiner.on('\t').join(noDelSequence.precisionList));
		System.out.println(Joiner.on('\t').join(noDelSequence.entropyList));
		System.out.println(Joiner.on('\t').join(noDelSequence.itemIndices));
		noDelEntropy = ArrayUti.sum(noDelEntropy, noDelSequence.entropyList);
		noDelPrecision = ArrayUti.sum(noDelPrecision,
				noDelSequence.precisionList);
		File IGEMF = new File(copyOutputNodel);
		// FileUtils.writeStringToFile(IGEMF ,
		// Joiner.on('\t').join(noDelSequence.precisionList)+"\n", true);
		// FileUtils.writeStringToFile(IGEMF,
		// Joiner.on('\t').join(noDelSequence.entropyList)+"\n", true);
		// FileUtils.writeStringToFile(IGEMF,
		// Joiner.on('\t').join(noDelSequence.itemIndices)+"\n", true);
	}

	public void randomCopySequence(String copyFolder) throws Exception {
		List<Double> sP = new ArrayList<Double>();
		Collections.fill(sP, new Double(0));
		List<Double> sE = new ArrayList<Double>();
		Collections.fill(sE, new Double(0));
		System.out.println("Randommmmmmmmmmmmmmmmmmmmmmm");
		for (int i = 0; i < 5; i++) {
			DataSet copiedDataSet2 = GALDataReader
					.readDatsetFromFolder(copyFolder);
			copiedDataSet2.initTrustworthiness();
			FeedbackSequence randomSequence = new BaseLineFeedbackSequence(
					copiedDataSet2.getNumDataItems());
			randomSequence.setData(copiedDataSet2);
			randomSequence.feedbackLoop();
			sP = ArrayUti.sum(sP, randomSequence.precisionList);
			sE = ArrayUti.sum(sE, randomSequence.entropyList);
			System.out.println(Joiner.on('\t').join(
					randomSequence.precisionList));
			System.out
					.println(Joiner.on('\t').join(randomSequence.entropyList));
		}
		List<Double> averageP = ArrayUti.average(sP, 5);
		List<Double> averageE = ArrayUti.average(sE, 5);
		copyBLEntropy = ArrayUti.sum(copyBLEntropy, averageE);
		copyBLPrecision = ArrayUti.sum(copyBLPrecision, averageP);
		File outBL = new File(copyOutputBL);
		// FileUtils.writeStringToFile(outBL ,
		// Joiner.on('\t').join(averageP)+"\n", true);
		// FileUtils.writeStringToFile(outBL,
		// Joiner.on('\t').join(averageE)+"\n", true);
	}

	public void runExperiment(int numIter) throws Exception {

		String inputFolder = inputPath;
		String copyFolder = copyPath;

		/*
		 * ######################################### Without copy data sources
		 * ###############################################
		 */
		// Load original data
		DataSet dataSet = GALDataReader.readDatsetFromFolder(inputFolder);
		dataSet.initTrustworthiness();
		System.out.println("Init numSrc " + dataSet.getNumSources());
		// ///////////////////////////////Random/////////////////////////////////////////////////////////////////////////////////////
		if (runBL) {
			randomNormalSequence(inputFolder);
		}

		// ///////////////////////////////IG+EM/////////////////////////////////////////////////////////////////////////////////////
		if (runMain) {
			IGEMNormalSequence(inputFolder);
		}

		// ///////////////////////////////IG+FG/////////////////////////////////////////////////////////////////////////////////////
//		if (runIGFG) {
//			IGFGNormalSequence(inputFolder);
//		}

		// ///////////////////////////////IG+FG/////////////////////////////////////////////////////////////////////////////////////
		if (runDist) {
			IGDistNormalSequence(inputFolder, 1.0);
		}

		// /*######################################### With copy data sources
		// ###############################################*/
		// //Generate copied data
		// CopySimulator simulator = new CopySimulator(dataSet);
		// simulator.genCopierFromWorstSrc(COPY_FALSE_STRATEGY.COPY_PARTLY_CHANGE,COPY_TRUE_STRATEGY.COPY_PARTLY_CHANGE);
		//
		// DataSet copyDataSet = null;
		// String outputCopyFolder = copyFolder;
		// File f = new File(outputCopyFolder);
		// if(f.isDirectory()){
		// // FileUtils.cleanDirectory(f);
		// Utils.writeGALInputFromDataSet(dataSet, outputCopyFolder);
		// copyDataSet = GALDataReader.readDatsetFromFolder(outputCopyFolder);
		// copyDataSet.initTrustworthiness();
		// }
		//
		// System.out.println("After add copies " +
		// copyDataSet.getNumSources());
		// // dataSet.prettyPrint();
		//
		// /////////////////////With delete
		// source/////////////////////////////////////////
		// IGEMDelSequence(outputCopyFolder);
		//
		// ///////////////////////Without delete
		// source//////////////////////////////////////////
		// IEMNoDelSequence(outputCopyFolder);
		//
		// ///////////////////////Random with
		// copy//////////////////////////////////////////
		// randomCopySequence(outputCopyFolder);
	}

	public void experimentLoop(int maxIter, double reliability, double std)
			throws Exception {
		// FileUtils.cleanDirectory(new File(outputPath));
		// FileUtils.cleanDirectory(new File(copyOutputPath));
		int numInter = maxIter;

		outputPath = output + "/" + reliability + "_" + std + "/";
		outputMain = outputPath + "main.txt";
		outputBL = outputPath + "baseline.txt";
		outputTL = outputPath + "topline.txt";
		outputFG = outputPath + "fg.txt";
		outputDist = outputPath + "dist.txt";
		outputAll = outputPath + "all.txt";

		mainPrecision.clear();
		mainEntropy.clear();

		IGFGPrecision.clear();
		IGFGEntropy.clear();

		BLPrecision.clear();
		BLEntropy.clear();

		delPrecision.clear();
		delEntropy.clear();

		noDelPrecision.clear();
		noDelEntropy.clear();

		copyBLPrecision.clear();
		copyBLEntropy.clear();

//		runBL = true;
//		runDist = true;
//		runMain = true;
		runIGFG = true;

		for (int i = 0; i < numInter; i++) {
			// Mainconfig.getInstance().initialized();
			Mainconfig.getInstance().getListConfig()
					.put("workersRatio", "0%;100%");
			Mainconfig
					.getInstance()
					.getListConfig()
					.put("typeOfDistributor",
							"NormalDistribution(0.4,0.01);NormalDistribution("
									+ reliability + "," + std + ")");
			// Binary, Multiple
			// Mainconfig.getInstance().getListConfig().put("InputDataType",
			// "Multiple");
			// Mainconfig.getInstance().getListConfig().put("InputNumberLabels",
			// "3");
			Mainconfig.getInstance().initialized();
			// Mainconfig.getInstance().getListConfig().put("InputDataType",
			// "Multiple");
			generateData();
			runExperiment(0);
		}

		System.out.println("Time Main " + timeMain / numInter);
		System.out.println("Time FGIG " + timeIGFG / numInter);
		System.out.println("Time Dist " + timeDist / numInter);

		this.BLEntropy = ArrayUti.average(BLEntropy, numInter);
		this.BLPrecision = ArrayUti.average(BLPrecision, numInter);
		// FileUtils.writeStringToFile(new File(outputBL) ,
		// Joiner.on('\t').join(BLEntropy)+"\n", true);
		FileUtils.writeStringToFile(new File(outputBL),
				Joiner.on('\t').join(BLPrecision) + "\n", true);

		this.IGDistEntropy = ArrayUti.average(IGDistEntropy, numInter);
		this.IGDistPrecision = ArrayUti.average(IGDistPrecision, numInter);
		// FileUtils.writeStringToFile(new File(outputDist) ,
		// Joiner.on('\t').join(IGDistEntropy)+"\n", true);
		FileUtils.writeStringToFile(new File(outputDist),
				Joiner.on('\t').join(IGDistPrecision) + "\n", true);

		// this.copyBLEntropy = ArrayUti.average(copyBLEntropy, numInter);
		// this.copyBLPrecision = ArrayUti.average(copyBLPrecision, numInter);
		// FileUtils.writeStringToFile(new File(copyOutputBL) ,
		// Joiner.on('\t').join(copyBLEntropy)+"\n", true);
		// FileUtils.writeStringToFile(new File(copyOutputBL) ,
		// Joiner.on('\t').join(copyBLPrecision)+"\n", true);
		//
		// this.delEntropy = ArrayUti.average(delEntropy, numInter);
		// this.delPrecision = ArrayUti.average(delPrecision, numInter);
		// FileUtils.writeStringToFile(new File(copyOutputDel) ,
		// Joiner.on('\t').join(delEntropy)+"\n", true);
		// FileUtils.writeStringToFile(new File(copyOutputDel) ,
		// Joiner.on('\t').join(delPrecision)+"\n", true);

		this.mainEntropy = ArrayUti.average(mainEntropy, numInter);
		this.mainPrecision = ArrayUti.average(mainPrecision, numInter);
		// FileUtils.writeStringToFile(new File(outputMain) ,
		// Joiner.on('\t').join(mainEntropy)+"\n", true);
		FileUtils.writeStringToFile(new File(outputMain),
				Joiner.on('\t').join(mainPrecision) + "\n", true);

		this.IGFGEntropy = ArrayUti.average(IGFGEntropy, numInter);
		this.IGFGPrecision = ArrayUti.average(IGFGPrecision, numInter);
		// FileUtils.writeStringToFile(new File(outputFG) ,
		// Joiner.on('\t').join(IGFGEntropy)+"\n", true);
		FileUtils.writeStringToFile(new File(outputFG),
				Joiner.on('\t').join(IGFGPrecision) + "\n", true);
		// this.noDelEntropy = ArrayUti.average(noDelEntropy, numInter);
		// this.noDelPrecision = ArrayUti.average(noDelPrecision, numInter);
		// FileUtils.writeStringToFile(new File(copyOutputNodel) ,
		// Joiner.on('\t').join(noDelEntropy)+"\n", true);
		// FileUtils.writeStringToFile(new File(copyOutputNodel) ,
		// Joiner.on('\t').join(noDelPrecision)+"\n", true);

		List<List<Double>> datas = new ArrayList<List<Double>>();
		for (int i = 0; i < IGDistEntropy.size(); i++) {
			List<Double> dat = new ArrayList<Double>();
			if (BLPrecision.size() > 0) {
				dat.add(BLPrecision.get(i));
			}
			if (IGDistPrecision.size() > 0) {
				dat.add(IGDistPrecision.get(i));
			}
			if (mainPrecision.size() > 0) {
				dat.add(mainPrecision.get(i));
			}
			if (IGFGPrecision.size() > 0) {
				dat.add(IGFGPrecision.get(i));
			}
			datas.add(dat);
		}

		writeToFile(outputAll, datas);
		// Mainconfig.getInstance().getListConfig().put("typeOfDistributor",
		// "UniformDistribution(0.3,0.9);NormalDistribution(0.4,0.001)");
		// // Mainconfig.getInstance().getListConfig().put("InputDataType",
		// "Multiple");
		// generateData();
		// runExperiment(1);
		//
		// Mainconfig.getInstance().getListConfig().put("typeOfDistributor",
		// "UniformDistribution(0.3,0.9);NormalDistribution(0.5,0.001)");
		// // Mainconfig.getInstance().getListConfig().put("InputDataType",
		// "Multiple");
		// generateData();
		// runExperiment(2);
		//
		// Mainconfig.getInstance().getListConfig().put("typeOfDistributor",
		// "UniformDistribution(0.3,0.9);NormalDistribution(0.6,0.001)");
		// // Mainconfig.getInstance().getListConfig().put("InputDataType",
		// "Multiple");
		// generateData();
		// runExperiment(3);
		//
		// Mainconfig.getInstance().getListConfig().put("typeOfDistributor",
		// "UniformDistribution(0.3,0.9);NormalDistribution(0.7,0.001)");
		// // Mainconfig.getInstance().getListConfig().put("InputDataType",
		// "Multiple");
		// generateData();
		// runExperiment(4);
		//
		// Mainconfig.getInstance().getListConfig().put("typeOfDistributor",
		// "UniformDistribution(0.3,0.9);NormalDistribution(0.8,0.001)");
		// // Mainconfig.getInstance().getListConfig().put("InputDataType",
		// "Multiple");
		// generateData();
		// runExperiment(5);
	}

	public void writeToFile(String filename, List<List<Double>> datas) {
		try {
			FileUtils.writeStringToFile(new File(filename),
					"Baseline\tDistance-ware\tIGEM\tIGFG\n", false);
			for (List<Double> list : datas) {
				FileUtils.writeStringToFile(new File(filename), Joiner.on('\t')
						.join(list) + "\n", true);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ExperimentAll a = new ExperimentAll();
		try {
			// double rel = 0.5;
			// double rel = 0.6;
			// double rel = 0.7;
			double rel = 0.5;
			double std = 0.1;
			a.experimentLoop(2, rel, std);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
