package experiment.synthetic;

import com.google.common.base.Joiner;
import data.generator.CopySimulator;
import data.model.DataSet;
import data.reader.GALDataReader;
import feedback.strategy.*;
import main.java.config.Mainconfig;
import main.java.feedback.FeedBackModel;
import main.java.utility.IDGenerator;
import utils.ArrayUti;
import experiment.ResultModel;
import utils.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Asus on 4/22/14.
 * Experiment to check how many steps are required to detect random spammerssao
 */
public class ExperimentCombined {
    public String input = "input/";

    public String inputPath = input + "data/";
    public String labelPath = inputPath + "label.txt";
    public String categoryPath = inputPath + "category.txt";
    public String goldPath = inputPath + "gold.txt";
    public String workerReliabilityPath = inputPath + "worker.txt";

    private DataSet originalDS;

    public void generateData(boolean generateDepend, double copyRatio) throws Exception {
        Mainconfig.getInstance().initialized();
        for (Mainconfig.ListDatasets data : Mainconfig.getInstance().getDatasets()) {
            Mainconfig.getInstance().setData(data);
        }
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("workersRatio"));
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("typeOfDistributor"));
        IDGenerator.resetWorkerID(-1);
        IDGenerator.resetQuestionID(-1);
        FeedBackModel model = new FeedBackModel();
        model.generateGALInputFilesMulti(labelPath, categoryPath, goldPath,
                workerReliabilityPath);


        /*######################################### With copy data sources###############################################*/
        if (generateDepend) {
            // Load original data
            DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
            dataSet.initTrustworthiness();
            System.out.println("Init numSrc " + dataSet.getNumSources());
            //Generate copied data
            CopySimulator simulator = new CopySimulator(dataSet);
            simulator.setCopyRatio(copyRatio);
            simulator.genCopierFromWorstSrc(CopySimulator.COPY_FALSE_STRATEGY.COPY_ALL, CopySimulator.COPY_TRUE_STRATEGY.COPY_ALL);
//            simulator.genCopierRandomly(CopySimulator.COPY_FALSE_STRATEGY.COPY_ALL, CopySimulator.COPY_TRUE_STRATEGY.COPY_ALL);

            String outputCopyFolder = inputPath;
            File f = new File(outputCopyFolder);
            if (f.isDirectory()) {
                // FileUtils.cleanDirectory(f);
                Utils.writeGALInputFromDataSet(dataSet, outputCopyFolder);
            }
        }

        originalDS = GALDataReader.readDatsetFromFolder(inputPath);
    }

    public ResultModel randomNormalSequence(int maxIter, String inputFolder, int loopID, double rel, double std) throws Exception {
        System.out.println("Randommmmmmmmmmmm");
        List<Double> sP = new ArrayList<Double>();
        Collections.fill(sP, new Double(0));
        List<Double> sE = new ArrayList<Double>();
        Collections.fill(sE, new Double(0));
        long start = System.currentTimeMillis();
        int numSrc = 0;
        for (int i = 0; i < 5; i++) {
            DataSet woDataSet2 = GALDataReader
                    .readDatsetFromFolder(inputFolder);
            woDataSet2.initTrustworthiness();
            numSrc = woDataSet2.getNumSources();
//            FeedbackSequence woRandom = new BaseLineFeedbackSequence(
//                    woDataSet2.getNumDataItems());
            FeedbackSequence woRandom = new BaseLineFeedbackSequence(maxIter);
            woRandom.setData(woDataSet2);
            woRandom.feedbackLoop();
            sP = ArrayUti.sum(sP, woRandom.precisionList);
            sE = ArrayUti.sum(sE, woRandom.entropyList);
            System.out.println(Joiner.on('\t').join(woRandom.precisionList));
            System.out.println(Joiner.on('\t').join(woRandom.entropyList));
        }
        long end = System.currentTimeMillis();
        long time = end - start;
        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> rP = new ArrayList<Double>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            rP.add(-1.0);
        }

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setPrecisions(averageP);
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setSpamPrec(rP);
        resultModel.setSpamRecall(rP);
        resultModel.setCopierPrec(rP);
        resultModel.setCopierRecall(rP);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomMV");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(numSrc + "");
        resultModel.setLabelProbabilities(rP);
        resultModel.setCorrectLabels(indices);

        return resultModel;
    }


    public ResultModel IGEMNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();
        FeedbackSequence woIGEM = new IGFeedbackSequence(
                woDataSet.getNumDataItems());
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("IGEM");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }


    public ResultModel EMWorkerNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();
        FeedbackSequence woIGEM = new EMWorkerFeedbackSequence(
                woDataSet.getNumDataItems());
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("EMWorker");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    /**
     * @param maxIter
     * @param inputFolder
     * @param loopID
     * @param rel
     * @param std
     * @param detectThreshold
     * @return
     * @throws Exception
     */
    public ResultModel IGEMRemoveRandomSequence(int maxIter, String inputFolder, int loopID, double rel, double std, double detectThreshold, double reviveThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();

        if (maxIter > woDataSet.getDataItems().size()) {
            maxIter = woDataSet.getDataItems().size();
        }

//        IGEMRandomSequence woIGEM = new IGEMRandomSequence(
//                woDataSet.getNumDataItems(), detectThreshold, true, true);
        IGEMRandomSequence woIGEM = new IGEMRandomSequence(maxIter, detectThreshold, true, true, reviveThreshold);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setCopierPrec(woIGEM.spamPrecisionList);
        resultModel.setCopierRecall(woIGEM.spamRecallList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("IGEMRandomRemoveSequence");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        resultModel.setLabelProbabilities(woIGEM.getProbabilityCorrectness());
        resultModel.setCorrectLabels(woIGEM.correctLabels);
        return resultModel;
    }


    public ResultModel GreedySequence(int maxIter, String inputFolder, int loopID, double rel, double std) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();

        if (maxIter > woDataSet.getDataItems().size()) {
            maxIter = woDataSet.getDataItems().size();
        }

        GreedySequence woIGEM = new GreedySequence(maxIter);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setCopierPrec(woIGEM.spamPrecisionList);
        resultModel.setCopierRecall(woIGEM.spamRecallList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("GreedySequence");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel RandomEMDetecRandomSequence(int maxIter, String inputFolder, int loopID, double rel, double std, double detectThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();

        List<Double> sP = new ArrayList<Double>();
        List<Double> sE = new ArrayList<Double>();
        List<Double> spamP = new ArrayList<Double>();
        List<Double> spamR = new ArrayList<Double>();

        if (maxIter > woDataSet.getDataItems().size()) {
            maxIter = woDataSet.getDataItems().size();
        }

        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
//            DataSet woDataSet2 = GALDataReader
//                    .readDatsetFromFolder(inputFolder);
            DataSet woDataSet2 = originalDS.clone();
            woDataSet2.initTrustworthiness();
            RandomEMRandomSequence sequence = new RandomEMRandomSequence(maxIter, detectThreshold, false, false);
            sequence.setData(woDataSet2);
            sequence.feedbackLoop();
            sP = ArrayUti.sum(sP, sequence.precisionList);
            sE = ArrayUti.sum(sE, sequence.entropyList);
            spamP = ArrayUti.sum(spamP, sequence.spamPrecisionList);
            spamR = ArrayUti.sum(spamR, sequence.spamRecallList);
        }
        long end = System.currentTimeMillis();
        long time = (end - start) / 5;

        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> indices2 = new ArrayList<Double>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices2.add(-1.0);
        }

        List<Double> aSpamP = ArrayUti.average(spamP, 5);
        List<Double> aSpamR = ArrayUti.average(spamR, 5);

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setPrecisions(averageP);
        resultModel.setSpamPrec(indices2);
        resultModel.setSpamRecall(indices2);
        resultModel.setCopierPrec(indices2);
        resultModel.setCopierRecall(indices2);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomEMDetectRandom");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel SpammerEMRemoveRandomSequence(String inputFolder, int loopID, double rel, double std, double detectThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();
        SpammerEMRandomSequence woIGEM = new SpammerEMRandomSequence(
                woDataSet.getNumDataItems(), detectThreshold, true, true);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("SpammerEMRemoveRandomSequence");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }


    public ResultModel combinedEMSequence(String inputFolder, int loopID, double rel, double std, double spammerThreshold, double copyThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();
        CombinedEMSequence woIGEM = new CombinedEMSequence(
                woDataSet.getNumDataItems(), spammerThreshold, true, true, copyThreshold, true, true);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setCopierPrec(woIGEM.copierPrecisionList);
        resultModel.setCopierRecall(woIGEM.copierRecallList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("CombinedEMSequence");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel SpammerEMRandomSequence(String inputFolder, int loopID, double rel, double std, double detectThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();
        SpammerEMRandomSequence woIGEM = new SpammerEMRandomSequence(
                woDataSet.getNumDataItems(), detectThreshold, true, false);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("SpammerEMRandomSequence");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel IGEMRandomSequence(String inputFolder, int loopID, double rel, double std, double detectThreshold, double reviveThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();
        IGEMRandomSequence woIGEM = new IGEMRandomSequence(
                woDataSet.getNumDataItems(), detectThreshold, true, false, reviveThreshold);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("IGEMRandomSequence");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel RandomEMDetectSequence(String inputFolder, int loopID, double rel, double std, double threshold) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();

        List<Double> sP = new ArrayList<Double>();
        List<Double> sE = new ArrayList<Double>();
        List<Double> spamP = new ArrayList<Double>();
        List<Double> spamR = new ArrayList<Double>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
            DataSet woDataSet2 = GALDataReader
                    .readDatsetFromFolder(inputFolder);
            woDataSet2.initTrustworthiness();
            RandomEMCopierSequence sequence = new RandomEMCopierSequence(woDataSet.getNumDataItems(), threshold, true, false);
            sequence.setData(woDataSet2);
            sequence.feedbackLoop();
            sP = ArrayUti.sum(sP, sequence.precisionList);
            sE = ArrayUti.sum(sE, sequence.entropyList);
            spamP = ArrayUti.sum(spamP, sequence.spamPrecisionList);
            spamR = ArrayUti.sum(spamR, sequence.spamRecallList);
        }
        long end = System.currentTimeMillis();
        long time = (end - start) / 5;

        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> aSpamP = ArrayUti.average(spamP, 5);
        List<Double> aSpamR = ArrayUti.average(spamR, 5);

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setPrecisions(averageP);
        resultModel.setSpamPrec(aSpamP);
        resultModel.setSpamRecall(aSpamR);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomEMDetect");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }


    public ResultModel RandomEMRandomRemoveSequence(String inputFolder, int loopID, double rel, double std, double detectThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();

        List<Double> sP = new ArrayList<Double>();
        List<Double> sE = new ArrayList<Double>();
        List<Double> spamP = new ArrayList<Double>();
        List<Double> spamR = new ArrayList<Double>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
//            DataSet woDataSet2 = GALDataReader
//                    .readDatsetFromFolder(inputFolder);
            DataSet woDataSet2 = originalDS.clone();
            woDataSet2.initTrustworthiness();
            RandomEMRandomSequence sequence = new RandomEMRandomSequence(woDataSet.getNumDataItems(), detectThreshold, true, true);
            sequence.setData(woDataSet2);
            sequence.feedbackLoop();
            sP = ArrayUti.sum(sP, sequence.precisionList);
            sE = ArrayUti.sum(sE, sequence.entropyList);
            spamP = ArrayUti.sum(spamP, sequence.spamPrecisionList);
            spamR = ArrayUti.sum(spamR, sequence.spamRecallList);
        }
        long end = System.currentTimeMillis();
        long time = (end - start) / 5;

        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> aSpamP = ArrayUti.average(spamP, 5);
        List<Double> aSpamR = ArrayUti.average(spamR, 5);

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setPrecisions(averageP);
        resultModel.setSpamPrec(aSpamP);
        resultModel.setSpamRecall(aSpamR);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomEMRemoveRandom");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        resultModel.setRemoveSpammer(true + "");
        return resultModel;
    }

    public List<ResultModel> experimentLoop(int maxIter, int numWorkers, int numLoop, int percent1, int percent2, double reliability1, double reliability2,
                                            double spammerThreshold, double std, double copyRatio, boolean generateDepend, double copyThreshold, double reviveThreshold,
                                            int feedbackPerQuestion, int workerTypeRat, BufferedWriter bw)
            throws Exception {
        List<ResultModel> resultModelList = new ArrayList<ResultModel>();
        int numInter = numLoop;

        for (int i = 0; i < numInter; i++) {
            // Mainconfig.getInstance().initialized();
            Mainconfig.getInstance().getListConfig().put("WorkerTypeRatio", workerTypeRat + "%"); // So luong worker truthful
            Mainconfig.getInstance().getListConfig().put("spammersType", "random;uniform;semi");
            Mainconfig.getInstance().getListConfig().put("spammersRatio", "50%;50%;0%");
//            Mainconfig.getInstance().getListConfig().put("feedbacksPerWorker", "0");
            Mainconfig.getInstance().getListConfig()
                    .put("workersRatio", "" + percent1 + "%;" + percent2 + "%");
            Mainconfig.getInstance().getListConfig().put("total", numWorkers + "");
            Mainconfig.getInstance().getListConfig().put("feedbacksDistributor", "FeedBacksConstraintDistributor");
            Mainconfig.getInstance().getListConfig().put("feedbacksPerQuestion", feedbackPerQuestion + "");
            Mainconfig
                    .getInstance()
                    .getListConfig()
                    .put("typeOfDistributor",
                            "NormalDistribution(" + reliability1 + ",0.01);NormalDistribution("
                                    + reliability2 + "," + std + ")");
            // Binary, Multiple
//            Mainconfig.getInstance().getListConfig().put("InputDataType",
//                    "Multiple");
//            Mainconfig.getInstance().getListConfig().put("InputNumberLabels",
//                    "4");
//            Mainconfig.getInstance().getListConfig().put("NumOfQuestion", "10");
            Mainconfig.getInstance().initialized();
            generateData(generateDepend, copyRatio);

            // ///////////////////////////////Random/////////////////////////////////////////////////////////////////////////////////////
//            ResultModel baseline = randomNormalSequence(maxIter, inputPath, i, reliability2, std);
//            baseline.setReliability(reliability1 + "");
//            baseline.setReliability2(reliability2 + "");
//            baseline.setPercent1(percent1 + "");
//            baseline.setPercent2(percent2 + "");
//            baseline.setCopyRatio(copyRatio + "");
//            baseline.setDetectThreshold(spammerThreshold + "");
//            baseline.setDetectCopyThreshold(copyThreshold + "");
//            baseline.setMaxIter(maxIter + "");

            // ///////////////////////////////IG+EM/////////////////////////////////////////////////////////////////////////////////////
//            ResultModel igem = IGEMNormalSequence(inputPath, i, reliability, std);

//            ResultModel emworker = EMWorkerNormalSequence(inputPath, i, reliability, std);

//            ResultModel randomEMDetect = RandomEMCopierSequence(inputPath, i, reliability2, std, 0.5);


//            ResultModel randomEMDetectRandom = RandomEMDetecRandomSequence(maxIter, inputPath, i, reliability2, std, spammerThreshold);
//            randomEMDetectRandom.setReliability(reliability1 + "");
//            randomEMDetectRandom.setReliability2(reliability2 + "");
//            randomEMDetectRandom.setPercent1(percent1 + "");
//            randomEMDetectRandom.setPercent2(percent2 + "");
//            randomEMDetectRandom.setCopyRatio(copyRatio + "");
//            randomEMDetectRandom.setDetectThreshold(spammerThreshold + "");
//            randomEMDetectRandom.setDetectCopyThreshold(copyThreshold + "");
//            randomEMDetectRandom.setMaxIter(maxIter + "");
//            resultModelList.add(randomEMDetectRandom);
//            bw.write(generateStringBuilder(randomEMDetectRandom).toString());
//            bw.flush();

            ResultModel greedySequence = GreedySequence(maxIter, inputPath, i, reliability2, std);
            greedySequence.setReliability(reliability1 + "");
            greedySequence.setReliability2(reliability2 + "");
            greedySequence.setPercent1(percent1 + "");
            greedySequence.setPercent2(percent2 + "");
            greedySequence.setCopyRatio(copyRatio + "");
            greedySequence.setDetectThreshold(spammerThreshold + "");
            greedySequence.setDetectCopyThreshold(copyThreshold + "");
            greedySequence.setMaxIter(maxIter + "");
            resultModelList.add(greedySequence);
            bw.write(generateStringBuilder(greedySequence).toString());
            bw.flush();

//            ResultModel removeSpammer = RandomEMRandomRemoveSequence(inputPath, i, reliability2, std, spammerThreshold);
//            removeSpammer.setReliability(reliability1 + "");
//            removeSpammer.setReliability2(reliability2 + "");
//            removeSpammer.setPercent1(percent1 + "");
//            removeSpammer.setPercent2(percent2 + "");
//            removeSpammer.setCopyRatio(copyRatio + "");
//            removeSpammer.setDetectThreshold(spammerThreshold + "");

            ResultModel igemremoveSpammer = IGEMRemoveRandomSequence(maxIter, inputPath, i, reliability2, std, spammerThreshold, reviveThreshold);
            igemremoveSpammer.setReliability(reliability1 + "");
            igemremoveSpammer.setReliability2(reliability2 + "");
            igemremoveSpammer.setPercent1(percent1 + "");
            igemremoveSpammer.setPercent2(percent2 + "");
            igemremoveSpammer.setCopyRatio(copyRatio + "");
            igemremoveSpammer.setDetectThreshold(spammerThreshold + "");
            igemremoveSpammer.setMaxIter(maxIter + "");
            igemremoveSpammer.setFeedbackPerQuestion(feedbackPerQuestion + "");
            resultModelList.add(igemremoveSpammer);
            bw.write(generateStringBuilder(igemremoveSpammer).toString());
            bw.flush();


//            ResultModel igemSpammer = IGEMRandomSequence(inputPath, i, reliability2, std, spammerThreshold);
//            igemSpammer.setReliability(reliability1 + "");
//            igemSpammer.setReliability2(reliability2 + "");
//            igemSpammer.setPercent1(percent1 + "");
//            igemSpammer.setPercent2(percent2 + "");
//            igemSpammer.setCopyRatio(copyRatio + "");
//            igemSpammer.setDetectThreshold(spammerThreshold + "");

//            ResultModel spammerEMRandomSequence = SpammerEMRandomSequence(inputPath, i, reliability2, std, spammerThreshold);
//            spammerEMRandomSequence.setReliability(reliability1 + "");
//            spammerEMRandomSequence.setReliability2(reliability2 + "");
//            spammerEMRandomSequence.setPercent1(percent1 + "");
//            spammerEMRandomSequence.setPercent2(percent2 + "");
//            spammerEMRandomSequence.setCopyRatio(copyRatio + "");
//            spammerEMRandomSequence.setDetectThreshold(spammerThreshold + "");


//            ResultModel spammerEMRemoveRandomSequence = SpammerEMRemoveRandomSequence(inputPath, i, reliability2, std, spammerThreshold);
//            spammerEMRemoveRandomSequence.setReliability(reliability1 + "");
//            spammerEMRemoveRandomSequence.setReliability2(reliability2 + "");
//            spammerEMRemoveRandomSequence.setPercent1(percent1 + "");
//            spammerEMRemoveRandomSequence.setPercent2(percent2 + "");
//            spammerEMRemoveRandomSequence.setCopyRatio(copyRatio + "");
//            spammerEMRemoveRandomSequence.setDetectThreshold(spammerThreshold + "");

//            ResultModel combinedEMSequence = combinedEMSequence(inputPath, i, reliability2, std, spammerThreshold, copyThreshold);
//            combinedEMSequence.setReliability(reliability1 + "");
//            combinedEMSequence.setReliability2(reliability2 + "");
//            combinedEMSequence.setPercent1(percent1 + "");
//            combinedEMSequence.setPercent2(percent2 + "");
//            combinedEMSequence.setCopyRatio(copyRatio + "");
//            combinedEMSequence.setDetectThreshold(spammerThreshold + "");
//            combinedEMSequence.setDetectCopyThreshold(copyThreshold + "");

//            resultModelList.add(ran);
//            resultModelList.add(igem);
//            resultModelList.add(emworker);
//            resultModelList.add(baseline);// Baseline

//            resultModelList.add(removeSpammer);

//            resultModelList.add(igemSpammer);
//            resultModelList.add(spammerEMRandomSequence);
//            resultModelList.add(spammerEMRemoveRandomSequence);
//            resultModelList.add(combinedEMSequence);

        }
        return resultModelList;
    }

    public static void main(String[] args) {
        ExperimentCombined a = new ExperimentCombined();
        String dateTime = new SimpleDateFormat("MMddhhmm").format(new Date());
        String outputFilePath = "expResults/" + a.getClass().getSimpleName() + "_" + dateTime + ".csv";
        List<ResultModel> results = new ArrayList<ResultModel>();

        int numLoop = 10;

        // Copy or not depends on this parameter
        boolean genarateDepend = false;
        double[] copyRatios = {0.6};
        double[] copyThresholds = {0.95};

        int[] feedbackPerQuestions = {50};

        //Effect of number of labels
//        double[] rel1s = {0.65};
//        double rel2 = 0.5;
//        double std = 0.01;
//        int[] per2s = {40};
//        double[] spammerThresholds = {0.01};
//        int[] numWorkers = {20};

        // Effect of percentage of spammers
        int workerTypeRat = 75;
        double[] rel1s = {0.65};
        double rel2 = 0.2;
        double std = 0.00001;
        int[] per2s = {33};
        double[] spammerThresholds = {0.01};
        int[] numWorkers = {100};

        // Effects of number of workers
//        double[] rel1s = {0.7};
//        double rel2 = 0.5;
//        double std = 0.01;
//        int[] per2s = {40};
//        double[] spammerThresholds = {0.01};
//        int[] numWorkers = {20, 30};


        // Effects of worker reliability
//        double rel2 = 0.5;
//        double[] rel1s = {0.75};
//        double std = 0.01;
//        int[] per2s = {40};
//        double[] spammerThresholds = {0.01};
//        int[] numWorkers = {20};

        // Effect of probability computation
//        double[] rel1s = {0.6}; // Reliability of normal workers
//        double rel2 = 0.5;
//        double std = 0.01; // Std of spammer generator
//        int[] per2s = {70, 80}; // Percentage of spammer
//        double[] spammerThresholds = {0.01};
//        int[] numWorkers = {20};

        // Running time
//        double[] rel1s = {0.9};
//        double rel2 = 0.5;
//        double std = 0.01;
//        int[] per2s = {40};
//        double[] spammerThresholds = {0.01};
//        int[] numWorkers = {20};


        double reviveThreshold = 0.5;

        // Normal thi co 50 data items
        int[] maxIters = {50};

        try {
            File file = new File(outputFilePath);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);


            for (int maxIter : maxIters) {
                for (int feedbackPerQuestion : feedbackPerQuestions) {
                    for (double rel1 : rel1s) {
                        for (int numWorker : numWorkers) {
                            for (int per2 : per2s) {
                                for (double spammerThreshold : spammerThresholds) {
                                    for (double copyThreshold : copyThresholds) {
                                        for (double copyRatio : copyRatios) {
                                            int per1 = 100 - per2;
                                            List<ResultModel> list = a.experimentLoop(maxIter, numWorker, numLoop, per1, per2, rel1, rel2, spammerThreshold,
                                                    std, copyRatio, genarateDepend, copyThreshold, reviveThreshold, feedbackPerQuestion, workerTypeRat, bw);
                                            results.addAll(list);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    static <T> String listInt2String(List<T> list) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size() - 1; i++) {
            T t = list.get(i);
            builder.append(t.toString()).append(",");
        }
        builder.append(list.get(list.size() - 1));
        return builder.toString();
    }

    public static StringBuilder generateStringBuilder(ResultModel resultModel) {
        // DetectThreshold, DetectCopyThreshold, Reliablity1,Rel2,Percent1,Percent2,CopyRatio,Std,LoopID,Algor,#Src,Time,Prec1,Prec2,...,Precn,Ent1,Ent2,..,Entn,SP1,Sp2,...,SPn,SR1,SR2,...,SRn,Ind1,Ind2,..,Indn
        String comma = ",";
        StringBuilder builder = new StringBuilder();
        builder.append(resultModel.getDetectThreshold()).append(comma);//
        builder.append(resultModel.getDetectCopyThreshold()).append(comma);//
        builder.append(resultModel.getReliability()).append(comma);//
        builder.append(resultModel.getReliability2()).append(comma);//
        builder.append(resultModel.getPercent1()).append(comma);//
        builder.append(resultModel.getPercent2()).append(comma);//
        builder.append(resultModel.getCopyRatio()).append(comma);//
        builder.append(resultModel.getStd()).append(comma);//
        builder.append(resultModel.getID()).append(comma);//
        builder.append(resultModel.getAlgorithm()).append(comma);//
        builder.append(resultModel.getNumSrc()).append(comma);//
        builder.append(resultModel.getTime()).append(comma);//
//                builder.append(listInt2String(resultModel.getLabelProbabilities())).append(comma);//
        builder.append(comma);
        builder.append(listInt2String(resultModel.getPrecisions())).append(comma);//
        builder.append(comma);
        builder.append(listInt2String(resultModel.getEntropies())).append(comma);//
        builder.append(comma);
        if (resultModel.getSpamPrec().size() > 0) {
            builder.append(listInt2String(resultModel.getSpamPrec())).append(comma);//
            builder.append(comma);
        }
        if (resultModel.getSpamRecall().size() > 0) {
            builder.append(listInt2String(resultModel.getSpamRecall())).append(comma);//
            builder.append(comma);
        }
        if (resultModel.getCopierPrec().size() > 0) {
            builder.append(listInt2String(resultModel.getCopierPrec())).append(comma);//
            builder.append(comma);
            builder.append(listInt2String(resultModel.getCopierRecall())).append(comma);//
            builder.append(comma);
        }
        //                builder.append(comma);
//                builder.append(listInt2String(resultModel.getCorrectLabels())).append(comma);//
        builder.append(listInt2String(resultModel.getItemIndices())).append("\n");//
        return builder;
    }


}
