package experiment.synthetic;

import com.google.common.base.Joiner;
import data.generator.CopySimulator;
import data.model.DataSet;
import data.reader.GALDataReader;
import feedback.strategy.*;
import main.java.config.Mainconfig;
import main.java.feedback.FeedBackModel;
import main.java.utility.IDGenerator;
import utils.ArrayUti;
import utils.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Asus on 4/22/14.
 * Experiment to check how many steps are required to detect copiers
 */
public class ExperimentDetectStep {
    public String input = "input/";

    public String inputPath = input + "data/";
    public String labelPath = inputPath + "label.txt";
    public String categoryPath = inputPath + "category.txt";
    public String goldPath = inputPath + "gold.txt";
    public String workerReliabilityPath = inputPath + "worker.txt";

    private DataSet originalDS;

    public void generateData(boolean generateDepend, double copyRatio) throws Exception {
        Mainconfig.getInstance().initialized();
        for (Mainconfig.ListDatasets data : Mainconfig.getInstance().getDatasets()) {
            Mainconfig.getInstance().setData(data);
        }
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("workersRatio"));
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("typeOfDistributor"));
        IDGenerator.resetWorkerID(-1);
        IDGenerator.resetQuestionID(-1);
        FeedBackModel model = new FeedBackModel();
        model.generateGALInputFilesMulti(labelPath, categoryPath, goldPath,
                workerReliabilityPath);


        /*######################################### With copy data sources###############################################*/
        if (generateDepend) {
            // Load original data
            DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
            dataSet.initTrustworthiness();
            System.out.println("Init numSrc " + dataSet.getNumSources());
            //Generate copied data
            CopySimulator simulator = new CopySimulator(dataSet);
            simulator.setCopyRatio(copyRatio);
            simulator.genCopierFromWorstSrc(CopySimulator.COPY_FALSE_STRATEGY.COPY_ALL, CopySimulator.COPY_TRUE_STRATEGY.COPY_ALL);
//            simulator.genCopierRandomly(CopySimulator.COPY_FALSE_STRATEGY.COPY_ALL, CopySimulator.COPY_TRUE_STRATEGY.COPY_ALL);

            String outputCopyFolder = inputPath;
            File f = new File(outputCopyFolder);
            if (f.isDirectory()) {
                // FileUtils.cleanDirectory(f);
                Utils.writeGALInputFromDataSet(dataSet, outputCopyFolder);
            }
        }
        originalDS = GALDataReader.readDatsetFromFolder(inputPath);
    }

    public ResultModel randomNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        System.out.println("Randommmmmmmmmmmm");
        List<Double> sP = new ArrayList<Double>();
        Collections.fill(sP, new Double(0));
        List<Double> sE = new ArrayList<Double>();
        Collections.fill(sE, new Double(0));
        long start = System.currentTimeMillis();
        int numSrc = 0;
        for (int i = 0; i < 5; i++) {
            DataSet woDataSet2 = GALDataReader
                    .readDatsetFromFolder(inputFolder);
            woDataSet2.initTrustworthiness();
            numSrc = woDataSet2.getNumSources();
            FeedbackSequence woRandom = new BaseLineFeedbackSequence(
                    woDataSet2.getNumDataItems());
            woRandom.setData(woDataSet2);
            woRandom.feedbackLoop();
            sP = ArrayUti.sum(sP, woRandom.precisionList);
            sE = ArrayUti.sum(sE, woRandom.entropyList);
            System.out.println(Joiner.on('\t').join(woRandom.precisionList));
            System.out.println(Joiner.on('\t').join(woRandom.entropyList));
        }
        long end = System.currentTimeMillis();
        long time = end - start;
        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setPrecisions(averageP);
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomMV");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(numSrc + "");
        return resultModel;
    }


    public ResultModel IGEMNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();
        FeedbackSequence woIGEM = new IGFeedbackSequence(
                woDataSet.getNumDataItems());
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("IGEM");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }


    public ResultModel EMWorkerNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();
        FeedbackSequence woIGEM = new EMWorkerFeedbackSequence(
                woDataSet.getNumDataItems());
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("EMWorker");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel IGEMRemoveCopierSequence(String inputFolder, int loopID, double rel, double std, double threshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();
        IGEMCopierSequence woIGEM = new IGEMCopierSequence(woDataSet.getNumDataItems(), threshold, true, true);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("IGEMRemoveCopierSequence");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel IGEMCopierSequence(String inputFolder, int loopID, double rel, double std, double threshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();
        IGEMCopierSequence woIGEM = new IGEMCopierSequence(woDataSet.getNumDataItems(), threshold, true, false);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("IGEMCopierSequence");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel RandomEMCopierSequence(String inputFolder, int loopID, double rel, double std, double threshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();

        List<Double> sP = new ArrayList<Double>();
        List<Double> sE = new ArrayList<Double>();
        List<Double> spamP = new ArrayList<Double>();
        List<Double> spamR = new ArrayList<Double>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
//            DataSet woDataSet2 = GALDataReader
//                    .readDatsetFromFolder(inputFolder);
            DataSet woDataSet2 = originalDS.clone();
            woDataSet2.initTrustworthiness();
            RandomEMCopierSequence sequence = new RandomEMCopierSequence(woDataSet.getNumDataItems(), threshold, true, false);
            sequence.setData(woDataSet2);
            sequence.feedbackLoop();
            sP = ArrayUti.sum(sP, sequence.precisionList);
            sE = ArrayUti.sum(sE, sequence.entropyList);
            spamP = ArrayUti.sum(spamP, sequence.spamPrecisionList);
            spamR = ArrayUti.sum(spamR, sequence.spamRecallList);
        }
        long end = System.currentTimeMillis();
        long time = (end - start) / 5;

        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> aSpamP = ArrayUti.average(spamP, 5);
        List<Double> aSpamR = ArrayUti.average(spamR, 5);

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setPrecisions(averageP);
        resultModel.setSpamPrec(aSpamP);
        resultModel.setSpamRecall(aSpamR);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomEMDetectCopiers");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel RandomEMCopierRemoveSequence(String inputFolder, int loopID, double rel, double std, double threshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();

        List<Double> sP = new ArrayList<Double>();
        List<Double> sE = new ArrayList<Double>();
        List<Double> spamP = new ArrayList<Double>();
        List<Double> spamR = new ArrayList<Double>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
//            DataSet woDataSet2 = GALDataReader
//                    .readDatsetFromFolder(inputFolder);
            DataSet woDataSet2 = originalDS.clone();
            woDataSet2.initTrustworthiness();
            RandomEMCopierSequence sequence = new RandomEMCopierSequence(woDataSet.getNumDataItems(), threshold, true, true);
            sequence.setData(woDataSet2);
            sequence.feedbackLoop();
            sP = ArrayUti.sum(sP, sequence.precisionList);
            sE = ArrayUti.sum(sE, sequence.entropyList);
            spamP = ArrayUti.sum(spamP, sequence.spamPrecisionList);
            spamR = ArrayUti.sum(spamR, sequence.spamRecallList);
        }
        long end = System.currentTimeMillis();
        long time = (end - start) / 5;

        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> aSpamP = ArrayUti.average(spamP, 5);
        List<Double> aSpamR = ArrayUti.average(spamR, 5);

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setPrecisions(averageP);
        resultModel.setSpamPrec(aSpamP);
        resultModel.setSpamRecall(aSpamR);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomEMRemoveCopiers");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public List<ResultModel> experimentLoop(int maxIter, int percent1, int percent2, double reliability1, double reliability2,
                                            double detectThreshold, double std, double copyRatio, boolean generateDepend)
            throws Exception {
        List<ResultModel> resultModelList = new ArrayList<ResultModel>();
        int numInter = maxIter;

        for (int i = 0; i < numInter; i++) {
            // Mainconfig.getInstance().initialized();
            Mainconfig.getInstance().getListConfig()
                    .put("workersRatio", "" + percent1 + "%;" + percent2 + "%");
            Mainconfig
                    .getInstance()
                    .getListConfig()
                    .put("typeOfDistributor",
                            "NormalDistribution(" + reliability1 + ",0.01);NormalDistribution("
                                    + reliability2 + "," + std + ")");
            // Binary, Multiple
//            Mainconfig.getInstance().getListConfig().put("InputDataType",
//                    "Multiple");
//            Mainconfig.getInstance().getListConfig().put("InputNumberLabels",
//                    "4");
//            Mainconfig.getInstance().getListConfig().put("NumOfQuestion", "10");
            Mainconfig.getInstance().initialized();
            generateData(generateDepend, copyRatio);

            // ///////////////////////////////Random/////////////////////////////////////////////////////////////////////////////////////
            ResultModel baseline = randomNormalSequence(inputPath, i, reliability2, std);
            baseline.setReliability(reliability1 + "");
            baseline.setReliability2(reliability2 + "");
            baseline.setPercent1(percent1 + "");
            baseline.setPercent2(percent2 + "");
            baseline.setCopyRatio(copyRatio + "");
            baseline.setCopyDetectThreshold(detectThreshold + "");

            // ///////////////////////////////IG+EM/////////////////////////////////////////////////////////////////////////////////////
//            ResultModel igem = IGEMNormalSequence(inputPath, i, reliability, std);

//            ResultModel emworker = EMWorkerNormalSequence(inputPath, i, reliability, std);

            ResultModel randomEMDetect = RandomEMCopierSequence(inputPath, i, reliability2, std, detectThreshold);
            randomEMDetect.setReliability(reliability1 + "");
            randomEMDetect.setReliability2(reliability2 + "");
            randomEMDetect.setPercent1(percent1 + "");
            randomEMDetect.setPercent2(percent2 + "");
            randomEMDetect.setCopyRatio(copyRatio + "");
            randomEMDetect.setCopyDetectThreshold(detectThreshold + "");

            ResultModel removeCopiers = RandomEMCopierRemoveSequence(inputPath, i, reliability2, std, detectThreshold);
            removeCopiers.setReliability(reliability1 + "");
            removeCopiers.setReliability2(reliability2 + "");
            removeCopiers.setPercent1(percent1 + "");
            removeCopiers.setPercent2(percent2 + "");
            removeCopiers.setCopyRatio(copyRatio + "");
            removeCopiers.setCopyDetectThreshold(detectThreshold + "");

            ResultModel igemcopiers = IGEMCopierSequence(inputPath, i, reliability2, std, detectThreshold);
            igemcopiers.setReliability(reliability1 + "");
            igemcopiers.setReliability2(reliability2 + "");
            igemcopiers.setPercent1(percent1 + "");
            igemcopiers.setPercent2(percent2 + "");
            igemcopiers.setCopyRatio(copyRatio + "");
            igemcopiers.setCopyDetectThreshold(detectThreshold + "");

            ResultModel igemremovecopiers = IGEMRemoveCopierSequence(inputPath, i, reliability2, std, detectThreshold);
            igemremovecopiers.setReliability(reliability1 + "");
            igemremovecopiers.setReliability2(reliability2 + "");
            igemremovecopiers.setPercent1(percent1 + "");
            igemremovecopiers.setPercent2(percent2 + "");
            igemremovecopiers.setCopyRatio(copyRatio + "");
            igemremovecopiers.setCopyDetectThreshold(detectThreshold + "");

//            resultModelList.add(ran);
//            resultModelList.add(igem);
//            resultModelList.add(emworker);
            resultModelList.add(randomEMDetect);
            resultModelList.add(removeCopiers);
            resultModelList.add(igemremovecopiers);
            resultModelList.add(igemcopiers);
        }

        return resultModelList;
    }

    public static void main(String[] args) {
        ExperimentDetectStep a = new ExperimentDetectStep();
        String dateTime = new SimpleDateFormat("MMddhhmm").format(new Date());
        String outputFilePath = "expResults/" + a.getClass().getSimpleName() + "_" + dateTime + ".csv";
        List<ResultModel> results = new ArrayList<ResultModel>();

        double[] copyRatios = {0.8};
        double[] detectThresholds = {0.8};

        double rel1 = 0.8;
        double rel2 = 0.0;
        int per1 = 90;
        int per2 = 10;
        double std = 0.01;
        assert per1 + per2 == 100;
        boolean genarateDepend = true;
        int numLoop = 25;
        List<ResultModel> list = null;

        try {
            for (double copyThreshold : detectThresholds) {
                for (double copyRatio : copyRatios) {
                    list = a.experimentLoop(numLoop, per1, per2, rel1, rel2, copyThreshold, std, copyRatio, genarateDepend);
                    results.addAll(list);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            File file = new File(outputFilePath);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);


            String comma = ",";
            // Write result to file
            // CopyRatio, CopyThreshold, Reliablity1,Rel2,Percent1,Percent2,Std,LoopID,Algor,#Src,Time,Prec1,Prec2,...,Precn,Ent1,Ent2,..,Entn,SP1,Sp2,...,SPn,SR1,SR2,...,SRn,Ind1,Ind2,..,Indn
            for (int i = 0; i < results.size(); i++) {
                ResultModel resultModel = results.get(i);
                StringBuilder builder = new StringBuilder();
                builder.append(resultModel.getCopyRatio()).append(comma);
                builder.append(resultModel.getCopyDetectThreshold()).append(comma);
                builder.append(resultModel.getReliability()).append(comma);
                builder.append(resultModel.getReliability2()).append(comma);
                builder.append(resultModel.getPercent1()).append(comma);
                builder.append(resultModel.getPercent2()).append(comma);
                builder.append(resultModel.getStd()).append(comma);
                builder.append(resultModel.getID()).append(comma);
                builder.append(resultModel.getAlgorithm()).append(comma);
                builder.append(resultModel.getNumSrc()).append(comma);
                builder.append(resultModel.getTime()).append(comma);
                builder.append(listInt2String(resultModel.getPrecisions())).append(comma);
                builder.append(comma);
                builder.append(listInt2String(resultModel.getEntropies())).append(comma);
                builder.append(comma);
                builder.append(listInt2String(resultModel.getSpamPrec())).append(comma);
                builder.append(comma);
                builder.append(listInt2String(resultModel.getSpamRecall())).append(comma);
                builder.append(comma);
                builder.append(listInt2String(resultModel.getItemIndices())).append("\n");
                bw.write(builder.toString());
            }

            bw.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    static <T> String listInt2String(List<T> list) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size() - 1; i++) {
            T t = list.get(i);
            builder.append(t.toString()).append(",");
        }
        builder.append(list.get(list.size() - 1));
        return builder.toString();
    }

    private class ResultModel {
        private String ID;
        private String algorithm;
        private String time;
        private List<Double> precisions;
        private List<Double> entropies;
        private List<Integer> itemIndices;
        private String reliability;
        private String std;
        private String numSrc;
        private List<Double> spamPrec;
        private List<Double> spamRecall;
        private String percent1;
        private String percent2;
        private String reliability2;
        private String copyRatio;
        private String copyDetectThreshold;


        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public List<Double> getPrecisions() {
            return precisions;
        }

        public void setPrecisions(List<Double> precisions) {
            this.precisions = precisions;
        }

        public List<Double> getEntropies() {
            return entropies;
        }

        public void setEntropies(List<Double> entropies) {
            this.entropies = entropies;
        }

        public List<Integer> getItemIndices() {
            return itemIndices;
        }

        public void setItemIndices(List<Integer> itemIndices) {
            this.itemIndices = itemIndices;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getAlgorithm() {
            return algorithm;
        }

        public void setAlgorithm(String algorithm) {
            this.algorithm = algorithm;
        }

        public String getReliability() {
            return reliability;
        }

        public void setReliability(String reliability) {
            this.reliability = reliability;
        }

        public String getStd() {
            return std;
        }

        public void setStd(String std) {
            this.std = std;
        }

        public String getNumSrc() {
            return numSrc;
        }

        public void setNumSrc(String numSrc) {
            this.numSrc = numSrc;
        }

        public List<Double> getSpamPrec() {
            return spamPrec;
        }

        public void setSpamPrec(List<Double> spamPrec) {
            this.spamPrec = spamPrec;
        }

        public List<Double> getSpamRecall() {
            return spamRecall;
        }

        public void setSpamRecall(List<Double> spamRecall) {
            this.spamRecall = spamRecall;
        }

        public String getPercent1() {
            return percent1;
        }

        public void setPercent1(String percent1) {
            this.percent1 = percent1;
        }

        public String getPercent2() {
            return percent2;
        }

        public void setPercent2(String percent2) {
            this.percent2 = percent2;
        }

        public String getReliability2() {
            return reliability2;
        }

        public void setReliability2(String reliability2) {
            this.reliability2 = reliability2;
        }

        public String getCopyRatio() {
            return copyRatio;
        }

        public void setCopyRatio(String copyRatio) {
            this.copyRatio = copyRatio;
        }

        public String getCopyDetectThreshold() {
            return copyDetectThreshold;
        }

        public void setCopyDetectThreshold(String copyDetectThreshold) {
            this.copyDetectThreshold = copyDetectThreshold;
        }
    }
}
