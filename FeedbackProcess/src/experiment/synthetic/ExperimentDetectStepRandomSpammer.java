package experiment.synthetic;

import com.google.common.base.Joiner;
import data.generator.CopySimulator;
import data.model.DataSet;
import data.reader.GALDataReader;
import feedback.strategy.*;
import main.java.config.Mainconfig;
import main.java.feedback.FeedBackModel;
import main.java.utility.IDGenerator;
import utils.ArrayUti;
import utils.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Asus on 4/22/14.
 * Experiment to check how many steps are required to detect random spammerssao
 */
public class ExperimentDetectStepRandomSpammer {
    public String input = "input/";

    public String inputPath = input + "data/";
    public String labelPath = inputPath + "label.txt";
    public String categoryPath = inputPath + "category.txt";
    public String goldPath = inputPath + "gold.txt";
    public String workerReliabilityPath = inputPath + "worker.txt";

    private DataSet originalDS;

    public void generateData(boolean generateDepend, double copyRatio) throws Exception {
        Mainconfig.getInstance().initialized();
        for (Mainconfig.ListDatasets data : Mainconfig.getInstance().getDatasets()) {
            Mainconfig.getInstance().setData(data);
        }
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("workersRatio"));
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("typeOfDistributor"));
        IDGenerator.resetWorkerID(-1);
        IDGenerator.resetQuestionID(-1);
        FeedBackModel model = new FeedBackModel();
        model.generateGALInputFilesMulti(labelPath, categoryPath, goldPath,
                workerReliabilityPath);


        /*######################################### With copy data sources###############################################*/
        if (generateDepend) {
            // Load original data
            DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
            dataSet.initTrustworthiness();
            System.out.println("Init numSrc " + dataSet.getNumSources());
            //Generate copied data
            CopySimulator simulator = new CopySimulator(dataSet);
            simulator.setCopyRatio(copyRatio);
            simulator.genCopierFromWorstSrc(CopySimulator.COPY_FALSE_STRATEGY.COPY_ALL, CopySimulator.COPY_TRUE_STRATEGY.COPY_ALL);
//            simulator.genCopierRandomly(CopySimulator.COPY_FALSE_STRATEGY.COPY_ALL, CopySimulator.COPY_TRUE_STRATEGY.COPY_ALL);

            String outputCopyFolder = inputPath;
            File f = new File(outputCopyFolder);
            if (f.isDirectory()) {
                // FileUtils.cleanDirectory(f);
                Utils.writeGALInputFromDataSet(dataSet, outputCopyFolder);
            }
        }

        originalDS = GALDataReader.readDatsetFromFolder(inputPath);
    }

    public ResultModel randomNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        System.out.println("Randommmmmmmmmmmm");
        List<Double> sP = new ArrayList<Double>();
        Collections.fill(sP, new Double(0));
        List<Double> sE = new ArrayList<Double>();
        Collections.fill(sE, new Double(0));
        long start = System.currentTimeMillis();
        int numSrc = 0;
        for (int i = 0; i < 5; i++) {
            DataSet woDataSet2 = GALDataReader
                    .readDatsetFromFolder(inputFolder);
            woDataSet2.initTrustworthiness();
            numSrc = woDataSet2.getNumSources();
            FeedbackSequence woRandom = new BaseLineFeedbackSequence(
                    woDataSet2.getNumDataItems());
            woRandom.setData(woDataSet2);
            woRandom.feedbackLoop();
            sP = ArrayUti.sum(sP, woRandom.precisionList);
            sE = ArrayUti.sum(sE, woRandom.entropyList);
            System.out.println(Joiner.on('\t').join(woRandom.precisionList));
            System.out.println(Joiner.on('\t').join(woRandom.entropyList));
        }
        long end = System.currentTimeMillis();
        long time = end - start;
        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> rP = new ArrayList<Double>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            rP.add(-1.0);
        }

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setPrecisions(averageP);
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setSpamPrec(rP);
        resultModel.setSpamRecall(rP);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomMV");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(numSrc + "");

        return resultModel;
    }


    public ResultModel IGEMNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();
        FeedbackSequence woIGEM = new IGFeedbackSequence(
                woDataSet.getNumDataItems());
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("IGEM");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }


    public ResultModel EMWorkerNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();
        FeedbackSequence woIGEM = new EMWorkerFeedbackSequence(
                woDataSet.getNumDataItems());
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("EMWorker");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

//    public ResultModel IGEMRemoveRandomSequence(String inputFolder, int loopID, double rel, double std, double detectThreshold) throws Exception {
////        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
//        DataSet woDataSet = originalDS.clone();
//        woDataSet.initTrustworthiness();
//        IGEMRandomSequence woIGEM = new IGEMRandomSequence(
//                woDataSet.getNumDataItems(), detectThreshold, true, true);
//        woIGEM.setData(woDataSet);
//        long start = System.currentTimeMillis();
//        woIGEM.feedbackLoop();
//        long end = System.currentTimeMillis();
//        long time = end - start;
//
//        ResultModel resultModel = new ResultModel();
//        resultModel.setTime(time + "");
//        resultModel.setEntropies(woIGEM.entropyList);
//        resultModel.setItemIndices(woIGEM.itemIndices);
//        resultModel.setPrecisions(woIGEM.precisionList);
//        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
//        resultModel.setSpamRecall(woIGEM.spamRecallList);
//        resultModel.setID(loopID + "");
//        resultModel.setAlgorithm("IGEMRandomRemoveSequence");
//        resultModel.setStd(std + "");
//        resultModel.setReliability(rel + "");
//        resultModel.setNumSrc(woDataSet.getNumSources() + "");
//        return resultModel;
//    }

    public ResultModel SpammerEMRemoveRandomSequence(String inputFolder, int loopID, double rel, double std, double detectThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();
        SpammerEMRandomSequence woIGEM = new SpammerEMRandomSequence(
                woDataSet.getNumDataItems(), detectThreshold, true, true);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("SpammerEMRemoveRandomSequence");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel SpammerEMRandomSequence(String inputFolder, int loopID, double rel, double std, double detectThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();
        SpammerEMRandomSequence woIGEM = new SpammerEMRandomSequence(
                woDataSet.getNumDataItems(), detectThreshold, true, false);
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
        resultModel.setSpamRecall(woIGEM.spamRecallList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("SpammerEMRandomSequence");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

//    public ResultModel IGEMRandomSequence(String inputFolder, int loopID, double rel, double std, double detectThreshold) throws Exception {
////        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
//        DataSet woDataSet = originalDS.clone();
//        woDataSet.initTrustworthiness();
//        IGEMRandomSequence woIGEM = new IGEMRandomSequence(
//                woDataSet.getNumDataItems(), detectThreshold, true, false);
//        woIGEM.setData(woDataSet);
//        long start = System.currentTimeMillis();
//        woIGEM.feedbackLoop();
//        long end = System.currentTimeMillis();
//        long time = end - start;
//
//        ResultModel resultModel = new ResultModel();
//        resultModel.setTime(time + "");
//        resultModel.setEntropies(woIGEM.entropyList);
//        resultModel.setItemIndices(woIGEM.itemIndices);
//        resultModel.setPrecisions(woIGEM.precisionList);
//        resultModel.setSpamPrec(woIGEM.spamPrecisionList);
//        resultModel.setSpamRecall(woIGEM.spamRecallList);
//        resultModel.setID(loopID + "");
//        resultModel.setAlgorithm("IGEMRandomSequence");
//        resultModel.setStd(std + "");
//        resultModel.setReliability(rel + "");
//        resultModel.setNumSrc(woDataSet.getNumSources() + "");
//        return resultModel;
//    }

    public ResultModel RandomEMDetectSequence(String inputFolder, int loopID, double rel, double std, double threshold) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();

        List<Double> sP = new ArrayList<Double>();
        List<Double> sE = new ArrayList<Double>();
        List<Double> spamP = new ArrayList<Double>();
        List<Double> spamR = new ArrayList<Double>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
            DataSet woDataSet2 = GALDataReader
                    .readDatsetFromFolder(inputFolder);
            woDataSet2.initTrustworthiness();
            RandomEMCopierSequence sequence = new RandomEMCopierSequence(woDataSet.getNumDataItems(), threshold, true, false);
            sequence.setData(woDataSet2);
            sequence.feedbackLoop();
            sP = ArrayUti.sum(sP, sequence.precisionList);
            sE = ArrayUti.sum(sE, sequence.entropyList);
            spamP = ArrayUti.sum(spamP, sequence.spamPrecisionList);
            spamR = ArrayUti.sum(spamR, sequence.spamRecallList);
        }
        long end = System.currentTimeMillis();
        long time = (end - start) / 5;

        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> aSpamP = ArrayUti.average(spamP, 5);
        List<Double> aSpamR = ArrayUti.average(spamR, 5);

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setPrecisions(averageP);
        resultModel.setSpamPrec(aSpamP);
        resultModel.setSpamRecall(aSpamR);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomEMDetect");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }


    public ResultModel RandomEMDetecRandomSequence(String inputFolder, int loopID, double rel, double std, double detectThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();

        List<Double> sP = new ArrayList<Double>();
        List<Double> sE = new ArrayList<Double>();
        List<Double> spamP = new ArrayList<Double>();
        List<Double> spamR = new ArrayList<Double>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
//            DataSet woDataSet2 = GALDataReader
//                    .readDatsetFromFolder(inputFolder);
            DataSet woDataSet2 = originalDS.clone();
            woDataSet2.initTrustworthiness();
            RandomEMRandomSequence sequence = new RandomEMRandomSequence(woDataSet.getNumDataItems(), detectThreshold, true, false);
            sequence.setData(woDataSet2);
            sequence.feedbackLoop();
            sP = ArrayUti.sum(sP, sequence.precisionList);
            sE = ArrayUti.sum(sE, sequence.entropyList);
            spamP = ArrayUti.sum(spamP, sequence.spamPrecisionList);
            spamR = ArrayUti.sum(spamR, sequence.spamRecallList);
        }
        long end = System.currentTimeMillis();
        long time = (end - start) / 5;

        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> aSpamP = ArrayUti.average(spamP, 5);
        List<Double> aSpamR = ArrayUti.average(spamR, 5);

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setPrecisions(averageP);
        resultModel.setSpamPrec(aSpamP);
        resultModel.setSpamRecall(aSpamR);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomEMDetectRandom");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public ResultModel RandomEMRandomRemoveSequence(String inputFolder, int loopID, double rel, double std, double detectThreshold) throws Exception {
//        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        DataSet woDataSet = originalDS.clone();
        woDataSet.initTrustworthiness();

        List<Double> sP = new ArrayList<Double>();
        List<Double> sE = new ArrayList<Double>();
        List<Double> spamP = new ArrayList<Double>();
        List<Double> spamR = new ArrayList<Double>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 5; i++) {
//            DataSet woDataSet2 = GALDataReader
//                    .readDatsetFromFolder(inputFolder);
            DataSet woDataSet2 = originalDS.clone();
            woDataSet2.initTrustworthiness();
            RandomEMRandomSequence sequence = new RandomEMRandomSequence(woDataSet.getNumDataItems(), detectThreshold, true, true);
            sequence.setData(woDataSet2);
            sequence.feedbackLoop();
            sP = ArrayUti.sum(sP, sequence.precisionList);
            sE = ArrayUti.sum(sE, sequence.entropyList);
            spamP = ArrayUti.sum(spamP, sequence.spamPrecisionList);
            spamR = ArrayUti.sum(spamR, sequence.spamRecallList);
        }
        long end = System.currentTimeMillis();
        long time = (end - start) / 5;

        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        List<Double> aSpamP = ArrayUti.average(spamP, 5);
        List<Double> aSpamR = ArrayUti.average(spamR, 5);

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setPrecisions(averageP);
        resultModel.setSpamPrec(aSpamP);
        resultModel.setSpamRecall(aSpamR);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomEMRemoveRandom");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        resultModel.setRemoveSpammer(true + "");
        return resultModel;
    }

    public List<ResultModel> experimentLoop(int maxIter, int percent1, int percent2, double reliability1, double reliability2,
                                            double threshold, double std, double copyRatio, boolean generateDepend)
            throws Exception {
        List<ResultModel> resultModelList = new ArrayList<ResultModel>();
        int numInter = maxIter;

        for (int i = 0; i < numInter; i++) {
            // Mainconfig.getInstance().initialized();
            Mainconfig.getInstance().getListConfig()
                    .put("workersRatio", "" + percent1 + "%;" + percent2 + "%");
            Mainconfig
                    .getInstance()
                    .getListConfig()
                    .put("typeOfDistributor",
                            "NormalDistribution(" + reliability1 + ",0.01);NormalDistribution("
                                    + reliability2 + "," + std + ")");
            // Binary, Multiple
//            Mainconfig.getInstance().getListConfig().put("InputDataType",
//                    "Multiple");
//            Mainconfig.getInstance().getListConfig().put("InputNumberLabels",
//                    "4");
//            Mainconfig.getInstance().getListConfig().put("NumOfQuestion", "10");
            Mainconfig.getInstance().initialized();
            generateData(generateDepend, copyRatio);

            // ///////////////////////////////Random/////////////////////////////////////////////////////////////////////////////////////
            ResultModel baseline = randomNormalSequence(inputPath, i, reliability2, std);
            baseline.setReliability(reliability1 + "");
            baseline.setReliability2(reliability2 + "");
            baseline.setPercent1(percent1 + "");
            baseline.setPercent2(percent2 + "");
            baseline.setCopyRatio(copyRatio + "");
            baseline.setDetectThreshold(threshold + "");

            // ///////////////////////////////IG+EM/////////////////////////////////////////////////////////////////////////////////////
//            ResultModel igem = IGEMNormalSequence(inputPath, i, reliability, std);

//            ResultModel emworker = EMWorkerNormalSequence(inputPath, i, reliability, std);

//            ResultModel randomEMDetect = RandomEMCopierSequence(inputPath, i, reliability2, std, 0.5);


//            ResultModel randomEMDetectRandom = RandomEMDetecRandomSequence(inputPath, i, reliability2, std, threshold);
//            randomEMDetectRandom.setReliability(reliability1 + "");
//            randomEMDetectRandom.setReliability2(reliability2 + "");
//            randomEMDetectRandom.setPercent1(percent1 + "");
//            randomEMDetectRandom.setPercent2(percent2 + "");
//            randomEMDetectRandom.setCopyRatio(copyRatio + "");
//            randomEMDetectRandom.setDetectThreshold(threshold + "");
//
//            ResultModel removeSpammer = RandomEMRandomRemoveSequence(inputPath, i, reliability2, std, threshold);
//            removeSpammer.setReliability(reliability1 + "");
//            removeSpammer.setReliability2(reliability2 + "");
//            removeSpammer.setPercent1(percent1 + "");
//            removeSpammer.setPercent2(percent2 + "");
//            removeSpammer.setCopyRatio(copyRatio + "");
//            removeSpammer.setDetectThreshold(threshold + "");

//            ResultModel igemremoveSpammer = IGEMRemoveRandomSequence(inputPath, i, reliability2, std, threshold);
//            igemremoveSpammer.setReliability(reliability1 + "");
//            igemremoveSpammer.setReliability2(reliability2 + "");
//            igemremoveSpammer.setPercent1(percent1 + "");
//            igemremoveSpammer.setPercent2(percent2 + "");
//            igemremoveSpammer.setCopyRatio(copyRatio + "");
//            igemremoveSpammer.setDetectThreshold(threshold + "");

//            ResultModel igemSpammer = IGEMRandomSequence(inputPath, i, reliability2, std, threshold);
//            igemSpammer.setReliability(reliability1 + "");
//            igemSpammer.setReliability2(reliability2 + "");
//            igemSpammer.setPercent1(percent1 + "");
//            igemSpammer.setPercent2(percent2 + "");
//            igemSpammer.setCopyRatio(copyRatio + "");
//            igemSpammer.setDetectThreshold(threshold + "");

//            ResultModel spammerEMRandomSequence = SpammerEMRandomSequence(inputPath, i, reliability2, std, threshold);
//            spammerEMRandomSequence.setReliability(reliability1 + "");
//            spammerEMRandomSequence.setReliability2(reliability2 + "");
//            spammerEMRandomSequence.setPercent1(percent1 + "");
//            spammerEMRandomSequence.setPercent2(percent2 + "");
//            spammerEMRandomSequence.setCopyRatio(copyRatio + "");
//            spammerEMRandomSequence.setDetectThreshold(threshold + "");


//            ResultModel spammerEMRemoveRandomSequence = SpammerEMRemoveRandomSequence(inputPath, i, reliability2, std, threshold);
//            spammerEMRemoveRandomSequence.setReliability(reliability1 + "");
//            spammerEMRemoveRandomSequence.setReliability2(reliability2 + "");
//            spammerEMRemoveRandomSequence.setPercent1(percent1 + "");
//            spammerEMRemoveRandomSequence.setPercent2(percent2 + "");
//            spammerEMRemoveRandomSequence.setCopyRatio(copyRatio + "");
//            spammerEMRemoveRandomSequence.setDetectThreshold(threshold + "");

//            resultModelList.add(ran);
//            resultModelList.add(igem);
//            resultModelList.add(emworker);
            resultModelList.add(baseline);// Baseline
//            resultModelList.add(randomEMDetectRandom);
//            resultModelList.add(removeSpammer);
//            resultModelList.add(igemremoveSpammer);
//            resultModelList.add(igemSpammer);
//            resultModelList.add(spammerEMRandomSequence);
//            resultModelList.add(spammerEMRemoveRandomSequence);

        }
        return resultModelList;
    }

    public static void main(String[] args) {
        ExperimentDetectStepRandomSpammer a = new ExperimentDetectStepRandomSpammer();
        String dateTime = new SimpleDateFormat("MMddhhmm").format(new Date());
        String outputFilePath = "expResults/" + a.getClass().getSimpleName() + "_" + dateTime + ".csv";
        List<ResultModel> results = new ArrayList<ResultModel>();

        double rel1 = 0.8;
        double rel2 = 0.5;
        double std = 0.01;
        int[] per2s = {60};
        double[] thresholds = {0.01};
        int numLoop = 20;

        int[] numWorkers = {20};

        boolean genarateDepend = false;
        double copyRatio = 0.2;

        try {
            for (int numWorker : numWorkers) {
                for (int per2 : per2s) {
                    for (double threshold : thresholds) {
                        int per1 = 100 - per2;
                        List<ResultModel> list = a.experimentLoop(numLoop, per1, per2, rel1, rel2, threshold, std, copyRatio, genarateDepend);
                        results.addAll(list);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            File file = new File(outputFilePath);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);


            String comma = ",";
            // Write result to file
            // DetectThreshold, Reliablity1,Rel2,Percent1,Percent2,Std,LoopID,Algor,#Src,Time,Prec1,Prec2,...,Precn,Ent1,Ent2,..,Entn,SP1,Sp2,...,SPn,SR1,SR2,...,SRn,Ind1,Ind2,..,Indn
            for (int i = 0; i < results.size(); i++) {
                ResultModel resultModel = results.get(i);
                StringBuilder builder = new StringBuilder();
                builder.append(resultModel.getDetectThreshold()).append(comma);
                builder.append(resultModel.getReliability()).append(comma);
                builder.append(resultModel.getReliability2()).append(comma);
                builder.append(resultModel.getPercent1()).append(comma);
                builder.append(resultModel.getPercent2()).append(comma);
                builder.append(resultModel.getStd()).append(comma);
                builder.append(resultModel.getID()).append(comma);
                builder.append(resultModel.getAlgorithm()).append(comma);
                builder.append(resultModel.getNumSrc()).append(comma);
                builder.append(resultModel.getTime()).append(comma);
                builder.append(listInt2String(resultModel.getPrecisions())).append(comma);
                builder.append(comma);
                builder.append(listInt2String(resultModel.getEntropies())).append(comma);
                builder.append(comma);
                builder.append(listInt2String(resultModel.getSpamPrec())).append(comma);
                builder.append(comma);
                builder.append(listInt2String(resultModel.getSpamRecall())).append(comma);
                builder.append(comma);
                builder.append(listInt2String(resultModel.getItemIndices())).append("\n");
                bw.write(builder.toString());
            }

            bw.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    static <T> String listInt2String(List<T> list) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size() - 1; i++) {
            T t = list.get(i);
            builder.append(t.toString()).append(",");
        }
        builder.append(list.get(list.size() - 1));
        return builder.toString();
    }

    private class ResultModel {
        private String ID;
        private String algorithm;
        private String time;
        private List<Double> precisions;
        private List<Double> entropies;
        private List<Integer> itemIndices;
        private String reliability;
        private String std;
        private String numSrc;
        private List<Double> spamPrec;
        private List<Double> spamRecall;
        private String percent1;
        private String percent2;
        private String reliability2;
        private String copyRatio;
        private String detectThreshold;
        private String removeSpammer;


        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public List<Double> getPrecisions() {
            return precisions;
        }

        public void setPrecisions(List<Double> precisions) {
            this.precisions = precisions;
        }

        public List<Double> getEntropies() {
            return entropies;
        }

        public void setEntropies(List<Double> entropies) {
            this.entropies = entropies;
        }

        public List<Integer> getItemIndices() {
            return itemIndices;
        }

        public void setItemIndices(List<Integer> itemIndices) {
            this.itemIndices = itemIndices;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getAlgorithm() {
            return algorithm;
        }

        public void setAlgorithm(String algorithm) {
            this.algorithm = algorithm;
        }

        public String getReliability() {
            return reliability;
        }

        public void setReliability(String reliability) {
            this.reliability = reliability;
        }

        public String getStd() {
            return std;
        }

        public void setStd(String std) {
            this.std = std;
        }

        public String getNumSrc() {
            return numSrc;
        }

        public void setNumSrc(String numSrc) {
            this.numSrc = numSrc;
        }

        public List<Double> getSpamPrec() {
            return spamPrec;
        }

        public void setSpamPrec(List<Double> spamPrec) {
            this.spamPrec = spamPrec;
        }

        public List<Double> getSpamRecall() {
            return spamRecall;
        }

        public void setSpamRecall(List<Double> spamRecall) {
            this.spamRecall = spamRecall;
        }

        public String getPercent1() {
            return percent1;
        }

        public void setPercent1(String percent1) {
            this.percent1 = percent1;
        }

        public String getPercent2() {
            return percent2;
        }

        public void setPercent2(String percent2) {
            this.percent2 = percent2;
        }

        public String getReliability2() {
            return reliability2;
        }

        public void setReliability2(String reliability2) {
            this.reliability2 = reliability2;
        }

        public String getCopyRatio() {
            return copyRatio;
        }

        public void setCopyRatio(String copyRatio) {
            this.copyRatio = copyRatio;
        }

        public String getDetectThreshold() {
            return detectThreshold;
        }

        public void setDetectThreshold(String detectThreshold) {
            this.detectThreshold = detectThreshold;
        }

        public String getRemoveSpammer() {
            return removeSpammer;
        }

        public void setRemoveSpammer(String removeSpammer) {
            this.removeSpammer = removeSpammer;
        }
    }
}
