package experiment.synthetic;

import com.google.common.base.Joiner;
import data.generator.CopySimulator;
import data.model.DataSet;
import data.reader.GALDataReader;
import feedback.strategy.BaseLineFeedbackSequence;
import feedback.strategy.EMWorkerFeedbackSequence;
import feedback.strategy.FeedbackSequence;
import feedback.strategy.IGFeedbackSequence;
import main.java.config.Mainconfig;
import main.java.feedback.FeedBackModel;
import main.java.utility.IDGenerator;
import utils.ArrayUti;
import utils.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Asus on 4/3/14.
 */
public class ExperimentSingleType {

    public String input = "input/";

    public String inputPath = input + "data/";
    public String labelPath = inputPath + "label.txt";
    public String categoryPath = inputPath + "category.txt";
    public String goldPath = inputPath + "gold.txt";
    public String workerReliabilityPath = inputPath + "worker.txt";

    public void generateData(boolean generateDepend) throws Exception {
        Mainconfig.getInstance().initialized();
        for (Mainconfig.ListDatasets data : Mainconfig.getInstance().getDatasets()) {
            Mainconfig.getInstance().setData(data);
        }
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("workersRatio"));
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("typeOfDistributor"));
        IDGenerator.resetWorkerID(-1);
        IDGenerator.resetQuestionID(-1);
        FeedBackModel model = new FeedBackModel();
        model.generateGALInputFilesMulti(labelPath, categoryPath, goldPath,
                workerReliabilityPath);


        /*######################################### With copy data sources###############################################*/
        if (generateDepend) {
            // Load original data
            DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
            dataSet.initTrustworthiness();
            System.out.println("Init numSrc " + dataSet.getNumSources());
            //Generate copied data
            CopySimulator simulator = new CopySimulator(dataSet);
            simulator.setCopyRatio(0.5);
            simulator.genCopierFromWorstSrc(CopySimulator.COPY_FALSE_STRATEGY.COPY_ALL, CopySimulator.COPY_TRUE_STRATEGY.COPY_ALL);
//            simulator.genCopierRandomly(CopySimulator.COPY_FALSE_STRATEGY.COPY_ALL, CopySimulator.COPY_TRUE_STRATEGY.COPY_ALL);

            String outputCopyFolder = inputPath;
            File f = new File(outputCopyFolder);
            if (f.isDirectory()) {
                // FileUtils.cleanDirectory(f);
                Utils.writeGALInputFromDataSet(dataSet, outputCopyFolder);
            }
        }

    }

    public ResultModel randomNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        System.out.println("Randommmmmmmmmmmm");
        List<Double> sP = new ArrayList<Double>();
        Collections.fill(sP, new Double(0));
        List<Double> sE = new ArrayList<Double>();
        Collections.fill(sE, new Double(0));
        long start = System.currentTimeMillis();
        int numSrc = 0;
        for (int i = 0; i < 5; i++) {
            DataSet woDataSet2 = GALDataReader
                    .readDatsetFromFolder(inputFolder);
            woDataSet2.initTrustworthiness();
            numSrc = woDataSet2.getNumSources();
            FeedbackSequence woRandom = new BaseLineFeedbackSequence(
                    woDataSet2.getNumDataItems());
            woRandom.setData(woDataSet2);
            woRandom.feedbackLoop();
            sP = ArrayUti.sum(sP, woRandom.precisionList);
            sE = ArrayUti.sum(sE, woRandom.entropyList);
            System.out.println(Joiner.on('\t').join(woRandom.precisionList));
            System.out.println(Joiner.on('\t').join(woRandom.entropyList));
        }
        long end = System.currentTimeMillis();
        long time = end - start;
        List<Double> averageP = ArrayUti.average(sP, 5);
        List<Double> averageE = ArrayUti.average(sE, 5);
        List<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < averageP.size() - 1; i++) {
            indices.add(-1);
        }

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setPrecisions(averageP);
        resultModel.setEntropies(averageE);
        resultModel.setItemIndices(indices);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("RandomMV");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(numSrc + "");
        return resultModel;
    }


    public ResultModel IGEMNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();
        FeedbackSequence woIGEM = new IGFeedbackSequence(
                woDataSet.getNumDataItems());
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("IGEM");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }


    public ResultModel EMWorkerNormalSequence(String inputFolder, int loopID, double rel, double std) throws Exception {
        DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        woDataSet.initTrustworthiness();
        FeedbackSequence woIGEM = new EMWorkerFeedbackSequence(
                woDataSet.getNumDataItems());
        woIGEM.setData(woDataSet);
        long start = System.currentTimeMillis();
        woIGEM.feedbackLoop();
        long end = System.currentTimeMillis();
        long time = end - start;

        ResultModel resultModel = new ResultModel();
        resultModel.setTime(time + "");
        resultModel.setEntropies(woIGEM.entropyList);
        resultModel.setItemIndices(woIGEM.itemIndices);
        resultModel.setPrecisions(woIGEM.precisionList);
        resultModel.setID(loopID + "");
        resultModel.setAlgorithm("EMWorker");
        resultModel.setStd(std + "");
        resultModel.setReliability(rel + "");
        resultModel.setNumSrc(woDataSet.getNumSources() + "");
        return resultModel;
    }

    public void runExperiment(int id, double rel, double std) throws Exception {

        String inputFolder = inputPath;

        // Load original data
        DataSet dataSet = GALDataReader.readDatsetFromFolder(inputFolder);
        dataSet.initTrustworthiness();
        System.out.println("Init numSrc " + dataSet.getNumSources());
        // ///////////////////////////////Random/////////////////////////////////////////////////////////////////////////////////////
        randomNormalSequence(inputFolder, id, rel, std);

        // ///////////////////////////////IG+EM/////////////////////////////////////////////////////////////////////////////////////
        IGEMNormalSequence(inputFolder, id, rel, std);

        EMWorkerNormalSequence(inputFolder, id, rel, std);

    }

    public List<ResultModel> experimentLoop(int maxIter, double reliability, double std, boolean generateDepend)
            throws Exception {
        List<ResultModel> resultModelList = new ArrayList<ResultModel>();
        int numInter = maxIter;

        for (int i = 0; i < numInter; i++) {
            // Mainconfig.getInstance().initialized();
            Mainconfig.getInstance().getListConfig()
                    .put("workersRatio", "80%;20%");
            Mainconfig
                    .getInstance()
                    .getListConfig()
                    .put("typeOfDistributor",
                            "NormalDistribution(0.3,0.01);NormalDistribution("
                                    + reliability + "," + std + ")");
            // Binary, Multiple
//            Mainconfig.getInstance().getListConfig().put("InputDataType",
//                    "Multiple");
//            Mainconfig.getInstance().getListConfig().put("InputNumberLabels",
//                    "4");
//            Mainconfig.getInstance().getListConfig().put("NumOfQuestion", "10");
            Mainconfig.getInstance().initialized();
            generateData(generateDepend);

            // ///////////////////////////////Random/////////////////////////////////////////////////////////////////////////////////////
            ResultModel ran = randomNormalSequence(inputPath, i, reliability, std);

            // ///////////////////////////////IG+EM/////////////////////////////////////////////////////////////////////////////////////
            ResultModel igem = IGEMNormalSequence(inputPath, i, reliability, std);

            ResultModel emworker = EMWorkerNormalSequence(inputPath, i, reliability, std);

            resultModelList.add(ran);
            resultModelList.add(igem);
            resultModelList.add(emworker);
        }

        return resultModelList;
    }

    public static void main(String[] args) {
        ExperimentSingleType a = new ExperimentSingleType();
        String outputFilePath = "output.csv";
        List<ResultModel> results = new ArrayList<ResultModel>();
        try {
            // double rel = 0.5;
            // double rel = 0.6;
            // double rel = 0.7;
            double rel = 0.8;
            double std = 0.01;
            boolean genarateDepend = true;
            List<ResultModel> list = a.experimentLoop(25, rel, std, genarateDepend);
            results.addAll(list);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            File file = new File(outputFilePath);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);


            String comma = ",";
            // Write result to file
            // Reliablity,Std,LoopID,Algor,#Src,Time,Prec1,Prec2,...,Precn,Ent1,Ent2,..,Entn,Ind1,Ind2,..,Indn
            for (int i = 0; i < results.size(); i++) {
                ResultModel resultModel = results.get(i);
                StringBuilder builder = new StringBuilder();
                builder.append(resultModel.getReliability()).append(comma).append(resultModel.getStd()).append(comma)
                        .append(resultModel.getID()).append(comma).append(resultModel.getAlgorithm()).append(comma)
                        .append(resultModel.getNumSrc()).append(comma).append(resultModel.getTime()).append(comma);
                builder.append(listInt2String(resultModel.getPrecisions())).append(comma);
                builder.append(listInt2String(resultModel.getEntropies())).append(comma);
                builder.append(listInt2String(resultModel.getItemIndices())).append("\n");
                bw.write(builder.toString());
            }

            bw.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    static <T> String listInt2String(List<T> list) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size() - 1; i++) {
            T t = list.get(i);
            builder.append(t.toString()).append(",");
        }
        builder.append(list.get(list.size() - 1));
        return builder.toString();
    }

    public class ResultModel {


        private String ID;
        private String algorithm;
        private String time;
        private List<Double> precisions;
        private List<Double> entropies;
        private List<Integer> itemIndices;
        private String reliability;
        private String std;
        private String numSrc;


        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public List<Double> getPrecisions() {
            return precisions;
        }

        public void setPrecisions(List<Double> precisions) {
            this.precisions = precisions;
        }

        public List<Double> getEntropies() {
            return entropies;
        }

        public void setEntropies(List<Double> entropies) {
            this.entropies = entropies;
        }

        public List<Integer> getItemIndices() {
            return itemIndices;
        }

        public void setItemIndices(List<Integer> itemIndices) {
            this.itemIndices = itemIndices;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getAlgorithm() {
            return algorithm;
        }

        public void setAlgorithm(String algorithm) {
            this.algorithm = algorithm;
        }

        public String getReliability() {
            return reliability;
        }

        public void setReliability(String reliability) {
            this.reliability = reliability;
        }

        public String getStd() {
            return std;
        }

        public void setStd(String std) {
            this.std = std;
        }

        public String getNumSrc() {
            return numSrc;
        }

        public void setNumSrc(String numSrc) {
            this.numSrc = numSrc;
        }
    }
}
