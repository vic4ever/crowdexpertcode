package feedback;

public abstract class FeedbackProcess {
	public final void feedbackProcess(){
		
		IOrderingStrategy orderingStrategy = null;
		IPropagateFeedbackStrategy propagateStrategy = null;
		IUserFeedbackStrategy userFeedbackStrategy = null;
		ITerminationStrategy terminationStrategy = null;
		
		while(!terminationStrategy.isTerminated()){
			//Select a data item
			Object selectedObject = orderingStrategy.selectObject();
			//Elicit user input
			Object feedbackedObject = userFeedbackStrategy.getUserFeedback(selectedObject);
			//Integrate the feedback
			propagateStrategy.propagateFeedback();
		}
	}
}
