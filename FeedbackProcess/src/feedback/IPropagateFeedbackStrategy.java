package feedback;

public interface IPropagateFeedbackStrategy {

	public void propagateFeedback();

}
