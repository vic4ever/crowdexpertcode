package feedback;

public interface ITerminationStrategy {
	public boolean isTerminated();
}
