package feedback;

public interface IUserFeedbackStrategy {

	public Object getUserFeedback(Object selectedObject);

}
