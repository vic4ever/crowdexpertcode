package feedback.depend;

import java.util.Map;

import utils.TupleR;
import data.model.DataSet;
import data.model.DataSource;

public abstract class DependentSrcRemover {
	
	public boolean DEBUG = false;
	
	protected double threshold = 0.9;
	
	private Map<TupleR<Integer, Integer>, Double> srcPairsAndProb;
	
	private DataSet dataSet;

	public DependentSrcRemover(){
		
	}
	
	public DependentSrcRemover(DataSet dataSet){
		this.setDataSet(dataSet);
	}
	
	public abstract void removeDependentSrc(int numIter);

	public DataSet getDataSet() {
		return dataSet;
	}

	public void setDataSet(DataSet dataSet) {
		this.dataSet = dataSet;
	}

	public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public Map<TupleR<Integer, Integer>, Double> getSrcPairsAndProb() {
		return srcPairsAndProb;
	}

	public void setSrcPairsAndProb(Map<TupleR<Integer, Integer>, Double> srcPairsAndProb) {
		this.srcPairsAndProb = srcPairsAndProb;
	}
}
