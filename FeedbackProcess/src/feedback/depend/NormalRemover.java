package feedback.depend;

import java.util.ArrayList;
import java.util.List;

import utils.TupleR;
import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSource;

public class NormalRemover extends DependentSrcRemover {

	//Source trustworthiness based
	public DataSource selectDeleteSrc(DataSource s1, DataSource s2) {
		int all1 = 0;
		int all2 = 0;
		int true1 = 0;
		int true2 = 0;
		List<Assignment> asses1 = s1.getAssignments();
		List<Assignment> feedbackedAss1 = new ArrayList<Assignment>();
		for (Assignment assignment : asses1) {
			DataItem itm = getDataSet().getDataItems().get(assignment.getDataItemIndex());
			assert itm.getDataItemIndex() == assignment.getDataItemIndex();
			if(itm.isFeedbacked()){
				if(itm.getGroundTruth() == assignment.getValue()){
					true1++;
				}
				all1++;
				feedbackedAss1.add(assignment);
			}
		}

		List<Assignment> asses2 = s2.getAssignments();
		List<Assignment> feedbackedAss2 = new ArrayList<Assignment>();
		for (Assignment assignment : asses2) {
			DataItem itm = getDataSet().getDataItems().get(assignment.getDataItemIndex());
			assert itm.getDataItemIndex() == assignment.getDataItemIndex();
			if(itm.isFeedbacked()){
				if(itm.getGroundTruth()== assignment.getValue()){
					true2++;
				}
				all2++;
				feedbackedAss2.add(assignment);
			}
		}

		//Calculate estimated trustworthiness right now ???
		double est1 = (double) ((double)true1/(double)all1);
		double est2 = (double) ((double)true2/(double)all2);
		if(est1>est2){
			return s2;
		}else{
			return s1;
		}
	}

	private boolean validate(DataSource s1, DataSource s2){
		if(s1.getOriginalSource() == -1){
			//s1 original s2 copy s1
			if(s2.getOriginalSource() == s1.getDataSourceIndex()){
				return true;
			}
		}else{
			//s1 copy from s2
			if(s1.getOriginalSource() == s2.getDataSourceIndex()){
				return true;
			}
			//s1 and s2 copy from s3
			if(s1.getOriginalSource() == s2.getOriginalSource()){
				return true;
			}
		}
		return false;
	}

	@Override
	public void removeDependentSrc(int numIter) {
		for (TupleR<Integer,Integer> tuple : getSrcPairsAndProb().keySet()) {
			if(getSrcPairsAndProb().get(tuple) < threshold){
				continue;
			}
			
			if(getDataSet().isSrcAvailable(tuple.first) && getDataSet().isSrcAvailable(tuple.second)){
				DataSource src1 = getDataSet().getSource(tuple.first);
				DataSource src2 = getDataSet().getSource(tuple.second);

//				if(DEBUG){
//					System.out.println(tuple.first +" "+ src1.getOriginalSource() + " " +tuple.second +" " + src2.getOriginalSource() +" " +getSrcPairsAndProb().get(tuple));
//				}

				try {
					//Selective source delete, based on diferrent set with already feedback ground truth
					DataSource toBeDeleteSrc = selectDeleteSrc(src1, src2);
					getDataSet().deleteSource(toBeDeleteSrc.getDataSourceIndex());
					
					if(DEBUG){
						System.out.println(validate(src1, src2)+ " " + src1.getDataSourceIndex() + " " + src2.getDataSourceIndex() + " " + toBeDeleteSrc.getDataSourceIndex() + " " +validate2(toBeDeleteSrc, src1, src2) + " " +numIter);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}		
	}
	
	
	private String validate2(DataSource toBeDeleteSrc, DataSource src1, DataSource src2){
		String deleteLowSrc = "";
		if(toBeDeleteSrc.getDataSourceIndex() == src1.getDataSourceIndex()){
			if(toBeDeleteSrc.getReliability() < src2.getReliability()){
				deleteLowSrc = "lowsrc";
			}else{
				deleteLowSrc = "highsrc";
			}
		}else{
			if(toBeDeleteSrc.getReliability() < src1.getReliability()){
				deleteLowSrc = "lowsrc";
			}else{
				deleteLowSrc = "highsrc";
			}
		}
		return deleteLowSrc;
	}
}
