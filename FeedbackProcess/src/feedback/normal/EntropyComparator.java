package feedback.normal;

import java.util.Comparator;

import data.model.DataItem;

public class EntropyComparator implements Comparator{

	@Override
	public int compare(Object arg0, Object arg1) {
		DataItem dataItem0 = (DataItem) arg0;
		DataItem dataItem1 = (DataItem) arg1;
		return (int) (dataItem0.calculateEntropy() - dataItem1.calculateEntropy());
	}

}
