package feedback.strategy;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.AssignedLabel;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;
import dependency.DetectRandomSpammer;
import feedback.strategy.selection.IGEMSelection;
import feedback.strategy.selection.SelectionAlgorithm;
import feedback.strategy.selection.SpammerEMSelection;
import org.apache.log4j.Logger;
import utils.Converter;
import utils.TupleR;

import java.util.*;

/**
 * Created by Asus on 4/28/14.
 * Ranking: Information gain and SpammerScore
 * Propagate: EM
 * Detector: for random spammers
 * Output: Precision of instantiation
 * Precision and recall of detection at each step
 */
public class EMSequence {
    public static Logger logger = Logger.getLogger(FeedbackSequence.class);

    public List<Double> precisionList = new ArrayList<Double>();
    public List<Integer> itemIndices = new ArrayList<Integer>();
    public List<Double> entropyList = new ArrayList<Double>();

    public List<Double> spamPrecisionList = new ArrayList<Double>();
    public List<Double> spamRecallList = new ArrayList<Double>();

    private static final boolean DEBUG = false;

    protected DataSet dataSet = null;

    public void setData(DataSet data) {
        this.dataSet = data;
    }

    public final void feedbackLoop() {
//		System.out.println("Initializing data");

        initData();

        double precision = dataSet.calculatePrecision();
        precisionList.add(precision);

        double entropy = dataSet.calculateEntropy();
        entropyList.add(entropy);
    }


    private void initData() {
        Engine engine = Converter.convert(dataSet);
        engine.executeWithoutContext();


        List<DataItem> dataItems = dataSet.getDataItems();
        if (DEBUG) {
            ////////////////DEBUG/////////////////////////////
            //Debug label
            for (AssignedLabel label : engine.getLabels()) {
                int dataItemInd = Integer.parseInt(label.getObjectName());
                int srcInd = Integer.parseInt(label.getWorkerName());
                int value = Integer.parseInt(label.getCategoryName());
                DataItem dataItem = dataItems.get(dataItemInd);

                boolean existLabel = false;
                for (Assignment ass : dataItem.getAssignments()) {
                    if (ass.getSourceIndex() == srcInd && ass.getValue() == value) {
                        existLabel = true;
                    }
                }

                assert existLabel == true;
            }

            dataItems = dataSet.getDataItems();

            //Debug dataItem
            Map<String, Datum> objects = engine.getDs().getObjects();
            for (String dtIndStr : objects.keySet()) {
                int dtInd = Integer.parseInt(dtIndStr);
                DataItem dtItm = dataItems.get(dtInd);
                Datum datum = objects.get(dtIndStr);
                assert datum.getAssignedLabels().size() == dtItm.getAssignments().size();
            }
            //Debug worker
            Map<String, Worker> workers = engine.getDs().getWorkers();
            Map<Integer, List<Assignment>> srcAsses = dataSet.getSourceAssignments();
            for (String srcIndStr : workers.keySet()) {
                Worker w = workers.get(srcIndStr);
                int srcInd = Integer.parseInt(srcIndStr);
                List<Assignment> assess = srcAsses.get(srcInd);
                Set<AssignedLabel> labels = w.getAssignedLabels();
                assert assess.size() == w.getAssignedLabels().size();

                for (AssignedLabel assignedLabel : labels) {
                    String dtName = assignedLabel.getObjectName();
                    int dtInd = Integer.parseInt(dtName);

                    boolean existAss = false;
                    for (Assignment ass : assess) {
                        if (ass.getDataItemIndex() == dtInd && ass.getValue() == Integer.parseInt(assignedLabel.getCategoryName())) {
                            existAss = true;
                        }
                    }
                    assert existAss == true;
                }
            }
            //////////////////////////////////////////////////
        }

        try {
            DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
            Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
            dataSet.setTrustworthiness(sourceTrust);

            Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
            for (TupleR<Integer, Integer> tuple : itemQual.keySet()) {
                DataItem item = dataSet.getDataItems().get(tuple.first);
                SortedMap<Integer, Double> valubProb = item.getValueProb();
                valubProb.put(tuple.second, itemQual.get(tuple));

            }
        } catch (Exception e) {

        }
        for (DataItem dataItem : dataSet.getDataItems()) {
            logger.info("Probability after init");
            logger.info("DataItem " + dataItem.getDataItemIndex() + " " + dataItem.getValueProb());
            logger.info("######################");
        }
    }
}
