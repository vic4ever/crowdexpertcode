package feedback.strategy;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import com.ipeirotis.gal.engine.EngineContext;
import data.model.DataItem;
import feedback.strategy.selection.EMWorkerDispersionSelection;
import feedback.strategy.selection.NoobEntropyBasedSelection;
import org.kohsuke.args4j.CmdLineParser;
import utils.Converter;
import utils.TupleR;

import java.util.Map;
import java.util.SortedMap;

public class EMWorkerFeedbackSequence extends FeedbackSequence{
	private int NUM_ITER =5;
	double threshold = 0.1;
	int numIter = 0;


    public EMWorkerFeedbackSequence(int numIter){
        this.NUM_ITER = numIter;
    }

	@Override
	public void initData(){
		EngineContext ctx = new EngineContext();
		CmdLineParser parser = new CmdLineParser(ctx);
		String labelFile = "labelFile.txt";
		String catFile = "catFile.txt";
		try{
			Converter.createGALInput(dataSet,labelFile ,catFile );
			parser.parseArgument(String.format("--input %s --categories %s",labelFile,catFile)
					.split("\\s+"));
			Engine engine = new Engine(ctx);
			engine.execute();

			DawidSkeneDecorator dsDecorator = new DawidSkeneDecorator(engine.getDs());

			Map<Integer, Double> sourceWorth = dsDecorator.
					getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
			dataSet.setTrustworthiness(sourceWorth);

			Map<TupleR<Integer, Integer>, Double> itemQual = dsDecorator.
					getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
			for (TupleR<Integer,Integer> tuple : itemQual.keySet()) {
				DataItem item = dataSet.getDataItems().get(tuple.first);
				SortedMap<Integer, Double> valueProb = item.getValueProb();
				Double qual = itemQual.get(tuple);
				valueProb.put(tuple.second, qual);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	

	@Override
	public boolean isTerminated() {
		numIter++;
		return numIter == NUM_ITER;
	}

	@Override
	public DataItem getFeedbackObject() {
		selection = new EMWorkerDispersionSelection(dataSet);
		DataItem retVal = selection.getFeedbackObject();
//		System.out.println("!!!!!!!!!!!!!!!!!!Selected item index "+retVal.getDataItemIndex());
		return retVal;
	}

	@Override
	public TupleR<Integer, Integer> getUserFeedback(DataItem item) {
		TupleR<Integer, Integer> tuple = new TupleR<Integer,Integer>(item.getDataItemIndex(),item.getGroundTruth());
		return tuple;
	}
	
	@Override
	public void propagateFeedback(TupleR<Integer, Integer> feedbackedValue) {
		propagateFeedbackNoNewSource();
	}
	
	public void propagateFeedbackNoNewSource() {
		EngineContext ctx = new EngineContext();
		CmdLineParser parser = new CmdLineParser(ctx);
		String labelFile = "labelFile.txt";
		String catFile = "catFile.txt";
		String goldFile = "goldFile.txt";
		
		try{
			Converter.createGALInput(dataSet,labelFile ,catFile );
			// Create gold label for EM to valuate worker and object
			Converter.createGALGold(dataSet, goldFile);
			parser.parseArgument(String.format("--input %s --categories %s --gold %s",labelFile,catFile,goldFile)
					.split("\\s+"));
			Engine engine = new Engine(ctx);
			engine.execute();

			DawidSkeneDecorator dsDecorator = new DawidSkeneDecorator(engine.getDs());

			Map<Integer, Double> sourceWorth = dsDecorator.
					getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
			dataSet.setTrustworthiness(sourceWorth);

			Map<TupleR<Integer, Integer>, Double> itemQual = dsDecorator.
					getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
			for (TupleR<Integer,Integer> tuple : itemQual.keySet()) {
				DataItem item = dataSet.getDataItems().get(tuple.first);
				SortedMap<Integer, Double> valueProb = item.getValueProb();
				Double qual = itemQual.get(tuple);
				valueProb.put(tuple.second, qual);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}


	@Override
	public void removeDependentSource() {
		// TODO Auto-generated method stub
		
	}

}
