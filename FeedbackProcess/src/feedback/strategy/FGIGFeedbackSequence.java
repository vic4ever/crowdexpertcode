//package feedback.strategy;
//
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.SortedMap;
//
//import org.apache.log4j.Logger;
//
//import cc.mallet.grmm.types.FactorGraph;
//
//import main.FactorGraphReasoning;
//import utils.TupleR;
//import data.model.DataItem;
//import feedback.strategy.selection.IGFGSelection;
//import feedback.strategy.selection.SelectionAlgorithm;
//
//public class FGIGFeedbackSequence extends FeedbackSequence{
//	
//	public static Logger logger =  Logger.getLogger(FGIGFeedbackSequence.class);
//	
//	private int MAX_ITER = 10;
//
//	int numIter = 0;
//	
//	public FGIGFeedbackSequence(int iter){
//		this.MAX_ITER = iter;
//	}
//
//	@Override
//	public void initData() {
//		FactorGraphReasoning reasoning = new FactorGraphReasoning(dataSet);
//		try {
//			FactorGraph fg = reasoning.generateFactorGraph();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		//Update category probability
//		Map<Integer, Map<Integer, Double>> itmCatProb = reasoning.calculateDataItemProbability();
//		Set<Integer> dtItemIndices = itmCatProb.keySet();
//
//		List<DataItem> dataItems = dataSet.getDataItems();
//		for (Integer dtInd : dtItemIndices) {
//			DataItem dataItem = dataItems.get(dtInd);
//			assert dataItem.getDataItemIndex() == dtInd;
//			Map<Integer, Double> catProb = itmCatProb.get(dtInd);
//			SortedMap<Integer, Double> valProb = dataItem.getValueProb();
//			Set<Integer> categories = catProb.keySet();
//			for (Integer category : categories) {
//				double value = catProb.get(category);
//				valProb.put(category, value);
//			}
//			dataItem.setValueProb(valProb);
//			logger.info("Probability after init");
//			logger.info("DataItem " + dtInd + " " + valProb);
//			logger.info("######################");
//		}
//		
//		//		//Update source probability
//		//		Map<Integer, Map<Integer, Double>> srcOutcomeProb = reasoning.calculateSourceProbability();
//		//		Set<Integer> srcIndices = srcOutcomeProb.keySet();
//		//		double[] values = new double[srcIndices.size()];
//		//		int count = 0;
//		//		for (Integer srcInd : srcIndices) {
//		//			Map<Integer, Double> outcomeProb = srcOutcomeProb.get(srcInd);
//		//			double value = outcomeProb.get(1);
//		//			values[count] = value;
//		//			count++;
//		//		}
//		//		dataSet.setTrustworthiness(values);
//	}
//
//
//
//	@Override
//	public boolean isTerminated() {
//		numIter++;
//		return numIter == MAX_ITER;
//	}
//
//
//	@Override
//	public DataItem getFeedbackObject() {
//		selection = new IGFGSelection(dataSet);
//		return selection.getFeedbackObject();
//	}
//
//	@Override
//	public TupleR<Integer, Integer> getUserFeedback(DataItem item) {
//		TupleR<Integer, Integer> tuple = new TupleR<Integer,Integer>(item.getDataItemIndex(),item.getGroundTruth());
//		return tuple;
//	}
//
//	@Override
//	public void propagateFeedback(TupleR<Integer, Integer> feedbackedValue) {
//		logger.info("Begin propagate feedback");
//		List<DataItem> dataItems = dataSet.getDataItems();
//		//Update value prob for the feedbacked dataitem and its category
//		DataItem feedbackedDtm = dataItems.get(feedbackedValue.first);
//		SortedMap<Integer, Double> feedbackedValProb = feedbackedDtm.getValueProb();
//		feedbackedDtm.calculateCategoryList();
//		for(Integer cat: feedbackedDtm.categoryList){
//			if(cat == feedbackedDtm.getGroundTruth()){
//				feedbackedValProb.put(cat, 1.0);
//			}else{
//				feedbackedValProb.put(cat, 0.0);
//			}
//		}
//		
//		//Calculate new probability
//		FactorGraphReasoning reasoning = new FactorGraphReasoning(dataSet);
//		try {
//			FactorGraph fg = reasoning.generateFactorGraph();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		//Update category probability
//		Map<Integer, Map<Integer, Double>> itmCatProb = reasoning.calculateDataItemProbability();
//		Set<Integer> dtItemIndices = itmCatProb.keySet();
//
//		
//		for (Integer dtInd : dtItemIndices) {
//			DataItem dataItem = dataItems.get(dtInd);
//			assert dataItem.getDataItemIndex() == dtInd;
//			Map<Integer, Double> catProb = itmCatProb.get(dtInd);
//			SortedMap<Integer, Double> valProb = dataItem.getValueProb();
//			Set<Integer> categories = catProb.keySet();
//			for (Integer category : categories) {
//				double value = catProb.get(category);
//				valProb.put(category, value);
//			}
//			dataItem.setValueProb(valProb);
//		}
//		
//		for(DataItem dataItem: dataSet.getDataItems()){
//			logger.info("DataItem " + dataItem.getDataItemIndex() + " " +dataItem.getValueProb());
//		}
//		logger.info("######################################");
//		//		//Update source probability
//				Map<Integer, Map<Integer, Double>> srcOutcomeProb = reasoning.calculateSourceProbability();
//		//		Set<Integer> srcIndices = srcOutcomeProb.keySet();
//		//		double[] values = new double[srcIndices.size()];
//		//		int count = 0;
//		//		for (Integer srcInd : srcIndices) {
//		//			Map<Integer, Double> outcomeProb = srcOutcomeProb.get(srcInd);
//		//			double value = outcomeProb.get(1);
//		//			values[count] = value;
//		//			count++;
//		//		}
//		//		dataSet.setTrustworthiness(values);
//	}
//
//	@Override
//	public void removeDependentSource() {
//		// TODO Auto-generated method stub
//		
//	}
//
//}
