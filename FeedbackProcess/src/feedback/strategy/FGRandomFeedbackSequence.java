//package feedback.strategy;
//
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.SortedMap;
//
//import cc.mallet.grmm.types.FactorGraph;
//
//import main.FactorGraphReasoning;
//import utils.TupleR;
//import data.model.DataItem;
//import feedback.strategy.selection.IGFGSelection;
//import feedback.strategy.selection.RandomSelection;
//import feedback.strategy.selection.SelectionAlgorithm;
//
//public class FGRandomFeedbackSequence extends FeedbackSequence{
//	private int MAX_ITER = 20;
//	int numIter = 0;
//	
//	public FGRandomFeedbackSequence(int iter){
//		this.MAX_ITER = iter;
//	}
//
//	@Override
//	public void initData() {
//		FactorGraphReasoning reasoning = new FactorGraphReasoning(dataSet);
//		try {
//			FactorGraph fg = reasoning.generateFactorGraph();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		//Update category probability
//		Map<Integer, Map<Integer, Double>> itmCatProb = reasoning.calculateDataItemProbability();
//		Set<Integer> dtItemIndices = itmCatProb.keySet();
//
//		List<DataItem> dataItems = dataSet.getDataItems();
//		for (Integer dtInd : dtItemIndices) {
//			DataItem dataItem = dataItems.get(dtInd);
//			assert dataItem.getDataItemIndex() == dtInd;
//			Map<Integer, Double> catProb = itmCatProb.get(dtInd);
//			SortedMap<Integer, Double> valProb = dataItem.getValueProb();
//			Set<Integer> categories = catProb.keySet();
//			for (Integer category : categories) {
//				double value = catProb.get(category);
//				valProb.put(category, value);
//			}
//			dataItem.setValueProb(valProb);
//		}
//
//
//		//		//Update source probability
//		//		Map<Integer, Map<Integer, Double>> srcOutcomeProb = reasoning.calculateSourceProbability();
//		//		Set<Integer> srcIndices = srcOutcomeProb.keySet();
//		//		double[] values = new double[srcIndices.size()];
//		//		int count = 0;
//		//		for (Integer srcInd : srcIndices) {
//		//			Map<Integer, Double> outcomeProb = srcOutcomeProb.get(srcInd);
//		//			double value = outcomeProb.get(1);
//		//			values[count] = value;
//		//			count++;
//		//		}
//		//		dataSet.setTrustworthiness(values);
//	}
//
//
//
//	@Override
//	public boolean isTerminated() {
//		numIter++;
//		return numIter == MAX_ITER;
//	}
//
//
//	@Override
//	public DataItem getFeedbackObject() {
//		selection = new RandomSelection(dataSet);
//		return selection.getFeedbackObject();
//	}
//
//	@Override
//	public TupleR<Integer, Integer> getUserFeedback(DataItem item) {
//		TupleR<Integer, Integer> tuple = new TupleR<Integer,Integer>(item.getDataItemIndex(),item.getGroundTruth());
//		return tuple;
//	}
//
//	@Override
//	public void propagateFeedback(TupleR<Integer, Integer> feedbackedValue) {
//		FactorGraphReasoning reasoning = new FactorGraphReasoning(dataSet);
//		try {
//			FactorGraph fg = reasoning.generateFactorGraph();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		//Update category probability
//		Map<Integer, Map<Integer, Double>> itmCatProb = reasoning.calculateDataItemProbability();
//		Set<Integer> dtItemIndices = itmCatProb.keySet();
//
//		List<DataItem> dataItems = dataSet.getDataItems();
//		for (Integer dtInd : dtItemIndices) {
//			DataItem dataItem = dataItems.get(dtInd);
//			assert dataItem.getDataItemIndex() == dtInd;
//			Map<Integer, Double> catProb = itmCatProb.get(dtInd);
//			SortedMap<Integer, Double> valProb = dataItem.getValueProb();
//			Set<Integer> categories = catProb.keySet();
//			for (Integer category : categories) {
//				double value = catProb.get(category);
//				valProb.put(category, value);
//			}
//			dataItem.setValueProb(valProb);
//		}
//
//		//		//Update source probability
//		//		Map<Integer, Map<Integer, Double>> srcOutcomeProb = reasoning.calculateSourceProbability();
//		//		Set<Integer> srcIndices = srcOutcomeProb.keySet();
//		//		double[] values = new double[srcIndices.size()];
//		//		int count = 0;
//		//		for (Integer srcInd : srcIndices) {
//		//			Map<Integer, Double> outcomeProb = srcOutcomeProb.get(srcInd);
//		//			double value = outcomeProb.get(1);
//		//			values[count] = value;
//		//			count++;
//		//		}
//		//		dataSet.setTrustworthiness(values);
//	}
//
//	@Override
//	public void removeDependentSource() {
//		// TODO Auto-generated method stub
//		
//	}
//
//}
