package feedback.strategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import utils.TupleR;
import data.model.DataItem;
import data.model.DataSet;
import dependency.Detector;
import feedback.depend.DependentSrcRemover;
import feedback.strategy.selection.SelectionAlgorithm;

public abstract class FeedbackSequence {
	
	public static Logger logger =  Logger.getLogger(FeedbackSequence.class);
	
	public List<Double> precisionList = new ArrayList<Double>();
	public List<Integer> itemIndices = new ArrayList<Integer>();
	public List<Double> entropyList = new ArrayList<Double>();

	private static final boolean DEBUG = false;

	public static final int NUM_SURROGATES = 1;
	
	protected DataSet dataSet = null;
	
	protected SelectionAlgorithm selection;
	
	protected DependentSrcRemover remover;
	
	public void setData(DataSet data){
		this.dataSet = data;
	}

	public final void feedbackLoop(){
//		System.out.println("Initializing data");
		
		initData();
		
		double precision = dataSet.calculatePrecision();
		precisionList.add(precision);
		
		double entropy = dataSet.calculateEntropy();
		entropyList.add(entropy);
//		
//		logger.info("After init data");
//		logger.info("Entropy = " + dataSet.calculateEntropy());
//		//logger.warn("Precision = " + precision);
//		logger.info("################");
		
//		System.out.println(dataSet.calculateEntropy()+ "\t" +dataSet.calculatePrecision());
//		System.out.println("Precision " + dataSet.calculatePrecision());
//		System.out.println(precision);
//		System.out.println("Entropy = " + dataSet.calculateEntropy());
//		System.out.println("SrcEntropy " + dataSet.calculateSourceEntropy());
		if(DEBUG){
//			dataSet.prettyPrint();
//			dataSet.printValueProb();
//			dataSet.printTrust();
//			System.out.println(dataSet.calculateEntropy() + "\t" + dataSet.calculatePrecision());
		}
		int cnt = -1;
		do{
			cnt++;
//			logger.info("***************************New loop*************************** " + cnt);
			//System.out.println("***************************New loop*************************** " + cnt);
			DataItem item = getFeedbackObject();
			if(item == null){
				break;
			}
			itemIndices.add(item.getDataItemIndex());
			
			System.out.println("!!!!!!!!!!!!!!!!!!Selected item index " + item.getDataItemIndex());
//			logger.info("!!!!!!!!!!!!!!!!!!Selected item index " + item.getDataItemIndex());
			TupleR<Integer,Integer> value = getUserFeedback(item);
			
			
			
			if(isDependentCheck()){
//				if(isDependentCheck() && cnt < 6){
//				System.out.println("Checked");
				removeDependentSource();
			}
			
			propagateFeedback(value);
			if(DEBUG){
//				dataSet.prettyPrint();
//				dataSet.printValueProb();
//				dataSet.printTrust();
//				System.out.println(dataSet.calculateEntropy() + "\t" + dataSet.calculatePrecision());
			}
			precision = dataSet.calculatePrecision();
			precisionList.add(precision);
			
			entropy = dataSet.calculateEntropy();
			entropyList.add(entropy);
			
//			System.out.println(dataSet.sourceTrustworthiness);
//			System.out.println(dataSet.calculateSrcTrust());
			
//			logger.info(dataSet.calculateEntropy());
			//logger.warn(precision);
//			System.out.println(dataSet.calculateEntropy()+ "\t" +dataSet.calculatePrecision());
			System.out.println("Precision " + dataSet.calculatePrecision());
//			System.out.println(precision);
//			System.out.println("Entropy = " + dataSet.calculateEntropy());
//			System.out.println("SrcEntropy " + dataSet.calculateSourceEntropy());
		}while(!isTerminated());
	}

	public abstract boolean isTerminated();
	public abstract void initData();
	public abstract DataItem getFeedbackObject();
	public abstract TupleR<Integer,Integer> getUserFeedback(DataItem item);
	public abstract void propagateFeedback(TupleR<Integer,Integer> feedbackedValue);
	public abstract void removeDependentSource(); 

	public boolean isDependentCheck() {
		return getRemover() != null;
	}

	public DependentSrcRemover getRemover() {
		return remover;
	}

	public void setRemover(DependentSrcRemover remover) {
		this.remover = remover;
	}
}
