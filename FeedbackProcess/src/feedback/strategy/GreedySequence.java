package feedback.strategy;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.AssignedLabel;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;
import dependency.DetectRandomSpammer;
import feedback.strategy.selection.IGEMSelection;
import feedback.strategy.selection.NoobEntropyBasedSelection;
import feedback.strategy.selection.SelectionAlgorithm;
import feedback.strategy.selection.SpammerEMSelection;
import org.apache.log4j.Logger;
import utils.Converter;
import utils.TupleR;

import java.util.*;

/**
 * Created by Asus on 4/28/14.
 * Ranking: Maximum entropy
 * Propagate: EM
 * Detector: for random spammers
 * Output: Precision of instantiation
 * Precision and recall of detection at each step
 */
public class GreedySequence {
    public static Logger logger = Logger.getLogger(FeedbackSequence.class);

    public List<Double> precisionList = new ArrayList<Double>();
    public List<Integer> itemIndices = new ArrayList<Integer>();
    public List<Double> entropyList = new ArrayList<Double>();

    public List<Double> spamPrecisionList = new ArrayList<Double>();
    public List<Double> spamRecallList = new ArrayList<Double>();

    public static final int NUM_SURROGATES = 1;

    protected DataSet dataSet = null;

    protected SelectionAlgorithm selection;

    private int NUM_ITER;

    public GreedySequence(int maxIter) {
        NUM_ITER = maxIter;
    }

    public void setData(DataSet data) {
        this.dataSet = data;
    }

    public final void feedbackLoop() {
//		System.out.println("Initializing data");

        initData();

        double precision = dataSet.calculatePrecision();
        precisionList.add(precision);

        double entropy = dataSet.calculateEntropy();
        entropyList.add(entropy);

        int cnt = -1;
        do {
            cnt++;
            //System.out.println("***************************New loop*************************** " + cnt);
            DataItem item = getFeedbackObject();
            if (item == null) {
                break;
            }
            itemIndices.add(item.getDataItemIndex());

            System.out.println("!!!!!!!!!!!!!!!!!!Selected item index " + item.getDataItemIndex());
            TupleR<Integer, Integer> value = getUserFeedback(item);

            propagateFeedback(value);
            precision = dataSet.calculatePrecision();
            precisionList.add(precision);

            entropy = dataSet.calculateEntropy();
            entropyList.add(entropy);

            System.out.println("Precision " + dataSet.calculatePrecision());
        } while (!isTerminated());
    }

    int numIter = 0;

    private boolean isTerminated() {
        numIter++;
        return numIter == NUM_ITER;
    }

    private void propagateFeedback(TupleR<Integer, Integer> value) {
        //TODO: update dataset value here
        List<DataItem> dataItems = dataSet.getDataItems();
        DataItem dataItem = dataItems.get(value.first);
        assert dataItem.getDataItemIndex() == value.first;
        SortedMap<Integer, Double> valProb = dataItem.getValueProb();
        for (Integer category : dataItem.categoryList) {
            if (category == dataItem.getGroundTruth()) {
                valProb.put(category, 1.0);
            } else {
                valProb.put(category, 0.0);
            }
        }

        //Propagate feedback
        propagateFeedbackNoNewSource();
    }

    public void propagateFeedbackNoNewSource() {
        Engine engine = Converter.convert(dataSet);
        engine.executeWithoutContext();

        try {
            DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
            Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
            dataSet.setTrustworthiness(sourceTrust);

            Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
            for (TupleR<Integer, Integer> tuple : itemQual.keySet()) {
                DataItem item = dataSet.getDataItems().get(tuple.first);
                SortedMap<Integer, Double> valueProb = item.getValueProb();
                valueProb.put(tuple.second, itemQual.get(tuple));
            }
        } catch (Exception e) {

        }
    }

    private void initData() {
        Engine engine = Converter.convert(dataSet);
        engine.executeWithoutContext();


        List<DataItem> dataItems = dataSet.getDataItems();

        try {
            DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
            Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
            dataSet.setTrustworthiness(sourceTrust);

            Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
            for (TupleR<Integer, Integer> tuple : itemQual.keySet()) {
                DataItem item = dataSet.getDataItems().get(tuple.first);
                SortedMap<Integer, Double> valubProb = item.getValueProb();
                valubProb.put(tuple.second, itemQual.get(tuple));

            }
        } catch (Exception e) {

        }
        for (DataItem dataItem : dataSet.getDataItems()) {
            logger.info("Probability after init");
            logger.info("DataItem " + dataItem.getDataItemIndex() + " " + dataItem.getValueProb());
            logger.info("######################");
        }
    }

    private TupleR<Integer, Integer> getUserFeedback(DataItem item) {
        TupleR<Integer, Integer> tuple = new TupleR<Integer, Integer>(item.getDataItemIndex(), item.getGroundTruth());
        return tuple;
    }

    private DataItem getFeedbackObject() {
        selection = new NoobEntropyBasedSelection(dataSet);
        return selection.getFeedbackObject();
    }

}
