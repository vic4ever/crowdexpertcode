package feedback.strategy;

import java.util.Set;
import java.util.SortedMap;

import utils.TupleR;
import data.model.DataItem;
import feedback.strategy.selection.NoobEntropyBasedSelection;
import feedback.strategy.selection.SelectionAlgorithm;

public class HITSFeedbackStrategy extends FeedbackSequence{

	@Override
	public void initData() {
//		HITSAlgorithm hits = new HITSAlgorithm(new HITSumDefault(), new HITSumWeightOnly(), new HITNormalizeVector(), new HITNormalizeVector());
////		System.out.println(dataSet);
//		//Hub la source
//		double[][] confValues = dataSet.generateMapTable();
//		for(double[] row:confValues){
//			for (double d : row) {
//				System.out.print(d+" ");	
//			}
//			System.out.println();
//		}
//		//Authors la ItemValue
//		double[] authors = dataSet.calculateItemValueProb();
//		for (double d : authors) {
//			System.out.print(d + " ");
//		}
//		System.out.println();
//		hits.setAuthors(authors);
//		hits.perform(confValues,100);
//		authors = hits.getAuthors();
//		for (double d : authors) {
//			System.out.print(d + " ");
//		}
//		System.out.println();
//		double[] hubs = hits.getHubs();
//		for (double d : hubs) {
//			System.out.print(d + " ");
//		}
//		//Reassign value to data
//		//For source trust
//		dataSet.setTrustworthiness(hubs);
//		//For itemvalue correctness
//		SortedMap<Integer, TupleR<Integer, Integer>> columnIndexMap = dataSet.getColumnIndexMap();
//		Set<Integer> columnIndices = columnIndexMap.keySet();
//		for (Integer integer : columnIndices) {
//			TupleR<Integer,Integer> tuple = columnIndexMap.get(integer);
//			double prob = authors[integer];
//			DataItem item = dataSet.getDataItems().get(tuple.first);
//			SortedMap<Integer, Double> valueProb = item.getValueProb();
//			valueProb.put(tuple.second, prob);
//		}
////		System.out.println(dataSet);
	}

	@Override
	public boolean isTerminated() {
		//System.out.println(dataSet.calculateEntropy());
		return dataSet.calculateEntropy() < 0.1;
	}

	@Override
	public DataItem getFeedbackObject() {
		selection = new NoobEntropyBasedSelection(dataSet);
		return selection.getFeedbackObject();
	}

	@Override
	public TupleR<Integer,Integer> getUserFeedback(DataItem item) {
		//item.setFeedbacked(true);
		TupleR<Integer, Integer> tuple = new TupleR<Integer,Integer>(item.getDataItemIndex(),item.getGroundTruth());
		return tuple;
	}

	@Override
	public void propagateFeedback(TupleR<Integer,Integer> feedbackedValue) {
		//dataSet.addUserFeedback(feedbackedValue.second, feedbackedValue.first);
		System.out.println(dataSet);
	}

	@Override
	public void removeDependentSource() {
		// TODO Auto-generated method stub
		
	}
}
