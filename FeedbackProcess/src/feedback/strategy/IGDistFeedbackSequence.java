package feedback.strategy;

import java.util.List;
import java.util.Set;
import java.util.SortedMap;

import utils.TupleR;
import data.model.Assignment;
import data.model.DataItem;
import dist.DistanceAware;
import feedback.strategy.selection.IGDistSelection;
import feedback.strategy.selection.RandomSelection;
import feedback.strategy.selection.SelectionAlgorithm;

public class IGDistFeedbackSequence extends FeedbackSequence {
	int MAX_ITER = 10;
	int numIter = 0;
	double delta = 1.0;

	public IGDistFeedbackSequence(int iter, double del) {
		this.MAX_ITER = iter;
		delta = del;
	}

	@Override
	public boolean isTerminated() {
		numIter++;
		return numIter == MAX_ITER;
	}

	@Override
	public void initData() {
		// Majority voting update probability
		List<DataItem> dataItems = dataSet.getDataItems();
		for (DataItem dataItem : dataItems) {
			SortedMap<Integer, Double> valProb = dataItem.getValueProb();
			if (dataItem.isFeedbacked()) {
				dataItem.calculateCategoryList();
				for (Integer cat : dataItem.categoryList) {
					if (cat == dataItem.getGroundTruth()) {
						valProb.put(dataItem.getGroundTruth(), 1.0);
					} else {
						valProb.put(cat, 0.0);
					}
				}
			} else {
				SortedMap<Integer, Integer> valCount = dataItem.getValueCount();
				Set<Integer> categories = valCount.keySet();
				for (Integer category : categories) {
					double prob = (double) ((double) valCount.get(category) / (double) dataItem
							.getAssignments().size());
					valProb.put(category, prob);
				}
			}
			dataItem.setValueProb(valProb);
		}
	}

	@Override
	public DataItem getFeedbackObject() {
		selection = new IGDistSelection(dataSet, delta);
		return selection.getFeedbackObject();
	}

	@Override
	public TupleR<Integer, Integer> getUserFeedback(DataItem item) {
		TupleR<Integer, Integer> tuple = new TupleR<Integer, Integer>(
				item.getDataItemIndex(), item.getGroundTruth());
		return tuple;
	}

	@Override
	public void propagateFeedback(TupleR<Integer, Integer> feedbackedValue) {
		// TODO: update dataset value here
		List<DataItem> dataItems = dataSet.getDataItems();
		DataItem dataItem = dataItems.get(feedbackedValue.first);
		assert dataItem.getDataItemIndex() == feedbackedValue.first;
		SortedMap<Integer, Double> valProb = dataItem.getValueProb();
		for (Integer category : dataItem.categoryList) {
			if (category == dataItem.getGroundTruth()) {
				valProb.put(category, 1.0);
			} else {
				valProb.put(category, 0.0);
			}
		}

		DistanceAware aware = new DistanceAware(dataSet, delta);
		aware.calculateTrustworthiness();
	}

	@Override
	public void removeDependentSource() {
	}

}
