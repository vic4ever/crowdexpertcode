package feedback.strategy;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.AssignedLabel;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;
import dependency.Detector;
import feedback.strategy.selection.IGEMSelection;
import feedback.strategy.selection.RandomSelection;
import feedback.strategy.selection.SelectionAlgorithm;
import org.apache.log4j.Logger;
import utils.Converter;
import utils.TupleR;

import java.util.*;

/**
 * Created by Asus on 4/22/14.
 * Ranking: Information gain
 * Propagate: EM
 * Detector: for dependency
 * This sequence is used to check how many feedback step is required to detect the copiers/attackers
 * Output: Precision and recall of detection at each feedback step
 */
public class IGEMCopierSequence {
    public static Logger logger = Logger.getLogger(FeedbackSequence.class);

    public List<Double> precisionList = new ArrayList<Double>();
    public List<Integer> itemIndices = new ArrayList<Integer>();
    public List<Double> entropyList = new ArrayList<Double>();

    public List<Double> spamPrecisionList = new ArrayList<Double>();
    public List<Double> spamRecallList = new ArrayList<Double>();

    private static final boolean DEBUG = false;

    public static final int NUM_SURROGATES = 1;

    protected DataSet dataSet = null;

    protected SelectionAlgorithm selection;

    private int NUM_ITER;
    private double threshold = 0.5;
    private boolean checkSpammer;
    private boolean removeSpammer;

    public IGEMCopierSequence(int maxIter, double copyThreshold, boolean checkSpammer, boolean removeSpammer) {
        NUM_ITER = maxIter;
        threshold = copyThreshold;
        this.checkSpammer = checkSpammer;
        this.removeSpammer = removeSpammer;
    }

    public void setData(DataSet data) {
        this.dataSet = data;
    }

    public final void feedbackLoop() {
//		System.out.println("Initializing data");

        initData();

        double precision = dataSet.calculatePrecision();
        precisionList.add(precision);

        double entropy = dataSet.calculateEntropy();
        entropyList.add(entropy);

        if (DEBUG) {
//			dataSet.prettyPrint();
//			dataSet.printValueProb();
//			dataSet.printTrust();
//			System.out.println(dataSet.calculateEntropy() + "\t" + dataSet.calculatePrecision());
        }
        int cnt = -1;
        do {
            cnt++;
//			logger.info("***************************New loop*************************** " + cnt);
            //System.out.println("***************************New loop*************************** " + cnt);
            DataItem item = getFeedbackObject();
            if (item == null) {
                break;
            }
            itemIndices.add(item.getDataItemIndex());

            System.out.println("!!!!!!!!!!!!!!!!!!Selected item index " + item.getDataItemIndex());
//			logger.info("!!!!!!!!!!!!!!!!!!Selected item index " + item.getDataItemIndex());
            TupleR<Integer, Integer> value = getUserFeedback(item);


            if (isCheckSpammer()) {
                Collection<DataSource> spammers = checkSpammer();
                if (isRemoveSpammer()) {
                    removeSpammer(spammers);
                }
            }

            propagateFeedback(value);
            if (DEBUG) {
//				dataSet.prettyPrint();
//				dataSet.printValueProb();
//				dataSet.printTrust();
//				System.out.println(dataSet.calculateEntropy() + "\t" + dataSet.calculatePrecision());
            }
            precision = dataSet.calculatePrecision();
            precisionList.add(precision);

            entropy = dataSet.calculateEntropy();
            entropyList.add(entropy);

            System.out.println("Precision " + dataSet.calculatePrecision());
        } while (!isTerminated());
    }

    int numIter = 0;

    private boolean isTerminated() {
        numIter++;
        return numIter == NUM_ITER;
    }

    private void removeSpammer(Collection<DataSource> spammers) {
        try {
            for (DataSource spammer : spammers) {
                dataSet.deleteSource(spammer.getDataSourceIndex());
            }
        } catch (Exception e) {
            System.err.print(e.toString());
        }

    }

    private void propagateFeedback(TupleR<Integer, Integer> value) {
        //TODO: update dataset value here
        List<DataItem> dataItems = dataSet.getDataItems();
        DataItem dataItem = dataItems.get(value.first);
        assert dataItem.getDataItemIndex() == value.first;
        SortedMap<Integer, Double> valProb = dataItem.getValueProb();
        for (Integer category : dataItem.categoryList) {
            if (category == dataItem.getGroundTruth()) {
                valProb.put(category, 1.0);
            } else {
                valProb.put(category, 0.0);
            }
        }

        //Propagate feedback
        propagateFeedbackNoNewSource();
    }

    public void propagateFeedbackNoNewSource() {
        Engine engine = Converter.convert(dataSet);
        engine.executeWithoutContext();

        try {
            DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
            Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
            dataSet.setTrustworthiness(sourceTrust);

            Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
            for (TupleR<Integer, Integer> tuple : itemQual.keySet()) {
                DataItem item = dataSet.getDataItems().get(tuple.first);
                SortedMap<Integer, Double> valueProb = item.getValueProb();
                valueProb.put(tuple.second, itemQual.get(tuple));
            }
        } catch (Exception e) {

        }
    }

    private Collection<DataSource> checkSpammer() {
        Detector detector = new Detector(dataSet);
        Map<TupleR<Integer, Integer>, Double> pairAndProb = detector.calculateDependentProbability();

        TupleR<Integer, Integer> maxTuple = new TupleR<Integer, Integer>(0, 0);
        double maxProb = -9;
        for (TupleR<Integer, Integer> tuple : pairAndProb.keySet()) {
            if (pairAndProb.get(tuple) > maxProb) {
                maxTuple.first = tuple.first;
                maxTuple.second = tuple.second;
                maxProb = pairAndProb.get(tuple);
            }
        }

        Set<DataSource> copiers = new HashSet<DataSource>();

        for (TupleR<Integer, Integer> tuple : pairAndProb.keySet()) {
            if (dataSet.isSrcAvailable(tuple.first) && dataSet.isSrcAvailable(tuple.second)) {
                DataSource src1 = dataSet.getSource(tuple.first);
                DataSource src2 = dataSet.getSource(tuple.second);
                if (DEBUG) {
                    System.out.println(tuple.first + " " + src1.getOriginalSource() + " " + tuple.second + " " + src2.getOriginalSource() + " " + pairAndProb.get(tuple));
                }
                if (pairAndProb.get(tuple) > threshold) {
                    // Src1 and src2 are attackers
                    copiers.add(src1);
                    copiers.add(src2);
                }

            }
        }

        Map<TupleR<Integer, Integer>, Double> abc = filterByThreshold(pairAndProb, threshold);

        int correct = 0;

        for (DataSource src : copiers) {
            if (src.isCopier()) {
                correct++;
            }
        }

        double p = -1.0;
        if (copiers.size() == 0) {
            p = 0;
        } else {
            p = (double) ((double) correct / (double) copiers.size());
        }
        double r = 0.0;
        if (dataSet.getNumCopiers(false) == 0) {
            r = 1.0;
        } else {
            r = (double) ((double) correct) / (double) dataSet.getNumCopiers(false);
        }

        spamPrecisionList.add(p);
        spamRecallList.add(r);
        return copiers;
    }

    private Map<TupleR<Integer, Integer>, Double> filterByThreshold(Map<TupleR<Integer, Integer>, Double> pairAndProb, double threshold) {
        Map<TupleR<Integer, Integer>, Double> retVal = new HashMap<TupleR<Integer, Integer>, Double>();
        for (TupleR<Integer, Integer> tuple : pairAndProb.keySet()) {
            if (dataSet.isSrcAvailable(tuple.first) && dataSet.isSrcAvailable(tuple.second)) {
                if (pairAndProb.get(tuple) > threshold) {
                    DataSource src1 = dataSet.getSource(tuple.first);
                    DataSource src2 = dataSet.getSource(tuple.second);
                    if (src1.getOriginalSource() == -1 && src2.getOriginalSource() == -1) {
                        retVal.put(tuple, pairAndProb.get(tuple));
                    }
                }

            }
        }
        return retVal;
    }

    private boolean isCheckSpammer() {
        return checkSpammer;
    }

    private void initData() {
        Engine engine = Converter.convert(dataSet);
        engine.executeWithoutContext();


        List<DataItem> dataItems = dataSet.getDataItems();
        if (DEBUG) {
            ////////////////DEBUG/////////////////////////////
            //Debug label
            for (AssignedLabel label : engine.getLabels()) {
                int dataItemInd = Integer.parseInt(label.getObjectName());
                int srcInd = Integer.parseInt(label.getWorkerName());
                int value = Integer.parseInt(label.getCategoryName());
                DataItem dataItem = dataItems.get(dataItemInd);

                boolean existLabel = false;
                for (Assignment ass : dataItem.getAssignments()) {
                    if (ass.getSourceIndex() == srcInd && ass.getValue() == value) {
                        existLabel = true;
                    }
                }

                assert existLabel == true;
            }

            dataItems = dataSet.getDataItems();

            //Debug dataItem
            Map<String, Datum> objects = engine.getDs().getObjects();
            for (String dtIndStr : objects.keySet()) {
                int dtInd = Integer.parseInt(dtIndStr);
                DataItem dtItm = dataItems.get(dtInd);
                Datum datum = objects.get(dtIndStr);
                assert datum.getAssignedLabels().size() == dtItm.getAssignments().size();
            }
            //Debug worker
            Map<String, Worker> workers = engine.getDs().getWorkers();
            Map<Integer, List<Assignment>> srcAsses = dataSet.getSourceAssignments();
            for (String srcIndStr : workers.keySet()) {
                Worker w = workers.get(srcIndStr);
                int srcInd = Integer.parseInt(srcIndStr);
                List<Assignment> assess = srcAsses.get(srcInd);
                Set<AssignedLabel> labels = w.getAssignedLabels();
                assert assess.size() == w.getAssignedLabels().size();

                for (AssignedLabel assignedLabel : labels) {
                    String dtName = assignedLabel.getObjectName();
                    int dtInd = Integer.parseInt(dtName);

                    boolean existAss = false;
                    for (Assignment ass : assess) {
                        if (ass.getDataItemIndex() == dtInd && ass.getValue() == Integer.parseInt(assignedLabel.getCategoryName())) {
                            existAss = true;
                        }
                    }
                    assert existAss == true;
                }
            }
            //////////////////////////////////////////////////
        }

        try {
            DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
            Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
            dataSet.setTrustworthiness(sourceTrust);

            Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
            for (TupleR<Integer, Integer> tuple : itemQual.keySet()) {
                DataItem item = dataSet.getDataItems().get(tuple.first);
                SortedMap<Integer, Double> valubProb = item.getValueProb();
                valubProb.put(tuple.second, itemQual.get(tuple));

            }
        } catch (Exception e) {

        }
        for (DataItem dataItem : dataSet.getDataItems()) {
            logger.info("Probability after init");
            logger.info("DataItem " + dataItem.getDataItemIndex() + " " + dataItem.getValueProb());
            logger.info("######################");
        }
    }

    private TupleR<Integer, Integer> getUserFeedback(DataItem item) {
        TupleR<Integer, Integer> tuple = new TupleR<Integer, Integer>(item.getDataItemIndex(), item.getGroundTruth());
        return tuple;
    }

    private DataItem getFeedbackObject() {
        selection = new IGEMSelection(dataSet);
        DataItem item = selection.getFeedbackObject();
        return item;
    }

    public boolean isRemoveSpammer() {
        return removeSpammer && numIter % 10 == 0;
    }
}
