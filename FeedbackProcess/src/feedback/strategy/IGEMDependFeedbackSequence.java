package feedback.strategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

import org.apache.log4j.Logger;

import utils.Converter;
import utils.TupleR;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.AssignedLabel;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;

import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSource;
import dependency.Detector;
import feedback.strategy.selection.SelectionAlgorithm;

public class IGEMDependFeedbackSequence extends FeedbackSequence{

	public static Logger logger =  Logger.getLogger(IGEMDependFeedbackSequence.class);

	private int NUM_ITER = 0;
	double threshold = 0.1;
	int numIter = 0;

	private Map<Integer,SelectionAlgorithm> selectionStrategy = new HashMap<Integer, SelectionAlgorithm>();

	boolean DEBUG = false;

	public IGEMDependFeedbackSequence(int numIter,  Map<Integer,SelectionAlgorithm> selectionStrategy){
		this.NUM_ITER = numIter;
		this.selectionStrategy = selectionStrategy;
	}

	@Override
	public void initData(){
		Engine engine = Converter.convert(dataSet);
		engine.executeWithoutContext();


		List<DataItem> dataItems = dataSet.getDataItems();
		if(DEBUG){
			////////////////DEBUG/////////////////////////////
			//Debug label
			for(AssignedLabel label : engine.getLabels()){
				int dataItemInd = Integer.parseInt(label.getObjectName());
				int srcInd = Integer.parseInt(label.getWorkerName());
				int value = Integer.parseInt(label.getCategoryName());
				DataItem dataItem = dataItems .get(dataItemInd);

				boolean existLabel = false;
				for(Assignment ass: dataItem.getAssignments()){
					if(ass.getSourceIndex() == srcInd && ass.getValue() == value){
						existLabel = true;
					}
				}

				assert existLabel == true;
			}

			dataItems = dataSet.getDataItems();

			//Debug dataItem
			Map<String, Datum> objects = engine.getDs().getObjects();
			for(String dtIndStr : objects.keySet()){
				int dtInd = Integer.parseInt(dtIndStr);
				DataItem dtItm = dataItems.get(dtInd);
				Datum datum = objects.get(dtIndStr);
				assert datum.getAssignedLabels().size() == dtItm.getAssignments().size();
			}
			//Debug worker
			Map<String, Worker> workers = engine.getDs().getWorkers();
			Map<Integer, List<Assignment>> srcAsses = dataSet.getSourceAssignments();
			for(String srcIndStr : workers.keySet()){
				Worker w = workers.get(srcIndStr);
				int srcInd = Integer.parseInt(srcIndStr);
				List<Assignment> assess = srcAsses.get(srcInd);
				Set<AssignedLabel> labels = w.getAssignedLabels();
				assert assess.size() == w.getAssignedLabels().size();

				for (AssignedLabel assignedLabel : labels) {
					String dtName = assignedLabel.getObjectName();
					int dtInd = Integer.parseInt(dtName);

					boolean existAss = false;
					for(Assignment ass : assess){
						if(ass.getDataItemIndex() == dtInd && ass.getValue() == Integer.parseInt(assignedLabel.getCategoryName())){
							existAss = true;
						}
					}
					assert existAss == true;
				}
			}
			//////////////////////////////////////////////////
		}

		try{
			DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
			Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
			dataSet.setTrustworthiness(sourceTrust);

			Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
			for (TupleR<Integer,Integer> tuple : itemQual.keySet()) {
				DataItem item = dataSet.getDataItems().get(tuple.first);
				SortedMap<Integer, Double> valubProb = item.getValueProb();
				valubProb.put(tuple.second, itemQual.get(tuple));

			}
		}
		catch(Exception e){

		}
		for(DataItem dataItem : dataSet.getDataItems()){
			logger.info("Probability after init");
			logger.info("DataItem " + dataItem.getDataItemIndex() + " " + dataItem.getValueProb());
			logger.info("######################");
		}
	}

	@Override
	public boolean isTerminated() {
		numIter++;
		return numIter == NUM_ITER;
	}

	@Override
	public DataItem getFeedbackObject() {
		logger.info("Try to choose which data item");
		//		selection = new IGEMDependSelection(dataSet);
		selection = getSelectionAlgorithm();
		DataItem item = selection.getFeedbackObject();
		logger.info("!!!!!!!!!!!!!!!!!!Selected item index "+item.getDataItemIndex()+ " GTruth: " + item.getGroundTruth());
		return item;
	}

	private SelectionAlgorithm getSelectionAlgorithm() {
		SelectionAlgorithm select = selectionStrategy.get(numIter);
		select.setDataSet(dataSet);
		return select;
	}

	@Override
	public TupleR<Integer, Integer> getUserFeedback(DataItem item) {
		TupleR<Integer, Integer> tuple = new TupleR<Integer,Integer>(item.getDataItemIndex(),item.getGroundTruth());
		return tuple;
	}

	@Override
	public void propagateFeedback(TupleR<Integer, Integer> feedbackedValue) {
		//TODO: update dataset value here
		List<DataItem> dataItems = dataSet.getDataItems();
		DataItem dataItem = dataItems.get(feedbackedValue.first);
		assert dataItem.getDataItemIndex() == feedbackedValue.first;
		SortedMap<Integer, Double> valProb = dataItem.getValueProb();
		for (Integer category : dataItem.categoryList) {
			if(category == dataItem.getGroundTruth()){
				valProb.put(category, 1.0);
			}else{
				valProb.put(category, 0.0);
			}
		}

		//Propagate feedback
		propagateFeedbackNoNewSource();
	}

	public void propagateFeedbackNoNewSource() {
		Engine engine = Converter.convert(dataSet);
		engine.executeWithoutContext();

		try{
			DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
			Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
			dataSet.setTrustworthiness(sourceTrust);

			Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
			for(TupleR<Integer,Integer> tuple: itemQual.keySet()){
				DataItem item = dataSet.getDataItems().get(tuple.first);
				SortedMap<Integer, Double> valueProb = item.getValueProb();
				valueProb.put(tuple.second, itemQual.get(tuple));
			}
		}catch(Exception e){

		}
	}

	@Override
	public void removeDependentSource() {
		Detector detector = new Detector(dataSet);
		Map<TupleR<Integer, Integer>, Double> pairAndProb = detector.calculateDependentProbability();
		
		remover.setDataSet(dataSet);
		remover.setSrcPairsAndProb(pairAndProb);
		remover.removeDependentSrc(this.numIter);
		

		if(DEBUG){
			System.out.println("Num srcs " + dataSet.getNumSources());
			for(DataSource s: dataSet.getDataSources(false)){
				System.out.print(" src " + s.getDataSourceIndex());
			}
			System.out.println();
			System.out.println("Num pair " + pairAndProb.size());
		}
		
		
		
	}

}
