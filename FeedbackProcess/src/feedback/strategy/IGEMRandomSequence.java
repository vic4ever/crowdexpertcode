package feedback.strategy;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.AssignedLabel;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;
import dependency.DetectRandomSpammer;
import feedback.strategy.selection.IGEMSelection;
import feedback.strategy.selection.RandomSelection;
import feedback.strategy.selection.SelectionAlgorithm;
import feedback.strategy.selection.SpammerEMSelection;
import org.apache.log4j.Logger;
import utils.Converter;
import utils.TupleR;

import java.util.*;

/**
 * Created by Asus on 4/28/14.
 * Ranking: Information gain and SpammerScore
 * Propagate: EM
 * Detector: for random spammers
 * Output: Precision of instantiation
 * Precision and recall of detection at each step
 */
public class IGEMRandomSequence {
    public static Logger logger = Logger.getLogger(FeedbackSequence.class);

    public List<Double> precisionList = new ArrayList<Double>();
    public List<Integer> itemIndices = new ArrayList<Integer>();
    public List<Double> entropyList = new ArrayList<Double>();

    public List<Double> spamPrecisionList = new ArrayList<Double>();
    public List<Double> spamRecallList = new ArrayList<Double>();

    private static final boolean DEBUG = false;

    public static final int NUM_SURROGATES = 1;

    protected DataSet dataSet = null;

    protected SelectionAlgorithm selection;

    private int NUM_ITER;
    private double detectThreshold;
    private boolean checkSpammer;
    private boolean removeSpammer;
    private double reviveThreshold;
    private int riskLevel = 0;

    public IGEMRandomSequence(int maxIter, double threshold, boolean checkSpammer, boolean removeSpammer, double reviveThreshold) {
        NUM_ITER = maxIter;
        this.detectThreshold = threshold;
        this.checkSpammer = checkSpammer;
        this.removeSpammer = removeSpammer;
        this.reviveThreshold = reviveThreshold;
    }

    public void setData(DataSet data) {
        this.dataSet = data;
    }

    public final void feedbackLoop() {
//		System.out.println("Initializing data");

        initData();

        double precision = dataSet.calculatePrecision();
        precisionList.add(precision);

        double entropy = dataSet.calculateEntropy();
        entropyList.add(entropy);

        if (DEBUG) {
//			dataSet.prettyPrint();
//			dataSet.printValueProb();
//			dataSet.printTrust();
//			System.out.println(dataSet.calculateEntropy() + "\t" + dataSet.calculatePrecision());
        }
        int cnt = -1;
        do {
            cnt++;
//			logger.info("***************************New loop*************************** " + cnt);
            //System.out.println("***************************New loop*************************** " + cnt);
            DataItem item = getFeedbackObject();
            if (item == null) {
                break;
            }
            itemIndices.add(item.getDataItemIndex());

            System.out.println("!!!!!!!!!!!!!!!!!!Selected item index " + item.getDataItemIndex());
//			logger.info("!!!!!!!!!!!!!!!!!!Selected item index " + item.getDataItemIndex());
            TupleR<Integer, Integer> value = getUserFeedback(item);


            if (isCheckSpammer()) {
                List<DataSource> spammers = checkSpammer();
                if (isRemoveSpammer()) {
                    riskLevel = 0;
                    removeSpammer(spammers);
                }
            }

            if (item.getEstimatedSelectiveValue() != value.second) {
                riskLevel++;
            } else {
                riskLevel--;
                if (riskLevel < 0) {
                    riskLevel = 0;
                }
            }

            System.out.println("Risk lvl " + riskLevel);

            propagateFeedback(value);
            if (DEBUG) {
//				dataSet.prettyPrint();
//				dataSet.printValueProb();
//				dataSet.printTrust();
//				System.out.println(dataSet.calculateEntropy() + "\t" + dataSet.calculatePrecision());
            }
            precision = dataSet.calculatePrecision();
            precisionList.add(precision);

            entropy = dataSet.calculateEntropy();
            entropyList.add(entropy);

            System.out.println("Precision " + dataSet.calculatePrecision());
        } while (!isTerminated());
    }

    int numIter = 0;

    private boolean isTerminated() {
        numIter++;
        return numIter == NUM_ITER;
    }

    private void propagateFeedback(TupleR<Integer, Integer> value) {
        //TODO: update dataset value here
        List<DataItem> dataItems = dataSet.getDataItems();
        DataItem dataItem = dataItems.get(value.first);
        assert dataItem.getDataItemIndex() == value.first;
        SortedMap<Integer, Double> valProb = dataItem.getValueProb();
        for (Integer category : dataItem.categoryList) {
            if (category == dataItem.getGroundTruth()) {
                valProb.put(category, 1.0);
            } else {
                valProb.put(category, 0.0);
            }
        }

        //Propagate feedback
        propagateFeedbackNoNewSource();
    }

    public void propagateFeedbackNoNewSource() {
        Engine engine = Converter.convert(dataSet);
        engine.executeWithoutContext();

        try {
            DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
            Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
            dataSet.setTrustworthiness(sourceTrust);

            Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
            for (TupleR<Integer, Integer> tuple : itemQual.keySet()) {
                DataItem item = dataSet.getDataItems().get(tuple.first);
                SortedMap<Integer, Double> valueProb = item.getValueProb();
                valueProb.put(tuple.second, itemQual.get(tuple));
            }
        } catch (Exception e) {

        }
    }

    private void removeSpammer(List<DataSource> spammers) {
        try {
            for (DataSource spammer : spammers) {
                dataSet.deleteSource(spammer.getDataSourceIndex());
            }
        } catch (Exception e) {
            System.err.print(e.toString());
        }

    }

    private List<DataSource> checkSpammer() {
        DetectRandomSpammer detectRandomSpammer = new DetectRandomSpammer(dataSet, detectThreshold);
        Map<DataSource, Double> src2score = detectRandomSpammer.detectRandomSpammer();

        List<DataSource> spammers = new ArrayList<DataSource>();
        for (DataSource src : src2score.keySet()) {
            double score = src2score.get(src);
            if (score < detectThreshold) {
                spammers.add(src);
            }
        }

        int correct = 0;
        for (DataSource src : spammers) {
            if (Math.abs(src.getReliability() - 0.5) <= 0.0001) {
                correct++;
            }
        }

        double p = -1;
        if (spammers.size() == 0) {
            p = 1;
        } else {
            p = (double) ((double) correct / (double) spammers.size());
        }
        double r = 0;
        if (dataSet.getNumUnreliable(0.1, false) == 0) {
            r = 1;
        } else {
            r = (double) ((double) correct) / (double) dataSet.getNumUnreliable(0.1, false);
        }
        spamPrecisionList.add(p);
        spamRecallList.add(r);

        // Revive the spammers
        for (DataSource src : src2score.keySet()) {
            double score = src2score.get(src);
            if (src.isDeleted() && score > reviveThreshold && score != 999999) {
                src.setDeleted(false);
            }
        }
        System.out.println("#Avail sources: " + dataSet.getNumSources(true));

        return spammers;
    }

    private boolean isCheckSpammer() {
        return checkSpammer;
    }

    private void initData() {
        Engine engine = Converter.convert(dataSet);
        engine.executeWithoutContext();


        List<DataItem> dataItems = dataSet.getDataItems();
        if (DEBUG) {
            ////////////////DEBUG/////////////////////////////
            //Debug label
            for (AssignedLabel label : engine.getLabels()) {
                int dataItemInd = Integer.parseInt(label.getObjectName());
                int srcInd = Integer.parseInt(label.getWorkerName());
                int value = Integer.parseInt(label.getCategoryName());
                DataItem dataItem = dataItems.get(dataItemInd);

                boolean existLabel = false;
                for (Assignment ass : dataItem.getAssignments()) {
                    if (ass.getSourceIndex() == srcInd && ass.getValue() == value) {
                        existLabel = true;
                    }
                }

                assert existLabel == true;
            }

            dataItems = dataSet.getDataItems();

            //Debug dataItem
            Map<String, Datum> objects = engine.getDs().getObjects();
            for (String dtIndStr : objects.keySet()) {
                int dtInd = Integer.parseInt(dtIndStr);
                DataItem dtItm = dataItems.get(dtInd);
                Datum datum = objects.get(dtIndStr);
                assert datum.getAssignedLabels().size() == dtItm.getAssignments().size();
            }
            //Debug worker
            Map<String, Worker> workers = engine.getDs().getWorkers();
            Map<Integer, List<Assignment>> srcAsses = dataSet.getSourceAssignments();
            for (String srcIndStr : workers.keySet()) {
                Worker w = workers.get(srcIndStr);
                int srcInd = Integer.parseInt(srcIndStr);
                List<Assignment> assess = srcAsses.get(srcInd);
                Set<AssignedLabel> labels = w.getAssignedLabels();
                assert assess.size() == w.getAssignedLabels().size();

                for (AssignedLabel assignedLabel : labels) {
                    String dtName = assignedLabel.getObjectName();
                    int dtInd = Integer.parseInt(dtName);

                    boolean existAss = false;
                    for (Assignment ass : assess) {
                        if (ass.getDataItemIndex() == dtInd && ass.getValue() == Integer.parseInt(assignedLabel.getCategoryName())) {
                            existAss = true;
                        }
                    }
                    assert existAss == true;
                }
            }
            //////////////////////////////////////////////////
        }

        try {
            DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
            Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
            dataSet.setTrustworthiness(sourceTrust);

            Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
            for (TupleR<Integer, Integer> tuple : itemQual.keySet()) {
                DataItem item = dataSet.getDataItems().get(tuple.first);
                SortedMap<Integer, Double> valubProb = item.getValueProb();
                valubProb.put(tuple.second, itemQual.get(tuple));

            }
        } catch (Exception e) {

        }
        for (DataItem dataItem : dataSet.getDataItems()) {
            logger.info("Probability after init");
            logger.info("DataItem " + dataItem.getDataItemIndex() + " " + dataItem.getValueProb());
            logger.info("######################");
        }
    }

    private TupleR<Integer, Integer> getUserFeedback(DataItem item) {
        TupleR<Integer, Integer> tuple = new TupleR<Integer, Integer>(item.getDataItemIndex(), item.getGroundTruth());
        return tuple;
    }

    private DataItem getFeedbackObject() {
        if (isRemoveSpammer()) {
            selection = new SpammerEMSelection(dataSet, detectThreshold);
            return selection.getFeedbackObject();
        } else {
            selection = new IGEMSelection(dataSet);
            return selection.getFeedbackObject();
        }

//        DataItem item = selection.getFeedbackObject();
//        return item;
    }

    public boolean isRemoveSpammer() {
//        return removeSpammer && numIter % getDetectStep() == 0;
        return removeSpammer && riskLevel != 0 && riskLevel % getDetectStep() == 0 && numIter > 5;
    }

    public int getDetectStep() {
        return detectStep;
    }

    public void setDetectStep(int detectStep) {
        this.detectStep = detectStep;
    }

    private int detectStep = 5;

    public List<Integer> correctLabels;

    public List<Double> getProbabilityCorrectness() {
        List<Double> retVal = new ArrayList<Double>();
        correctLabels = new ArrayList<Integer>();
        for (DataItem dataItem : dataSet.getDataItems()) {
            for (Integer label : dataItem.getValueProb().keySet()) {
                retVal.add(dataItem.getValueProb().get(label));
                if (dataItem.getGroundTruth() == label) {
                    correctLabels.add(1);
                } else {
                    correctLabels.add(0);
                }

            }
        }
        return retVal;
    }
}
