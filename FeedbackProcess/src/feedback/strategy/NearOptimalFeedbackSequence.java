//package feedback.strategy;
//
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.SortedMap;
//
//import main.FactorGraphReasoning;
//
//import org.apache.log4j.Logger;
//import org.kohsuke.args4j.CmdLineParser;
//
//import utils.Converter;
//import utils.TupleR;
//
//import cc.mallet.grmm.types.FactorGraph;
//
//import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
//import com.ipeirotis.gal.core.Datum;
//import com.ipeirotis.gal.core.Worker;
//import com.ipeirotis.gal.engine.Engine;
//import com.ipeirotis.gal.engine.EngineContext;
//
//import data.model.DataItem;
//import feedback.strategy.selection.IGEMSelection;
//import feedback.strategy.selection.NearOptimalSelection;
//import feedback.strategy.selection.NoobEntropyBasedSelection;
//import feedback.strategy.selection.RandomSelection;
//import feedback.strategy.selection.SelectionAlgorithm;
//
//public class NearOptimalFeedbackSequence extends FeedbackSequence{
//
//	public static Logger logger =  Logger.getLogger(NearOptimalFeedbackSequence.class);
//
//	private int NUM_ITER = 0;
//	double threshold = 0.1;
//	int numIter = 0;
//	boolean EM = true;
//
//	public NearOptimalFeedbackSequence(int numIter, boolean em){
//		this.NUM_ITER = numIter;
//		this.EM = em;
//	}
//
//	@Override
//	public void initData(){
//		Engine engine = Converter.convert(dataSet);
//		engine.executeWithoutContext();
//
//		try{
//			DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
//			Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
//			dataSet.setTrustworthiness(sourceTrust);
//
//			Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
//			for (TupleR<Integer,Integer> tuple : itemQual.keySet()) {
//				DataItem item = dataSet.getDataItems().get(tuple.first);
//				SortedMap<Integer, Double> valubProb = item.getValueProb();
//				valubProb.put(tuple.second, itemQual.get(tuple));
//
//			}
//		}
//		catch(Exception e){
//
//		}
//		for(DataItem dataItem : dataSet.getDataItems()){
//			logger.info("Probability after init");
//			logger.info("DataItem " + dataItem.getDataItemIndex() + " " + dataItem.getValueProb());
//			logger.info("######################");
//		}
//	}
//
//	@Override
//	public boolean isTerminated() {
//		numIter++;
//		return numIter == NUM_ITER;
//	}
//
//	@Override
//	public DataItem getFeedbackObject() {
//		logger.info("Try to choose which data item");
//		selection = new NearOptimalSelection(dataSet,true);
//		DataItem item = selection.getFeedbackObject();
//		logger.info("!!!!!!!!!!!!!!!!!!Selected item index "+item.getDataItemIndex()+ " GTruth: " + item.getGroundTruth());
//		return item;
//	}
//
//	@Override
//	public TupleR<Integer, Integer> getUserFeedback(DataItem item) {
//		TupleR<Integer, Integer> tuple = new TupleR<Integer,Integer>(item.getDataItemIndex(),item.getGroundTruth());
//		return tuple;
//	}
//
//	@Override
//	public void propagateFeedback(TupleR<Integer, Integer> feedbackedValue) {
//		//TODO: update dataset value here
//		List<DataItem> dataItems = dataSet.getDataItems();
//		DataItem dataItem = dataItems.get(feedbackedValue.first);
//		assert dataItem.getDataItemIndex() == feedbackedValue.first;
//		SortedMap<Integer, Double> valProb = dataItem.getValueProb();
//		for (Integer category : dataItem.categoryList) {
//			if(category == dataItem.getGroundTruth()){
//				valProb.put(category, 1.0);
//			}else{
//				valProb.put(category, 0.0);
//			}
//		}
//
//		//Propagate feedback
//		if(EM){
//			propagateFeedbackNoNewSource();
//		}else{
//			FGPropagate();
//		}
//	}
//
//	public void propagateFeedbackNoNewSource() {
//		Engine engine = Converter.convert(dataSet);
//		engine.executeWithoutContext();
//
//		try{
//			DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
//			Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
//			dataSet.setTrustworthiness(sourceTrust);
//
//			Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
//			for(TupleR<Integer,Integer> tuple: itemQual.keySet()){
//				DataItem item = dataSet.getDataItems().get(tuple.first);
//				SortedMap<Integer, Double> valueProb = item.getValueProb();
//				valueProb.put(tuple.second, itemQual.get(tuple));
//			}
//		}catch(Exception e){
//
//		}
//	}
//	
//	private void FGPropagate(){
//		FactorGraphReasoning reasoning = new FactorGraphReasoning(dataSet);
//		List<DataItem> dataItems = dataSet.getDataItems();
//		try {
//			FactorGraph fg = reasoning.generateFactorGraph();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		//Update category probability
//		Map<Integer, Map<Integer, Double>> itmCatProb = reasoning.calculateDataItemProbability();
//		Set<Integer> dtItemIndices = itmCatProb.keySet();
//
//		
//		for (Integer dtInd : dtItemIndices) {
//			DataItem dataItem = dataItems.get(dtInd);
//			assert dataItem.getDataItemIndex() == dtInd;
//			Map<Integer, Double> catProb = itmCatProb.get(dtInd);
//			SortedMap<Integer, Double> valProb = dataItem.getValueProb();
//			Set<Integer> categories = catProb.keySet();
//			for (Integer category : categories) {
//				double value = catProb.get(category);
//				valProb.put(category, value);
//			}
//			dataItem.setValueProb(valProb);
//		}
//	}
//
//	@Override
//	public void removeDependentSource() {
//		// TODO Auto-generated method stub
//		
//	}
//
//}
