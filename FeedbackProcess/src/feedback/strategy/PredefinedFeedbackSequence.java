package feedback.strategy;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import org.kohsuke.args4j.CmdLineParser;

import utils.Converter;
import utils.TupleR;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import com.ipeirotis.gal.engine.EngineContext;

import data.model.DataItem;
import feedback.strategy.selection.NoobEntropyBasedSelection;
import feedback.strategy.selection.PredefinedSequenceSelection;
import feedback.strategy.selection.SelectionAlgorithm;

public class PredefinedFeedbackSequence extends FeedbackSequence{
	
	private static final int NUM_ITER = 10;
	double threshold = 0.1;
	int numIter = 0;
	
	public List<Integer> itemIndicesOrdering;
	
	public PredefinedFeedbackSequence(List<Integer> itemIndicesOrdering){
		this.itemIndicesOrdering = itemIndicesOrdering;
	}
	
	@Override
	public void initData(){
		EngineContext ctx = new EngineContext();
		CmdLineParser parser = new CmdLineParser(ctx);
		String labelFile = "labelFile.txt";
		String catFile = "catFile.txt";
		try{
			//System.out.println("WTTTTTTTTTHhhhhhhhhhhhhhh " +dataSet.getNumSources() + " "+ dataSet.sourceTrustworthiness.size());
			Converter.createGALInput(dataSet,labelFile ,catFile );
			parser.parseArgument(String.format("--input %s --categories %s",labelFile,catFile)
					.split("\\s+"));
			Engine engine = new Engine(ctx);
			engine.execute();

			DawidSkeneDecorator dsDecorator = new DawidSkeneDecorator(engine.getDs());

			Map<Integer, Double> sourceWorth = dsDecorator.
					getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
			dataSet.setTrustworthiness(sourceWorth);

			Map<TupleR<Integer, Integer>, Double> itemQual = dsDecorator.
					getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
			for (TupleR<Integer,Integer> tuple : itemQual.keySet()) {
				DataItem item = dataSet.getDataItems().get(tuple.first);
				SortedMap<Integer, Double> valueProb = item.getValueProb();
				Double qual = itemQual.get(tuple);
				//System.out.println(qual);
				valueProb.put(tuple.second, qual);
			}
			//dataSet.prettyPrint();
			//System.out.println(dataSet);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public boolean isTerminated() {
		numIter++;
		return numIter == NUM_ITER;
	}

	@Override
	public DataItem getFeedbackObject() {
		Integer dataItemIndex = itemIndicesOrdering.get(numIter);
		selection = new PredefinedSequenceSelection(dataSet, dataItemIndex);
//		System.out.println(selection.getFeedbackObject().getDataItemIndex());
		DataItem item = selection.getFeedbackObject();
		return item;
	}

	@Override
	public TupleR<Integer, Integer> getUserFeedback(DataItem item) {
		//item.setFeedbacked(true);
		TupleR<Integer, Integer> tuple = new TupleR<Integer,Integer>(item.getDataItemIndex(),item.getGroundTruth());
		return tuple;
	}
	
	@Override
	public void propagateFeedback(TupleR<Integer, Integer> feedbackedValue) {
		propagateFeedbackNoNewSource();
	}
	
	public void propagateFeedbackNoNewSource() {
		EngineContext ctx = new EngineContext();
		CmdLineParser parser = new CmdLineParser(ctx);
		String labelFile = "labelFile.txt";
		String catFile = "catFile.txt";
		String goldFile = "goldFile.txt";
		
		try{
			//System.out.println("WTTTTTTTTTHhhhhhhhhhhhhhh " +dataSet.getNumSources() + " "+ dataSet.sourceTrustworthiness.size());
			Converter.createGALInput(dataSet,labelFile ,catFile );
			// Create gold label for EM to valuate worker and object
			Converter.createGALGold(dataSet, goldFile);
			parser.parseArgument(String.format("--input %s --categories %s --gold %s",labelFile,catFile,goldFile)
					.split("\\s+"));
			Engine engine = new Engine(ctx);
			engine.execute();

			DawidSkeneDecorator dsDecorator = new DawidSkeneDecorator(engine.getDs());

			Map<Integer, Double> sourceWorth = dsDecorator.
					getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
			dataSet.setTrustworthiness(sourceWorth);

			Map<TupleR<Integer, Integer>, Double> itemQual = dsDecorator.
					getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
			for (TupleR<Integer,Integer> tuple : itemQual.keySet()) {
				DataItem item = dataSet.getDataItems().get(tuple.first);
				SortedMap<Integer, Double> valueProb = item.getValueProb();
				Double qual = itemQual.get(tuple);
				//System.out.println(qual);
				valueProb.put(tuple.second, qual);
			}
			//dataSet.prettyPrint();
			//System.out.println(dataSet);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void removeDependentSource() {
		// TODO Auto-generated method stub
		
	}
	

	
//	public void propagateFeedbackNewSource(TupleR<Integer, Integer> feedbackedValue) {
//		//dataSet.prettyPrint();
//		//int count = 0;
//		//		System.out.println("Numass " +dataSet.getNumAssignments());
//		//		assert dataSet.getNumAssignments() == 7;
//		
//		//Update data -- Add source and assignment
//		if(dataSet.isFeedbacked()){
//			for(int i = dataSet.getNumSources() - 1; i>=dataSet.getNumSources() - dataSet.numSurrogates; i--){
//				//	count++;
//				//System.out.printf("AASource id %d item id %d value %d\n",i,feedbackedValue.first,feedbackedValue.second);
//				dataSet.addAssignment(i, feedbackedValue.first, feedbackedValue.second);
//			}
//		}else{
//			dataSet.setFeedbacked(true);
//			int numSources = dataSet.getNumSources();
//			//Generate 20 sources
//			//Assign value to 20 sources
//			for(int i=0; i<NUM_SURROGATES; i++){
//				//count++;
//				dataSet.addSource();
//				dataSet.addAssignment(numSources + i, feedbackedValue.first, feedbackedValue.second);
//				//System.out.printf("Source id %d item id %d value %d\n",numSources+ i,feedbackedValue.first,feedbackedValue.second);
//			}
//		}
//		//System.out.println("Num count " + count);
//		//dataSet.prettyPrint();
//		//Recalculate prob and trust
//		//TODO: stupid approach
//		////////////////////////////////////////////////////////////////////////////////
//		EngineContext ctx = new EngineContext();
//		CmdLineParser parser = new CmdLineParser(ctx);
//		String labelFile = "labelFile.txt";
//		String catFile = "catFile.txt";
//		try{
//			//System.out.println("WTTTTTTTTTHhhhhhhhhhhhhhh " +dataSet.getNumSources() + " "+ dataSet.sourceTrustworthiness.size());
//			Converter.createGALInput(dataSet,labelFile ,catFile );
//			parser.parseArgument(String.format("--input %s --categories %s",labelFile,catFile)
//					.split("\\s+"));
//			Engine engine = new Engine(ctx);
//			engine.execute();
//
//			DawidSkeneDecorator dsDecorator = new DawidSkeneDecorator(engine.getDs());
//
//			Map<Integer, Double> sourceWorth = dsDecorator.
//					getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
//			dataSet.setTrustworthiness(sourceWorth);
//
//			Map<TupleR<Integer, Integer>, Double> itemQual = dsDecorator.
//					getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
//			for (TupleR<Integer,Integer> tuple : itemQual.keySet()) {
//				DataItem item = dataSet.getDataItems().get(tuple.first);
//				SortedMap<Integer, Double> valueProb = item.getValueProb();
//				Double qual = itemQual.get(tuple);
//				//System.out.println(qual);
//				valueProb.put(tuple.second, qual);
//			}
//			//dataSet.prettyPrint();
//			//System.out.println(dataSet);
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//	}


	

}
