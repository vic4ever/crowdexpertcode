package feedback.strategy.selection;

import java.util.List;

import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;

public class CommonFalseBasedSelection extends SelectionAlgorithm{

	public CommonFalseBasedSelection(DataSet dataSet) {
		super(dataSet);
	}

	@Override
	public DataItem getFeedbackObject() {
		DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
		if(dataItem != null) 
			dataItem.setFeedbacked(true);
		return dataItem;
	}

	@Override
	protected DataItem getFeedbackObjectNoSetFeedbacked() {
		List<DataItem> dataItems = getDataSet().getDataItems();
		DataItem maxDataItem = null;
		double maxExpectation = Double.MIN_VALUE;
		
		for(DataItem item : dataItems){
			if(item.isFeedbacked()){
				continue;
			}
			double expectation = 0;
			for(Integer category : item.categoryList){
				//if this category is correct, how many common false values detected ?
				int countCommonFalse = 0;
				for(DataSource src1: getDataSet().getDataSources(true)){
					Assignment ass1 = hasFeedbackedOnDataItem(src1, item);
					if(ass1 == null){
						continue;
					}
					
					for(DataSource src2: getDataSet().getDataSources(true)){
						if(src1.getDataSourceIndex() != src2.getDataSourceIndex()){
							Assignment ass2 = hasFeedbackedOnDataItem(src2, item);
							if(ass2==null){
								continue;
							}
							if(ass1.getValue()== ass2.getValue() && ass1.getValue() != category){
								countCommonFalse++;
							}
						}
					}
				}
				expectation += countCommonFalse * item.getValueProb().get(category);
			}
			if(expectation > maxExpectation){
				maxExpectation = expectation;
				maxDataItem = item;
			}
		}
		
		return maxDataItem;
	}

    @Override
    public double calculateScore(DataItem input) throws Exception {
        return 0;
    }

    private Assignment hasFeedbackedOnDataItem(DataSource src, DataItem item){
		for(Assignment ass1: src.getAssignments()){
			if(ass1.getDataItemIndex()==item.getDataItemIndex()){
				return ass1;
			}
		}
		return null;
	}

}
