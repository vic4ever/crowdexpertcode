package feedback.strategy.selection;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import data.model.DataItem;
import data.model.DataSet;
import dependency.Detector;
import org.apache.log4j.Logger;
import utils.Converter;
import utils.TupleR;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * Select the next data item to feedback based on Expected copy likelihood and propagation using EM
 */
public class CopierEMSelection extends SelectionAlgorithm {

    public static Logger logger = Logger.getLogger(CopierEMSelection.class);

    public CopierEMSelection(DataSet dataSet) {
        super(dataSet);
        // TODO Auto-generated constructor stub
    }

    @Override
    public DataItem getFeedbackObject() {
        DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
        if (dataItem != null) {
            dataItem.setFeedbacked(true);
        }
        return dataItem;
    }

    @Override
    protected DataItem getFeedbackObjectNoSetFeedbacked() {
        try {
            List<DataItem> dataItems = getDataSet().getDataItems();
            //First find unfeedbacked dataItem
            DataItem maxDataItem = null;
            double maxIG = Double.MIN_VALUE;
            for (DataItem dataItem : dataItems) {
                if (!dataItem.isFeedbacked()) {
                    maxDataItem = dataItem;
                    maxIG = calculateExpectedCopyLikelihood(dataItem);
                    break;
                }
            }

            for (DataItem dataItem : dataItems) {
                if (dataItem.isFeedbacked() || dataItem == maxDataItem) {
                    continue;
                }

                double tmpCntIG = calculateExpectedCopyLikelihood(dataItem);
                if (tmpCntIG >= maxIG) {
                    maxDataItem = dataItem;
                    maxIG = tmpCntIG;
                }
            }
            //System.out.println("MaxIG " + maxIG);
            return maxDataItem;
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public double calculateScore(DataItem input) throws Exception {
        return calculateExpectedCopyLikelihood(input);
    }

    private double calculateExpectedCopyLikelihood(DataItem item) throws Exception {
        Map<Integer, Double> catEntropy = new HashMap<Integer, Double>();
        for (Integer category : item.getValueProb().keySet()) {
            double contEntropy = calculateCopyLikelihood(item, category);
            catEntropy.put(category, contEntropy);
        }

        //Calculate conditional entropy
        double conditionalEntropy = 0;
        for (Integer category : item.getValueProb().keySet()) {
            double valll = item.getValueProb().get(category) * catEntropy.get(category);
            conditionalEntropy += valll;
        }
        return conditionalEntropy;
    }


    private double calculateCopyLikelihood(DataItem item, Integer category) throws Exception {
        //Create new dataset with item[category] = groundtruth
        DataSet cloned = this.getDataSet().clone();
        DataItem clonedItm = cloned.getDataItems().get(item.getDataItemIndex());
        clonedItm.setFeedbacked(true);
        clonedItm.setGroundTruth(category);
        SortedMap<Integer, Double> clonedValProb = clonedItm.getValueProb();
        double old = clonedValProb.get(category);
        //TODO: update feedback probability here
        for (Integer cat : clonedItm.categoryList) {
            if (cat == category) {
                clonedValProb.put(cat, 1.0);
            } else {
                clonedValProb.put(cat, 0.0);
            }
        }

        Detector detector = new Detector(cloned);
        Map<TupleR<Integer, Integer>, Double> pairAndProb = detector.calculateDependentProbability();

        double retVal = 0;
        for (TupleR<Integer, Integer> obj : pairAndProb.keySet()) {
            retVal += pairAndProb.get(obj);
        }

        return retVal;
    }

}
