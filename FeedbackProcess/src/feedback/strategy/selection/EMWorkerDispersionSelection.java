package feedback.strategy.selection;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import data.model.DataItem;
import data.model.DataSet;
import utils.Converter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * Created by Asus on 4/5/14.
 */
public class EMWorkerDispersionSelection extends SelectionAlgorithm {

    public EMWorkerDispersionSelection(DataSet dataSet) {
        super(dataSet);
    }

    @Override
    public DataItem getFeedbackObject() {
        DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
        if (dataItem != null) {
            dataItem.setFeedbacked(true);
        }
        return dataItem;
    }

    @Override
    protected DataItem getFeedbackObjectNoSetFeedbacked() {
        // For each data item
        List<DataItem> dataItems = getDataSet().getDataItems();

        DataItem maxDataItem = null;
        try {
            maxDataItem = null;
            double maxDispersion = Double.MIN_VALUE;
            for (DataItem dataItem : dataItems) {
                if (!dataItem.isFeedbacked()) {
                    maxDataItem = dataItem;
                    maxDispersion = calculateDispersion(dataItem);
                    break;
                }
            }

            for (DataItem dataItem : dataItems) {
                if (dataItem.isFeedbacked() || dataItem == maxDataItem) {
                    continue;
                }

                double tmpDispersionVal = calculateDispersion(dataItem);
                if (tmpDispersionVal > maxDispersion) {
                    maxDataItem = dataItem;
                    maxDispersion = tmpDispersionVal;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return maxDataItem;
    }

    @Override
    public double calculateScore(DataItem input) throws Exception {
        return calculateDispersion(input);
    }

    private double calculateDispersion(DataItem dataItem) throws Exception {
        Map<Integer, Map<Integer, Double>> category2distribution = new HashMap<Integer, Map<Integer, Double>>();
        for (Integer category : dataItem.getValueProb().keySet()) {
            category2distribution.put(category, calculateConditional(dataItem, category));
        }

        double maxVal = Double.MIN_VALUE;
        for (Integer category : category2distribution.keySet()) {
            Double p1 = dataItem.getValueProb().get(category);
            Map<Integer,Double> a = category2distribution.get(category);
            for (Integer category2 : category2distribution.keySet()) {
                if (category2 == category) {
                    continue;
                }
                Double p2 = dataItem.getValueProb().get(category2);
                Map<Integer,Double> b = category2distribution.get(category2);
                double val = p1*p2*calculateDispersionTwoDistributions(a,b);
                if(val > maxVal){
                    maxVal = val;
                }
            }
        }

        return maxVal;
    }

    private double calculateDispersionTwoDistributions(Map<Integer, Double> a, Map<Integer, Double> b) {
        double retVal = 0;
        for (Integer workerId : a.keySet()) {
            retVal += Math.abs(a.get(workerId) - b.get(workerId));
        }
        return retVal;
    }


    private Map<Integer, Double> calculateConditional(DataItem dataItem, Integer category) throws Exception {
        // Clone the dataset
        DataSet cloned = this.getDataSet().clone();
        // Clone the dataitem
        DataItem clonedItm = cloned.getDataItems().get(dataItem.getDataItemIndex());
        clonedItm.setFeedbacked(true);
        clonedItm.setGroundTruth(category);
        // Update the probability for the labels
        SortedMap<Integer, Double> clonedValProb = clonedItm.getValueProb();
        double old = clonedValProb.get(category);
        for (Integer cat : clonedItm.categoryList) {
            if (cat == category) {
                clonedValProb.put(cat, 1.0);
            } else {
                clonedValProb.put(cat, 0.0);
            }
        }

        // Run EM with new data
        Engine engine = Converter.convert(cloned);
        engine.executeWithoutContext();

        DawidSkeneDecorator dsDecorator = new DawidSkeneDecorator(engine.getDs());
        return dsDecorator.
                getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
    }
}
