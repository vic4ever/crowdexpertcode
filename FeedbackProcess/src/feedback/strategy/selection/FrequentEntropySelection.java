package feedback.strategy.selection;

import java.util.List;
import java.util.SortedMap;

import data.model.DataItem;
import data.model.DataSet;

public class FrequentEntropySelection extends SelectionAlgorithm{

	public FrequentEntropySelection(DataSet dataSet) {
		super(dataSet);
	}

	@Override
	public DataItem getFeedbackObject() {
//		System.out.println("Frequent");
		DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
		if(dataItem != null) 
			dataItem.setFeedbacked(true);
		return dataItem;
	}

	@Override
	protected DataItem getFeedbackObjectNoSetFeedbacked() {
		List<DataItem> dataItems = getDataSet().getDataItems();
		//First find unfeedbacked dataItem
		DataItem maxDataItem = null;
		double maxEntropy = Double.MIN_VALUE;
		for (DataItem dataItem : dataItems) {
			if(!dataItem.isFeedbacked()){
				maxDataItem = dataItem;
				maxEntropy = calculateFrequentEntropy(dataItem);
				break;
			}
		}
		
		for (DataItem dataItem : dataItems) {
			if(dataItem.isFeedbacked()){
				continue;
			}
			double tempEntropy = calculateFrequentEntropy(dataItem);
//			System.out.println(String.format("Item#%d H=%f", dataItem.getDataItemIndex(),tempEntropy));
			if(tempEntropy > maxEntropy){
				maxEntropy = tempEntropy;
				maxDataItem = dataItem;
			}else{
				//Favor data item with more assignment
				if(tempEntropy==maxEntropy && dataItem.getAssignments().size() > maxDataItem.getAssignments().size()){
					maxDataItem = dataItem;
				}
			}
		}
		return maxDataItem;
	}


    @Override
    public double calculateScore(DataItem input) throws Exception {
        return calculateFrequentEntropy(input);
    }

    private double calculateFrequentEntropy(DataItem itm){
		SortedMap<Integer, Integer> counts = itm.getValueCount();
		double sum = 0;
		for(Integer cat : counts.keySet()){
			double prob = (double) ((double) counts.get(cat) / (double) itm.getAssignments().size());
			sum += -prob*Math.log(prob);
		}
		return sum;
	}

}
