//package feedback.strategy.selection;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.SortedMap;
//
//import main.FakeFactorGraphReasoning;
//
//import org.apache.log4j.Logger;
//
//import data.model.DataItem;
//import data.model.DataSet;
//
//public class IGFakeFGSelection extends SelectionAlgorithm{
//	
//	public static Logger logger =  Logger.getLogger(IGFakeFGSelection.class);
//
//	public IGFakeFGSelection(DataSet dataSet) {
//		super(dataSet);
//		// TODO Auto-generated constructor stub
//	}
//
//	@Override
//	public DataItem getFeedbackObject() {
//		DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
//		if(dataItem != null){
//			dataItem.setFeedbacked(true);
//		}
//		return dataItem;
//	}
//
//	@Override
//	protected DataItem getFeedbackObjectNoSetFeedbacked() {
//		try{
//			logger.info("For each data item,");
//			List<DataItem> dataItems = getDataSet().getDataItems();
//			//First find unfeedbacked dataItem
//			DataItem maxDataItem = null;
//			double maxIG = Double.MIN_VALUE;
//			for (DataItem dataItem : dataItems) {
//				if(!dataItem.isFeedbacked()){
//					maxDataItem = dataItem;
//					maxIG = calculateInformationGain(dataItem);
//					break;
//				}
//			}
//			
//			logger.info("###Calculating dataset entropy");
//			logger.info("#######################");
//			for(DataItem item : this.getDataSet().getDataItems()){
//				logger.info(item.getDataItemIndex() + " ");
//				logger.info(item.getValueProb());
//			}
//			logger.info("Dataset entropy:" + this.getDataSet().calculateEntropy());
//			logger.info("#######################");
//			logger.info("Information gain " + maxIG);
//			
//			for (DataItem dataItem : dataItems) {
//				logger.info("%%%%Calculate item#" + dataItem.getDataItemIndex() +" information gain");
//				
//				if(dataItem.isFeedbacked()||dataItem==maxDataItem){
//					continue;
//				}
//
//				double tmpCntIG = calculateInformationGain(dataItem);
//				logger.info("%%%Item and IG " + dataItem.getDataItemIndex() + " " +tmpCntIG);
//				if(tmpCntIG >= maxIG){
//					maxDataItem = dataItem;
//					maxIG = tmpCntIG;
//				}
//			}
//			System.out.println("MaxIG " + maxIG);
//			return maxDataItem;
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	private double calculateInformationGain(DataItem item) throws Exception{
//		Map<Integer,Double> catEntropy = new HashMap<Integer,Double>();
//		item.calculateCategoryList();
//		for(Integer category : item.categoryList){
//			logger.info("%%%%%%Conditional entropy for item#" + item.getDataItemIndex() +" category " + category);
//			double contEntropy = calculateConditionalEntropy(item, category);
//			catEntropy.put(category, contEntropy);
//		}
//
//		//Calculate conditional entropy
//		double totalConditionalEntropy = 0;
//		for(Integer category : item.categoryList){
//			double product = item.getValueProb().get(category)*catEntropy.get(category);
//			totalConditionalEntropy+= product;
//			logger.info("Item"+item.getDataItemIndex() + " "+item.getValueProb().get(category)+ " " + catEntropy.get(category) + " "+ product);
//		}
////		logger.info("Item H(|) "+item.getDataItemIndex() + " " + conditionalEntropy);
//		return calculateDataSetEntropy() - totalConditionalEntropy;
//	}
//
//	private double calculateDataSetEntropy(){
//		logger.info("Dataset entropy");
//		double entropy = this.getDataSet().calculateEntropy();
//		logger.info(entropy);
//		logger.info("###############");
//		return entropy;
//		
//	}
//
//	private double calculateConditionalEntropy(DataItem item, Integer category) throws Exception{
//		//Create new dataset with item[category] = groundtruth
//		DataSet cloned = this.getDataSet().clone();
//		DataItem clonedItm = cloned.getDataItems().get(item.getDataItemIndex());
//		clonedItm.setFeedbacked(true);
//		clonedItm.setGroundTruth(category);
//		SortedMap<Integer, Double> clonedValProb = clonedItm.getValueProb();
//		double old = clonedValProb.get(category);
//		//TODO: set this dataItem category ground truth = 1 ?
//		clonedItm.calculateCategoryList();
//		for(Integer cat: clonedItm.categoryList){
//			if(cat == category){
//				clonedValProb.put(cat, 1.0);
//			}else{
//				clonedValProb.put(cat, 0.0);
//			}
//		}
//		logger.info("Old"+ clonedItm.getDataItemIndex() + " " + category +" " + old);
//		
//		// Run FG with new data
//		
//		////////////////////////DEBUG/////////////////////////////////////		
//		logger.info("%%%%%%%%%%%%%%%%%%%%%%");
//		logger.info("If T" + clonedItm.getDataItemIndex() +"=" + category);
//		//////////////////////////////////////////////////////////////////
//		
//		FakeFactorGraphReasoning fgReason = new FakeFactorGraphReasoning(cloned);
//		fgReason.generateFactorGraph();
//		
//		Map<Integer, Map<Integer, Double>> dtProb = fgReason.calculateDataItemProbability();	
//		List<DataItem> dataItems = cloned.getDataItems();
//		Set<Integer> dtIndices = dtProb.keySet();
//		for (Integer dtIndex : dtIndices) {
////			assert dtIndex != clonedItm.getDataItemIndex();
//			Map<Integer, Double> catProbMap = dtProb.get(dtIndex);
//			DataItem dtItem = dataItems.get(dtIndex);
//			assert dtItem.getDataItemIndex() == dtIndex;
//			SortedMap<Integer, Double> catProb = dtItem.getValueProb();
//			Set<Integer> categories = catProbMap.keySet();
//			for (Integer cat : categories) {
//				double value = catProbMap.get(cat);
//				catProb.put(cat, value);
//			}
//			dtItem.setValueProb(catProb);
//		}
//		
//		////////////////////////DEBUG/////////////////////////////////////
//		for(DataItem im : cloned.getDataItems()){
//			logger.info(im.getDataItemIndex()+" "+im.isFeedbacked() + " " + im.getGroundTruth());
//			logger.info(im.getValueProb());
//		}
//		logger.info("%%%%%%%%%%%%%%%%%%%%%%");
//		//////////////////////////////////////////////////////////////////
//		
//		
//		double retVal = cloned.calculateEntropy();
//		logger.info("%%%%%%Condtional entropy item" + clonedItm.getDataItemIndex() + " cat " + category + "=" + retVal);
//		return retVal;
//	}
//
//}
