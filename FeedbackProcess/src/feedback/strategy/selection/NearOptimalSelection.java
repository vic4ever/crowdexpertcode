//package feedback.strategy.selection;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.SortedMap;
//
//import utils.Converter;
//import utils.TupleR;
//
//import cc.mallet.fst.confidence.NBestViterbiConfidenceEstimator;
//
//import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
//import com.ipeirotis.gal.core.Datum;
//import com.ipeirotis.gal.core.Worker;
//import com.ipeirotis.gal.engine.Engine;
//
//import main.FactorGraphReasoning;
//
//import data.model.DataItem;
//import data.model.DataSet;
//
//public class NearOptimalSelection extends SelectionAlgorithm{
//	boolean EM = true; 
//
//	public NearOptimalSelection(DataSet dataSet, boolean em) {
//		super(dataSet);
//		this.EM = em;
//	}
//
//	@Override
//	public DataItem getFeedbackObject() {
//		DataItem itm = this.getFeedbackObjectNoSetFeedbacked();
//		if(itm != null){
//			itm.setFeedbacked(true);
//		}
//		return itm;
//	}
//
//	@Override
//	protected DataItem getFeedbackObjectNoSetFeedbacked() {
//		List<DataItem> dataItems = getDataSet().getDataItems();
//		List<DataItem> nofbDtItms = new ArrayList<DataItem>();
//		for (DataItem dataItem : dataItems) {
//			if(!dataItem.isFeedbacked()){
//				nofbDtItms.add(dataItem);
//			}
//		}
//		
//		DataItem bestDtm = null;
//		double bestPrecision = Double.MIN_VALUE;
//		
//		for(DataItem dataItem: nofbDtItms){
//			for(Integer category : dataItem.categoryList){
//				//Since user only feedback on ground truth, no need to find others
//				if(category != dataItem.getGroundTruth()){
//					continue;
//				}
//				
//				//Generate new dataset for feedback calculate
//				DataSet cloned = this.getDataSet().clone();
//				DataItem clonedDtm = cloned.getDataItems().get(dataItem.getDataItemIndex());
//				clonedDtm.setGroundTruth(category);
//				clonedDtm.setFeedbacked(true);
//				//Update prob
//				for(Integer cat: clonedDtm.categoryList){
//					if(cat == category){
//						clonedDtm.getValueProb().put(cat, 1.0);
//					}else{
//						clonedDtm.getValueProb().put(cat, 0.0);
//					}
//				}
//				//Update method here
//				try {
//					if(EM){
//						EMUpdateMethod(cloned);
//					}else{
//						FGUpdateMethod(cloned);
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				
//				//Calculate the precision
//				double curPrecision = cloned.calculatePrecision();
//				if(bestDtm == null){
//					bestDtm = dataItem;
//					bestPrecision = curPrecision;
//				}else{
//					if(curPrecision > bestPrecision){
//						bestDtm = dataItem;
//						bestPrecision = curPrecision;
//					}
//				}
//			}
//		}
//		
//		return bestDtm;
//	}
//	
//	private void FGUpdateMethod(DataSet cloned) throws Exception{
//		FactorGraphReasoning fgReason = new FactorGraphReasoning(cloned);
//		fgReason.generateFactorGraph();
//		
//		Map<Integer, Map<Integer, Double>> dtProb = fgReason.calculateDataItemProbability();	
//		List<DataItem> dataItems = cloned.getDataItems();
//		Set<Integer> dtIndices = dtProb.keySet();
//		for (Integer dtIndex : dtIndices) {
//			Map<Integer, Double> catProbMap = dtProb.get(dtIndex);
//			DataItem dtItem = dataItems.get(dtIndex);
//			assert dtItem.getDataItemIndex() == dtIndex;
//			SortedMap<Integer, Double> catProb = dtItem.getValueProb();
//			Set<Integer> categories = catProbMap.keySet();
//			for (Integer cat : categories) {
//				double value = catProbMap.get(cat);
//				catProb.put(cat, value);
//			}
//			dtItem.setValueProb(catProb);
//		}
//	}
//	
//	private void EMUpdateMethod(DataSet cloned) throws Exception{
//		Engine engine =  Converter.convert(cloned);
//		engine.executeWithoutContext();
//
//		DawidSkeneDecorator dsDecorator = new DawidSkeneDecorator(engine.getDs());
//		Map<Integer, Double> sourceWorth = dsDecorator.
//				getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
//		cloned.setTrustworthiness(sourceWorth);
//
//		Map<TupleR<Integer, Integer>, Double> itemQual = dsDecorator.
//				getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
//
//		for (TupleR<Integer, Integer> tuple : itemQual.keySet()) {
//			DataItem itm = cloned.getDataItems().get(tuple.first);
//			SortedMap<Integer, Double> valueProb = itm.getValueProb();
//			Double qual = itemQual.get(tuple);
//			valueProb.put(tuple.second, qual);
//		}
//	}
//
//}
