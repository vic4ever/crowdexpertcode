package feedback.strategy.selection;

import java.util.List;
import java.util.concurrent.ExecutionException;

import data.model.DataItem;
import data.model.DataSet;

public class NoobEntropyBasedSelection extends SelectionAlgorithm {

    public NoobEntropyBasedSelection(DataSet dataSet) {
        super(dataSet);
    }

    @Override
    public DataItem getFeedbackObject() {
        System.out.println("Noob");
        DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
        if (dataItem != null)
            dataItem.setFeedbacked(true);
        return dataItem;
    }

    @Override
    public double calculateScore(DataItem input) throws Exception {
        return input.calculateEntropy();
    }

    @Override
    protected DataItem getFeedbackObjectNoSetFeedbacked() {
        try {
            List<DataItem> dataItems = getDataSet().getDataItems();
            //First find unfeedbacked dataItem
            DataItem maxDataItem = null;
            double maxEntropy = Double.MIN_VALUE;
            for (DataItem dataItem : dataItems) {
                if (!dataItem.isFeedbacked()) {
                    maxDataItem = dataItem;
                    maxEntropy = dataItem.calculateEntropy();
                    break;
                }
            }

            List<DataItem> nonFeedbacked = getDataItemsNonFeedBacked(maxDataItem);
            List<Output> outputs = null;
            outputs = processInputs(nonFeedbacked);

            for (Output output : outputs) {
                if (output.score > maxEntropy) {
                    maxEntropy = output.score;
                    maxDataItem = output.dataItem;
                }
            }

            return maxDataItem;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

}
