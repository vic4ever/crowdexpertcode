package feedback.strategy.selection;

import java.util.List;

import data.model.DataItem;
import data.model.DataSet;

public class PredefinedSequenceSelection extends SelectionAlgorithm{
	
	private int dataItemIndex;

	public PredefinedSequenceSelection(DataSet dataSet,int dataItemIndex) {
		super(dataSet);
		this.dataItemIndex = dataItemIndex;
	}

	@Override
	public DataItem getFeedbackObject() {
		DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
		dataItem.setFeedbacked(true);
		return dataItem;
	}

	@Override
	protected DataItem getFeedbackObjectNoSetFeedbacked() {
		DataItem dataItem = getDataSet().getDataItems().get(dataItemIndex);
		return dataItem;
	}

    @Override
    public double calculateScore(DataItem input) throws Exception {
        return 0;
    }

}
