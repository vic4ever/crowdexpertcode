package feedback.strategy.selection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import data.model.DataItem;
import data.model.DataSet;

public class RandomSelection extends SelectionAlgorithm {

	public RandomSelection(DataSet dataSet) {
		super(dataSet);
	}

	@Override
	public DataItem getFeedbackObject() {
		DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
		dataItem.setFeedbacked(true);
		return dataItem;
	}

    @Override
    public double calculateScore(DataItem input) throws Exception {
        return 0;
    }

    @Override
	protected DataItem getFeedbackObjectNoSetFeedbacked() {
		//Create not feedback dataset
		List<Integer> unfeedbackDataItems = new ArrayList<Integer>();
		List<DataItem> dataItems = getDataSet().getDataItems();
		for(DataItem item: dataItems){
			if(!item.isFeedbacked()){
				unfeedbackDataItems.add(item.getDataItemIndex());
			}
		}
		Collections.shuffle(unfeedbackDataItems, new Random());
		DataItem retVal = dataItems.get(unfeedbackDataItems.get(0));
		return retVal;
	}

}
