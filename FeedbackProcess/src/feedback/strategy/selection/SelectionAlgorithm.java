package feedback.strategy.selection;

import data.model.DataItem;
import data.model.DataSet;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public abstract class SelectionAlgorithm {

    public enum ALGORITHM_LIST {IGEMSelection, IGEMDependSelection, NoobEntropyBasedSelection}

    ;

    private DataSet dataSet;

    public SelectionAlgorithm() {

    }

    public SelectionAlgorithm(DataSet dataSet) {
        this.setDataSet(dataSet);
    }

    public abstract DataItem getFeedbackObject();

    protected abstract DataItem getFeedbackObjectNoSetFeedbacked();

    public boolean isDependCheck() {
        return dependCheck;
    }

    public void setDependCheck(boolean dependCheck) {
        this.dependCheck = dependCheck;
    }

    public DataSet getDataSet() {
        return dataSet;
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    private boolean dependCheck = false;

    public List<DataItem> getDataItemsNonFeedBacked(DataItem maxDataItem) {
        List<DataItem> retVal = new ArrayList<DataItem>();
        for (DataItem dataItem : getDataSet().getDataItems()) {
            if (dataItem.isFeedbacked() || dataItem == maxDataItem) {
                continue;
            }
            retVal.add(dataItem);
        }
        return retVal;
    }

    public abstract double calculateScore(DataItem input) throws Exception;

    public List<Output> processInputs(List<DataItem> inputs)
            throws InterruptedException, ExecutionException {

        int threads = Runtime.getRuntime().availableProcessors();
        ExecutorService service = Executors.newFixedThreadPool(threads);

        List<Future<Output>> futures = new ArrayList<Future<Output>>();
        for (final DataItem input : inputs) {
            Callable<Output> callable = new Callable<Output>() {
                public Output call() throws Exception {
                    double tmpCntIG = calculateScore(input);
                    Output output = new Output();
                    output.score = tmpCntIG;
                    output.dataItem = input;
                    // process your input here and compute the output
                    return output;
                }
            };
            futures.add(service.submit(callable));
        }

        service.shutdown();

        List<Output> outputs = new ArrayList<Output>();
        for (Future<Output> future : futures) {
            outputs.add(future.get());
        }
        return outputs;
    }

    public class Output {
        public double score;
        public DataItem dataItem;
    }
}
