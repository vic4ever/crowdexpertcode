package feedback.strategy.selection;

import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;
import dependency.DetectRandomSpammer;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * Select the next data item to feedback based on Expected Spammer Score and propagation using EM
 */
public class SpammerEMSelection extends SelectionAlgorithm {

    public static Logger logger = Logger.getLogger(SpammerEMSelection.class);
    private double detectThreshold;

    public SpammerEMSelection(DataSet dataSet, double detectThreshold) {
        super(dataSet);
        // TODO Auto-generated constructor stub
        this.detectThreshold = detectThreshold;
    }

    @Override
    public DataItem getFeedbackObject() {
        DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
        if (dataItem != null) {
            dataItem.setFeedbacked(true);
        }
        return dataItem;
    }

    @Override
    protected DataItem getFeedbackObjectNoSetFeedbacked() {
        try {
            List<DataItem> dataItems = getDataSet().getDataItems();
            //First find unfeedbacked dataItem
            DataItem minDataItem = null;
            double minIG = Double.MAX_VALUE;
            for (DataItem dataItem : dataItems) {
                if (!dataItem.isFeedbacked()) {
                    minDataItem = dataItem;
                    minIG = calculateExpectedSpammerScore(dataItem);
                    break;
                }
            }


            List<DataItem> nonFeedbacked = getDataItemsNonFeedBacked(minDataItem);
            List<Output> spammerScores = processInputs(nonFeedbacked);

            for(Output spammerScore: spammerScores){
                double tmpCntIG = spammerScore.score;
                if (tmpCntIG <= minIG) {
                    minDataItem = spammerScore.dataItem;
                    minIG = tmpCntIG;
                }
            }

            return minDataItem;
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public double calculateScore(DataItem input) throws Exception {
        return calculateExpectedSpammerScore(input);
    }

    private double calculateExpectedSpammerScore(DataItem item) throws Exception {
        Map<Integer, Double> catEntropy = new HashMap<Integer, Double>();
        for (Integer category : item.getValueProb().keySet()) {
            double contEntropy = calculateConditionalSpammerScore(item, category);
            catEntropy.put(category, contEntropy);
        }

        //Calculate conditional entropy
        double conditionalEntropy = 0;
        for (Integer category : item.getValueProb().keySet()) {
            double valll = item.getValueProb().get(category) * catEntropy.get(category);
            conditionalEntropy += valll;
        }
        return conditionalEntropy;
    }

    private double calculateConditionalSpammerScore(DataItem item, Integer category) throws Exception {
        //Create new dataset with item[category] = groundtruth
        DataSet cloned = this.getDataSet().clone();
        DataItem clonedItm = cloned.getDataItems().get(item.getDataItemIndex());
        clonedItm.setFeedbacked(true);
        clonedItm.setGroundTruth(category);
        SortedMap<Integer, Double> clonedValProb = clonedItm.getValueProb();
        double old = clonedValProb.get(category);
        //TODO: update feedback probability here
        for (Integer cat : clonedItm.categoryList) {
            if (cat == category) {
                clonedValProb.put(cat, 1.0);
            } else {
                clonedValProb.put(cat, 0.0);
            }
        }

        // Calculate spammer score
        DetectRandomSpammer detectRandomSpammer = new DetectRandomSpammer(cloned, detectThreshold);
        Map<DataSource, Double> src2score = detectRandomSpammer.detectRandomSpammer();

        double retVal = 0;
        for (DataSource src : src2score.keySet()) {
            Double val = src2score.get(src);
            retVal += val;
        }

        return retVal;
    }

    public void setDetectThreshold(double detectThreshold) {
        this.detectThreshold = detectThreshold;
    }
}
