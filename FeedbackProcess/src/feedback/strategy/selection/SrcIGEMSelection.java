package feedback.strategy.selection;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import data.model.DataItem;
import data.model.DataSet;
import org.apache.log4j.Logger;
import utils.Converter;
import utils.TupleR;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

public class SrcIGEMSelection extends SelectionAlgorithm{

	public static Logger logger =  Logger.getLogger(SrcIGEMSelection.class);

	public SrcIGEMSelection(DataSet dataSet) {
		super(dataSet);
		// TODO Auto-generated constructor stub
	}

	@Override
	public DataItem getFeedbackObject() {
		DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
		if(dataItem != null){
			dataItem.setFeedbacked(true);
		}
		return dataItem;
	}

	@Override
	protected DataItem getFeedbackObjectNoSetFeedbacked() {
		try{
			logger.info("For each data item,");
			List<DataItem> dataItems = getDataSet().getDataItems();
			//First find unfeedbacked dataItem
			DataItem maxDataItem = null;
			double maxIG = Double.MIN_VALUE;
			for (DataItem dataItem : dataItems) {
				if(!dataItem.isFeedbacked()){
					maxDataItem = dataItem;
					maxIG = calculateInformationGain(dataItem);
					break;
				}
			}

			logger.info("###Calculating dataset entropy");
			logger.info("#######################");
			for(DataItem item : this.getDataSet().getDataItems()){
				logger.info(item.getDataItemIndex() + " ");
				logger.info(item.getValueProb());
			}
			logger.info("Dataset entropy:" + this.getDataSet().calculateEntropy());
			logger.info("#######################");
			logger.info("Information gain " + maxIG);

			for (DataItem dataItem : dataItems) {
				logger.info("%%%%Calculate item#" + dataItem.getDataItemIndex() +" information gain");
				if(dataItem.isFeedbacked()||dataItem==maxDataItem){
					continue;
				}

				double tmpCntIG = calculateInformationGain(dataItem);
				logger.info("%%%Item and IG " + dataItem.getDataItemIndex() + " " +tmpCntIG);
				if(tmpCntIG >= maxIG){
					maxDataItem = dataItem;
					maxIG = tmpCntIG;
				}
			}
			System.out.println("MaxIG " + maxIG);
			return maxDataItem;
		}
		catch(Exception e){
		}
		return null;
	}

    @Override
    public double calculateScore(DataItem input) throws Exception {
        return calculateInformationGain(input);
    }

    private double calculateInformationGain(DataItem item) throws Exception{
		Map<Integer,Double> catEntropy = new HashMap<Integer,Double>();
		for(Integer category : item.getValueProb().keySet()){
			double contEntropy = calculateConditionalEntropy(item, category);
			catEntropy.put(category, contEntropy);
		}

		//Calculate conditional entropy
		double conditionalEntropy = 0;
		for(Integer category : item.getValueProb().keySet()){
			double valll = item.getValueProb().get(category)*catEntropy.get(category);
			conditionalEntropy+= valll;
		}
		return getDataSet().calculateSourceEntropy() - conditionalEntropy;
	}

	private double calculateConditionalEntropy(DataItem item, Integer category) throws Exception{
		//Create new dataset with item[category] = groundtruth
		DataSet cloned = this.getDataSet().clone();
		DataItem clonedItm = cloned.getDataItems().get(item.getDataItemIndex());
		clonedItm.setFeedbacked(true);
		clonedItm.setGroundTruth(category);
		SortedMap<Integer, Double> clonedValProb = clonedItm.getValueProb();
		double old = clonedValProb.get(category);
		logger.info("Old"+ clonedItm.getDataItemIndex() + " " + category +" " + old);
		//TODO: update feedback probability here
		for(Integer cat: clonedItm.categoryList){
			if(cat == category){
				clonedValProb.put(cat, 1.0);
			}else{
				clonedValProb.put(cat, 0.0);
			}
		}

		// Run EM with new data
		Engine engine =  Converter.convert(cloned);
		engine.executeWithoutContext();

		DawidSkeneDecorator dsDecorator = new DawidSkeneDecorator(engine.getDs());
		Map<Integer, Double> sourceWorth = dsDecorator.
				getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
		cloned.setTrustworthiness(sourceWorth);

		Map<TupleR<Integer, Integer>, Double> itemQual = dsDecorator.
				getDataItemQuality(Datum.ClassificationMethod.DS_Soft);

		for (TupleR<Integer, Integer> tuple : itemQual.keySet()) {
			DataItem itm = cloned.getDataItems().get(tuple.first);
			SortedMap<Integer, Double> valueProb = itm.getValueProb();
			Double qual = itemQual.get(tuple);
			valueProb.put(tuple.second, qual);
		}
		
		double retVal = cloned.calculateSourceEntropy();

		return retVal;
	}

}
