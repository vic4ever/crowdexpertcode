package feedback.strategy.selection;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import data.model.DataItem;
import data.model.DataSet;
import org.apache.log4j.Logger;
import utils.Converter;
import utils.TupleR;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

public class WorkerIGEMSelection extends SelectionAlgorithm{

	public static Logger logger =  Logger.getLogger(WorkerIGEMSelection.class);

	public WorkerIGEMSelection(DataSet dataSet) {
		super(dataSet);
		// TODO Auto-generated constructor stub
	}



    @Override
	public DataItem getFeedbackObject() {
		DataItem dataItem = getFeedbackObjectNoSetFeedbacked();
		if(dataItem != null){
			dataItem.setFeedbacked(true);
		}
		return dataItem;
	}

	@Override
	protected DataItem getFeedbackObjectNoSetFeedbacked() {
		try{
			logger.info("For each data item,");
			List<DataItem> dataItems = getDataSet().getDataItems();
			//First find unfeedbacked dataItem
			DataItem maxDataItem = null;
			double maxIG = Double.MIN_VALUE;
			for (DataItem dataItem : dataItems) {
				if(!dataItem.isFeedbacked()){
					maxDataItem = dataItem;
					maxIG = calculateInformationGain(dataItem);
					break;
				}
			}

			logger.info("###Calculating dataset entropy");
			logger.info("#######################");
			for(DataItem item : this.getDataSet().getDataItems()){
				logger.info(item.getDataItemIndex() + " ");
				logger.info(item.getValueProb());
			}
			logger.info("Dataset entropy:" + this.getDataSet().calculateEntropy());
			logger.info("#######################");
			logger.info("Information gain " + maxIG);

			for (DataItem dataItem : dataItems) {
				logger.info("%%%%Calculate item#" + dataItem.getDataItemIndex() +" information gain");
				if(dataItem.isFeedbacked()||dataItem==maxDataItem){
					continue;
				}

                // Calculate the information gain if get feedback on this item
				double tmpCntIG = calculateInformationGain(dataItem);
				logger.info("%%%Item and IG " + dataItem.getDataItemIndex() + " " +tmpCntIG);
				if(tmpCntIG >= maxIG){
                    // Store the max entropy value and the index of the item
					maxDataItem = dataItem;
					maxIG = tmpCntIG;
				}
			}
			//System.out.println("MaxIG " + maxIG);
			return maxDataItem;
		}
		catch(Exception e){
		}
		return null;
	}

    @Override
    public double calculateScore(DataItem input) throws Exception {
        return calculateInformationGain(input);
    }

	private double calculateInformationGain(DataItem item) throws Exception{
		Map<Integer,Double> catEntropy = new HashMap<Integer,Double>();
		for(Integer category : item.getValueProb().keySet()){
			logger.info("%%%%%%Conditional entropy for item#" + item.getDataItemIndex() +" category " + category);
//			System.out.println("%%%%%%Conditional entropy for item#" + item.getDataItemIndex() +" category " + category);
            // The uncertainty of the dataset if we feedback on the category is the correct one for this data item
			double contEntropy = calculateConditionalEntropy(item, category);
			catEntropy.put(category, contEntropy);
		}

		//Calculate conditional entropy
		double conditionalEntropy = 0;
		for(Integer category : item.getValueProb().keySet()){
			double valll = item.getValueProb().get(category)*catEntropy.get(category);
			conditionalEntropy+= valll;
			logger.info("Item"+item.getDataItemIndex() + " "+item.getValueProb().get(category)+ " " + catEntropy.get(category) + " "+ valll);
		}
		//		logger.info("Item H(|) "+item.getDataItemIndex() + " " + conditionalEntropy);
		return calculateDataSetEntropy() - conditionalEntropy;
	}

	private double calculateDataSetEntropy(){
		logger.info("Dataset entropy");
		double entropy = this.getDataSet().calculateEntropy();
		logger.info(entropy);
		logger.info("###############");
		return entropy;
	}

	private double calculateConditionalEntropy(DataItem item, Integer category) throws Exception{
		//Create new dataset with item[category] = groundtruth
		DataSet cloned = this.getDataSet().clone();
		DataItem clonedItm = cloned.getDataItems().get(item.getDataItemIndex());
		clonedItm.setFeedbacked(true);
		clonedItm.setGroundTruth(category);
		SortedMap<Integer, Double> clonedValProb = clonedItm.getValueProb();
		double old = clonedValProb.get(category);
		logger.info("Old"+ clonedItm.getDataItemIndex() + " " + category +" " + old);
		//TODO: update feedback probability here
		for(Integer cat: clonedItm.categoryList){
			if(cat == category){
				clonedValProb.put(cat, 1.0);
			}else{
				clonedValProb.put(cat, 0.0);
			}
		}

		////////////////////////DEBUG/////////////////////////////////////		
		logger.info("%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("If T" + clonedItm.getDataItemIndex() +"=" + category);
		//////////////////////////////////////////////////////////////////

		// Run EM with new data
		Engine engine =  Converter.convert(cloned);
		engine.executeWithoutContext();

		DawidSkeneDecorator dsDecorator = new DawidSkeneDecorator(engine.getDs());
		Map<Integer, Double> sourceWorth = dsDecorator.
				getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
		cloned.setTrustworthiness(sourceWorth);

		Map<TupleR<Integer, Integer>, Double> itemQual = dsDecorator.
				getDataItemQuality(Datum.ClassificationMethod.DS_Soft);

		for (TupleR<Integer, Integer> tuple : itemQual.keySet()) {
			DataItem itm = cloned.getDataItems().get(tuple.first);
			SortedMap<Integer, Double> valueProb = itm.getValueProb();
			Double qual = itemQual.get(tuple);
			valueProb.put(tuple.second, qual);
			//logger.info("Item#"+tuple.first+" "+ tuple.second+" "+qual +" " + itm.isFeedbacked());
		}
		
//		for(DataItem im : cloned.getDataItems()){
//			logger.info(im.getDataItemIndex()+" "+im.isFeedbacked());
//			logger.info(im.getValueProb());
//		}
		
		logger.info("%%%%%%%%%%%%%%%%%%%%%%");
		double newP = clonedItm.getValueProb().get(category);
		//		assert newP != old;
		double retVal = cloned.calculateEntropy();

		////////////////////////DEBUG/////////////////////////////////////
		for(DataItem im : cloned.getDataItems()){
			logger.info(im.getDataItemIndex()+" "+im.isFeedbacked() + " " + im.getGroundTruth());
			logger.info(im.getValueProb() + " " + im.getSelectiveValue());
		}
		logger.info("%%%%%%%%%%%%%%%%%%%%%%");
		//////////////////////////////////////////////////////////////////

		logger.info("%%%%%%Condtional entropy item" + clonedItm.getDataItemIndex() + " cat " + category + "=" + retVal);
		return retVal;
	}

}
