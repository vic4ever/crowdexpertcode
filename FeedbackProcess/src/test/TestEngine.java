package test;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.CategoryPair;
import com.ipeirotis.gal.core.ConfusionMatrix;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import com.ipeirotis.gal.engine.EngineContext;
import data.model.DataSet;
import data.reader.GALDataReader;
import main.java.config.Mainconfig;
import main.java.feedback.FeedBackModel;
import main.java.utility.IDGenerator;
import org.junit.Before;
import org.junit.Test;
import org.kohsuke.args4j.CmdLineParser;
import utils.TupleR;
import utils.Converter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Asus on 8/2/14.
 */
public class TestEngine {
    private EngineContext ctx;

    private CmdLineParser parser;

    public String input = "input/";

    public String inputPath = input + "data/";
    public String labelPath = inputPath + "label.txt";
    public String categoryPath = inputPath + "category.txt";
    public String goldPath = inputPath + "gold.txt";
    public String workerReliabilityPath = inputPath + "worker.txt";
    private Map<String, ConfusionMatrix> workerMatrices;

    @Before
    public void before() {
        ctx = new EngineContext();

        parser = new CmdLineParser(ctx);

        Mainconfig.getInstance().initialized();
        for (Mainconfig.ListDatasets data : Mainconfig.getInstance().getDatasets()) {
            Mainconfig.getInstance().setData(data);
        }
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("workersRatio"));
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("typeOfDistributor"));
        IDGenerator.resetWorkerID(-1);
        IDGenerator.resetQuestionID(-1);
        FeedBackModel model = new FeedBackModel();
        try {
            model.generateGALInputFilesMulti(labelPath, categoryPath, goldPath,
                    workerReliabilityPath);
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            System.out.println("###########################################################");
            System.out.println("0 feedback without prior");
            DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
            System.out.println("Num feedback " + dataSet.getFeedbackedDataItems().size());
            Engine engine = Converter.convert(dataSet);
            engine.executeWithoutContext();
            workerMatrices = engine.getWorkerMatrices();

            DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
            Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
            dataSet.setTrustworthiness(sourceTrust);

            Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
        } catch (Exception e) {

        }
    }

//    @Test
//    public void testHappyPath() throws Exception {
//        parser.parseArgument("--input data/AdultContent/test-unlabeled.txt --categories data/AdultContent/test-categories.txt"
//                .split("\\s+"));
//
//        assertEquals(ctx.getInputFile(), "data/AdultContent/test-unlabeled.txt");
//        assertEquals(ctx.getCategoriesFile(), "data/AdultContent/test-categories.txt");
//
//        Engine engine = new Engine(ctx);
//
//        engine.execute();
//    }

    //    @Test
    public void testData() throws Exception {

    }


    @Test
    public void testDataWith1FeedbackNoPrior() throws Exception {
        System.out.println("###########################################################");
        System.out.println("1 feedback without prior");
        DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
        dataSet.getDataItems().get(0).setFeedbacked(true);
        System.out.println("Num feedback " + dataSet.getFeedbackedDataItems().size());
        Engine engine = Converter.convert(dataSet);
        engine.executeWithoutContext();

        try {
            DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
            Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
            dataSet.setTrustworthiness(sourceTrust);

            Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
        } catch (Exception e) {

        }
    }


    @Test
    public void testDataWith1FeedbackWithPrior() throws Exception {
        System.out.println("###########################################################");
        System.out.println("1 feedback with prior");
        HashMap<String, Double> priors = new HashMap<String, Double>();
        priors.put("1", 0.45424);
        priors.put("0", 0.54576);

        DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
        dataSet.getDataItems().get(0).setFeedbacked(true);
        System.out.println("Num feedback " + dataSet.getFeedbackedDataItems().size());
        Engine engine = Converter.convert(dataSet);
        engine.executeWithoutContext(null, workerMatrices);

        try {
            DawidSkeneDecorator decorator = new DawidSkeneDecorator(engine.getDs());
            Map<Integer, Double> sourceTrust = decorator.getSourceTrustworthiness(Worker.ClassificationMethod.DS_MaxLikelihood_Estm);
            dataSet.setTrustworthiness(sourceTrust);

            Map<TupleR<Integer, Integer>, Double> itemQual = decorator.getDataItemQuality(Datum.ClassificationMethod.DS_Soft);
        } catch (Exception e) {

        }
    }

}
