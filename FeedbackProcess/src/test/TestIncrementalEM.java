package test;

import com.ipeirotis.gal.algorithms.DawidSkeneDecorator;
import com.ipeirotis.gal.core.ConfusionMatrix;
import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.engine.Engine;
import data.model.DataItem;
import data.model.DataSet;
import data.reader.GALDataReader;
import feedback.strategy.selection.RandomSelection;
import org.junit.Test;
import utils.ArrayUti;
import utils.Converter;
import utils.TupleR;

import java.util.*;

/**
 * Created by Asus on 8/3/14.
 */
public class TestIncrementalEM {
    public String input = "input/";

    public String inputPath = input + "data/";
    public String labelPath = inputPath + "label.txt";
    public String categoryPath = inputPath + "category.txt";
    public String goldPath = inputPath + "gold.txt";
    public String workerReliabilityPath = inputPath + "worker.txt";

    int seed = 10;

    protected DataItem getFeedbackObjectNoSetFeedbacked(DataSet dataSet) {
        //Create not feedback dataset
        List<Integer> unfeedbackDataItems = new ArrayList<Integer>();
        List<DataItem> dataItems = dataSet.getDataItems();
        for (DataItem item : dataItems) {
            if (!item.isFeedbacked()) {
                unfeedbackDataItems.add(item.getDataItemIndex());
            }
        }
        Collections.shuffle(unfeedbackDataItems, new Random(seed));
        DataItem retVal = dataItems.get(unfeedbackDataItems.get(0));
        return retVal;
    }

    @Test
    public void test() {
        List<Integer> noprior = new ArrayList<Integer>();
        List<Integer> prior = new ArrayList<Integer>();

        int numIn = 50;
        for (int i = 1; i < numIn; i++) {
            seed++;
            noprior = ArrayUti.sumInt(noprior, testNoPriors());
            prior = ArrayUti.sumInt(prior, testPriors());
        }

        List<Double> averageP = ArrayUti.averageInt(noprior, numIn);
        List<Double> averageE = ArrayUti.averageInt(prior, numIn);
        System.out.println(averageP);
        System.out.println(averageE);

    }


    public List<Integer> testNoPriors() {
        try {
            System.out.println("###########################################################");
            System.out.println("feedback without prior");
            List<Integer> iterations = new ArrayList<Integer>();
            Map<String, ConfusionMatrix> workerMatrices;
            DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
            int nItems = dataSet.getDataItems().size();
            for (int i = 0; i < nItems; i++) {
                getFeedbackObjectNoSetFeedbacked(dataSet).setFeedbacked(true);
//                System.out.println("Num feedback " + dataSet.getFeedbackedDataItems().size());
                Engine engine = Converter.convert(dataSet);
                engine.executeWithoutContext();
                iterations.add(engine.count);
            }
//            System.out.print(iterations);
            return iterations;
        } catch (Exception e) {

        }
        return null;
    }

    public Map<String, ConfusionMatrix> initLoop() {
        try {
            System.out.println("###########################################################");
            DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
            System.out.println("Num feedback " + dataSet.getFeedbackedDataItems().size());
            Engine engine = Converter.convert(dataSet);
            engine.executeWithoutContext();
            return engine.getWorkerMatrices();

        } catch (Exception e) {

        }
        return null;
    }

    public List<Integer> testPriors() {
        try {
            System.out.println("###########################################################");
            System.out.println("feedback with prior");
            Map<String, ConfusionMatrix> workerMatrices = null;
            workerMatrices = initLoop();
            DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
            List<Integer> iterations = new ArrayList<Integer>();
            int nItems = dataSet.getDataItems().size();
            for (int i = 0; i < nItems; i++) {
                getFeedbackObjectNoSetFeedbacked(dataSet).setFeedbacked(true);
//                System.out.println("Num feedback " + dataSet.getFeedbackedDataItems().size());
                Engine engine = Converter.convert(dataSet);
                engine.executeWithoutContext(null, workerMatrices);
                iterations.add(engine.count);
                workerMatrices = engine.getWorkerMatrices();
            }
//            System.out.print(iterations);
            return iterations;
        } catch (Exception e) {

        }
        return null;
    }
}
