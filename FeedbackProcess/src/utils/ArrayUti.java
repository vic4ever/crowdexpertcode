package utils;

import java.util.ArrayList;
import java.util.List;

public class ArrayUti {
    public static <T> String arrayToString(List<T> abc) {
        String ret = "";
        for (int i = 0; i < abc.size(); i++) {
            T a = abc.get(i);
            ret += a.toString() + ",";
        }
        return ret;
    }

    public static List<Double> sum(List<Double> a, List<Double> b) {
        List<Double> sum = new ArrayList<Double>();
        if (a.size() == 0) {
            return b;
        }
        if (b.size() == 0) {
            return a;
        }
        for (int i = 0; i < b.size(); i++) {
            sum.add(i, a.get(i) + b.get(i));
        }
        return sum;
    }


    public static List<Integer> sumInt(List<Integer> a, List<Integer> b) {
        List<Integer> sum = new ArrayList<Integer>();
        if (a.size() == 0) {
            return b;
        }
        if (b.size() == 0) {
            return a;
        }
        for (int i = 0; i < b.size(); i++) {
            sum.add(i, a.get(i) + b.get(i));
        }
        return sum;
    }

    public static List<Double> average(List<Double> sum, int count) {
        List<Double> avg = new ArrayList<Double>();
        for (int i = 0; i < sum.size(); i++) {
            avg.add(i, sum.get(i) / count);
        }
        return avg;
    }

    public static List<Double> averageInt(List<Integer> sum, int count) {
        List<Double> avg = new ArrayList<Double>();
        for (int i = 0; i < sum.size(); i++) {
            avg.add(i, sum.get(i) / (double) count);
        }
        return avg;
    }
}
