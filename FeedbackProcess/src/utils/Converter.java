package utils;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ipeirotis.gal.core.AssignedLabel;
import com.ipeirotis.gal.core.Category;
import com.ipeirotis.gal.core.CorrectLabel;
import com.ipeirotis.gal.engine.Engine;
import com.ipeirotis.gal.engine.EngineContext;

import data.model.Assignment;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;


public class Converter{

	public static Engine convert(DataSet dataSet){
		// DataItem -> Object
		// Source -> Worker
		// Value -> Category
		// Source,DataItem,Value -> AssignedLabel
		// DataItem,GroundTruth -> CorrectLabel
		List<DataItem> dataItems = dataSet.getDataItems();
        //TODO: check this
		List<DataSource> dataSources = dataSet.getDataSources(false);

		Engine engine = new Engine(null);
		engine.setCategories(loadCategories(dataItems));
		engine.setLabels(loadAssignedLabels(dataItems, dataSources));
		//		System.out.println("Abc xyz " +engine.getLabels().size());
		engine.setCorrect(loadCorrectLabels(dataItems));
		//engine.setEvaluation(loadEvaluationLabels(dataItems));
		

		return engine;
	}

//	public static Engine convert(List<DataItem> dataItems){
//		Engine engine = new Engine(null);
//		engine.setCategories(loadCategories(dataItems));
//		engine.setLabels(loadAssignedLabels(dataItems));
//		//		System.out.println("Abc xyz " +engine.getLabels().size());
//		engine.setCorrect(loadCorrectLabels(dataItems));
//		//engine.setEvaluation(loadEvaluationLabels(dataItems));
//		return engine;
//	}

	public static Set<CorrectLabel> loadCorrectLabels(List<DataItem> dataItems) {

		Set<CorrectLabel> labels = new HashSet<CorrectLabel>();
		for (DataItem dataItem: dataItems) {

            // Generate gold label only for feedbacked data item
			if(dataItem.isFeedbacked()){
				String objectname = String.valueOf(dataItem.getDataItemIndex());
				String categoryname = String.valueOf(dataItem.getGroundTruth());

				CorrectLabel cl = new CorrectLabel(objectname, categoryname);
				labels.add(cl);
			}
		}
		return labels;
	}

	public static Set<CorrectLabel> loadEvaluationLabels(List<DataItem> dataItems) {

		Set<CorrectLabel> labels = new HashSet<CorrectLabel>();
		for (DataItem dataItem: dataItems) {

			String objectname = String.valueOf(dataItem.getDataItemIndex());
			List<Assignment> assignments = dataItem.getAssignments();
			for (Assignment assignment : assignments) {
				String categoryname = String.valueOf(assignment.getValue());

				CorrectLabel cl = new CorrectLabel(objectname, categoryname);
				labels.add(cl);
			}
		}
		return labels;
	}
	
	private static DataSource getSourceFromDataSourceList(int srcInd, List<DataSource> srcs){
		for(DataSource s: srcs){
			if(s.getDataSourceIndex() == srcInd){
				return s;
			}
		}
		return null;
	}

	public static Set<AssignedLabel> loadAssignedLabels(List<DataItem> dataItems, List<DataSource> dataSources) {

		Set<AssignedLabel> labels = new HashSet<AssignedLabel>();
		for (DataItem dataItem : dataItems) {
			List<Assignment> assignments = dataItem.getAssignments();
			for (Assignment assignment : assignments) {
				//This means this data source has been deleted
				if(dataSources.size() <= assignment.getSourceIndex()){
					continue;
				}
				DataSource source = getSourceFromDataSourceList(assignment.getSourceIndex(),dataSources);
				assert source.getDataSourceIndex() == assignment.getSourceIndex();
				if(source.isDeleted()){
					continue;
				}
				String workername = String.valueOf(assignment.getSourceIndex());
				String objectname = String.valueOf(assignment.getDataItemIndex());
				String categoryname = String.valueOf(assignment.getValue());

				//System.out.printf("W%s Ob%s Cat%s\n",workername,objectname,categoryname);
				AssignedLabel al = new AssignedLabel(workername, objectname,
						categoryname);
				labels.add(al);	
			}
		}
		//System.out.println("NUm of label " + labels.size());
		return labels;
	}

	public static Set<Category> loadCategories(List<DataItem> dataItems) {
		Set<Category> categories = new HashSet<Category>();
		//TODO: assume that the category are the same for every data item
		for (Integer value:dataItems.get(0).categoryList) {
			Category c = new Category(value.toString());
			categories.add(c);
		}
		return categories;
	}

	public static void createGALInput(DataSet dataSet, String labelFile, String catFile) throws IOException{
		FileWriter file = new FileWriter(labelFile);
		BufferedWriter writer = new BufferedWriter(file);
		Set<Integer> values= new HashSet<Integer>();

		for(int i=0; i< dataSet.getDataItems().size();i++){
			DataItem dataItem = dataSet.getDataItems().get(i);
			List<Assignment> assignments = dataItem.getAssignments();
			for(int j=0; j<assignments.size(); j++){
				Assignment assignment = assignments.get(j);
				values.add(assignment.getValue());
				if(i == dataSet.getDataItems().size() - 1 && j == assignments.size() -1){
					writer.write(String.format("%d\t%d\t%d", assignment.getSourceIndex(),dataItem.getDataItemIndex(),assignment.getValue()));
				}else{
					writer.write(String.format("%d\t%d\t%d\n", assignment.getSourceIndex(),dataItem.getDataItemIndex(),assignment.getValue()));
				}
			}
		}
		writer.close();

		//Writer
		FileWriter catfile = new FileWriter(catFile);
		BufferedWriter catWriter = new BufferedWriter(catfile);
		//Tinh prior ntn ?
		int count = 0;
		for(Integer val:values){
			count++;
			if(count==values.size()){
				catWriter.write(val.toString());
			}else{
				catWriter.write(val+"\n");
			}
		}
		catWriter.close();
	}

	public static void createGALGold(DataSet dataSet, String goldFileName) throws IOException{
		FileWriter file = new FileWriter(goldFileName);
		BufferedWriter writer = new BufferedWriter(file);
		List<DataItem> dataItems = dataSet.getDataItems();
		for (DataItem dataItem : dataItems) {
			if(dataItem.isFeedbacked()){
				writer.write(String.format("%d\t%d\n", dataItem.getDataItemIndex(),dataItem.getGroundTruth()));
			}
		}
		writer.close();
	}

	public static EngineContext createEngineContext(String goldFileName, String labelFile, String catFile) throws IOException{
		EngineContext ctx = new EngineContext();
		ctx.setCategoriesFile(catFile);
		ctx.setCorrectFile(goldFileName);
		ctx.setInputFile(labelFile);
		return ctx;
	}
}
