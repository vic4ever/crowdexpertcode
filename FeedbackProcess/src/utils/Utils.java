package utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import data.model.*;

/**
 * @author Asus
 */
public class Utils {

    /**
     * Create a sequence of integers from begin (inclusive) to end (exclusive)
     *
     * @param begin
     * @param end
     * @return
     */
    public static List<Integer> makeSequence(int begin, int end) {
        List<Integer> ret = new ArrayList(end - begin + 1);

        for (int i = begin; i < end; i++) {
            ret.add(i);
        }

        return ret;
    }

    public static void main(String[] args) {
        System.out.println(Utils.makeSequence(0, 10));
    }

    public static <E> List<List<E>> generatePerm(List<E> original) {
        if (original.size() == 0) {
            List<List<E>> result = new ArrayList<List<E>>();
            result.add(new ArrayList<E>());
            return result;
        }
        E firstElement = original.remove(0);
        List<List<E>> returnValue = new ArrayList<List<E>>();
        List<List<E>> permutations = generatePerm(original);
        for (List<E> smallerPermutated : permutations) {
            for (int index = 0; index <= smallerPermutated.size(); index++) {
                List<E> temp = new ArrayList<E>(smallerPermutated);
                temp.add(index, firstElement);
                returnValue.add(temp);
            }
        }
        return returnValue;
    }

    public static void writeGALInputFromDataSet(DataSet dataSet, String folderName) {
        String wFile = folderName + "/" + "worker.txt";
        String lFile = folderName + "/" + "label.txt";
        String cFile = folderName + "/" + "category.txt";
        String gFile = folderName + "/" + "gold.txt";

        try {
            writeCategoryFromDataSet(dataSet, cFile);
            writeLabelsFromDataSet(dataSet, lFile);
            writeWorkerReliabilityFromDataSet(dataSet, wFile);
            writeGoldFromDataSet(dataSet, gFile);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void writeWorkerReliabilityFromDataSet(DataSet dataSet, String filename) throws IOException {
        FileWriter writer = new FileWriter(filename);
        List<DataSource> srcs = dataSet.getDataSources(true);
        for (int i = 0; i < dataSet.getNumSources(); i++) {
            DataSource src = srcs.get(i);
            writer.write(src.getDataSourceIndex() + "\t" + src.getReliability() + "\t" + src.getOriginalSource() + "\n");
        }
        writer.close();
    }

    public static void writeCategoryFromDataSet(DataSet dataSet, String filename) throws IOException {
        FileWriter writer = new FileWriter(filename);
        DataItem itm = dataSet.getDataItems().get(0);
        for (Integer cat : itm.categoryList) {
            writer.write(cat + "\n");
        }
        writer.close();
    }

    public static void writeGoldFromDataSet(DataSet dataSet, String filename) throws IOException {
        FileWriter writer = new FileWriter(filename);
        List<DataItem> dataItems = dataSet.getDataItems();
        for (DataItem dataItem : dataItems) {
            writer.write(dataItem.getDataItemIndex() + "\t" + dataItem.getGroundTruth() + "\n");
        }
        writer.close();
    }

    public static void writeLabelsFromDataSet(DataSet dataSet, String filename) throws IOException {
        FileWriter writer = new FileWriter(filename);
        List<DataItem> srcs = dataSet.getDataItems();
        for (DataItem dataItem : srcs) {
            List<Assignment> assess = dataItem.getAssignments();
            for (Assignment assignment : assess) {
                writer.write(assignment.getSourceIndex() + "\t" + assignment.getDataItemIndex() + "\t" + assignment.getValue() + "\n");
            }
        }
        writer.close();
    }

    public static int[][] generateReverseConfusionMatrix(DataSet dataSet, DataSource src) {
        int[][] confusionMatrix = new int[dataSet.getNumLabels()][dataSet.getNumLabels()];
        for (DataItem dataItem : dataSet.getFeedbackedDataItems()) {
            Assignment assignment = src.getAssignment(dataItem.getDataItemIndex());
            //TODO: assume label index from 0 and +1 each
            confusionMatrix[assignment.getValue()][dataItem.getGroundTruth()]++;
        }
        return confusionMatrix;
    }

    public static int[][] generateConfusionMatrix(DataSet dataSet, DataSource src) {
        int[][] confusionMatrix = new int[dataSet.getNumLabels()][dataSet.getNumLabels()];
        for (DataItem dataItem : dataSet.getFeedbackedDataItems()) {
            Assignment assignment = src.getAssignment(dataItem.getDataItemIndex());
            //TODO: assume label index from 0 and +1 each
            if (assignment != null) {
                confusionMatrix[dataItem.getGroundTruth()][assignment.getValue()]++;
            }
        }
        return confusionMatrix;
    }

}
