package dependency;

import java.util.Map;

import org.testng.annotations.Test;

import utils.TupleR;
import data.generator.CopySimulator;
import data.generator.CopySimulator.COPY_FALSE_STRATEGY;
import data.generator.CopySimulator.COPY_TRUE_STRATEGY;
import data.model.DataItem;
import data.model.DataSet;
import data.model.DataSource;
import data.reader.GALDataReader;

public class TestDetector {
	@Test
	public void testDetector() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/testinput.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/goldtestinput.txt", dataSet1);
		DataSet dataSet3 = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/labeltestinput.txt", dataSet2);
		DataSet dataSet = GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/workertestinput.txt", dataSet3);
		dataSet.initTrustworthiness();
		
		CopySimulator simulator = new CopySimulator(dataSet);
		simulator.setCopyRatio(0.5);
		
		simulator.genCopierFromWorstSrc(COPY_FALSE_STRATEGY.COPY_ALL, COPY_TRUE_STRATEGY.COPY_ALL);
		
		
		for(DataItem itm : dataSet.getDataItems()){
			itm.setFeedbacked(true);
		}
		
		Detector detector = new Detector(dataSet);
		Map<TupleR<Integer, Integer>, Double> retval = detector.calculateDependentProbability();
		for (TupleR<Integer,Integer> tuple : retval.keySet()) {
			DataSource src1 = dataSet.getSource(tuple.first);
			DataSource src2 = dataSet.getSource(tuple.second);
			System.out.println(tuple.first +" "+ src1.getOriginalSource() + " " +tuple.second +" " + src2.getOriginalSource() +" " + retval.get(tuple));
		}
	}
}
