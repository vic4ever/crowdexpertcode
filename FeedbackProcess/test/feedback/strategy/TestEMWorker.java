package feedback.strategy;

import data.model.DataSet;
import data.reader.GALDataReader;
import main.java.config.Mainconfig;
import main.java.feedback.FeedBackModel;
import main.java.utility.IDGenerator;

import java.io.IOException;

/**
 * Created by Asus on 4/5/14.
 */
public class TestEMWorker {

    public String input = "input/";

    public String inputPath = input + "data/";
    public String labelPath = inputPath + "label.txt";
    public String categoryPath = inputPath + "category.txt";
    public String goldPath = inputPath + "gold.txt";
    public String workerReliabilityPath = inputPath + "worker.txt";

    public void generateData() throws IOException {
        Mainconfig.getInstance().initialized();
        for (Mainconfig.ListDatasets data : Mainconfig.getInstance().getDatasets()) {
            Mainconfig.getInstance().setData(data);
        }
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("workersRatio"));
        System.out.println(Mainconfig.getInstance().getListConfig()
                .get("typeOfDistributor"));
        IDGenerator.resetWorkerID(-1);
        IDGenerator.resetQuestionID(-1);
        FeedBackModel model = new FeedBackModel();
        model.generateGALInputFilesMulti(labelPath, categoryPath, goldPath,
                workerReliabilityPath);
    }

    public void testDebug() throws Exception{
        DataSet dataSet = GALDataReader.readDatsetFromFolder(inputPath);
        dataSet.initTrustworthiness();
        FeedbackSequence sequence = new EMWorkerFeedbackSequence(10);
        sequence.setData(dataSet);
        sequence.feedbackLoop();
    }

    public void run(double reliability, double std) throws Exception {
        Mainconfig.getInstance().getListConfig()
                .put("workersRatio", "80%;10%");
        Mainconfig
                .getInstance()
                .getListConfig()
                .put("typeOfDistributor",
                        "NormalDistribution(0.2,0.01);NormalDistribution("
                                + reliability + "," + std + ")");
        // Binary, Multiple
        // Mainconfig.getInstance().getListConfig().put("InputDataType",
        // "Multiple");
        // Mainconfig.getInstance().getListConfig().put("InputNumberLabels",
        // "3");
        Mainconfig.getInstance().initialized();
        generateData();

        testDebug();
    }

    public static void main(String[] args){
        TestEMWorker testEMWorker = new TestEMWorker();
        try {
            testEMWorker.run(0.8,0.01);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
