package feedback.strategy;

import org.testng.annotations.Test;
import java.io.IOException;

import data.model.DataSet;
import data.reader.DataReader;
import data.reader.GALDataReader;

public class TestEntropyEM {
//	@Test
//	public void test1() throws IOException{
//		DataSet dataSet = DataReader.readTxt("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input");
//		FeedbackSequence hitsSequence = new EMFeedbackSequence();
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//	}
//	
//	@Test
//	public void test2() throws IOException{
//		DataSet dataSet = DataReader.readTxt("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input3");
//		//dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new EMFeedbackSequence();
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//	
//	@Test
//	public void test3() throws IOException{
//		DataSet dataSet = DataReader.readTxt("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input5");
//		//dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new EMFeedbackSequence();
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
	
	@Test
	public void test4() throws Exception{
		DataSet dataSet = GALDataReader.readGALLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6");
		//dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMFeedbackSequence();
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void test6() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6Gold", dataSet1);
		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMFeedbackSequence();
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	
	@Test
	public void test5() throws Exception{
		DataSet dataSet = GALDataReader.readGALLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input8");
		//dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMFeedbackSequence();
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp1() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMFeedbackSequence();
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
}
