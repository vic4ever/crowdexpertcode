package feedback.strategy;

import org.testng.annotations.Test;

import data.model.DataSet;
import data.reader.GALDataReader;

public class TestIGEM {
	@Test
	public void test1() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp1() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp1SrcIG() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new SrcIGEMFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
//	@Test
//	public void exp1NearOptimal() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
////		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(100,true);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
		
	@Test
	public void exp2() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp2SrcIG() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new SrcIGEMFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
//	@Test
//	public void exp2NearOptimal() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold3.txt", dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
////		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(100,true);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
	
	@Test
	public void exp3BL() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new BaseLineFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp3Random() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		dataSet.prettyPrint();
	}
	
	@Test
	public void exp3DtIG() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp3SrcIG() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new SrcIGEMFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
//	@Test
//	public void exp3NearOptimal() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt", dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", dataSet2);
////		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(100,true);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
	
	
	@Test
	public void exp3DtIGLow() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/lexp3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/gexp3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/cexp3.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp3SrcIGLow() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/lexp3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/gexp3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/cexp3.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new SrcIGEMFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
//	@Test
//	public void exp3NearOptimalLow() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/gexp3.txt", dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/cexp3.txt", dataSet2);
////		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(100,true);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
	
	@Test
	public void exp3BL2() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/lexp3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/gexp3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/cexp3.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new BaseLineFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp3Random2() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/lexp3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/gexp3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/cexp3.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp3DtIG2() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/lexp3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/gexp3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/cexp3.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
//	@Test
//	public void exp3NearOptimal2() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/gexp3.txt", dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/cexp3.txt", dataSet2);
////		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(100,true);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
	
	@Test
	public void exp3SrcIG2() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/lexp3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/gexp3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/cexp3.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new SrcIGEMFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void testSmallMulti() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/test3/testinput.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/test3/goldtestinput.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/test3/category.txt", dataSet2);
		FeedbackSequence hitsSequence = new IGFeedbackSequence(3);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
	}
	
	
	@Test
	public void testSmall10() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels10.txt");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold10.txt", dataSet1);
		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new IGFeedbackSequence(10);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		dataSet.prettyPrint();
	}
	
	@Test
	public void testSmall10Random() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels10.txt");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold10.txt", dataSet1);
		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(10);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		dataSet.prettyPrint();
	}
	
	@Test
	public void test6() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6Gold", dataSet1);
		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new IGFeedbackSequence(5);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void testDebug() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/debug/testinput.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/debug/goldtestinput.txt", dataSet1);
		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/debug/labeltestinput.txt", dataSet2);
		dataSet.initTrustworthiness();
		FeedbackSequence sequence = new IGFeedbackSequence(dataSet.getNumDataItems());
		sequence.setData(dataSet);
		sequence.feedbackLoop();
	}
	
	@Test
	public void testSlide() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/SyntheticData/data/input6");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/SyntheticData/data/input6Gold", dataSet1);
		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/SyntheticData/data/cat6", dataSet2);
		dataSet.initTrustworthiness();
		FeedbackSequence sequence = new IGFeedbackSequence(dataSet.getNumDataItems());
		sequence.setData(dataSet);
		sequence.feedbackLoop();
	}
	
	@Test
	public void expReal() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/real/llabels2.csv");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/real/goldl.csv", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/real/categories.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new IGFeedbackSequence(1185);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
}
