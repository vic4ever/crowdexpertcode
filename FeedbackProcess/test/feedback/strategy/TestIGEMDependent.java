package feedback.strategy;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import com.google.common.base.Joiner;

import utils.Utils;

import data.generator.CopySimulator;
import data.generator.CopySimulator.COPY_FALSE_STRATEGY;
import data.generator.CopySimulator.COPY_TRUE_STRATEGY;
import data.model.DataSet;
import data.reader.GALDataReader;
import feedback.depend.DependentSrcRemover;
import feedback.depend.NormalRemover;
import feedback.strategy.selection.CommonFalseBasedSelection;
import feedback.strategy.selection.FrequentEntropySelection;
import feedback.strategy.selection.IGEMDependSelection;
import feedback.strategy.selection.NoobEntropyBasedSelection;
import feedback.strategy.selection.SelectionAlgorithm;

public class TestIGEMDependent {
//	@Test
//	public void test1() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
//		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp1() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp1SrcIG() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new SrcIGEMFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
////	@Test
////	public void exp1NearOptimal() throws Exception{
////		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
////		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
////		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
////		//		dataSet.prettyPrint();
////		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(100,true);
////		hitsSequence.setData(dataSet);
////		hitsSequence.feedbackLoop();
////		//dataSet.prettyPrint();
////	}
//
//	@Test
//	public void exp2() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold3.txt", dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp2SrcIG() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold3.txt", dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new SrcIGEMFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
////	@Test
////	public void exp2NearOptimal() throws Exception{
////		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels3.txt");
////		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold3.txt", dataSet1);
////		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
////		//		dataSet.prettyPrint();
////		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(100,true);
////		hitsSequence.setData(dataSet);
////		hitsSequence.feedbackLoop();
////		//dataSet.prettyPrint();
////	}
//
//	@Test
//	public void exp3BL() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", dataSet2);
//		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/wexp3.txt", dataSet3);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new BaseLineFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp3Random() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", dataSet2);
//		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/wexp3.txt", dataSet3);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp3DtIG() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", dataSet2);
//		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/wexp3.txt", dataSet3);
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		CopySimulator simulator = new CopySimulator(dataSet);
//		simulator.genCopierFromWorstSrc(COPY_FALSE_STRATEGY.COPY_ALL,COPY_TRUE_STRATEGY.COPY_ALL);
//
//		Utils.writeGALInputFromDataSet(dataSet, "C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1Copy");
//
//		System.out.println("After add copies " + dataSet.getNumSources());
//		//		dataSet.prettyPrint();
//
//		//Delete strategy
//		DependentSrcRemover remover =new NormalRemover();
//		remover.setThreshold(0.9);
//
//		//Selection strategy
//		Map<Integer,SelectionAlgorithm> algors = new HashMap<Integer,SelectionAlgorithm>();
//		SelectionAlgorithm IG = new IGEMDependSelection(dataSet,remover);
//		SelectionAlgorithm noobEntropy = new FrequentEntropySelection(dataSet);
//		for(int i = 0; i< dataSet.getNumDataItems(); i++){
//			//					if(i%4 ==0 || i%4 == 1 || i <= 5){
//			if(i<=5){
//				algors.put(i, noobEntropy);
//			}else{
//				algors.put(i, IG);
//			}
//		}
//
//
//
//		FeedbackSequence hitsSequence = new IGEMDependFeedbackSequence(100,algors);
//		hitsSequence.setRemover(remover);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp3DtIGNoDel() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1Copy/label.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1Copy/gold.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1Copy/category.txt", dataSet2);
//		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1Copy/worker.txt", dataSet3);
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp3SrcIG() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", dataSet2);
//		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/wexp3.txt", dataSet3);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new SrcIGEMFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
////	@Test
////	public void exp3NearOptimal() throws Exception{
////		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/lexp3.txt");
////		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/gexp3.txt", dataSet1);
////		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/cexp3.txt", dataSet2);
////		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart1/wexp3.txt", dataSet3);
////		//		dataSet.prettyPrint();
////		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(100,true);
////		hitsSequence.setData(dataSet);
////		hitsSequence.feedbackLoop();
////		//dataSet.prettyPrint();
////	}
//
//
//	@Test
//	public void exp3DtIGLow() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/gexp3.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/cexp3.txt", dataSet2);
//		DataSet dataSet = GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/wexp3.txt", dataSet3);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp3SrcIGLow() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/gexp3.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/cexp3.txt", dataSet2);
//		DataSet dataSet = GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/wexp3.txt", dataSet3);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new SrcIGEMFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
////	@Test
////	public void exp3NearOptimalLow() throws Exception{
////		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/lexp3.txt");
////		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/gexp3.txt", dataSet1);
////		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/cexp3.txt", dataSet2);
////		DataSet dataSet = GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/LowWorker/wexp3.txt", dataSet3);
////		//		dataSet.prettyPrint();
////		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(100,true);
////		hitsSequence.setData(dataSet);
////		hitsSequence.feedbackLoop();
////		//dataSet.prettyPrint();
////	}
//
//	@Test
//	public void exp3BL2() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/gexp3.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/cexp3.txt", dataSet2);
//		DataSet dataSet = GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/wexp3.txt", dataSet3);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new BaseLineFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp3Random2() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/gexp3.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/cexp3.txt", dataSet2);
//		DataSet dataSet = GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/wexp3.txt", dataSet3);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp3DtIG2() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/gexp3.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/cexp3.txt", dataSet2);
//		DataSet dataSet = GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/wexp3.txt", dataSet3);
//		//		dataSet.prettyPrint();
//
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		CopySimulator simulator = new CopySimulator(dataSet);
//		simulator.genCopierFromWorstSrc(COPY_FALSE_STRATEGY.COPY_ALL,COPY_TRUE_STRATEGY.COPY_ALL);
//
//		Utils.writeGALInputFromDataSet(dataSet, "C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2Copy");
//
//		System.out.println("After add copies " + dataSet.getNumSources());
//		//		dataSet.prettyPrint();
//
//		//Delete strategy
//		DependentSrcRemover remover =new NormalRemover();
//		remover.setThreshold(0.9);
//
//		//Selection strategy
//		Map<Integer,SelectionAlgorithm> algors = new HashMap<Integer,SelectionAlgorithm>();
//		SelectionAlgorithm IG = new IGEMDependSelection(dataSet,remover);
//		SelectionAlgorithm noobEntropy = new FrequentEntropySelection(dataSet);
//		for(int i = 0; i< dataSet.getNumDataItems(); i++){
//			//					if(i%4 ==0 || i%4 == 1 || i <= 5){
//			if(i<=5){
//				algors.put(i, noobEntropy);
//			}else{
//				algors.put(i, IG);
//			}
//		}
//
//		//		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
//		FeedbackSequence hitsSequence = new IGEMDependFeedbackSequence(100, algors);
//		hitsSequence.setRemover(remover);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void exp3DtIG2NoDel() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2Copy/label.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2Copy/gold.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2Copy/category.txt", dataSet2);
//		DataSet dataSet = GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2Copy/worker.txt", dataSet3);
//		//		dataSet.prettyPrint();
//
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//	}
//
////	@Test
////	public void exp3NearOptimal2() throws Exception{
////		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/lexp3.txt");
////		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/gexp3.txt", dataSet1);
////		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/cexp3.txt", dataSet2);
////		DataSet dataSet = GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/wexp3.txt", dataSet3);
////		//		dataSet.prettyPrint();
////		FeedbackSequence hitsSequence = new NearOptimalFeedbackSequence(100,true);
////		hitsSequence.setData(dataSet);
////		hitsSequence.feedbackLoop();
////		//dataSet.prettyPrint();
////	}
//
//	@Test
//	public void exp3SrcIG2() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/lexp3.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/gexp3.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/cexp3.txt", dataSet2);
//		DataSet dataSet = GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/EMMixedPart2/wexp3.txt", dataSet3);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new SrcIGEMFeedbackSequence(100);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void testSmallMulti() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/test3/testinput.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/test3/goldtestinput.txt", dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/test3/category.txt", dataSet2);
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(3);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//	}
//
//
//	@Test
//	public void testSmall10() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels10.txt");
//		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold10.txt", dataSet1);
//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(10);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		dataSet.prettyPrint();
//	}
//
//	@Test
//	public void testSmall10Random() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels10.txt");
//		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold10.txt", dataSet1);
//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(10);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		dataSet.prettyPrint();
//	}
//
//	@Test
//	public void test6() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6");
//		DataSet dataSet = GALDataReader.readGALGoldLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6Gold", dataSet1);
//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(5);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void testDebug() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/testinput.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/goldtestinput.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/labeltestinput.txt", dataSet2);
//		dataSet.initTrustworthiness();
//
//		CopySimulator simulator = new CopySimulator(dataSet);
//		simulator.genCopierFromWorstSrc(COPY_FALSE_STRATEGY.COPY_ALL,COPY_TRUE_STRATEGY.COPY_ALL);
//
//		//Delete strategy
//		DependentSrcRemover remover =new NormalRemover();
//		remover.setThreshold(0.9);
//
//		FeedbackSequence sequence = new IGFeedbackSequence(dataSet.getNumDataItems());
//		sequence.setRemover(remover);
//		sequence.setData(dataSet);
//
//		sequence.feedbackLoop();
//	}
//
//	@Test
//	public void expReal() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/real/llabels2.csv");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/real/goldl.csv", dataSet1);
//		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/real/categories.txt", dataSet2);
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(1185);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//
//	@Test
//	public void expD20W10() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2/label.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2/gold.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2/category.txt", dataSet2);
//		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2/worker.txt", dataSet3);
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		CopySimulator simulator = new CopySimulator(dataSet);
//		simulator.genCopierFromWorstSrc(COPY_FALSE_STRATEGY.COPY_ALL,COPY_TRUE_STRATEGY.COPY_ALL);
//
//		Utils.writeGALInputFromDataSet(dataSet, "C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2Copy");
//
//
//		//Delete strategy
//		DependentSrcRemover remover =new NormalRemover();
//		remover.setThreshold(0.9);
//
//		System.out.println("After add copies " + dataSet.getNumSources());
//		//		dataSet.prettyPrint();
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(20);
//		hitsSequence.setRemover(remover);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void expD20W10NoDel() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2Copy/label.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2Copy/gold.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2Copy/category.txt", dataSet2);
//		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2Copy/worker.txt", dataSet3);
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(20);
//		//		hitsSequence.setDependentCheck(true);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//
//	@Test
//	public void expD20W102() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2/label.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2/gold.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2/category.txt", dataSet2);
//		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2/worker.txt", dataSet3);
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		CopySimulator simulator = new CopySimulator(dataSet);
//		simulator.genCopierFromWorstSrc(COPY_FALSE_STRATEGY.COPY_ALL,COPY_TRUE_STRATEGY.COPY_ALL);
//
//		Utils.writeGALInputFromDataSet(dataSet, "C:/Users/Asus/workspace/FeedbackProcess/input/D20W10_2Copy");
//
//		System.out.println("After add copies " + dataSet.getNumSources());
//		//		dataSet.prettyPrint();
//
//		//Delete strategy
//		DependentSrcRemover remover =new NormalRemover();
//		remover.setThreshold(0.9);
//
//		//Selection strategy
//		Map<Integer,SelectionAlgorithm> algors = new HashMap<Integer,SelectionAlgorithm>();
//		SelectionAlgorithm IG = new IGEMDependSelection(dataSet,remover);
//		SelectionAlgorithm noobEntropy = new FrequentEntropySelection(dataSet);
//		for(int i = 0; i< dataSet.getNumDataItems(); i++){
//			//			if(i%4 ==0 || i%4 == 1 || i <= 5){
//			if(i<=5){
//				algors.put(i, noobEntropy);
//			}else{
//				algors.put(i, IG);
//			}
//		}
//
//		//Feedback
//		FeedbackSequence hitsSequence = new IGEMDependFeedbackSequence(20,algors);
//		hitsSequence.setRemover(remover);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//	@Test
//	public void expD100W15() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/D100W15/label.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/D100W15/gold.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/D100W15/category.txt", dataSet2);
//		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/D100W15/worker.txt", dataSet3);
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		CopySimulator simulator = new CopySimulator(dataSet);
//		simulator.genCopierFromWorstSrc(COPY_FALSE_STRATEGY.COPY_ALL,COPY_TRUE_STRATEGY.COPY_ALL);
//
//		Utils.writeGALInputFromDataSet(dataSet, "C:/Users/Asus/workspace/FeedbackProcess/input/D100W15Copy");
//
//		System.out.println("After add copies " + dataSet.getNumSources());
//		//		dataSet.prettyPrint();
//
//		//Delete strategy
//		DependentSrcRemover remover =new NormalRemover();
//		remover.setThreshold(0.9);
//
//		//Selection strategy
//		Map<Integer,SelectionAlgorithm> algors = new HashMap<Integer,SelectionAlgorithm>();
//		SelectionAlgorithm IG = new IGEMDependSelection(dataSet,remover);
//		SelectionAlgorithm noobEntropy = new FrequentEntropySelection(dataSet);
//		for(int i = 0; i< dataSet.getNumDataItems(); i++){
//			//			if(i%4 ==0 || i%4 == 1 || i <= 5){
//			if(i<=5){
//				algors.put(i, noobEntropy);
//			}else{
//				algors.put(i, IG);
//			}
//		}
//
//		//Feedback
//		FeedbackSequence hitsSequence = new IGEMDependFeedbackSequence(100,algors);
//		hitsSequence.setRemover(remover);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//
//	@Test
//	public void expD100W15NoDel() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/D100W15Copy/label.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/D100W15Copy/gold.txt", dataSet1);
//		DataSet dataSet3 =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/D100W15Copy/category.txt", dataSet2);
//		DataSet dataSet =  GALDataReader.readWorkerReliability("C:/Users/Asus/workspace/FeedbackProcess/input/D100W15Copy/worker.txt", dataSet3);
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		FeedbackSequence hitsSequence = new IGFeedbackSequence(100);
//		//		hitsSequence.setDependentCheck(true);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//	}
//
//
//	@Test
//	public void expD100W15Partial() throws Exception{
//		//Load original data
//		DataSet dataSet = GALDataReader.readDatsetFromFolder("C:/Users/Asus/workspace/FeedbackProcess/input/D100W15");
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		//Generate copied data
//		CopySimulator simulator = new CopySimulator(dataSet);
//		simulator.genCopierFromWorstSrc(COPY_FALSE_STRATEGY.COPY_ALL,COPY_TRUE_STRATEGY.COPY_PARTLY);
//		String outputCopyFolder = "C:/Users/Asus/workspace/FeedbackProcess/input/D100W15Copy/allpartial";
//		Utils.writeGALInputFromDataSet(dataSet, outputCopyFolder);
//
//		//		DataSet dataSet = GALDataReader.readDatsetFromFolder(outputCopyFolder);
//
//		System.out.println("After add copies " + dataSet.getNumSources());
//		//		dataSet.prettyPrint();
//
//		/////////////////////With delete source/////////////////////////////////////////
//		//Delete strategy
//		DependentSrcRemover remover =new NormalRemover();
//		remover.setThreshold(0.9);
//
//		//Selection strategy
//		Map<Integer,SelectionAlgorithm> algors = new HashMap<Integer,SelectionAlgorithm>();
//		SelectionAlgorithm IG = new IGEMDependSelection(dataSet,remover);
//		//		SelectionAlgorithm noobEntropy = new FrequentEntropySelection(dataSet);
//		SelectionAlgorithm noobEntropy= new CommonFalseBasedSelection(dataSet);
//		for(int i = 0; i< dataSet.getNumDataItems(); i++){
//			//			if(i%4 ==0 || i%4 == 1 || i <= 5){
//			if(i<=5){
//				algors.put(i, noobEntropy);
//			}else{
//				algors.put(i, IG);
//			}
//		}
//
//		//Feedback
//		FeedbackSequence hitsSequence = new IGEMDependFeedbackSequence(100,algors);
//		hitsSequence.setRemover(remover);
//		hitsSequence.setData(dataSet);
//		hitsSequence.feedbackLoop();
//		//dataSet.prettyPrint();
//
//		///////////////////////Without delete source//////////////////////////////////////////
//		DataSet copiedDataSet = GALDataReader.readDatsetFromFolder(outputCopyFolder);
//		copiedDataSet.initTrustworthiness();
//		FeedbackSequence noDelSequence = new IGFeedbackSequence(copiedDataSet.getNumDataItems());
//		noDelSequence.setData(copiedDataSet);
//		noDelSequence.feedbackLoop();
//	}
//
//	@Test
//	public void expD100W15PartialMulti() throws Exception{
//		String inputFolder = "C:/Users/Asus/workspace/FeedbackProcess/input/RunningTime/L2D100W15";
//		String copyFolder = inputFolder + "Copy";
//
//		/*######################################### Without copy data sources ###############################################*/
//		//Load original data
//		DataSet dataSet = GALDataReader.readDatsetFromFolder(inputFolder);
//		dataSet.initTrustworthiness();
//		System.out.println("Init numSrc " + dataSet.getNumSources());
//
//		/////////////////////////////////Random/////////////////////////////////////////////////////////////////////////////////////
//		System.out.println("RanDommmmmmmmmmmm");
//		for(int i =0; i <5; i++){
//			DataSet woDataSet2 = GALDataReader.readDatsetFromFolder(inputFolder);
//			woDataSet2.initTrustworthiness();
//			FeedbackSequence woRandom = new BaseLineFeedbackSequence(woDataSet2.getNumDataItems());
//			woRandom.setData(woDataSet2);
//			woRandom.feedbackLoop();
//			System.out.println(Joiner.on('\t').join(woRandom.precisionList));
//			System.out.println(Joiner.on('\t').join(woRandom.entropyList));
//		}
//
//		/////////////////////////////////IG+EM/////////////////////////////////////////////////////////////////////////////////////
//		DataSet woDataSet = GALDataReader.readDatsetFromFolder(inputFolder);
//		woDataSet.initTrustworthiness();
//		FeedbackSequence woIGEM = new IGFeedbackSequence(woDataSet.getNumDataItems());
//		woIGEM.setData(woDataSet);
//		long start = System.currentTimeMillis();
//		woIGEM.feedbackLoop();
//		long end = System.currentTimeMillis();
//		System.out.println("Timeeeee: " + Long.toString(end - start));
//		System.out.println(Joiner.on('\t').join(woIGEM.precisionList));
//		System.out.println(Joiner.on('\t').join(woIGEM.entropyList));
//		System.out.println(Joiner.on('\t').join(woIGEM.itemIndices));
//
//		/*######################################### With copy data sources ###############################################*/
//		//Generate copied data
//		CopySimulator simulator = new CopySimulator(dataSet);
//		simulator.genCopierFromWorstSrc(COPY_FALSE_STRATEGY.COPY_PARTLY_CHANGE,COPY_TRUE_STRATEGY.COPY_PARTLY_CHANGE);
//
//		DataSet copyDataSet = null;
//		String outputCopyFolder = copyFolder + "/pcpc";
//		File f = new File(outputCopyFolder);
//		if(f.isDirectory()){
//			if(f.list().length == 0){
//				Utils.writeGALInputFromDataSet(dataSet, outputCopyFolder);
//			}
//			copyDataSet = GALDataReader.readDatsetFromFolder(outputCopyFolder);
//			copyDataSet.initTrustworthiness();
//		}
//
//		System.out.println("After add copies " + copyDataSet.getNumSources());
//		//		dataSet.prettyPrint();
//
//		/////////////////////With delete source/////////////////////////////////////////
//
//		//Deletion strategy
//		DependentSrcRemover remover = new NormalRemover();
//		remover.setThreshold(0.9);
//		remover.DEBUG = false;
//
//		//Selection strategy
//		Map<Integer,SelectionAlgorithm> algors = new HashMap<Integer,SelectionAlgorithm>();
//		SelectionAlgorithm IG = new IGEMDependSelection(copyDataSet,remover);
//		//		SelectionAlgorithm noobEntropy = new FrequentEntropySelection(dataSet);
//		SelectionAlgorithm noobEntropy= new CommonFalseBasedSelection(copyDataSet);
//		//Has set not delete src after 5 iteration
//		for(int i = 0; i< copyDataSet.getNumDataItems(); i++){
//			//			if(i%4 ==0 || i%4 == 1 || i <= 5){
//			if(i<=5){
//				algors.put(i, noobEntropy);
//			}else{
//				algors.put(i, IG);
//			}
//		}
//
//
//		DependentSrcRemover remover2 = new NormalRemover();
//		remover2.setThreshold(0.9);
//		remover2.DEBUG = true;
//
//		//Feedback
//		FeedbackSequence hitsSequence = new IGEMDependFeedbackSequence(100,algors);
//		hitsSequence.setRemover(remover2);
//		hitsSequence.setData(copyDataSet);
//		long startD = System.currentTimeMillis();
//		hitsSequence.feedbackLoop();
//		long endD = System.currentTimeMillis();
//		System.out.println("Timeeeee: " + Long.toString(endD - startD));
//		System.out.println("Delllllllllllllllllllllllllllllllllll");
//		System.out.println(Joiner.on('\t').join(hitsSequence.precisionList));
//		System.out.println(Joiner.on('\t').join(hitsSequence.entropyList));
//		System.out.println(Joiner.on('\t').join(hitsSequence.itemIndices));
//
//
//		///////////////////////Without delete source//////////////////////////////////////////
//		DataSet copiedDataSet = GALDataReader.readDatsetFromFolder(outputCopyFolder);
//		copiedDataSet.initTrustworthiness();
//		FeedbackSequence noDelSequence = new IGFeedbackSequence(copiedDataSet.getNumDataItems());
//		noDelSequence.setData(copiedDataSet);
//		long startND = System.currentTimeMillis();
//		noDelSequence.feedbackLoop();
//		long endND = System.currentTimeMillis();
//		System.out.println("Timeeeee: " + Long.toString(endND - startND));
//		System.out.println("Delllllllllllllllllllllllllllllllllll");
//		System.out.println("Nodelllllllllllllllllllllllllllllllllll");
//		System.out.println(Joiner.on('\t').join(noDelSequence.precisionList));
//		System.out.println(Joiner.on('\t').join(noDelSequence.entropyList));
//		System.out.println(Joiner.on('\t').join(noDelSequence.itemIndices));
//
//		///////////////////////Random with copy//////////////////////////////////////////
//		System.out.println("Randommmmmmmmmmmmmmmmmmmmmmm");
//		for(int i =0 ; i<5; i++){
//			DataSet copiedDataSet2 = GALDataReader.readDatsetFromFolder(outputCopyFolder);
//			copiedDataSet2.initTrustworthiness();
//			FeedbackSequence randomSequence = new BaseLineFeedbackSequence(copiedDataSet2.getNumDataItems());
//			randomSequence.setData(copiedDataSet2);
//			randomSequence.feedbackLoop();
//			System.out.println(Joiner.on('\t').join(randomSequence.precisionList));
//			System.out.println(Joiner.on('\t').join(randomSequence.entropyList));
//		}
//	}
}
