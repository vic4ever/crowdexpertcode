package feedback.strategy;

import org.testng.annotations.Test;

import data.model.DataSet;
import data.reader.GALDataReader;

public class TestIGFGLoop {
	
//	@Test
//	public void testDebug() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/testinput.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/goldtestinput.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/labeltestinput.txt", dataSet2);
//		dataSet.initTrustworthiness();
//		FeedbackSequence sequence = new FGLoopIGFeedbackSequence(3);
//		sequence.setData(dataSet);
//		sequence.feedbackLoop();
//	}
//
//	@Test
//	public void expLow() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/lFGlow.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gFGlow.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/cFGlow.txt", dataSet2);
//		dataSet.initTrustworthiness();
//		FeedbackSequence sequence = new FGLoopIGFeedbackSequence(20);
//		sequence.setData(dataSet);
//		sequence.feedbackLoop();
//	}
//	
//	@Test
//	public void expHigh() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/lFGhigh.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gFGhigh.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/cFGhigh.txt", dataSet2);
//		dataSet.initTrustworthiness();
//		FeedbackSequence sequence = new FGLoopIGFeedbackSequence(20);
//		sequence.setData(dataSet);
//		sequence.feedbackLoop();
//	}
//	
//	@Test
//	public void expLowNoob() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/lFGlow.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gFGlow.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/cFGlow.txt", dataSet2);
//		dataSet.initTrustworthiness();
//		FeedbackSequence sequence = new FGLoopIGFeedbackSequence(20);
//		sequence.setData(dataSet);
//		sequence.feedbackLoop();
//	}
//	
//	@Test
//	public void expHighNoob() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/lFGhigh.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gFGhigh.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/cFGhigh.txt", dataSet2);
//		dataSet.initTrustworthiness();
//		FeedbackSequence sequence = new FGLoopIGFeedbackSequence(20);
//		sequence.setData(dataSet);
//		sequence.feedbackLoop();
//	}
}
