package feedback.strategy;

import java.io.File;
import java.util.List;

import org.testng.annotations.Test;

import utils.Utils;
import data.model.DataSet;
import data.reader.GALDataReader;

public class TestPredefinedEM {
	@Test
	public void test1() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void test2() throws Exception{
//		dataSet.prettyPrint();
		List<Integer> original = Utils.makeSequence(0, 5);
		List<List<Integer>> listSequences = Utils.generatePerm(original);
		for (List<Integer> list : listSequences) {
			DataSet dataSet1 = GALDataReader.readGALLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6");
			DataSet dataSet = GALDataReader.readGALGoldLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6Gold", dataSet1);
			System.out.println("NEW SEQUENCEEEEEEEEEEEEEEEEEEEEE");
			System.out.println(list.toString());
			FeedbackSequence hitsSequence = new PredefinedFeedbackSequence(list);
			hitsSequence.setData(dataSet);
			hitsSequence.feedbackLoop();

		}
	}
	
	@Test
	public void testSmall10() throws Exception{
//		dataSet.prettyPrint();
		List<Integer> original = Utils.makeSequence(0, 10);
		List<List<Integer>> listSequences = Utils.generatePerm(original);
		for (List<Integer> list : listSequences) {
			DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels10.txt");
			DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold10.txt", dataSet1);
			System.out.println("NEW SEQUENCEEEEEEEEEEEEEEEEEEEEE");
			System.out.println(list.toString());
			FeedbackSequence hitsSequence = new PredefinedFeedbackSequence(list);
			hitsSequence.setData(dataSet);
			hitsSequence.feedbackLoop();

		}
	}
	
	@Test
	public void test6() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6Gold", dataSet1);
		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(5);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
}
