package feedback.strategy;

import org.testng.annotations.Test;

import data.model.DataSet;
import data.reader.GALDataReader;

public class TestRandomEM {
	@Test
	public void exp1() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp1BL() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold.txt", dataSet1);
		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new BaseLineFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp2() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels3.txt");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold3.txt", dataSet1);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void exp2BL() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels3.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold3.txt", dataSet1);
		DataSet dataSet =  GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/categories.txt", dataSet2);
//		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new BaseLineFeedbackSequence(100);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void test2() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6Gold", dataSet1);
		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(5);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
	
	@Test
	public void test6() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:\\Users\\Asus\\workspace\\SyntheticData\\data\\input6Gold", dataSet1);
		dataSet.prettyPrint();
		FeedbackSequence hitsSequence = new EMRandomFeedbackSequence(5);
		hitsSequence.setData(dataSet);
		hitsSequence.feedbackLoop();
		//dataSet.prettyPrint();
	}
}
