package feedback.strategy;

import org.testng.annotations.Test;

import data.model.DataSet;
import data.reader.GALDataReader;

public class TestRandomFG {
//	@Test
//	public void testSmall() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels15.txt");
//		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold15.txt", dataSet1);
//		dataSet.initTrustworthiness();
//		FeedbackSequence sequence = new FGRandomFeedbackSequence(10);
//		sequence.setData(dataSet);
//		sequence.feedbackLoop();
//	}
	
//	@Test
//	public void testSmallb() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/labels15b.txt");
//		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gold15b.txt", dataSet1);
//		dataSet.initTrustworthiness();
//		FeedbackSequence sequence = new FGRandomFeedbackSequence(10);
//		sequence.setData(dataSet);
//		sequence.feedbackLoop();
//	}
	
//	@Test
//	public void expLow() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/lFGlow.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gFGlow.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/cFGlow.txt", dataSet2);
//		dataSet.initTrustworthiness();
//		FeedbackSequence sequence = new FGRandomFeedbackSequence(20);
//		sequence.setData(dataSet);
//		sequence.feedbackLoop();
//	}
	
	@Test
	public void expLowBaseline() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/lFGlow.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gFGlow.txt", dataSet1);
		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/cFGlow.txt", dataSet2);
		dataSet.initTrustworthiness();
		FeedbackSequence sequence = new BaseLineFeedbackSequence(20);
		sequence.setData(dataSet);
		sequence.feedbackLoop();
	}
	
//	@Test
//	public void expHigh() throws Exception{
//		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/lFGhigh.txt");
//		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gFGhigh.txt", dataSet1);
//		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/cFGhigh.txt", dataSet2);
//		dataSet.initTrustworthiness();
//		FeedbackSequence sequence = new FGRandomFeedbackSequence(20);
//		sequence.setData(dataSet);
//		sequence.feedbackLoop();
//	}
//	
	@Test
	public void expHighBaseLine() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/lFGhigh.txt");
		DataSet dataSet2 = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/gFGhigh.txt", dataSet1);
		DataSet dataSet = GALDataReader.readCategories("C:/Users/Asus/workspace/FeedbackProcess/input/cFGhigh.txt", dataSet2);
		dataSet.initTrustworthiness();
		FeedbackSequence sequence = new BaseLineFeedbackSequence(20);
		sequence.setData(dataSet);
		sequence.feedbackLoop();
	}
	
	@Test
	public void testDebug() throws Exception{
		DataSet dataSet1 = GALDataReader.readGALLabels("C:/Users/Asus/workspace/FeedbackProcess/input/testinput.txt");
		DataSet dataSet = GALDataReader.readGALGoldLabels("C:/Users/Asus/workspace/FeedbackProcess/input/goldtestinput.txt", dataSet1);
		dataSet.initTrustworthiness();
		FeedbackSequence sequence = new BaseLineFeedbackSequence(3);
		sequence.setData(dataSet);
		sequence.feedbackLoop();
	}
}
