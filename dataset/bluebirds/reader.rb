require 'yaml'
require 'csv'
thing = YAML.load_file('labels.yaml')

question_id = -1;
question_hash = Hash.new
thing[39].each do |key, value|
	question_id = question_id + 1
	question_hash[key] = question_id
end

#p question_hash

worker_id = -1;
worker_hash = Hash.new
CSV.open("label.txt", "wb", {:col_sep => "\t"}) do |csv|
	CSV.open("worker.txt", "wb", {:col_sep => "\t"}) do |csv2|	
		thing.each do |worker,value|
			worker_id = worker_id + 1
			worker_hash[worker_id] = worker

			right = 0
			
			value.each do |question, answer|
				if(answer)
					csv << [worker_id, question_hash[question], 1]
					right = right + 1
				else
					csv << [worker_id, question_hash[question], 0]
					
				end
			end
			p right
			csv2 << [worker_id, right/108.0, -1]
		end
	end
end


gold = YAML.load_file('gt.yaml')


CSV.open("gold.txt", "wb", {:col_sep => "\t"}) do |csv|	
	gold.each do |question_key,value|
		if(value)
			csv << [question_hash[question_key], 1]
		else
			csv << [question_hash[question_key], 0]
		end
	end
end