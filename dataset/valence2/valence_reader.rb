require 'yaml'
require 'csv'

worker_id = -1;
worker_hash = Hash.new

question_id = -1
question_hash = Hash.new

gold_hash = Hash.new

worker_array = Array.new
question_array = Array.new
gold_array = Array.new

#{:headers => true, :header_converters => :symbol}
CSV.foreach("valence.standardized.tsv", quote_char: '"', col_sep: "\t", row_sep: :auto, headers: true, header_converters: :symbol) do |row|
	CSV.open("label.txt", "ab", {:col_sep => "\t"}) do |csv|
		CSV.open("worker.txt", "ab", {:col_sep => "\t"}) do |csv2|
		 	CSV.open("gold.txt", "ab", {:col_sep => "\t"}) do |csv3|

				worker = row[1]
				current_worker_id = -1;
				if(worker_hash.has_key? worker)
					current_worker_id = worker_hash[worker]
				else
					worker_id = worker_id +1
					current_worker_id = worker_id
					worker_hash[worker] = current_worker_id
					csv2 << [worker_id, 0.5, -1]
					# worker_array << worker_id.to_s + "\t" + "0.5" + "\t" + "-1"
				end
				#p current_worker_id

				question = row[2]
				current_question_id = -1
				if(question_hash.has_key? question)
					current_question_id = question_hash[question]
				else
					question_id = question_id + 1
					current_question_id = question_id
					question_hash[question] = current_question_id
				end

				answer = Integer(row[3])
				if(answer >= 0)
					csv << [current_worker_id , current_question_id, 1]
				else
					csv << [current_worker_id , current_question_id, 0]
				end

				rgold = Integer(row[4])
				gold = 0
				if(rgold >= 0)
					gold=1
				else
					gold=0
				end

				if(!gold_hash.has_key?(current_question_id))
					gold_hash[current_question_id] = gold
					csv3 << [current_question_id, gold]
				end
				
			end
		end
	 end
end