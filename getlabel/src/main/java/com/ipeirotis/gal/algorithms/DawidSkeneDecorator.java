package com.ipeirotis.gal.algorithms;

import java.util.HashMap;
import java.util.Map;

import utils.TupleR;

import com.ipeirotis.gal.core.Datum;
import com.ipeirotis.gal.core.Worker;
import com.ipeirotis.gal.decorator.Decorator;
import com.ipeirotis.gal.decorator.WorkerDecorator;

public class DawidSkeneDecorator extends Decorator<DawidSkene>{

	private DawidSkene ds; 
	public DawidSkeneDecorator(DawidSkene wrapped) {
		super(wrapped);
		this.ds = wrapped;
		// TODO Auto-generated constructor stub
	}

	public Map<Integer,Double> getSourceTrustworthiness(Worker.ClassificationMethod method) throws Exception{
		Map<Integer,Double> retVals = new HashMap<Integer, Double>();
		for (String wId : ds.getWorkers().keySet()) {
			Worker worker = ds.getWorkers().get(wId);
			WorkerDecorator decorator = new WorkerDecorator(worker);
			double trustW = 0;
			switch (method) {
			case DS_Soft_Estm:
				trustW = decorator.getExpectedCost();
				break;
			case DS_MinCost_Estm:
				trustW = decorator.getMinCost();
				break;
			case DS_MaxLikelihood_Estm:
				trustW = decorator.getMaxLikelihoodCost();
				break;
			case DS_Soft_Eval:
				trustW = decorator.getExpCostEval();
				break;
			case DS_MinCost_Eval:
				trustW = decorator.getMinCostEval();
				break;
			case DS_MaxLikelihood_Eval:
				trustW = decorator.getWeightedMaxLikelihoodCostEval();
				break;
			default:
				throw new Exception();
			}
			int sourceIndex = Integer.parseInt(wId);
			//System.out.println(sourceIndex + " "  + trustW);
			retVals.put(sourceIndex, trustW);
		}
		return retVals;
	}
	
	public Map<TupleR<Integer,Integer>,Double> getDataItemQuality(Datum.ClassificationMethod method){
		Map<TupleR<Integer,Integer>,Double> retVals = new HashMap<TupleR<Integer,Integer>, Double>();
		for (Datum datum : ds.getObjects().values()) {
			int dataItemIndex = Integer.parseInt(datum.getName());
			for (String cateString : ds.getCategories().keySet()) {
				double quality = datum.getCategoryProbability(method, cateString);
				TupleR<Integer,Integer> tuple = new TupleR<Integer, Integer>(dataItemIndex, Integer.parseInt(cateString));
				retVals.put(tuple, quality);
				//System.out.println(dataItemIndex + " "+ cateString + " " +quality);
			}
		}
		return retVals;
	}

}
