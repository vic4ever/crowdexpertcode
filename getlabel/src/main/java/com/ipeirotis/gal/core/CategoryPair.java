package com.ipeirotis.gal.core;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by Asus on 8/3/14.
 */
public class CategoryPair {

    private String from;
    private String to;

    public CategoryPair(String from, String to) {

        this.from = from;
        this.to = to;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((from == null) ? 0 : from.hashCode());
        result = prime * result + ((to == null) ? 0 : to.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof CategoryPair))
            return false;
        CategoryPair other = (CategoryPair) obj;
        if (from == null) {
            if (other.from != null)
                return false;
        } else if (!from.equals(other.from))
            return false;
        if (to == null) {
            if (other.to != null)
                return false;
        } else if (!to.equals(other.to))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("from", this.from)
                .append("to", this.to).toString();
    }

}
