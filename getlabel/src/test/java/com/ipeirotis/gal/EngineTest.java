package com.ipeirotis.gal;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.kohsuke.args4j.CmdLineParser;

import com.ipeirotis.gal.engine.Engine;
import com.ipeirotis.gal.engine.EngineContext;

public class EngineTest {
	private EngineContext ctx;

	private CmdLineParser parser;

	@Before
	public void before() {
		ctx = new EngineContext();

		parser = new CmdLineParser(ctx);
	}

	@Test
	public void testHappyPath() throws Exception {
		parser.parseArgument("--input data/AdultContent/test-unlabeled.txt --categories data/AdultContent/test-categories.txt"
				.split("\\s+"));

		assertEquals(ctx.getInputFile(), "data/AdultContent/test-unlabeled.txt");
		assertEquals(ctx.getCategoriesFile(), "data/AdultContent/test-categories.txt");
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void testHappyPath7() throws Exception {
		parser.parseArgument("--input data/test/test-unlabeled.txt --categories data/test/test-categories.txt"
				.split("\\s+"));

		//assertEquals(ctx.getInputFile(), "data/AdultContent/test-unlabeled.txt");
		//assertEquals(ctx.getCategoriesFile(), "data/AdultContent/test-categories.txt");
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void testHappyPath5() throws Exception {
		parser.parseArgument("--input C:/Users/Asus/workspace/SyntheticData/results/input/galinput.txt --categories C:/Users/Asus/workspace/SyntheticData/results/input/categories.txt"
				.split("\\s+"));

		assertEquals(ctx.getInputFile(), "C:/Users/Asus/workspace/SyntheticData/results/input/galinput.txt");
		assertEquals(ctx.getCategoriesFile(), "C:/Users/Asus/workspace/SyntheticData/results/input/categories.txt");
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void testHappyPath4() throws Exception {
		parser.parseArgument("--input data/AdultContent4/test-unlabeled.txt --categories data/AdultContent4/test-categories.txt"
				.split("\\s+"));

		assertEquals(ctx.getInputFile(), "data/AdultContent4/test-unlabeled.txt");
		assertEquals(ctx.getCategoriesFile(), "data/AdultContent4/test-categories.txt");
		
		Engine engine = new Engine(ctx);
		//System.out.println(engine.getLabels().size());
		//System.out.println(engine.getDs().getNumberOfObjects());
		//System.out.println(engine.getDs().getNumberOfObjects());
		engine.execute();
	}
	
	@Test
	public void testHappyPath2() throws Exception {
		parser.parseArgument("--input data/AdultContent3/test-unlabeled.txt --categories data/AdultContent3/test-categories.txt"
				.split("\\s+"));

		assertEquals(ctx.getInputFile(), "data/AdultContent3/test-unlabeled.txt");
		assertEquals(ctx.getCategoriesFile(), "data/AdultContent3/test-categories.txt");
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void testHappyPath32() throws Exception {
		parser.parseArgument("--input data/AdultContent3-HCOMP2010/labels.txt --categories data/AdultContent3-HCOMP2010/categories.txt"
				.split("\\s+"));

		//assertEquals(ctx.getInputFile(), "data/AdultContent3/test-unlabeled.txt");
		//assertEquals(ctx.getCategoriesFile(), "data/AdultContent3/test-categories.txt");
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void testHappyPath33() throws Exception {
		parser.parseArgument("--input C:/Users/Asus/workspace/FeedbackProcess/labelFile.txt --categories C:/Users/Asus/workspace/FeedbackProcess/catFile.txt"
				.split("\\s+"));

		//assertEquals(ctx.getInputFile(), "data/AdultContent3/test-unlabeled.txt");
		//assertEquals(ctx.getCategoriesFile(), "data/AdultContent3/test-categories.txt");
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	

	@Test
	public void testHappyPath6() throws Exception {
//		parser.parseArgument("--input C:/Users/Asus/workspace/SyntheticData/data/input6 --categories C:/Users/Asus/workspace/SyntheticData/data/cat6 --gold C:/Users/Asus/workspace/SyntheticData/data/input6Gold --eval C:/Users/Asus/workspace/SyntheticData/data/input6Eval"
//				.split("\\s+"));		
//		parser.parseArgument("--input C:/Users/Asus/workspace/SyntheticData/data/input6 --categories C:/Users/Asus/workspace/SyntheticData/data/cat6 --gold C:/Users/Asus/workspace/SyntheticData/data/input6Gold"
//				.split("\\s+"));
		parser.parseArgument("--input C:/Users/Asus/workspace/SyntheticData/data/input6 --categories C:/Users/Asus/workspace/SyntheticData/data/cat6"
				.split("\\s+"));
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void testHappyPath67() throws Exception {
		parser.parseArgument("--input C:/Users/Asus/workspace/SyntheticData/data/input6 --categories C:/Users/Asus/workspace/SyntheticData/data/cat6"
				.split("\\s+"));		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	
	
	@Test
	public void testMoreComplexPath() throws Exception {
		parser.parseArgument("--input data/BarzanMozafari/input.txt --categories data/BarzanMozafari/categories-prior.txt --eval data/BarzanMozafari/evaluation.txt"
				.split("\\s+"));
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void testFullCase() throws Exception {
		parser.parseArgument("--input data/BarzanMozafari/input.txt --categories data/BarzanMozafari/categories-prior.txt --gold data/BarzanMozafari/gold-30items.txt --cost data/BarzanMozafari/costs.txt --eval data/BarzanMozafari/evaluation.txt"
				.split("\\s+"));
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void test5KDataPriorGold() throws Exception {
		parser.parseArgument("--iterations 100 --verbose --input data/BarzanMozafari/input.txt --categories data/BarzanMozafari/categories-prior.txt --gold data/BarzanMozafari/gold-30items.txt --eval data/BarzanMozafari/evaluation.txt"
				.split("\\s+"));
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void test5KDataNoPriorGold() throws Exception {
		parser.parseArgument("--input data/BarzanMozafari/input.txt --categories data/BarzanMozafari/categories.txt --gold data/BarzanMozafari/gold-30items.txt --eval data/BarzanMozafari/evaluation.txt"
				.split("\\s+"));
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void test5KDataPriorNoGold() throws Exception {
		parser.parseArgument("--input data/BarzanMozafari/input.txt --categories data/BarzanMozafari/categories-prior.txt --eval data/BarzanMozafari/evaluation.txt"
				.split("\\s+"));
		
		Engine engine = new Engine(ctx);
		
		engine.execute();
	}
	
	@Test
	public void test5KDataNoPriorNoGold() throws Exception {
		parser.parseArgument("--input data/BarzanMozafari/input.txt --categories data/BarzanMozafari/categories.txt --eval data/BarzanMozafari/evaluation.txt"
				.split("\\s+"));
		
		Engine engine = new Engine(ctx);
		engine.execute();
		
		//engine.getDs().g
	}
	
}
