sch(s1).
sch(s2).
sch(s3).
attr(x1,s2).
attr(x2,s3).
attr(x3,s1).
attr(x4,s1).
attr(x5,s1).
attr(x6,s2).
attr(x7,s1).
attr(x8,s3).
attr(x9,s3).
attr(x10,s3).
attr(x11,s1).
attr(x12,s3).
attr(x13,s1).
attr(x14,s2).
attr(x15,s2).
attr(x16,s2).
attr(x17,s1).
attr(x18,s2).
attr(x19,s2).
attr(x20,s3).
attr(x21,s3).
attr(x22,s1).
attr(x23,s3).
attr(x24,s3).
attr(x25,s3).
attr(x26,s2).
attr(x27,s1).
attr(x28,s2).
attr(x29,s2).
attr(x30,s2).
attr(x31,s2).
attr(x32,s3).
attr(x33,s2).
attr(x34,s3).
attr(x35,s3).
attr(x36,s3).
cor(x2,x15).%0.52588606
cor(x24,x30).%0.7091225
cor(x24,x28).%0.61913407
cor(x24,x15).%0.58747214
cor(x24,x31).%0.5226463
cor(x24,x16).%0.5051547
cor(x21,x15).%0.51351905
cor(x36,x19).%0.59430265
cor(x36,x1).%0.5894387
cor(x36,x30).%0.5300588
cor(x36,x15).%0.5230523
cor(x35,x15).%0.5036317
cor(x22,x30).%0.68595994
cor(x22,x28).%0.5943232
cor(x22,x15).%0.5610977
cor(x17,x19).%0.70065385
cor(x17,x1).%0.6957899
cor(x17,x29).%0.642799
cor(x17,x18).%0.63793504
cor(x17,x14).%0.6245376
cor(x17,x26).%0.6239751
cor(x17,x33).%0.6196736
cor(x17,x6).%0.6191111
cor(x17,x30).%0.60191333
cor(x17,x15).%0.5949068
cor(x11,x15).%0.5036317
cor(x3,x2).%0.9914438
cor(x3,x35).%0.99012345
cor(x22,x24).%0.9914438
cor(x22,x23).%0.7623025
cor(x22,x8).%0.7059382
cor(x17,x36).%0.8571868
cor(x17,x20).%0.79364747
cor(x17,x10).%0.789373
cor(x17,x25).%0.75365496
cor(x17,x32).%0.6648171
cor(x17,x9).%0.660857
cor(x17,x12).%0.65825367
cor(x5,x21).%0.9914438
cor(x11,x35).%1.0
cor(x11,x2).%0.98169935
cor(x24,x22).%0.9914438
cor(x24,x13).%0.7623025
cor(x24,x4).%0.70551044
cor(x35,x11).%1.0
cor(x35,x3).%0.99012345
cor(x15,x24).%0.58747214
cor(x15,x2).%0.52588606
cor(x15,x36).%0.52305233
cor(x15,x21).%0.51351905
cor(x15,x34).%0.5045461
cor(x15,x35).%0.5036317
cor(x28,x24).%0.61913407
cor(x28,x34).%0.6025466
cor(x15,x17).%0.5949068
cor(x15,x22).%0.5610976
cor(x15,x7).%0.52422935
cor(x15,x11).%0.5036317
cor(x28,x22).%0.5943232
cor(x28,x27).%0.5793407
%s3	2
%s2	1
%s1	0
%x1	MDM.Record.MainAddress.CommType
%x2	SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.ControlData.NumberRangeGroupCode
%x3	CRM.PartnerInformation.GeneralInformation.GeneralData.ControlData.NumberRangeGroupCode
%x4	CRM.PartnerInformation.GeneralInformation.GeneralData.CentralData.FormOfAddressCode
%x5	CRM.PartnerInformation.GeneralInformation.GeneralData.GroupData.GroupType
%x6	MDM.Record.MainAddress.TaxJurCode
%x7	CRM.PartnerInformation.TaxNumberInformation.TaxNumber.TaxNumberDataKey.TaxTypeCode
%x8	SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.CentralData.FormOfAddressCode
%x9	SRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.TaxJurisdictionCode
%x11	CRM.PartnerInformation.GeneralInformation.ControlData.NumberRangeGroupCode
%x10	SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CountryCode
%x12	SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.EMailAddress
%x13	CRM.PartnerInformation.GeneralInformation.GeneralData.CentralData.LanguageCode
%x14	MDM.Record.Addresses.AddressUsageCode
%x15	MDM.Record.GroupTypeCode
%x16	MDM.Record.PartnerNumber
%x17	CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CommunicationTypeCode
%x18	MDM.Record.MainAddress.PostalCode
%x19	MDM.Record.Addresses.CommType
%x20	SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode
%x21	SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.GroupData.GroupType
%x22	CRM.PartnerInformation.GeneralInformation.GeneralData.CentralData.PartnerTypeCode
%x23	SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.CentralData.LanguageCode
%x24	SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.CentralData.PartnerTypeCode
%x26	MDM.Record.Addresses.TaxJurCode
%x25	SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.StreetPostalCode
%x27	CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.AcademicTitleCode1
%x29	MDM.Record.Addresses.PostalCode
%x28	MDM.Record.PartnerTitleCode
%x30	MDM.Record.PartnerTypeCode
%x31	MDM.Record.GenderCode
%x32	SRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.TransportationZoneCode
%x33	MDM.Record.MainAddress.AddressUsageCode
%x34	SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.PersonData.AcademicTitleCode1
%x36	SRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CommunicationTypeCode
%x35	SRM.PartnerInformation.GeneralInformation.ControlData.NumberRangeGroupCode
