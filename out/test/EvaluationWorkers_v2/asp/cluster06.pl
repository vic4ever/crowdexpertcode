sch(s1).
sch(s2).
sch(s3).
attr(x1,s3).
attr(x2,s3).
attr(x3,s2).
attr(x4,s1).
attr(x5,s2).
attr(x6,s2).
attr(x7,s2).
attr(x8,s2).
attr(x9,s3).
attr(x10,s1).
attr(x11,s2).
attr(x12,s2).
attr(x13,s2).
attr(x14,s3).
attr(x15,s1).
attr(x16,s1).
attr(x17,s2).
attr(x18,s1).
attr(x19,s1).
attr(x20,s1).
attr(x21,s1).
attr(x22,s2).
attr(x23,s1).
attr(x24,s2).
attr(x25,s2).
attr(x26,s1).
attr(x27,s3).
attr(x28,s2).
attr(x29,s3).
cor(x10,x17).%0.620312
cor(x10,x7).%0.6167127
cor(x10,x6).%0.61298144
cor(x10,x13).%0.60938215
cor(x10,x3).%0.5271283
cor(x16,x11).%0.7320385
cor(x16,x28).%0.7270764
cor(x16,x25).%0.6815441
cor(x16,x5).%0.67614347
cor(x16,x24).%0.6707166
cor(x16,x8).%0.66428405
cor(x16,x22).%0.65888345
cor(x16,x17).%0.64216185
cor(x16,x7).%0.6367612
cor(x16,x6).%0.627338
cor(x20,x17).%0.62010515
cor(x20,x7).%0.61750185
cor(x20,x6).%0.60057324
cor(x20,x13).%0.59764373
cor(x23,x17).%0.62179554
cor(x23,x7).%0.6181962
cor(x23,x6).%0.6000143
cor(x23,x13).%0.5964716
cor(x27,x17).%0.7644001
cor(x27,x7).%0.75949043
cor(x27,x6).%0.7455822
cor(x27,x13).%0.74067265
cor(x27,x12).%0.6623889
cor(x27,x3).%0.6574793
cor(x27,x8).%0.65633416
cor(x27,x22).%0.6514245
cor(x27,x25).%0.64164865
cor(x27,x24).%0.6389408
cor(x29,x11).%0.7460649
cor(x29,x28).%0.74155396
cor(x29,x25).%0.6957261
cor(x29,x5).%0.6908164
cor(x29,x24).%0.68494165
cor(x29,x8).%0.6786844
cor(x29,x22).%0.6737748
cor(x29,x17).%0.6556408
cor(x29,x7).%0.65073115
cor(x29,x12).%0.6413829
cor(x16,x29).%0.963496
cor(x16,x1).%0.8045465
cor(x16,x14).%0.7857685
cor(x16,x9).%0.7857685
cor(x16,x27).%0.76190805
cor(x16,x2).%0.75376
cor(x10,x27).%0.81312716
cor(x10,x2).%0.70998085
cor(x20,x27).%0.8141092
cor(x20,x2).%0.70618904
cor(x23,x27).%0.8176365
cor(x23,x2).%0.7046779
cor(x27,x23).%0.8176365
cor(x27,x20).%0.8141092
cor(x27,x10).%0.81312716
cor(x27,x19).%0.76897943
cor(x27,x16).%0.76190805
cor(x27,x21).%0.7544583
cor(x27,x4).%0.7437602
cor(x27,x18).%0.7097663
cor(x27,x26).%0.706239
cor(x27,x15).%0.70525694
cor(x17,x27).%0.7644001
cor(x17,x2).%0.7419144
cor(x17,x29).%0.6556408
cor(x17,x14).%0.64751035
cor(x17,x1).%0.64070827
cor(x17,x9).%0.6302946
cor(x17,x16).%0.64216185
cor(x17,x19).%0.6331499
cor(x17,x21).%0.6272334
cor(x17,x23).%0.62179554
cor(x17,x10).%0.620312
cor(x17,x20).%0.62010515
cor(x17,x4).%0.61620396
cor(x17,x18).%0.59705627
cor(x17,x15).%0.59557277
cor(x17,x26).%0.5953659
cor(x7,x16).%0.6367612
cor(x7,x23).%0.6181962
cor(x7,x20).%0.61750185
cor(x7,x10).%0.6167127
cor(x7,x18).%0.593457
cor(x7,x26).%0.59276265
cor(x7,x15).%0.5919735
%s3	2
%s2	1
%s1	0
%x2	SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CompleteTelephoneNumber
%x1	SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RoomID
%x3	MDM.Record.MainAddress.MobilePhoneNumber
%x4	CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.BuildingID
%x7	MDM.Record.MainAddress.FaxCompleteNumber
%x6	MDM.Record.Addresses.PhoneCompleteNumber
%x5	MDM.Record.MainAddress.RoomNumber
%x8	MDM.Record.Addresses.HouseNumber
%x9	SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.BuildingID
%x10	CRM.PartnerInformation.AddressInformation.Address.AddressData.CommunicationInformation.TelephoneInformation.Telephone.Contact.ContactData.CompleteFacsimileNumber
%x11	MDM.Record.Addresses.Floor
%x12	MDM.Record.Addresses.MobilePhoneNumber
%x13	MDM.Record.MainAddress.PhoneCompleteNumber
%x14	SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID
%x15	CRM.PartnerInformation.AddressInformation.Address.AddressData.CommunicationInformation.TelephoneInformation.Telephone.Contact.ContactData.CompleteTelephoneNumber
%x16	CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.FloorID
%x17	MDM.Record.Addresses.FaxCompleteNumber
%x18	CRM.PartnerInformation.AddressInformation.Address.AddressData.CommunicationInformation.FacsimileInformation.Facsimile.Contact.ContactData.CompleteTelephoneNumber
%x19	CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID
%x20	CRM.PartnerInformation.AddressInformation.Address.AddressData.CommunicationInformation.SMTPInformation.SMTP.Contact.ContactData.CompleteFacsimileNumber
%x21	CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RoomID
%x22	MDM.Record.MainAddress.HouseNumber
%x23	CRM.PartnerInformation.AddressInformation.Address.AddressData.CommunicationInformation.FacsimileInformation.Facsimile.Contact.ContactData.CompleteFacsimileNumber
%x24	MDM.Record.Addresses.AddressNumber
%x25	MDM.Record.Addresses.RoomNumber
%x26	CRM.PartnerInformation.AddressInformation.Address.AddressData.CommunicationInformation.SMTPInformation.SMTP.Contact.ContactData.CompleteTelephoneNumber
%x27	SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CompleteFacsimileNumber
%x28	MDM.Record.MainAddress.Floor
%x29	SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.FloorID
