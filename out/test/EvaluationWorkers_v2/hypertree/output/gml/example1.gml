graph [

  directed 0

  node [
    id 1
    label "{E4}    {V2, V7, V5}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 2
    label "{E5}    {V8, V9}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 3
    label "{E1, E2}    {V1, V2, V3, V6, V7}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  node [
    id 4
    label "{E3}    {V1, V6, V4}"
    vgj [
      labelPosition "in"
      shape "Rectangle"
    ]
  ]

  edge [
    source 1
    target 2
  ]

  edge [
    source 1
    target 3
  ]

  edge [
    source 3
    target 4
  ]

]
