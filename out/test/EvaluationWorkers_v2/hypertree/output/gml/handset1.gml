Creator	"yFiles"
Version	"2.8"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"{R1, R3}
{a1b2, a2b2, a1b1, b1c1, a2c1}"
		graphics
		[
			x	186.8475341796875
			y	15.0
			w	174.17919921875
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"{R1, R3}
{a1b2, a2b2, a1b1, b1c1, a2c1}"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	1
		label	"{R2, R5}
{a2b2, a1b1, a2b1}"
		graphics
		[
			x	186.8475341796875
			y	78.92639923095703
			w	107.41439819335938
			h	30.0
			type	"rectangle"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"{R2, R5}
{a2b2, a1b1, a2b1}"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	edge
	[
		source	0
		target	1
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	1.0
			yTarget	-1.0
		]
	]
]
