Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-1-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.CountryOfOrigin-CORR-MDM.Record.CountryOfOrigin	0.99469	0.00531	
0-TO-2-attrib-CRM.PartnerHeader.ObjectInstance.Partner.schemeAgencyID-CORR-SRM.PartnerHeader.ObjectInstance.Partner.schemeAgencyID	0.06339	0.93661	
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.StreetName-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.StreetName	0.96999	0.03001	
0-TO-2-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.CentralData.PartnerTypeCode-CORR-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.CentralData.PartnerTypeCode	0.01657	0.98343	
1-TO-0-attrib-MDM.Record.CountryOfOrigin-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.CountryOfOrigin	0.39462	0.60538	
1-TO-0-attrib-MDM.Record.MainAddress.Street-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.StreetName	0.96999	0.03001	
1-TO-2-attrib-MDM.Record.VATNumber-CORR-SRM.PartnerInformation.TaxNumberInformation.TaxNumber.TaxNumberDataKey.TaxNumber	0.03207	0.96793	
2-TO-0-attrib-SRM.PartnerHeader.ObjectInstance.Partner.schemeAgencyID-CORR-CRM.PartnerHeader.ObjectInstance.Partner.schemeAgencyID	0.99218	0.00782	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.CentralData.PartnerTypeCode-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.CentralData.PartnerTypeCode	0.02093	0.97907	
2-TO-1-attrib-SRM.PartnerInformation.TaxNumberInformation.TaxNumber.TaxNumberDataKey.TaxNumber-CORR-MDM.Record.TaxNumbers.TNumber	0.06339	0.93661	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.StreetName-CORR-MDM.Record.Addresses.Street	0.98548	0.01452	
