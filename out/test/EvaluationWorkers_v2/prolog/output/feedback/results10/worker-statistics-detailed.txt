Worker: user0
Error Rate: 42.1%
Quality (Expected): 3%
Quality (Optimized): 16%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=78.036%	P[yes->no]=21.964%	
P[no->yes]=62.236%	P[no->no]=37.764%	

Worker: user1
Error Rate: 17.16%
Quality (Expected): 45%
Quality (Optimized): 66%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=93.056%	P[yes->no]=6.944%	
P[no->yes]=27.377%	P[no->no]=72.623%	

Worker: user2
Error Rate: 33.58%
Quality (Expected): 17%
Quality (Optimized): 33%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=96.454%	P[yes->no]=3.546%	
P[no->yes]=63.616%	P[no->no]=36.384%	

Worker: user3
Error Rate: 65.67%
Quality (Expected): 15%
Quality (Optimized): 31%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=63.555%	P[yes->no]=36.445%	
P[no->yes]=94.893%	P[no->no]=5.107%	

Worker: user4
Error Rate: 34.12%
Quality (Expected): 18%
Quality (Optimized): 32%
Number of Annotations: 6
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.377%	P[yes->no]=0.623%	
P[no->yes]=67.62%	P[no->no]=32.38%	

Worker: user5
Error Rate: 7.31%
Quality (Expected): 73%
Quality (Optimized): 85%
Number of Annotations: 6
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=94.302%	P[yes->no]=5.698%	
P[no->yes]=8.916%	P[no->no]=91.084%	

Worker: user6
Error Rate: 51.22%
Quality (Expected): 0%
Quality (Optimized): 2%
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=48.78%	P[yes->no]=51.22%	
P[no->yes]=51.222%	P[no->no]=48.778%	

Worker: user7
Error Rate: 0.13%
Quality (Expected): 100%
Quality (Optimized): 100%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.75%	P[yes->no]=0.25%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user8
Error Rate: 0.33%
Quality (Expected): 99%
Quality (Optimized): 99%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.335%	P[yes->no]=0.665%	
P[no->yes]=0.0%	P[no->no]=100.0%	

