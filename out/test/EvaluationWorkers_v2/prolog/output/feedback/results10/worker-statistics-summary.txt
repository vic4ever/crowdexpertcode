Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	42.1%	3%	16%	10	0
user1	17.16%	45%	66%	10	0
user2	33.58%	17%	33%	10	0
user3	65.67%	15%	31%	10	0
user4	34.12%	18%	32%	6	0
user5	7.31%	73%	85%	6	0
user6	51.22%	0%	2%	4	0
user7	0.13%	100%	100%	2	0
user8	0.33%	99%	99%	2	0
