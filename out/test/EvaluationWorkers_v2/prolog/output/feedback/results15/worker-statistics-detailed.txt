Worker: user0
Error Rate: 50.17%
Quality (Expected): 0%
Quality (Optimized): 0%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=72.598%	P[yes->no]=27.402%	
P[no->yes]=72.942%	P[no->no]=27.058%	

Worker: user1
Error Rate: 15.83%
Quality (Expected): 51%
Quality (Optimized): 68%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.237%	P[yes->no]=0.763%	
P[no->yes]=30.906%	P[no->no]=69.094%	

Worker: user10
Error Rate: 62.75%
Quality (Expected): 15%
Quality (Optimized): 26%
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=74.492%	P[yes->no]=25.508%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user11
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user12
Error Rate: 97.66%
Quality (Expected): 91%
Quality (Optimized): 95%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.633%	P[yes->no]=99.367%	
P[no->yes]=95.948%	P[no->no]=4.052%	

Worker: user2
Error Rate: 57.21%
Quality (Expected): 8%
Quality (Optimized): 14%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=85.532%	P[yes->no]=14.468%	
P[no->yes]=99.944%	P[no->no]=0.056%	

Worker: user3
Error Rate: 52.07%
Quality (Expected): 0%
Quality (Optimized): 4%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=71.18%	P[yes->no]=28.82%	
P[no->yes]=75.316%	P[no->no]=24.684%	

Worker: user4
Error Rate: 45.16%
Quality (Expected): 1%
Quality (Optimized): 10%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=85.454%	P[yes->no]=14.546%	
P[no->yes]=75.771%	P[no->no]=24.229%	

Worker: user5
Error Rate: 7.94%
Quality (Expected): 72%
Quality (Optimized): 84%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.638%	P[yes->no]=0.362%	
P[no->yes]=15.518%	P[no->no]=84.482%	

Worker: user6
Error Rate: 70.3%
Quality (Expected): 24%
Quality (Optimized): 41%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=57.107%	P[yes->no]=42.893%	
P[no->yes]=97.71%	P[no->no]=2.29%	

Worker: user7
Error Rate: 34.35%
Quality (Expected): 10%
Quality (Optimized): 31%
Number of Annotations: 6
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=57.19%	P[yes->no]=42.81%	
P[no->yes]=25.881%	P[no->no]=74.119%	

Worker: user8
Error Rate: 80.06%
Quality (Expected): 37%
Quality (Optimized): 60%
Number of Annotations: 5
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=25.174%	P[yes->no]=74.826%	
P[no->yes]=85.299%	P[no->no]=14.701%	

Worker: user9
Error Rate: 63.1%
Quality (Expected): 15%
Quality (Optimized): 26%
Number of Annotations: 5
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=73.799%	P[yes->no]=26.201%	
P[no->yes]=100.0%	P[no->no]=0.0%	

