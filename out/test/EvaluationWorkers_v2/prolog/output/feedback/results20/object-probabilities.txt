Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode-CORR-MDM.Record.Addresses.Region	0.04485	0.95515	
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CareOfName-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CareOfName	0.01476	0.98524	
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID	0.94861	0.05139	
1-TO-0-attrib-MDM.Record.Addresses.HouseNumber-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID	0.05952	0.94048	
1-TO-0-attrib-MDM.Record.MainAddress.CareOfName-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CareOfName	0.00926	0.99074	
1-TO-2-attrib-MDM.Record.MainAddress.EMailAddress-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.EMailAddress	0.99746	0.00254	
1-TO-2-attrib-MDM.Record.MainAddress.Region-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode	0.02929	0.97071	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RegionCode	0.99525	0.00475	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.CareOfName-CORR-MDM.Record.Addresses.CareOfName	0.95046	0.04954	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.EMailAddress-CORR-MDM.Record.Addresses.EMailAddress	0.95327	0.04673	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.HouseID-CORR-MDM.Record.Addresses.HouseNumber	0.94861	0.05139	
