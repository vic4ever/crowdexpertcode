Worker: user0
Error Rate: 59.26%
Quality (Expected): 4%
Quality (Optimized): 19%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=64.716%	P[yes->no]=35.284%	
P[no->yes]=83.228%	P[no->no]=16.772%	

Worker: user1
Error Rate: 43.5%
Quality (Expected): 2%
Quality (Optimized): 13%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=69.067%	P[yes->no]=30.933%	
P[no->yes]=56.075%	P[no->no]=43.925%	

Worker: user10
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user11
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user12
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user13
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user14
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user15
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user16
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user17
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user18
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user19
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user2
Error Rate: 56.19%
Quality (Expected): 5%
Quality (Optimized): 12%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=85.769%	P[yes->no]=14.231%	
P[no->yes]=98.144%	P[no->no]=1.856%	

Worker: user20
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user21
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user22
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user23
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user24
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user25
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user26
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user27
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user28
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user29
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user3
Error Rate: 45.34%
Quality (Expected): 1%
Quality (Optimized): 9%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=67.569%	P[yes->no]=32.431%	
P[no->yes]=58.257%	P[no->no]=41.743%	

Worker: user30
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user31
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user32
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user33
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user34
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user35
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user36
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user37
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user38
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user39
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user4
Error Rate: 21.32%
Quality (Expected): 34%
Quality (Optimized): 57%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=69.942%	P[yes->no]=30.058%	
P[no->yes]=12.577%	P[no->no]=87.423%	

Worker: user40
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user41
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user42
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user43
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user44
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user45
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user46
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user47
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user48
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user49
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user5
Error Rate: 42.43%
Quality (Expected): 6%
Quality (Optimized): 15%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=97.169%	P[yes->no]=2.831%	
P[no->yes]=82.024%	P[no->no]=17.976%	

Worker: user50
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user51
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user52
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user53
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user54
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user55
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user56
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user57
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user58
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user59
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user6
Error Rate: 58.38%
Quality (Expected): 8%
Quality (Optimized): 17%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=82.196%	P[yes->no]=17.804%	
P[no->yes]=98.956%	P[no->no]=1.044%	

Worker: user60
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user61
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user62
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user63
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user64
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user65
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user66
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user67
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user68
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user69
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user7
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 7
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user70
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user71
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user72
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user73
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user74
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user75
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user76
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user77
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user78
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user79
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user8
Error Rate: 9.72%
Quality (Expected): 65%
Quality (Optimized): 81%
Number of Annotations: 6
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=93.16%	P[yes->no]=6.84%	
P[no->yes]=12.598%	P[no->no]=87.402%	

Worker: user80
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user81
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user82
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user83
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user84
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user85
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user86
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user87
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user88
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user89
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user9
Error Rate: 5.81%
Quality (Expected): 79%
Quality (Optimized): 88%
Number of Annotations: 5
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=98.964%	P[yes->no]=1.036%	
P[no->yes]=10.584%	P[no->no]=89.416%	

Worker: user90
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user91
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user92
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user93
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user94
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user95
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user96
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user97
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user98
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user99
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

