Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.DistrictName-CORR-MDM.Record.Addresses.District	0.99357	0.00643	
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.DistrictName-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.DistrictName	0.98692	0.01308	
0-TO-2-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.CentralData.FormOfAddressCode-CORR-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.CentralData.FormOfAddressCode	1.0E-5	0.99999	
1-TO-0-attrib-MDM.Record.LegalFormCode-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.CentralData.FormOfAddressCode	0.00334	0.99666	
1-TO-0-attrib-MDM.Record.MainAddress.District-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.DistrictName	1.0	0.0	
1-TO-2-attrib-MDM.Record.MainAddress.RoomNumber-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RoomID	0.05373	0.94627	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.CentralData.FormOfAddressCode-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.CentralData.FormOfAddressCode	5.3E-4	0.99947	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.DistrictName-CORR-MDM.Record.Addresses.District	0.98524	0.01476	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.RoomID-CORR-MDM.Record.Addresses.RoomNumber	0.96891	0.03109	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.CentralData.FormOfAddressCode-CORR-MDM.Record.LegalFormCode	0.00254	0.99746	
