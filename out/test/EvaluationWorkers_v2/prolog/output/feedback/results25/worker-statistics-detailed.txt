Worker: user0
Error Rate: 52.07%
Quality (Expected): 0%
Quality (Optimized): 4%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=68.241%	P[yes->no]=31.759%	
P[no->yes]=72.374%	P[no->no]=27.626%	

Worker: user1
Error Rate: 20.88%
Quality (Expected): 38%
Quality (Optimized): 58%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=95.791%	P[yes->no]=4.209%	
P[no->yes]=37.558%	P[no->no]=62.442%	

Worker: user2
Error Rate: 68.0%
Quality (Expected): 16%
Quality (Optimized): 36%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=53.767%	P[yes->no]=46.233%	
P[no->yes]=89.766%	P[no->no]=10.234%	

Worker: user3
Error Rate: 45.4%
Quality (Expected): 1%
Quality (Optimized): 9%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=83.911%	P[yes->no]=16.089%	
P[no->yes]=74.711%	P[no->no]=25.289%	

Worker: user4
Error Rate: 53.76%
Quality (Expected): 1%
Quality (Optimized): 8%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=66.793%	P[yes->no]=33.207%	
P[no->yes]=74.305%	P[no->no]=25.695%	

Worker: user5
Error Rate: 67.26%
Quality (Expected): 12%
Quality (Optimized): 35%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=39.835%	P[yes->no]=60.165%	
P[no->yes]=74.346%	P[no->no]=25.654%	

Worker: user6
Error Rate: 9.74%
Quality (Expected): 66%
Quality (Optimized): 81%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=95.445%	P[yes->no]=4.555%	
P[no->yes]=14.918%	P[no->no]=85.082%	

Worker: user7
Error Rate: 12.7%
Quality (Expected): 58%
Quality (Optimized): 75%
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=98.243%	P[yes->no]=1.757%	
P[no->yes]=23.64%	P[no->no]=76.36%	

