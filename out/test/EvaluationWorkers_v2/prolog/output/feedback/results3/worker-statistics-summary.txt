Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	31.24%	23%	38%	11	0
user1	18.25%	41%	63%	11	0
user2	17.62%	44%	65%	11	0
user3	28.79%	24%	42%	11	0
user4	30.54%	20%	39%	3	0
user5	19.27%	39%	61%	2	0
user6	50.0%	---	---	1	0
user7	50.0%	---	---	1	0
user8	50.0%	---	---	1	0
user9	50.0%	---	---	1	0
