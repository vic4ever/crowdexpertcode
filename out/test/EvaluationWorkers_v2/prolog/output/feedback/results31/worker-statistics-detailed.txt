Worker: user0
Error Rate: 39.88%
Quality (Expected): 5%
Quality (Optimized): 20%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=80.028%	P[yes->no]=19.972%	
P[no->yes]=59.796%	P[no->no]=40.204%	

Worker: user1
Error Rate: 49.47%
Quality (Expected): 0%
Quality (Optimized): 1%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=40.52%	P[yes->no]=59.48%	
P[no->yes]=39.47%	P[no->no]=60.53%	

Worker: user10
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 3
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user11
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 3
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user12
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 3
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user13
Error Rate: 98.47%
Quality (Expected): 94%
Quality (Optimized): 97%
Number of Annotations: 3
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=2.976%	P[yes->no]=97.024%	
P[no->yes]=99.915%	P[no->no]=0.085%	

Worker: user14
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 3
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user15
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 3
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user16
Error Rate: 0.06%
Quality (Expected): 100%
Quality (Optimized): 100%
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=0.112%	P[no->no]=99.888%	

Worker: user17
Error Rate: 0.42%
Quality (Expected): 98%
Quality (Optimized): 99%
Number of Annotations: 3
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.153%	P[yes->no]=0.847%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user18
Error Rate: 0.02%
Quality (Expected): 100%
Quality (Optimized): 100%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.999%	P[yes->no]=0.001%	
P[no->yes]=0.044%	P[no->no]=99.956%	

Worker: user19
Error Rate: 0.0%
Quality (Expected): 100%
Quality (Optimized): 100%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=0.008%	P[no->no]=99.992%	

Worker: user2
Error Rate: 69.23%
Quality (Expected): 23%
Quality (Optimized): 38%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=60.992%	P[yes->no]=39.008%	
P[no->yes]=99.451%	P[no->no]=0.549%	

Worker: user3
Error Rate: 40.42%
Quality (Expected): 4%
Quality (Optimized): 19%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=59.499%	P[yes->no]=40.501%	
P[no->yes]=40.334%	P[no->no]=59.666%	

Worker: user4
Error Rate: 31.06%
Quality (Expected): 15%
Quality (Optimized): 38%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=78.611%	P[yes->no]=21.389%	
P[no->yes]=40.725%	P[no->no]=59.275%	

Worker: user5
Error Rate: 12.27%
Quality (Expected): 59%
Quality (Optimized): 75%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=96.289%	P[yes->no]=3.711%	
P[no->yes]=20.82%	P[no->no]=79.18%	

Worker: user6
Error Rate: 32.51%
Quality (Expected): 13%
Quality (Optimized): 35%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=59.975%	P[yes->no]=40.025%	
P[no->yes]=25.004%	P[no->no]=74.996%	

Worker: user7
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 8
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user8
Error Rate: 75.0%
Quality (Expected): 33%
Quality (Optimized): 50%
Number of Annotations: 6
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=49.995%	P[no->no]=50.005%	

Worker: user9
Error Rate: 62.44%
Quality (Expected): 14%
Quality (Optimized): 25%
Number of Annotations: 6
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=75.035%	P[yes->no]=24.965%	
P[no->yes]=99.918%	P[no->no]=0.082%	

