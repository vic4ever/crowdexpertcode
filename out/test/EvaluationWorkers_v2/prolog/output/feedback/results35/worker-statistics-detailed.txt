Worker: user0
Error Rate: 37.95%
Quality (Expected): 6%
Quality (Optimized): 24%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=65.841%	P[yes->no]=34.159%	
P[no->yes]=41.732%	P[no->no]=58.268%	

Worker: user1
Error Rate: 54.97%
Quality (Expected): 1%
Quality (Optimized): 10%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=49.918%	P[yes->no]=50.082%	
P[no->yes]=59.862%	P[no->no]=40.138%	

Worker: user10
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user11
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user12
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user13
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user14
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user15
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user16
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user17
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user18
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user19
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user2
Error Rate: 53.12%
Quality (Expected): 0%
Quality (Optimized): 6%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=51.627%	P[yes->no]=48.373%	
P[no->yes]=57.863%	P[no->no]=42.137%	

Worker: user20
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user21
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user22
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user23
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user24
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user25
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user26
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user27
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user28
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user29
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user3
Error Rate: 25.24%
Quality (Expected): 25%
Quality (Optimized): 50%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=77.75%	P[yes->no]=22.25%	
P[no->yes]=28.24%	P[no->no]=71.76%	

Worker: user30
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user31
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user32
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user33
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user34
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user35
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user36
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user37
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user38
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user39
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user4
Error Rate: 26.69%
Quality (Expected): 22%
Quality (Optimized): 47%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=77.043%	P[yes->no]=22.957%	
P[no->yes]=30.414%	P[no->no]=69.586%	

Worker: user40
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user41
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user42
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user43
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user44
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user45
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user46
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user47
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user48
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user49
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user5
Error Rate: 27.16%
Quality (Expected): 21%
Quality (Optimized): 46%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=76.795%	P[yes->no]=23.205%	
P[no->yes]=31.123%	P[no->no]=68.877%	

Worker: user50
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user51
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user52
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user53
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user54
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user55
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user56
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user57
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user58
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user59
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user6
Error Rate: 19.47%
Quality (Expected): 38%
Quality (Optimized): 61%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=85.11%	P[yes->no]=14.89%	
P[no->yes]=24.059%	P[no->no]=75.941%	

Worker: user60
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user61
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user62
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user63
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user64
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user65
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user66
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user67
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user68
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user69
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user7
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user70
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user71
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user72
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user73
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user74
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user75
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user76
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user77
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user78
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user79
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user8
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user80
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user81
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user82
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user83
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user84
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user85
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user86
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user87
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user88
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user89
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user9
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user90
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user91
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user92
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user93
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user94
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user95
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user96
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user97
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user98
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user99
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

