Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-1-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.ControlData.CategoryCode-CORR-MDM.Record.CategoryCode	0.99999	1.0E-5	
0-TO-1-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.NationalityCode-CORR-MDM.Record.NationalityCode	1.0	0.0	
0-TO-2-attrib-CRM.PartnerInformation.GeneralInformation.GeneralData.GroupData.GroupType-CORR-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.GroupData.GroupType	0.97922	0.02078	
1-TO-2-attrib-MDM.Record.AcademicTitleCode-CORR-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.PersonData.AcademicTitleCode1	0.07316	0.92684	
1-TO-2-attrib-MDM.Record.CategoryCode-CORR-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.ControlData.CategoryCode	0.95698	0.04302	
1-TO-2-attrib-MDM.Record.NationalityCode-CORR-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.PersonData.NationalityCode	4.9E-4	0.99951	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.ControlData.CategoryCode-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.ControlData.CategoryCode	0.97922	0.02078	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.GroupData.GroupType-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.GroupData.GroupType	0.95698	0.04302	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.PersonData.NationalityCode-CORR-CRM.PartnerInformation.GeneralInformation.GeneralData.PersonData.NationalityCode	5.0E-5	0.99995	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.GeneralInformation.GeneralData.PersonData.AcademicTitleCode1-CORR-MDM.Record.AcademicTitleCode	0.06658	0.93342	
