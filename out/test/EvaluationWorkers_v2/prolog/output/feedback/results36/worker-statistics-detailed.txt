Worker: user0
Error Rate: 32.14%
Quality (Expected): 17%
Quality (Optimized): 36%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=93.831%	P[yes->no]=6.169%	
P[no->yes]=58.107%	P[no->no]=41.893%	

Worker: user1
Error Rate: 20.78%
Quality (Expected): 34%
Quality (Optimized): 58%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=77.029%	P[yes->no]=22.971%	
P[no->yes]=18.593%	P[no->no]=81.407%	

Worker: user10
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user11
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user12
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user13
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user14
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user15
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user16
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user17
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user18
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user19
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user2
Error Rate: 57.93%
Quality (Expected): 8%
Quality (Optimized): 16%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=83.571%	P[yes->no]=16.429%	
P[no->yes]=99.427%	P[no->no]=0.573%	

Worker: user20
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user21
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user22
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user23
Error Rate: ---
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=---%	P[no->no]=---%	

Worker: user24
Error Rate: 6.09%
Quality (Expected): 78%
Quality (Optimized): 88%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=87.823%	P[yes->no]=12.177%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user25
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user26
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user27
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user28
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user29
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user3
Error Rate: 28.01%
Quality (Expected): 28%
Quality (Optimized): 44%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.377%	P[yes->no]=0.623%	
P[no->yes]=55.401%	P[no->no]=44.599%	

Worker: user30
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user31
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user4
Error Rate: 27.04%
Quality (Expected): 30%
Quality (Optimized): 46%
Number of Annotations: 6
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.867%	P[yes->no]=0.133%	
P[no->yes]=53.943%	P[no->no]=46.057%	

Worker: user5
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user6
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user7
Error Rate: 99.76%
Quality (Expected): 99%
Quality (Optimized): 100%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.475%	P[yes->no]=99.525%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user8
Error Rate: 1.55%
Quality (Expected): 94%
Quality (Optimized): 97%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=96.907%	P[yes->no]=3.093%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user9
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

