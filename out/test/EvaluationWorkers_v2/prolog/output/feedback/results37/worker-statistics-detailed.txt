Worker: user0
Error Rate: 8.98%
Quality (Expected): 67%
Quality (Optimized): 82%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=93.539%	P[yes->no]=6.461%	
P[no->yes]=11.493%	P[no->no]=88.507%	

Worker: user1
Error Rate: 66.31%
Quality (Expected): 19%
Quality (Optimized): 33%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=66.878%	P[yes->no]=33.122%	
P[no->yes]=99.507%	P[no->no]=0.493%	

Worker: user10
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user11
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user12
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user13
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user14
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user15
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user16
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user17
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user18
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user2
Error Rate: 65.82%
Quality (Expected): 18%
Quality (Optimized): 32%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=67.213%	P[yes->no]=32.787%	
P[no->yes]=98.848%	P[no->no]=1.152%	

Worker: user3
Error Rate: 61.21%
Quality (Expected): 5%
Quality (Optimized): 22%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=50.836%	P[yes->no]=49.164%	
P[no->yes]=73.264%	P[no->no]=26.736%	

Worker: user4
Error Rate: 54.22%
Quality (Expected): 1%
Quality (Optimized): 8%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=66.597%	P[yes->no]=33.403%	
P[no->yes]=75.045%	P[no->no]=24.955%	

Worker: user5
Error Rate: 11.09%
Quality (Expected): 63%
Quality (Optimized): 78%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=78.799%	P[yes->no]=21.201%	
P[no->yes]=0.979%	P[no->no]=99.021%	

Worker: user6
Error Rate: 7.32%
Quality (Expected): 74%
Quality (Optimized): 85%
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.675%	P[yes->no]=0.325%	
P[no->yes]=14.318%	P[no->no]=85.682%	

Worker: user7
Error Rate: 4.65%
Quality (Expected): 83%
Quality (Optimized): 91%
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.849%	P[yes->no]=0.151%	
P[no->yes]=9.142%	P[no->no]=90.858%	

Worker: user8
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user9
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

