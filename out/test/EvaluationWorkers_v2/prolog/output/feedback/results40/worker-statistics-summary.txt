Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	57.09%	8%	14%	10	0
user1	68.94%	20%	38%	10	0
user2	41.16%	4%	18%	10	0
user3	19.79%	37%	60%	10	0
user4	18.66%	41%	63%	10	0
user5	63.34%	14%	27%	10	0
user6	50.0%	---	---	10	0
user7	31.42%	15%	37%	10	0
user8	33.27%	11%	33%	5	0
user9	50.0%	---	---	1	0
