Worker: user0
Error Rate: 44.12%
Quality (Expected): 4%
Quality (Optimized): 12%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=95.66%	P[yes->no]=4.34%	
P[no->yes]=83.903%	P[no->no]=16.097%	

Worker: user1
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user10
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user11
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user12
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user13
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user14
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user15
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user16
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user17
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user18
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user19
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user2
Error Rate: 16.97%
Quality (Expected): 45%
Quality (Optimized): 66%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=92.15%	P[yes->no]=7.85%	
P[no->yes]=26.091%	P[no->no]=73.909%	

Worker: user20
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user21
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user22
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user23
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user24
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user25
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user26
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user27
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user28
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user29
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user3
Error Rate: 83.18%
Quality (Expected): 45%
Quality (Optimized): 66%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=7.897%	P[yes->no]=92.103%	
P[no->yes]=74.255%	P[no->no]=25.745%	

Worker: user30
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user31
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user32
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user33
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user34
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user35
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user36
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user37
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user38
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user39
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user4
Error Rate: 23.1%
Quality (Expected): 35%
Quality (Optimized): 54%
Number of Annotations: 10
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=56.358%	P[yes->no]=43.642%	
P[no->yes]=2.549%	P[no->no]=97.451%	

Worker: user40
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user41
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user42
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user43
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user44
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user45
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user46
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user47
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user48
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user49
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user5
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 3
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user50
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user51
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user52
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user53
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user54
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user55
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user56
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user57
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user58
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user59
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user6
Error Rate: 8.68%
Quality (Expected): 69%
Quality (Optimized): 83%
Number of Annotations: 6
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=95.68%	P[yes->no]=4.32%	
P[no->yes]=13.039%	P[no->no]=86.961%	

Worker: user60
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user61
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user62
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user63
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user64
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user65
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user66
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user67
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user68
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user69
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user7
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user70
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user71
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user72
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user73
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user74
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user75
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user76
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user77
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user78
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user79
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user8
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user80
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user81
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user82
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user83
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user84
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user85
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user86
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user87
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user88
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user89
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user9
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user90
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user91
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user92
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user93
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user94
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user95
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=0.0%	P[yes->no]=100.0%	
P[no->yes]=0.0%	P[no->no]=100.0%	

Worker: user96
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user97
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user98
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

Worker: user99
Error Rate: 50.0%
Quality (Expected): ---
Quality (Optimized): ---
Number of Annotations: 1
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=100.0%	P[yes->no]=0.0%	
P[no->yes]=100.0%	P[no->no]=0.0%	

