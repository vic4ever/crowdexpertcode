Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	50.0%	---	---	10	0
user1	12.73%	59%	75%	10	0
user10	50.0%	---	---	1	0
user11	50.0%	---	---	1	0
user12	50.0%	---	---	1	0
user13	50.0%	---	---	1	0
user14	50.0%	---	---	1	0
user15	50.0%	---	---	1	0
user16	50.0%	---	---	1	0
user17	50.0%	---	---	2	0
user18	50.0%	---	---	1	0
user19	50.0%	---	---	1	0
user2	23.16%	29%	54%	10	0
user20	50.0%	---	---	1	0
user21	50.0%	---	---	1	0
user22	50.0%	---	---	1	0
user23	50.0%	---	---	1	0
user24	50.0%	---	---	1	0
user25	50.0%	---	---	1	0
user26	50.0%	---	---	1	0
user27	50.0%	---	---	1	0
user28	50.0%	---	---	1	0
user29	50.0%	---	---	1	0
user3	62.72%	15%	25%	10	0
user30	50.0%	---	---	1	0
user31	50.0%	---	---	1	0
user32	50.0%	---	---	1	0
user33	50.0%	---	---	1	0
user34	50.0%	---	---	1	0
user35	50.0%	---	---	1	0
user36	50.0%	---	---	1	0
user37	50.0%	---	---	1	0
user38	50.0%	---	---	1	0
user39	50.0%	---	---	2	0
user4	50.0%	---	---	10	0
user40	50.0%	---	---	1	0
user41	50.0%	---	---	1	0
user42	50.0%	---	---	1	0
user43	50.0%	---	---	1	0
user44	50.0%	---	---	1	0
user45	50.0%	---	---	1	0
user46	50.0%	---	---	1	0
user47	50.0%	---	---	1	0
user48	50.0%	---	---	1	0
user49	50.0%	---	---	1	0
user5	24.4%	27%	51%	10	0
user50	50.0%	---	---	1	0
user51	50.0%	---	---	1	0
user52	50.0%	---	---	1	0
user53	50.0%	---	---	1	0
user54	50.0%	---	---	1	0
user55	50.0%	---	---	1	0
user56	50.0%	---	---	1	0
user57	50.0%	---	---	1	0
user58	50.0%	---	---	1	0
user59	50.0%	---	---	1	0
user6	12.97%	56%	74%	6	0
user60	50.0%	---	---	1	0
user61	50.0%	---	---	1	0
user62	50.0%	---	---	1	0
user63	50.0%	---	---	1	0
user64	50.0%	---	---	1	0
user65	50.0%	---	---	1	0
user66	50.0%	---	---	1	0
user67	50.0%	---	---	1	0
user68	50.0%	---	---	1	0
user69	50.0%	---	---	1	0
user7	50.0%	---	---	5	0
user70	50.0%	---	---	1	0
user71	50.0%	---	---	1	0
user72	50.0%	---	---	1	0
user73	50.0%	---	---	1	0
user74	50.0%	---	---	1	0
user75	50.0%	---	---	1	0
user76	50.0%	---	---	1	0
user77	50.0%	---	---	1	0
user78	50.0%	---	---	1	0
user79	50.0%	---	---	1	0
user8	50.0%	---	---	1	0
user80	50.0%	---	---	1	0
user81	50.0%	---	---	1	0
user82	50.0%	---	---	1	0
user83	50.0%	---	---	1	0
user84	50.0%	---	---	1	0
user85	50.0%	---	---	1	0
user86	50.0%	---	---	1	0
user87	50.0%	---	---	1	0
user88	50.0%	---	---	2	0
user89	50.0%	---	---	2	0
user9	50.0%	---	---	1	0
user90	50.0%	---	---	1	0
user91	50.0%	---	---	1	0
user92	50.0%	---	---	1	0
user93	50.0%	---	---	1	0
user94	50.0%	---	---	1	0
user95	50.0%	---	---	1	0
user96	50.0%	---	---	1	0
