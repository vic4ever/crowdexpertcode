Worker	Error Rate	Quality (Expected)	Quality (Optimized)	Number of Annotations	Gold Tests
user0	50.0%	---	---	8	0
user1	44.55%	2%	11%	8	0
user2	63.45%	10%	27%	8	0
user3	56.51%	2%	13%	8	0
user4	77.23%	31%	54%	8	0
user5	38.54%	11%	23%	8	0
user6	63.56%	10%	27%	8	0
user7	50.0%	---	---	4	0
