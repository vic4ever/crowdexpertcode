Worker: user0
Error Rate: 28.42%
Quality (Expected): 27%
Quality (Optimized): 43%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.011%	P[yes->no]=0.989%	
P[no->yes]=55.857%	P[no->no]=44.143%	

Worker: user1
Error Rate: 29.02%
Quality (Expected): 26%
Quality (Optimized): 42%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=98.86%	P[yes->no]=1.14%	
P[no->yes]=56.91%	P[no->no]=43.09%	

Worker: user2
Error Rate: 15.61%
Quality (Expected): 49%
Quality (Optimized): 69%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=92.877%	P[yes->no]=7.123%	
P[no->yes]=24.105%	P[no->no]=75.895%	

Worker: user3
Error Rate: 32.2%
Quality (Expected): 13%
Quality (Optimized): 36%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=68.718%	P[yes->no]=31.282%	
P[no->yes]=33.109%	P[no->no]=66.891%	

Worker: user4
Error Rate: 32.23%
Quality (Expected): 13%
Quality (Optimized): 36%
Number of Annotations: 4
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=60.981%	P[yes->no]=39.019%	
P[no->yes]=25.45%	P[no->no]=74.55%	

