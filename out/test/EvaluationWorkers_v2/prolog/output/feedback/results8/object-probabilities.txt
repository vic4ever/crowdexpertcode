Object	Pr[yes]	Pr[no]	Pre-DS Majority Label	Pre-DS Min Cost Label	Post-DS Majority Label	Post-DS Min Cost Label
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.FloorID-CORR-MDM.Record.Addresses.Floor	0.02259	0.97741	
0-TO-1-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.POBoxPostalCode-CORR-MDM.Record.Addresses.POBoxPostalCode	0.9743	0.0257	
0-TO-2-attrib-CRM.PartnerHeader.ObjectInstance.Partner.PartyID-CORR-SRM.PartnerHeader.ObjectInstance.Partner.PartyID	0.01619	0.98381	
0-TO-2-attrib-CRM.PartnerInformation.AddressInformation.Address.AddressData.CommunicationInformation.SMTPInformation.SMTP.Contact.ContactData.EMailAddress-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.EMailAddress	0.94015	0.05985	
1-TO-0-attrib-MDM.Record.Addresses.Floor-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.FloorID	0.99448	0.00552	
1-TO-0-attrib-MDM.Record.Addresses.POBoxPostalCode-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.POBoxPostalCode	0.9978	0.0022	
1-TO-0-attrib-MDM.Record.PartnerNumber-CORR-CRM.PartnerHeader.ObjectInstance.Partner.PartyID	0.03649	0.96351	
1-TO-2-attrib-MDM.Record.Addresses.POBox-CORR-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.POBoxID	0.9087	0.0913	
2-TO-0-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.EMailAddress-CORR-CRM.PartnerInformation.AddressInformation.Address.AddressData.CommunicationInformation.SMTPInformation.SMTP.Contact.ContactData.EMailAddress	0.97213	0.02787	
2-TO-1-attrib-SRM.PartnerHeader.ObjectInstance.Partner.PartyID-CORR-MDM.Record.PartnerNumber	0.0022	0.9978	
2-TO-1-attrib-SRM.PartnerRecord.PartnerInformation.AddressInformation.Address.AddressData.PostalAddress.PostalAddressData.POBoxID-CORR-MDM.Record.Addresses.POBox	0.01039	0.98961	
