Worker: user0
Error Rate: 54.46%
Quality (Expected): 1%
Quality (Optimized): 9%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=50.384%	P[yes->no]=49.616%	
P[no->yes]=59.313%	P[no->no]=40.687%	

Worker: user1
Error Rate: 63.5%
Quality (Expected): 13%
Quality (Optimized): 27%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=68.993%	P[yes->no]=31.007%	
P[no->yes]=95.991%	P[no->no]=4.009%	

Worker: user10
Error Rate: 6.73%
Quality (Expected): 75%
Quality (Optimized): 87%
Number of Annotations: 2
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=96.672%	P[yes->no]=3.328%	
P[no->yes]=10.124%	P[no->no]=89.876%	

Worker: user2
Error Rate: 31.78%
Quality (Expected): 14%
Quality (Optimized): 36%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=80.632%	P[yes->no]=19.368%	
P[no->yes]=44.19%	P[no->no]=55.81%	

Worker: user3
Error Rate: 47.02%
Quality (Expected): 0%
Quality (Optimized): 6%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=66.398%	P[yes->no]=33.602%	
P[no->yes]=60.44%	P[no->no]=39.56%	

Worker: user4
Error Rate: 39.96%
Quality (Expected): 5%
Quality (Optimized): 20%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=82.054%	P[yes->no]=17.946%	
P[no->yes]=61.983%	P[no->no]=38.017%	

Worker: user5
Error Rate: 20.82%
Quality (Expected): 34%
Quality (Optimized): 58%
Number of Annotations: 11
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=81.425%	P[yes->no]=18.575%	
P[no->yes]=23.061%	P[no->no]=76.939%	

Worker: user6
Error Rate: 47.78%
Quality (Expected): 0%
Quality (Optimized): 4%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=79.794%	P[yes->no]=20.206%	
P[no->yes]=75.351%	P[no->no]=24.649%	

Worker: user7
Error Rate: 69.22%
Quality (Expected): 23%
Quality (Optimized): 38%
Number of Annotations: 9
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=60.366%	P[yes->no]=39.634%	
P[no->yes]=98.813%	P[no->no]=1.187%	

Worker: user8
Error Rate: 7.62%
Quality (Expected): 72%
Quality (Optimized): 85%
Number of Annotations: 8
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=96.528%	P[yes->no]=3.472%	
P[no->yes]=11.76%	P[no->no]=88.24%	

Worker: user9
Error Rate: 7.04%
Quality (Expected): 75%
Quality (Optimized): 86%
Number of Annotations: 5
Number of Gold Tests: 0
Confusion Matrix: 
P[yes->yes]=99.582%	P[yes->no]=0.418%	
P[no->yes]=13.669%	P[no->no]=86.331%	

