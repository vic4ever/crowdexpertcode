{vio1(a1,b1), vio2(a1,b1), vio2(b1,c1)}

% input example:
attr(a1,sa).
attr(b1,sb).
attr(b2,sb).
attr(c1,sc).

cor(a1,b1).
cor(a1,b2).
cor(a1,c1).
cor(b1,c1).
cor(b2,c1).

include(a1,b2).
include(a1,c1).

% end of input example