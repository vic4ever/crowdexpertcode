% input example:
attr(a1,sa).
attr(a2,sa).
attr(b1,sb).
attr(c1,sc).
attr(d1,sd).
attr(e1,se).

cor(a1,b1).
cor(b1,c1).
cor(c1,a2).

cor(a1,d1).
cor(d1,e1).
cor(e1,a2).
% end of input example